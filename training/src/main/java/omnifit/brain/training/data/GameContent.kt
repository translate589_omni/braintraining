package omnifit.brain.training.data

import com.squareup.moshi.Json

data class GameContent(
    // @formatter:off
    @Json(name = "fitBrainSeq") var sequence: Int = -1,
    @Json(name = "appNm")       var title: String = "",
    @Json(name = "smry")        var summary: String = "",
    @Json(name = "ord")         var order: Int = 0,
    @Json(name = "useYn")       var useContents: String = "",
    @Json(name = "regDt")       var registrationDate: String = "",
    @Json(name = "updDt")       var updateDate: String = "",
    @Json(name = "iconFileSeq") var iconFileNo: Int = -1,
    @Json(name = "imgFileSeq")  var imgFileNo: Int = -1,
    @Json(name = "gameFileSeq") var gameFileNo: Int = -1,
    @Json(name = "iconFile")    var iconFile: ContentFileInfo? = null,
    @Json(name = "imgFile")     var imageFile: ContentFileInfo? = null,
    @Json(name = "gameFile")    var gameFile: ContentFileInfo? = null
    // @formatter:on
)