package omnifit.brain.training.data

import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.algorithm.ServiceAlgorithm
import omnifit.brain.training.algorithm.toDoubleArray
import omnifit.brain.training.db.model.ServiceResult
import omnifit.commons.algorithm.Algorithm
import omnifit.commons.common.*

data class ComparisionData (
    val beforeScore:Int,
    val afterScore:Int,
    val beforeAverage:Double,
    val afterAverage:Double,
    val comparisonRate:Double
) {
    var beforeSubList:DoubleArray = doubleArrayOf()
    var afterSubList:DoubleArray = doubleArrayOf()

    companion object {
        private val EFFECTIVE_COMPARISION_TIME = 60L.secondsToMillis()

        private fun calcScore(dataArray:DoubleArray) = Algorithm.computeEffectiveTime(
                dataArray, ServiceAlgorithm.EFFECTIVE_TIME_THRESHOLD
            )
            .let { effectiveTime ->
                ServiceAlgorithm.computeEffectiveTimeRatio(effectiveTime, EFFECTIVE_COMPARISION_TIME)
            }
            .let { effectiveTimeRatio ->
                ServiceAlgorithm.convEffectiveTimeRatioToScore(effectiveTimeRatio)
            }

        fun create(result:ServiceResult, isReverse:Boolean = false) = result.indicatorValues.toDoubleArray().run {
            /*
                Translate589 TODO : 만약 전후비교 1분이 가능한 데이터인 경우 (ex : 총 2분 이상) 비교를 하고
                 그게 불가한 경우 최초 1건과 마지막 1건에 대한 비교를 실시함
             */
            val compareDataCount = (1.minutesToSeconds() / 2)
            var tempBeforeSubList:DoubleArray = doubleArrayOf()
            var tempAfterSubList:DoubleArray = doubleArrayOf()

            takeIf {
                it.size >= (compareDataCount * 2)
            }?.run {
                tempBeforeSubList = copyOfRange(0, compareDataCount)
                    .takeIf {
                        it.isAvailable()
                    } ?: let {
                    ignoreNegative().copyOfRange(0,0)
                }
                tempAfterSubList = copyOfRange(size - compareDataCount, size)
            } ?: ignoreNegative().run {
                tempBeforeSubList = copyOfRange(0,0)
                tempAfterSubList = copyOfRange(lastIndex,lastIndex)
            }

            val beforeAverage = tempBeforeSubList.ignoreNegativeAverage()
                .takeIf {
                    it.isAvailable()
                } ?: 0.0
            val afterAverage = tempAfterSubList.ignoreNegativeAverage()
                .takeIf {
                    it.isAvailable()
                } ?: 0.0

            ComparisionData(
                calcScore(tempBeforeSubList),
                calcScore(tempAfterSubList),
                beforeAverage,
                afterAverage,
                afterAverage - beforeAverage
            ).apply {
                beforeSubList = tempBeforeSubList
                afterSubList = tempAfterSubList
            }
        }
    }
}