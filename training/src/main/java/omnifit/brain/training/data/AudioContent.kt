package omnifit.brain.training.data

import com.squareup.moshi.Json

data class AudioContent(
    // @formatter:off
    @Json(name = "musicContsSeq")   var sequence: Int = -1,
    @Json(name = "musicNm")         var title: String = "",
    @Json(name = "ord")             var order: Int = 0,
    @Json(name = "xpln")            var explanation: String = "",
    @Json(name = "musicGrpCd")      var groupSignature: String = "",
    @Json(name = "externalDndlUrl") var externalDownloadUrl: String = "",
    @Json(name = "introYn")         var introducable: String = "",
    @Json(name = "imgFile")         var imageFile: ContentFileInfo? = null,
    @Json(name = "file")            var audioFile: ContentFileInfo? = null
    // @formatter:on
)