package omnifit.brain.training.data

import com.squareup.moshi.Json

data class ContentFileInfo(
    // @formatter:off
    @Json(name = "fileSeq")       var sequence: Int = -1,
    @Json(name = "fileNm")        var name: String = "",
    @Json(name = "fileSize")      var size: Long = 0L,
    @Json(name = "fileTypeCd")    var filSignature: String = "",
    @Json(name = "filePath")      var downloadUrl: String = "",
    @Json(name = "fileExtension") var extension: String = "",
    @Json(name = "playTime")      var duration: Long = 0L
    // @formatter:on
)