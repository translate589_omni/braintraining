package omnifit.brain.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextSwitcher
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import omnifit.brain.training.*
import omnifit.brain.training.component.BrainAudioTrainingContentItemComponent
import omnifit.brain.training.helper.ContentHelper
import omnifit.brain.training.view.NonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.anko.AnkoBindableComponentAdapter
import omnifit.commons.anko.AnkoBindableComponentHolder
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.common.DTP_MM_SS
import omnifit.commons.common.millisToFormatString
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.asSequence
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import travel.ithaka.android.horizontalpickerlib.PickerLayoutManager
import kotlin.properties.Delegates

class BrainStabilityTrainingContentListScreen : UIScreen() {

    private val contents: MutableList<AudioPlayer.AudioSource> by lazy {
        ContentHelper
            .getNormalSoundContents()
            .asSequence()
            .toMutableList()
    }

    private var selectedPosition: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            title?.setText(contents[n].title)
            subTitle?.setText(contents[n].duration.millisToFormatString(DTP_MM_SS))

            changeArrowVisibility(n)
        }
    }

    private var title: TextSwitcher? = null
    private var subTitle: TextSwitcher? = null
    private var recycerView: RecyclerView? = null
    private var arrowLeft: ImageView? = null
    private var arrowRight: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            backgroundResource = R.drawable.shape_bg_main

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_bk) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            // 로고
            textView {
                id = R.id.screen_widget_id_02
                includeFontPadding = false
                lines = 1
                textColorResource = R.color.x_2e2e2e
                textSize = 35.0f
                typeface = Font.kopubDotumBold
                textResource = R.string.main_screen_060
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
            }

            // 밧데리잔량
            constraintLayout {
                id = R.id.screen_widget_id_03
                backgroundResource = R.drawable.shape_r28_op04_000000

                // 헤드셋 이미지
                imageView(R.drawable.ic_headset_bk) {
                    id = R.id.screen_widget_id_04
                }.lparams(wrapContent, wrapContent) {
                    startToStart = PARENT_ID
                    marginStart = dip(25.3f)
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                }
                // 헤드셋 밧데리 잔량
                textView {
                    id = R.id.screen_widget_id_05
                    typeface = Font.latoRegular
                    textSize = 21.3f
                    textColorResource = R.color.x_303030
                    lines = 1
                    includeFontPadding = false
                    bindBatteryLevel(owner, Bindable.batteryLevel)
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_04
                    endToEnd = PARENT_ID
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    horizontalBias = 0.3f
                    verticalBias = 0.5f
                }
            }.lparams(dip(137.3f), dip(56.0f)) {
                topToTop = R.id.screen_widget_id_06
                endToStart = R.id.screen_widget_id_06
                marginEnd = dip(28.7f)
                bottomToBottom = R.id.screen_widget_id_06
            }

            // 설정
            imageButton(R.drawable.selector_setting_bk) {
                id = R.id.screen_widget_id_06
                scaleType = ImageView.ScaleType.CENTER
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
//                        openHeadsetSetting()
                    screenTo(SettingScreen())
                }
            }.lparams(wrapContent, wrapContent) {
                endToEnd = PARENT_ID
                marginEnd = dip(36.7f)
                topToTop = R.id.screen_widget_id_01
                bottomToBottom = R.id.screen_widget_id_01
            }

            @Suppress("UNCHECKED_CAST")
            recycerView = recyclerView {
                id = R.id.screen_widget_id_07
                clipToPadding = false
                setPadding(dip(384.0f), 0, dip(381.3f), 0)
                layoutManager =
                    PickerLayoutManager(context, PickerLayoutManager.HORIZONTAL, false).apply {
                        isChangeAlpha = true
                        scaleDownBy = 0.1f
                        scaleDownDistance = 0.8f
                        setOnScrollStopListener { v ->
                            (v.tag as? AudioPlayer.AudioSource)?.let { source ->
                                selectedPosition = contents.indexOf(source)
                            }
                        }

                    }
                LinearSnapHelper().attachToRecyclerView(this)
                setHasFixedSize(true)      // 정적 리스트 데이터 성능용
                setItemViewCacheSize(contents.size) // 정적 리스트 캐시 사이즈
                adapter = HealingContentListAdapter(contents, this@BrainStabilityTrainingContentListScreen)
                smoothScrollBy(1, 0)
            }.lparams(matchConstraint, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_01
                topMargin = dip(44.7f)
                endToEnd = PARENT_ID
            }

            title = textSwitcher {
                id = R.id.screen_widget_id_08
                setFactory {
                    NonSpacingTextView(context).apply {
                        typeface = Font.kopubDotumBold
                        textSize = 36.0f
                        textColorResource = R.color.x_2e2e2e
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                        layoutParams = FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT
                        )
                    }
                }
                measureAllChildren = false
                inAnimation = fadeIn
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_07
                topMargin = dip(39.7f)
                endToEnd = PARENT_ID
            }

            subTitle = textSwitcher {
                id = R.id.screen_widget_id_09
                setFactory {
                    NonSpacingTextView(context).apply {
                        typeface = Font.kopubDotumMedium
                        textSize = 24.0f
                        textColorResource = R.color.x_a8a8a8
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                        layoutParams = FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT
                        )
                    }
                }
                measureAllChildren = false
                inAnimation = fadeIn
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_08
                topMargin = dip(17.3f)
                endToEnd = PARENT_ID
            }

            arrowLeft = imageView(R.drawable.selector_btn_list_ar_left) {
                onClick {
                    recycerView?.smoothScrollToPosition(selectedPosition -1)
                }
            }.lparams(wrapContent, wrapContent) {
                bottomMargin = dip(382)
                bottomToBottom = PARENT_ID
                startToStart = PARENT_ID
            }

            arrowRight = imageView(R.drawable.selector_btn_list_ar_right) {
                onClick {
                    recycerView?.smoothScrollToPosition(selectedPosition +1)
                }
            }.lparams(wrapContent, wrapContent) {
                bottomMargin = dip(382)
                bottomToBottom = PARENT_ID
                endToEnd = PARENT_ID
            }
        }.apply {
            selectedPosition = 0
        }
    }.view

    private fun changeArrowVisibility(index:Int) {
        arrowLeft?.isVisible = index != 0
        arrowRight?.isVisible = index != (contents.size -1)
    }
}

class HealingContentListAdapter constructor(
    items: MutableList<AudioPlayer.AudioSource>,
    owner: BrainStabilityTrainingContentListScreen
) :
    AnkoBindableComponentAdapter<MutableList<AudioPlayer.AudioSource>, BrainStabilityTrainingContentListScreen, HealingContentListAdapter.ComponentHolder>(
        items,
        owner
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(
            BrainAudioTrainingContentItemComponent(
                parent.context,
                owner
            )
        ) { _, _, position ->
            owner.checkHeadsetRequirements {
                owner.screenTo(BrainStabilityTrainingScreen(items[position]))
            }
        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<AudioPlayer.AudioSource, BrainStabilityTrainingContentListScreen>,
        action: (AnkoBindableComponent<AudioPlayer.AudioSource, BrainStabilityTrainingContentListScreen>, AudioPlayer.AudioSource, Int) -> Unit
    ) : AnkoBindableComponentHolder<AudioPlayer.AudioSource, BrainStabilityTrainingContentListScreen>(component) {
        init {
            (component as? BrainAudioTrainingContentItemComponent)?.run {
                content?.onDebounceClick {
                    action(
                        component,
                        items[adapterPosition],
                        adapterPosition
                    )
                }
            }
        }
    }
}