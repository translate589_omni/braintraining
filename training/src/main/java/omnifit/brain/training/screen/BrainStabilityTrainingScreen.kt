package omnifit.brain.training.screen

import android.animation.LayoutTransition
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import at.favre.lib.dali.Dali
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.R
import omnifit.brain.training.component.*
import omnifit.brain.training.helper.ServiceResultHelper
import omnifit.brain.training.view.headsetStateVisualizer
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.common.*
import omnifit.commons.omnifitbrain.Error
import omnifit.commons.omnifitbrain.useOmnifitBrain
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import omnifit.commons.peripheral.common.toMultipleElectroencephalogram
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import timber.log.Timber
import java.util.concurrent.TimeUnit

class BrainStabilityTrainingScreen(val content: AudioPlayer.AudioSource) : UIScreen() {
    private var trainingTimeView: TextView? = null
    private var analyzingView: View? = null
    private var resultErrorView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            layoutTransition = LayoutTransition()

            // 배경
            imageView {
                into(content.thumbnail as? Drawable)
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }
            // 불투명 처리
            imageView {
                backgroundColorResource = R.color.x_000000_op40
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_wt_2) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            // 그래프
            headsetStateVisualizer(icon = R.drawable.ic_headset_wave) {
                id = R.id.screen_widget_id_02
                bindVisualized(owner, Bindable.isHeadsetStateVisualized)
            }.lparams(matchParent, wrapContent) {
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(134.7f)
            }

            // 측정시간
            trainingTimeView = textView("00:00") {
                id = R.id.screen_widget_id_03
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textColorResource = R.color.x_ffffff
                textSize = 93.0f
                typeface = Font.latoRegular
            }.lparams(0, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_01
                topMargin = dip(192.7f)
                endToEnd = PARENT_ID
            }

            nonSpacingTextView(content.duration.millisToFormatString(DTP_MM_SS)) {
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textColorResource = R.color.x_ffffff_op50
                textSize = 30.0f
                typeface = Font.latoLight
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_03
                topToBottom = R.id.screen_widget_id_03
                topMargin = dip(28.0f)
                endToEnd = R.id.screen_widget_id_03
            }

            analyzingView = AnalyzingViewComponent<BrainStabilityTrainingScreen>(ServiceCategory.BRAIN_STABILITY_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@BrainStabilityTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_04
                    v.isVisible = false
                    addView(v)
                }

            resultErrorView = ResultErrorViewComponent<BrainStabilityTrainingScreen>(ServiceCategory.BRAIN_STABILITY_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@BrainStabilityTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_05
                    v.isVisible = false
                    addView(v)
                }
        }
    }.view

    @ExperimentalUnsignedTypes
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AudioPlayer.observePlaybackStatus(this) { state ->
            when (state) {
                is AudioPlayer.PlaybackState.Prepare -> Unit
                is AudioPlayer.PlaybackState.Start -> observeBrainFeedback()
                is AudioPlayer.PlaybackState.Playing -> updateTrainingTime(state.currentPosition)
                is AudioPlayer.PlaybackState.Pause -> Unit
                is AudioPlayer.PlaybackState.Resume -> Unit
                is AudioPlayer.PlaybackState.Stop -> useOmnifitBrain {
                    cancelMeasurement()
                }
                is AudioPlayer.PlaybackState.Complete -> useOmnifitBrain {
                    stopMeasurement()
                }
                is AudioPlayer.PlaybackState.Error -> Unit
                else -> Unit
            }
        }

        // 최초 팝업
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                hideSoftKeyboard()
                maxWidth(literal = dip(514.0f))
                customView(
                    view = TrainingStartGuidePopComponent<MaterialDialog>(
                        R.string.healing_music_content_screen_050,
                        R.string.healing_music_content_screen_060
                    )
                        .createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(333.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                    startTraining()
                }
            }
    }

    @ExperimentalUnsignedTypes
    override fun onBackPressed(): Boolean {
        if(isTraining.value) {
            when (requireValidMeasurement()) {
                true  -> stopTraining()
                false -> requireStopWithValidTraining { isStop ->
                    if (isStop) stopTraining()
                }
            }
            return true
        }

        if (analyzingView?.isVisible == true) {
            alert(
                requireContext(),
                R.string.healing_music_content_screen_030,
                R.color.x_000000_op60
            )
            return true
        }

        useOmnifitBrain {
            cancelMeasurement()
        }
        stopTraining()
        //return screenTo(NeuroFeedbackContentListScreen())
        return false
    }

    private val isTraining: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    @ExperimentalUnsignedTypes
    private fun startTraining() {
        isTraining.value = true
        playMusic()
    }

    private fun stopTraining() {
        isTraining.value = false
        stopMusic()
    }

    @ExperimentalUnsignedTypes
    private fun playMusic() {
        AudioPlayer.play(requireContext(), this, listOf(content))
    }

    private fun stopMusic() {
        AudioPlayer.stop()
    }

    private fun updateTrainingTime(millis: Int = 0) {
        trainingTimeView?.text = millis.millisToFormatString(DTP_MM_SS)
    }

    @ExperimentalUnsignedTypes
    private fun observeBrainFeedback() {
        useOmnifitBrain {
            isNotMeasuring {
                startMeasurement {
                    onStart {
                        Timber.v("[두뇌안정훈련] --> 측정시작")
                    }
                    onMeasure { m ->
                        Timber.v("[두뇌안정훈련] --> 측정진행[${m.elapsedTime.secondsToFormatString(DTP_MM_SS)}]")
                        // TODO(부착상태확인)
//                        alert(requireContext(), R.string.notifications_020)
                    }
                    onFinish {
                        Timber.v("[두뇌안정훈련] --> 측정종료")
                        runOnUiThread {
                            processingToResult(
                                content.sequence,
                                content.duration.millisToSeconds(),
                                it.result.toMultipleElectroencephalogram()
                            )
                        }
                    }
                    onCancel { _, error ->
                        Timber.v("[두뇌안정훈련] --> 측정취소")
                        when (error) {
                            Error.OMNIFIT_BRAIN_HEADSET_UNLINKED -> {
                                showBrainDisconnectErrorPopup()
                            }
                            else                                 -> Unit
                        }
                    }
                }
            }
        }
    }

    private var neuroFeedbackElapsedTime: Int = 0
    private fun requireValidMeasurement(): Boolean {
        return neuroFeedbackElapsedTime.toLong() >= 3.minutesToMillis()
    }

    @ExperimentalUnsignedTypes
    private fun requireStopWithValidTraining(block: (Boolean) -> Unit): Boolean {
        return requireValidMeasurement()
            .also { valid ->
                MaterialDialog(requireContext())
                    .show {
                        hideNavigation()
                        customView(
                            view = BasicPopComponent<MaterialDialog>(
//                                    context.getString(R.string.main_screen_060),
//                                    context.getString(R.string.healing_music_content_screen_040),
                                context.getString(R.string.common_050),
                                context.getString(R.string.common_060),
                                Gravity.CENTER,
                                buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                                    override fun onPositive() {
                                        block(true)
                                        if(!valid) performBackPressed()
                                        dismiss()
                                    }

                                    override fun onNegative() {
                                        block(false)
                                        dismiss()
                                    }
                                }
                            )
                                .createView(AnkoContext.create(context, this))
                                .also { v ->
                                    (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                                },
                            noVerticalPadding = true
                        )
                        cornerRadius(15.0f)
                        cancelable(false)
                        cancelOnTouchOutside(false)
                        setOnDismissListener {
                        }
                    }
            }
    }

    private fun checkValidResult(
        result: MultipleElectroencephalogram,
        block: () -> Unit
    ): Boolean {
        return result
            .meanRelaxationIndicatorValues
            .any {
                it > 0.0
            }
            .also { isValid ->
                when {
                    isValid -> block()
                    else    -> showResultErrorView()
                }
            }
    }

    private fun processingToResult(
        sequence: Int,
        elapsedTime: Int,
        result: MultipleElectroencephalogram
    ) {
        checkValidResult(result) {
            showAnalyzingView()
            ServiceResultHelper.createResult(
                context = requireContext(),
                user = currentUser,
                category = ServiceCategory.BRAIN_STABILITY_TRAINING,
                contentSequence = sequence,
                elapsedTime = elapsedTime,
                result = result
            ) { timeTaken, _result ->
                Timber.d("[두뇌안정훈련] --> 서비스 결과 생성 ID : ${_result.id}, 소요시간 : ${timeTaken.millisToFormatString(DTP_SS_SSS)}")
                Single.timer(1L, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .bindUntilEvent(this, Lifecycle.Event.ON_DESTROY)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            runOnUiThread {
                                screenTo(ServiceResultScreen(_result), R.anim.slide_hold, R.anim.slide_exit_right)
                            }
                        },
                        { e -> Timber.d(e) }
                    )
            }
        }
    }

    private val blurBackgroundDrawable: BitmapDrawable?
        get() = this.view
            ?.run {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    .also { bmp ->
                        draw(Canvas(bmp))
                    }
            }
            ?.let { bmp ->
                Dali.create(context)
                    .load(bmp)
                    .blurRadius(25)
                    .concurrent()
                    .skipCache()
                    .get()
            }

    private fun showAnalyzingView() {
        analyzingView?.backgroundDrawable = blurBackgroundDrawable
        analyzingView?.isVisible = true
        analyzingView?.bringToFront()
    }

    private fun showResultErrorView() {
        resultErrorView?.backgroundDrawable = blurBackgroundDrawable
        resultErrorView?.isVisible = true
    }

    private fun showBrainDisconnectErrorPopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BrainDisconnectPopComponent<MaterialDialog>(
                        onConfirm = { screenTo(MainScreen()) }
                    ).createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(633.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }
}