package omnifit.brain.training.screen

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import at.favre.lib.dali.Dali
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.component.AnalyzingViewComponent
import omnifit.brain.training.component.BasicPopComponent
import omnifit.brain.training.component.BrainDisconnectPopComponent
import omnifit.brain.training.component.ResultErrorViewComponent
import omnifit.brain.training.db.model.GameContent
import omnifit.brain.training.helper.ServiceResultHelper
import omnifit.brain.training.helper.getSourcePath
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.common.DTP_SS_SSS
import omnifit.commons.common.json.toJson
import omnifit.commons.common.millisToFormatString
import omnifit.commons.common.whenNotEmpty
import omnifit.commons.omnifitbrain.Error
import omnifit.commons.omnifitbrain.useOmnifitBrain
import omnifit.commons.peripheral.common.Eyes
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import omnifit.commons.peripheral.common.toMultipleElectroencephalogram
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.runOnUiThread
import timber.log.Timber
import java.util.concurrent.TimeUnit


class ConcentrationTrainingScreen(private val content: GameContent) : UIScreen() {

    private var gameView: WebView? = null
    private var gameScore: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            backgroundResource = R.drawable.shape_bg_game
            layoutTransition = LayoutTransition()

            gameView = webView {
                backgroundColor = Color.TRANSPARENT
            }.lparams(matchParent, matchParent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_bk) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            AnalyzingViewComponent<ConcentrationTrainingScreen>(ServiceCategory.CONCENTRATION_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@ConcentrationTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_02
                    v.isVisible = false
                    addView(v)
                }

            ResultErrorViewComponent<ConcentrationTrainingScreen>(ServiceCategory.CONCENTRATION_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@ConcentrationTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_03
                    v.isVisible = false
                    addView(v)
                }
        }

    }.view

    @SuppressLint("CheckResult")
    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        if (isEnter) {
            Handler(Looper.getMainLooper()).post { launchGame(this) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        release()
    }

    override fun onBackPressed(): Boolean {
        if (find<View>(R.id.screen_widget_id_02).visibility == View.VISIBLE) {
            alert(requireContext(), R.string.game_content_screen_030, R.color.x_000000_op60)
            return true
        }
        return !requireNotMeasuring()
    }

    private var measurementElapsedTime: Int = 0

    private fun launchGame(owner: ConcentrationTrainingScreen) {
        // TODO : 크롬 업데이트로 인해 software 가속을 사용하면 검은 화면이 나옴
        //gameView?.setLayerType(View.LAYER_TYPE_SOFTWARE, null) // 애니메이션 없는 화면을 더 선명하게 보이기하기위함.
        gameView!!.settings.run {
            WebView.setWebContentsDebuggingEnabled(true)
            javaScriptEnabled = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = false
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            allowFileAccessFromFileURLs = true
        }
        gameView?.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                view.isInvisible = true
                view.loadUrl("javascript:onCreate()")
            }

            override fun onPageFinished(view: WebView, url: String?) {
                super.onPageFinished(view, url)
                createGameConfigToJson()
                    .let { option ->
                        Timber.d("--> option : $option")
                        view.loadUrl("javascript:onStart('$option')")
                    }
                Handler(Looper.getMainLooper()).postDelayed({ view.isInvisible = false }, 100L)
            }
        }

        // 익명으로 브릿지 인터페이스 생성
        gameView?.addJavascriptInterface(object {
            /**
             * 웹게임 종료시 Web->App 호출.
             */
            @JavascriptInterface
            fun onDestroy(data: String) {
                onBackPressed()
            }

            /**
             * 그래픽 가속 사용여부 결정.
             */
            @JavascriptInterface
            fun gameStart(data: String) {
                runOnUiThread { gameView?.setLayerType(View.LAYER_TYPE_HARDWARE, null) }
            }

            /***
             * Javascript 에서 게임Play 시작시 호출
             * data : 스테이지 단계 Data --> Easy : 1 / Normal : 2 / Hard : 3
             ***/
            @JavascriptInterface
            fun onGameStart(stageFromWeb: String) {

                Timber.i("스테이지 : $stageFromWeb")

                measurementElapsedTime = durationTime

                useOmnifitBrain {
                    startMeasurement(eyesState = Eyes.State.OPENED) {
                        onStart {
                        }
                        onMeasure { m ->
                            m.result
                                .whenNotEmpty {
                                    first()
                                        .takeIf {
                                            it.concentrationIndicatorValue >= 0.0
                                        }
                                        ?.concentrationIndicatorValue
                                        ?.toInt()
                                        ?.toString()
                                        ?.let { v ->
                                            postValueToWeb(v)
                                        }
                                        ?: checkHeadsetWeared()
                                }
                        }
                        onFinish {
                            processingToResult(content.sequence, it.elapsedTime, it.result.toMultipleElectroencephalogram())
                        }
                        onCancel { data, error ->
                            Timber.e("$error : ${error?.cause}")
                            when (error) {
                                Error.OMNIFIT_BRAIN_HEADSET_UNLINKED -> {
                                    release()
                                    showBrainDisconnectErrorPopup()
                                }
                                else                                 -> Unit
                            }
                        }
                    }
                }
            }

            /***
             * Web에서 게임종료시 app 함수 호출. ( 실 게임 time 종료 )
             * data : 게임총 점수 . ex: 590
             ***/
            @JavascriptInterface
            fun onGameEnd(score: String) {
                gameScore += score.toInt()

                Timber.i("--> onGameEnd 게임 점수 : $score")
            }

            /***
             * Web에서 다음게임 호출
             * app에선 종료하면 됨
             ***/
            @JavascriptInterface
            fun onNextGame(gameResult: String) {

                Timber.i("--> onNextGame 게임 결과 : $gameResult")

                release()
                useOmnifitBrain {
                    stopMeasurement()
                }
            }
        }, "omnigame")

        gameView?.loadUrl(content.getSourcePath())
        gameView?.invalidate()
    }

    private fun release() {
        Handler(Looper.getMainLooper()).post {
            gameView?.removeAllViews()
            gameView?.destroy()
        }
    }

    private fun postValueToWeb(v: String) {
        gameView?.loadUrl("javascript:callJS('$v')")
    }

    private fun checkValidResult(
        result: MultipleElectroencephalogram,
        block: () -> Unit
    ): Boolean {
        return result
            .concentrationIndicatorValues
            .any {
                it > 0.0
            }
            .also { isValid ->
                when {
                    isValid -> block()
                    else    -> showResultErrorView()
                }
            }
    }

    @SuppressLint("CheckResult")
    private fun processingToResult(
        sequence: Int,
        elapsedTime: Int,
        result: MultipleElectroencephalogram
    ) {
        checkValidResult(result) {
            showAnalyzingView()
            ServiceResultHelper.createResult(
                context = requireContext(),
                user = currentUser,
                category = ServiceCategory.CONCENTRATION_TRAINING,
                contentSequence = sequence,
                elapsedTime = elapsedTime,
                result = result
            ) { timeTaken, _result ->
                Timber.d("--> 게임 결과 ID : ${_result.id}, 소요시간 : ${timeTaken.millisToFormatString(DTP_SS_SSS)}")

                Single.timer(1000L, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .bindUntilEvent(this@ConcentrationTrainingScreen, Lifecycle.Event.ON_DESTROY)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { screenTo(ServiceResultScreen(_result), R.anim.slide_hold, R.anim.slide_exit_right) },
                        { e -> Timber.d(e) }
                    )
            }
        }
    }

    private fun showAnalyzingView() {
        // Blur 백그라운드 생성
        this@ConcentrationTrainingScreen.view
            ?.run {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    .also { bmp ->
                        draw(Canvas(bmp))
                    }
            }
            ?.let { bmp ->
                Dali.create(context)
                    .load(bmp)
                    .blurRadius(25)
                    .concurrent()
                    .skipCache()
                    .get()
            }
            .let { screenshot ->
                find<View>(R.id.screen_widget_id_02).backgroundDrawable = screenshot
                find<View>(R.id.screen_widget_id_02).isVisible = true
            }
    }

    private fun showResultErrorView() {
        this@ConcentrationTrainingScreen.view
            ?.run {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    .also { bmp ->
                        draw(Canvas(bmp))
                    }
            }
            ?.let { bmp ->
                Dali.create(context)
                    .load(bmp)
                    .blurRadius(25)
                    .concurrent()
                    .skipCache()
                    .get()
            }
            .let { screenshot ->
                find<View>(R.id.screen_widget_id_03).backgroundDrawable = screenshot
                find<View>(R.id.screen_widget_id_03).isVisible = true
            }
    }

    private fun requireNotMeasuring(): Boolean {
        return useOmnifitBrain {
            isMeasuring {
                MaterialDialog(requireContext())
                    .show {
                        hideNavigation()
                        customView(
                            view = BasicPopComponent<MaterialDialog>(
//                                title = context.getString(R.string.main_screen_040),
//                                message = context.getString(R.string.game_content_screen_040),
                                title = context.getString(R.string.common_050),
                                message = context.getString(R.string.common_060),
                                buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                                    override fun onPositive() {
                                        cancelMeasurement()
                                        screenTo(ConcentrationTrainingContentListScreen())
                                        dismiss()
                                    }

                                    override fun onNegative() {
                                        dismiss()
                                    }
                                }
                            )
                                .createView(AnkoContext.create(context, this))
                                .also { v ->
                                    (v as? ConstraintLayout)?.layoutParams =
                                        FrameLayout.LayoutParams(
                                            dip(295.5f),
                                            FrameLayout.LayoutParams.WRAP_CONTENT
                                        )
                                },
                            noVerticalPadding = true
                        )
                        cornerRadius(15.0f)
                        cancelable(false)
                        cancelOnTouchOutside(false)
                        setOnDismissListener {
                        }
                    }
            }
        }.not()
    }

    private fun showBrainDisconnectErrorPopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BrainDisconnectPopComponent<MaterialDialog>(
                        onConfirm = { screenTo(MainScreen()) }
                    ).createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)
                                ?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(633.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }

    private val durationTime: Int = 180

    private fun createGameConfigToJson(): String {
        return GameConfig(
            logo_skip_yn = "Y",
            intro_skip_yn = "Y",
            result_skip_yn = "Y",
            last_yn = "Y",
            play_time = durationTime.toString(),
            result_auto_skip_yn = "Y"
        ).toJson()
    }

    data class GameConfig(
        val logo_skip_yn: String = "N",
        val intro_skip_yn: String = "N",
        val result_skip_yn: String = "N",
        val last_yn: String = "N",
        val sham_mode_yn: String = "N",
        val define_game_level: String = "2",
        val play_time: String = "60",
        val logo_auto_skip_yn: String = "N",
        val logo_skip_wait_time: String = "5",
        val result_auto_skip_yn: String = "N",
        val result_skip_wait_time: String = "5"
    )
}