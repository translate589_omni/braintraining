package omnifit.brain.training.screen

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.lifecycle.Lifecycle
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.liulishuo.okdownload.OkDownload
import com.liulishuo.okdownload.core.Util
import com.tedpark.tedpermission.rx2.TedRx2Permission
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.component.LoginPopComponent
import omnifit.brain.training.http.WebApi
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import timber.log.Timber

class LoginMenuScreen : UIScreen() {

    private lateinit var loginPopupComponent:LoginPopComponent<MaterialDialog>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.shape_bg_main

                // 타이틀
                imageView(R.drawable.top_logo) {
                    id = R.id.screen_widget_id_01
                }.lparams(wrapContent, wrapContent) {
                    marginStart = dip(45.3f)
                    topMargin = dip(50f)
                    startToStart = PARENT_ID
                    topToTop = PARENT_ID
                }
                // 로그인 버튼

                // 밧데리잔량
                constraintLayout {
                    id = R.id.screen_widget_id_02
                    backgroundResource = R.drawable.shape_r28_op15_000000

                    // 헤드셋 이미지
                    imageView(R.drawable.ic_headset_wt) {
                        id = R.id.screen_widget_id_03
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        marginStart = dip(25.3f)
                        topToTop = PARENT_ID
                        bottomToBottom = PARENT_ID
                    }
                    // 헤드셋 밧데리 잔량
                    textView {
                        id = R.id.screen_widget_id_04
                        lines = 1
                        includeFontPadding = false
                        textColorResource = R.color.x_ffffff
                        textSize = 21.3f
                        typeface = Font.latoRegular
                        bindBatteryLevel(owner, Bindable.batteryLevel)
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_widget_id_03
                        endToEnd = PARENT_ID
                        topToTop = PARENT_ID
                        bottomToBottom = PARENT_ID
                        horizontalBias = 0.3f
                        verticalBias = 0.5f
                    }
                }.lparams(dip(137.3f), dip(56.0f)) {
                    topToTop = R.id.screen_widget_id_05
                    endToStart = R.id.screen_widget_id_05
                    marginEnd = dip(28.7f)
                    bottomToBottom = R.id.screen_widget_id_05
                }

                // 설정
                imageButton(R.drawable.selector_setting_bk) {
                    id = R.id.screen_widget_id_05
                    scaleType = ImageView.ScaleType.CENTER
                    backgroundColor = Color.TRANSPARENT
                    onDebounceClick {
                        screenTo(SettingScreen())
                    }
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = PARENT_ID
                    marginEnd = dip(36.7f)
                    topToTop = R.id.screen_widget_id_01
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 왼쪽 버튼
                cardView {
                    id = R.id.screen_widget_id_06
                    backgroundColorResource = R.color.x_ffffff
                    cardElevation = dip(5.0f).toFloat()
                    radius = dip(20).toFloat()

                    constraintLayout {
                        imageView(R.drawable.img_main_join) {
                        }.lparams(dip(355f),dip(295f)) {
                            topToTop = PARENT_ID
                            startToStart = PARENT_ID
                            endToEnd = PARENT_ID
                        }

                        button(R.string.login_menu_010) {
                            backgroundResource = R.drawable.selector_bg_r40_btn_01
                            textSize = 34.7f
                            textColorResource = R.color.x_ffffff
                            letterSpacing = -0.03f
                            onDebounceClick {
                                when {
                                    isAdminLogin -> screenTo(SignUpScreen())
                                    else -> loginPopupDialog(true)
                                }
                            }
                        }.lparams(dip(420),dip(80)) {
                            bottomToBottom = PARENT_ID
                            startToStart = PARENT_ID
                            endToEnd = PARENT_ID
                        }
                    }.lparams(matchParent, matchParent) {
                        topMargin = dip(82)
                        bottomMargin = dip(62.7f)
                    }
                }.lparams(dip(573.3f),0) {
                    setMargins(dip(46),dip(68.7f),0,dip(62))
                    startToStart = PARENT_ID
                    topToBottom = R.id.screen_widget_id_01
                    bottomToBottom = PARENT_ID
                }


                // 오른쪽 버튼
                cardView {
                    id = R.id.screen_widget_id_07
                    backgroundColorResource = R.color.x_ffffff
                    cardElevation = dip(5.0f).toFloat()
                    radius = dip(20).toFloat()

                    constraintLayout {
                        imageView(R.drawable.img_main_choose) {
                        }.lparams(dip(307f),dip(298f)) {
                            topToTop = PARENT_ID
                            startToStart = PARENT_ID
                            endToEnd = PARENT_ID
                        }

                        button(R.string.login_menu_020) {
                            backgroundResource = R.drawable.selector_bg_r40_btn_01
                            textSize = 34.7f
                            textColorResource = R.color.x_ffffff
                            letterSpacing = -0.03f
                            onDebounceClick {
                                when {
                                    !checkNetworkAvailable() -> alert(requireContext(), R.string.app_update_screen_060, gravity = Gravity.CENTER)
                                    isAdminLogin -> screenTo(UserListScreen())
                                    else -> loginPopupDialog(false)
                                }
                            }
                        }.lparams(dip(420),dip(80)) {
                            bottomToBottom = PARENT_ID
                            startToStart = PARENT_ID
                            endToEnd = PARENT_ID
                        }
                    }.lparams(matchParent, matchParent) {
                        topMargin = dip(82)
                        bottomMargin = dip(62.7f)
                    }
                }.lparams(dip(573.3f),0) {
                    marginStart = dip(38)
                    startToEnd = R.id.screen_widget_id_06
                    topToTop = R.id.screen_widget_id_06
                    bottomToBottom = R.id.screen_widget_id_06
                }
            }
        }.view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        requirePermission()
    }

    override fun onBackPressed(): Boolean {
        hideSoftKeyboard()
        return appFinish()
    }

    @SuppressLint("CheckResult")
    private fun requirePermission() {
        if (TedRx2Permission.isGranted(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            onPermissionGranted()
        } else {
            TedRx2Permission.with(context)
                .setPermissions(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .request()
                .subscribeOn(Schedulers.io())
                .bindUntilEvent(this@LoginMenuScreen, Lifecycle.Event.ON_DESTROY)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { r ->
                        if (r.deniedPermissions != null) onPermissionDenied(r.deniedPermissions)
                        else onPermissionGranted()
                    },
                    { e -> Timber.e(e) }
                )
        }
    }

    private fun onPermissionGranted() {
        if(!isAdminLogin) {
            doAsync {
                WebApi.init(requireContext())
            }
        }
    }

    private fun onPermissionDenied(permissions: List<String>) {
        appFinish()
    }

    private fun checkNetworkAvailable() : Boolean {
        return OkDownload.with().context().getSystemService(Context.CONNECTIVITY_SERVICE)?.run {
            Util.isNetworkAvailable(this as ConnectivityManager)
        } ?: false
    }

    private fun loginPopupDialog(isSignUp: Boolean) {
        fun moveScreen() {
            runOnUiThread {
                when (isSignUp) {
                    true  -> screenTo(SignUpScreen())
                    false -> screenTo(UserListScreen())
                }
            }
        }

        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                maxWidth(literal = dip(672))
                customView(
                    view = LoginPopComponent<MaterialDialog>(
                        onLoginSuccess = {
                            isAdminLogin = true
                            moveScreen()
                            dismiss()
                        },
                        onLoginFailed = {
                            isAdminLogin = false
                            moveScreen()
                            dismiss()
                        }
                    )
                        .apply {
                            loginPopupComponent = this
                        }
                        .createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams =
                                FrameLayout.LayoutParams(dip(672.0f), wrapContent)
                        },
                    noVerticalPadding = true
                )
                cornerRadius(15.0f)
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                }
                setOnShowListener {
                    currentFocus?.let { view ->
                        context.inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                    }
                }
            }
    }
}