package omnifit.brain.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import omnifit.brain.training.*
import omnifit.brain.training.component.ResultComparisonComponent
import omnifit.brain.training.component.ResultSummaryComponent
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.onPageChangeListener
import org.jetbrains.anko.support.v4.viewPager

class ServiceResultScreen(val result: ServiceResult) : UIScreen() {

    private lateinit var resultSummaryComponent:ResultSummaryComponent<ServiceResultScreen>
    private lateinit var indicator: RadioGroup
    private lateinit var viewPager: ViewPager
    private lateinit var leftArrow: ImageView
    private lateinit var rightArrow: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {

        constraintLayout {
            backgroundResource = R.drawable.shape_bg_result

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_home) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    screenTo(MainScreen())
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            // 로고
            textView(
                when (ServiceCategory.of(result.category)) {
                    ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> R.string.content_result_screen_011
                    else                                                -> R.string.content_result_screen_010
//                    ServiceCategory.CONCENTRATION_TRAINING              -> R.string.content_result_screen_010
//                    ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.string.content_result_screen_020
//                    ServiceCategory.NEURO_FEEDBACK                      -> R.string.content_result_screen_008
                }.let { resId ->
                    getString(resId)
                }
            ) {
                id = R.id.screen_widget_id_02
                includeFontPadding = false
                lines = 1
                textColorResource = R.color.x_2e2e2e
                textSize = 35.0f
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
            }

            viewPager = viewPager {
                id = R.id.screen_widget_id_03
                setPadding(0,dip(46.0f),0,0)

                adapter = object : PagerAdapter() {
                    override fun instantiateItem(container: ViewGroup, position: Int): Any {
                        return when(position) {
                            0 -> ResultSummaryComponent<ServiceResultScreen>(result)
                                .also { component ->
                                    resultSummaryComponent = component
                                }
                                .createView(AnkoContext.create(requireContext(), this@ServiceResultScreen))
                                .also { v ->
                                    container.addView(v)
                                }
                            else -> ResultComparisonComponent<ServiceResultScreen>(result)
                                .createView(AnkoContext.create(requireContext(), this@ServiceResultScreen))
                                .also { v ->
                                    container.addView(v)
                                }
                        }
                    }
                    override fun isViewFromObject(view: View, obj: Any): Boolean = view == (obj as? View)
                    override fun getCount(): Int = when(ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> 1
                        else -> 2
                    }
                    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) = container.removeView(obj as? View)
                }

                onPageChangeListener {
                    onPageSelected { position ->
                        when(position) {
                            0    -> {
                                indicator.check(R.id.screen_widget_id_04)
                                leftArrow.isVisible = false
                                rightArrow.isVisible = ServiceCategory.of(result.category) != ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING
                            }
                            else -> {
                                indicator.check(R.id.screen_widget_id_05)
                                leftArrow.isVisible = true
                                rightArrow.isVisible = false
                            }
                        }
                    }
                }
            }.lparams(0, wrapContent) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                topToBottom = R.id.screen_widget_id_02
            }

            leftArrow = imageView(R.drawable.selector_arrow_left) {
                isVisible = false
                onDebounceClick {
                    viewPager.setCurrentItem(0,true)
                }
            }.lparams(dip(26), dip(50)) {
                setMargins(dip(23.3f),dip(406),0,0)
                startToStart = PARENT_ID
                topToTop = PARENT_ID
            }

            rightArrow = imageView(R.drawable.selector_arrow_right) {
                isVisible = ServiceCategory.of(result.category) != ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING
                onDebounceClick {
                    viewPager.setCurrentItem(1,true)
                }
            }.lparams(dip(26), dip(50)) {
                setMargins(0,dip(406),dip(23.3f),0)
                endToEnd = PARENT_ID
                topToTop = PARENT_ID
            }

            indicator = radioGroup {
                orientation = LinearLayout.HORIZONTAL

                radioButton {
                    id = R.id.screen_widget_id_04
                }
                radioButton {
                    id = R.id.screen_widget_id_05
                }

                check(R.id.screen_widget_id_04)
                isVisible = ServiceCategory.of(result.category) != ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(35.3f)
            }.applyRecursively { v ->
                when (v) {
                    is RadioButton -> {
                        v.buttonDrawable = null
                        v.backgroundResource = R.drawable.selector_indicator
                        v.layoutParams = RadioGroup.LayoutParams(dip(9.3f), dip(9.3f)).apply {
                            if (v.id > 1) marginStart = dip(12.7f)
                        }
                    }
                }
            }
        }
    }.view

    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        super.onScreenTransitionEnd(screen, isEnter)

        if(isEnter) {
            resultSummaryComponent.startScrollAnimation()
        }
    }

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}