package omnifit.brain.training.screen

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_SPREAD
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIScreen
import omnifit.brain.training.component.PolicyPopComponent
import omnifit.brain.training.component.SignUpDataPopComponent
import omnifit.brain.training.helper.SignUpData
import omnifit.brain.training.hideNavigation
import omnifit.brain.training.http.WebApi
import omnifit.brain.training.http.XomnifitHeader
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.common.DTP_YYYYMMDD
import omnifit.commons.http.retrofit.error.HttpExceptionWrapper
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import java.net.SocketTimeoutException
import java.util.regex.Pattern


class SignUpScreen : UIScreen() {

    private var curringCreateUser: ((String, String, String, String?) -> SignUpData)? = null

    private fun generateCurringCreateUser(isMale:Boolean)
            = { name:String, birth:String, phoneNumber:String, email:String? ->
        SignUpData.create(
            name,
            when(isMale) {
                true -> SignUpData.Companion.Gender.MALE
                false -> SignUpData.Companion.Gender.FEMALE
            },
            birth,
            phoneNumber,
            email
        )
    }

    private lateinit var nameInputView: EditText
    private lateinit var birthInputView: EditText
    private lateinit var phoneInputView: EditText
    private lateinit var emailInputView: EditText
    private lateinit var domainInputView: EditText
    private lateinit var domainSpinnerView: Spinner
    private lateinit var domainSpinnerArrowView: ImageView
    private lateinit var agreeCheckBox: CheckBox

    private var emailString:String? = ""
    private var domainString = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            backgroundResource = R.drawable.shape_bg_main

            view {
                id = R.id.screen_widget_id_01
                backgroundResource = R.drawable.layer_bg_common_header
            }.lparams(dip(0.0f), dip(81.3f)) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            textView(R.string.sign_up_010) {
                id = R.id.screen_widget_id_02
                textSize = 33f
                textColorResource = R.color.x_000000
                typeface = Font.kopubDotumBold
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(0, 0) {
                topToTop = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
            }

            imageButton(R.drawable.selector_btn_popup_close) {
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(19.3f),dip(25.3f),dip(18.7f),dip(27.3f))
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, 0) {
                marginEnd = dip(20)
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
            }

            // TODO : spannable 처리 할 것
            textView (
                Spanner().apply {
                    context.getString(R.string.sign_up_020).split('|').let { strings ->
                        val color_1 = ResourcesCompat.getColor(resources, R.color.x_312929, null)
                        val color_2 = ResourcesCompat.getColor(resources, R.color.x_ff2687, null)
                        append(strings[0], Spans.foreground(color_1))
                        append(strings[1], Spans.foreground(color_2))
                        append(strings[2], Spans.foreground(color_1))
                    }
                }.trim()
            ) {
                id = R.id.screen_widget_id_03
                textSize = 20f
                textColorResource = R.color.x_312929
                letterSpacing = -0.05f
                gravity = Gravity.CENTER
                backgroundColorResource = R.color.x_d9d9d9
                typeface = Font.kopubDotumBold
            }.lparams(0, dip(52.0f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                topToBottom = R.id.screen_widget_id_01
            }

            constraintLayout {
                guideline {
                    id = R.id.screen_guide_line_01
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideBegin = dip(211.3f)
                }

                guideline {
                    id = R.id.screen_guide_line_02
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideEnd = dip(63.3f)
                }

                // 화면의 50%
                guideline {
                    id = R.id.screen_guide_line_03
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideBegin = dip(640.0f)
                }

                guideline {
                    id = R.id.screen_guide_line_04
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideBegin = dip(640f + 135.3f)
                }

                // 관리자 정보
                nonSpacingTextView(R.string.sign_up_030) {
                    id = R.id.screen_widget_id_04
                    textSize = 22.0f
                    textColorResource = R.color.x_353739
                    letterSpacing = -0.08f
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    startToStart = PARENT_ID
                    topToTop = PARENT_ID
                    topMargin = dip(22)
                    marginStart = dip(38.7f)
                }

                nonSpacingTextView(R.string.sign_up_040) {
                    textSize = 20f
                    textColorResource = R.color.x_929597
                    letterSpacing = -0.05f
                }.lparams(wrapContent, wrapContent) {
                    marginStart = dip(50)
                    startToEnd = R.id.screen_widget_id_04
                    topToTop = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_04
                }

                view {
                    id = R.id.screen_widget_id_05
                    //backgroundResource = R.drawable.shape_r10_b1_d8e1e6_ffffff
                    backgroundColorResource = R.color.x_ffffff
                }.lparams(dip(0.0f), dip(184.4f)) {
                    startToStart = PARENT_ID
                    topToBottom = R.id.screen_widget_id_04
                    topMargin = dip(13.3f)
                    endToEnd = PARENT_ID
                }

                nonSpacingTextView(R.string.sign_up_050) {
                    id = R.id.screen_widget_id_06
                    includeFontPadding = false
                    textSize = 26.0f
                    letterSpacing = -0.08f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(130.0f), wrapContent) {
                    verticalChainStyle = CHAIN_SPREAD
                    topToTop = R.id.screen_widget_id_05
                    bottomToTop = R.id.screen_widget_id_12
                    startToStart = PARENT_ID
                    marginStart = dip(60f)
                }

                // TODO : 이름 입력 칸
                nameInputView = editText {
                    id = R.id.screen_widget_id_07
                    textSize = 26.7f
                    textColorResource = R.color.x_353739
                    hint = getString(R.string.sign_up_140)
                    hintTextColor = ContextCompat.getColor(context,R.color.x_dbdddf)
                    typeface = Font.kopubDotumBold
                    backgroundResource = R.drawable.selector_bg_r11_3_view_01
                    gravity = Gravity.CENTER_VERTICAL
                    inputType = InputType.TYPE_CLASS_TEXT
                    maxLines = 1
                    setPadding(dip(28),0,0,0)
                }.lparams(0, dip(70)) {
                    marginEnd = dip(43.3f)
                    startToEnd = R.id.screen_guide_line_01
                    topToTop = R.id.screen_widget_id_06
                    endToStart = R.id.screen_guide_line_03
                    bottomToBottom = R.id.screen_widget_id_06
                }

                nonSpacingTextView(R.string.sign_up_060) {
                    id = R.id.screen_widget_id_08
                    includeFontPadding = false
                    textSize = 26.0f
                    letterSpacing = -0.08f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(130.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_06
                    bottomToBottom = R.id.screen_widget_id_06
                    startToEnd = R.id.screen_guide_line_03
                    marginStart = dip(10.0f)
                }

                // TODO : 성별 radio button

                radioGroup {
                    id = R.id.screen_widget_id_09
                    orientation = LinearLayout.HORIZONTAL

                    radioButton {
                        id = R.id.screen_widget_id_10
                        backgroundResource = R.drawable.selector_gender_man
                    }.lparams(0, matchParent) {
                        weight = 1f
                    }
                    radioButton {
                        id = R.id.screen_widget_id_11
                        backgroundResource = R.drawable.selector_gender_woman
                    }.lparams(0, matchParent) {
                        weight = 1f
                        marginStart = dip(10)
                    }

                    onCheckedChange { group, checkedId ->
                        curringCreateUser = generateCurringCreateUser(checkedId == R.id.screen_widget_id_10)
                    }
                }.lparams(0, dip(70)) {
                    startToEnd = R.id.screen_guide_line_04
                    topToTop = R.id.screen_widget_id_08
                    endToStart = R.id.screen_guide_line_02
                    bottomToBottom = R.id.screen_widget_id_08
                }.applyRecursively {
                    when(it) {
                        is RadioButton -> {
                            it.textSize = 26.7f
                            it.buttonDrawable = null
                            it.elevation = dip(5.0f).toFloat()
                            it.gravity = Gravity.CENTER
                        }
                    }
                }

                // 생년월일 입력 칸
                nonSpacingTextView(R.string.sign_up_070) {
                    id = R.id.screen_widget_id_12
                    includeFontPadding = false
                    textSize = 26.0f
                    letterSpacing = -0.08f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(130.0f), wrapContent) {
                    startToStart = R.id.screen_widget_id_06
                    topToBottom = R.id.screen_widget_id_06
                    bottomToBottom = R.id.screen_widget_id_05
                    topMargin = dip(17.0f)
                }

                birthInputView = editText {
                    textSize = 26.7f
                    textColorResource = R.color.x_353739
                    hint = getString(R.string.sign_up_150)
                    hintTextColor = ContextCompat.getColor(context,R.color.x_dbdddf)
                    typeface = Font.kopubDotumBold
                    backgroundResource = R.drawable.selector_bg_r11_3_view_01
                    inputType = InputType.TYPE_CLASS_NUMBER
                    maxLines = 1
                    filters = arrayOf(InputFilter.LengthFilter(8))
                    setPadding(dip(28),0,0,0)
                }.lparams(0, dip(70)) {
                    marginEnd = dip(43.3f)
                    startToEnd = R.id.screen_guide_line_01
                    topToTop = R.id.screen_widget_id_12
                    endToStart = R.id.screen_guide_line_03
                    bottomToBottom = R.id.screen_widget_id_12
                }

                nonSpacingTextView(R.string.sign_up_080) {
                    id = R.id.screen_widget_id_13
                    includeFontPadding = false
                    textSize = 26.0f
                    letterSpacing = -0.08f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(130.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_12
                    bottomToBottom = R.id.screen_widget_id_12
                    startToStart = R.id.screen_widget_id_08
                }

                phoneInputView = editText {
                    textSize = 26.7f
                    textColorResource = R.color.x_353739
                    hint = getString(R.string.sign_up_160)
                    hintTextColor = ContextCompat.getColor(context,R.color.x_dbdddf)
                    backgroundResource = R.drawable.selector_bg_r11_3_view_01
                    gravity = Gravity.CENTER_VERTICAL
                    typeface = Font.kopubDotumBold
                    inputType = InputType.TYPE_CLASS_NUMBER
                    maxLines = 1
                    filters = arrayOf(InputFilter.LengthFilter(11))
                    setPadding(dip(28),0,0,0)
                }.lparams(0, dip(70)) {
                    startToEnd = R.id.screen_guide_line_04
                    topToTop = R.id.screen_widget_id_13
                    endToStart = R.id.screen_guide_line_02
                    bottomToBottom = R.id.screen_widget_id_13
                }

                // TODO : 휴대폰 입력 란

                // 관리자 정보
                nonSpacingTextView(R.string.sign_up_090) {
                    id = R.id.screen_widget_id_14
                    maxLines = 1
                    textSize = 22.0f
                    textColorResource = R.color.x_979b9d
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(22)
                    startToStart = R.id.screen_widget_id_04
                    topToBottom = R.id.screen_widget_id_05
                }


                view {
                    id = R.id.screen_widget_id_15
                    backgroundColorResource = R.color.x_ffffff
                }.lparams(dip(0.0f), dip(110.0f)) {
                    startToStart = PARENT_ID
                    topToBottom = R.id.screen_widget_id_14
                    topMargin = dip(14.0f)
                    endToEnd = PARENT_ID
                }

                nonSpacingTextView(R.string.sign_up_100) {
                    id = R.id.screen_widget_id_16
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(130.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_15
                    bottomToBottom = R.id.screen_widget_id_15
                    startToStart = R.id.screen_widget_id_06
                }

                emailInputView = editText {
                    id = R.id.screen_widget_id_20
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumBold
                    inputType = InputType.TYPE_CLASS_TEXT
                    maxLines = 1
                    backgroundResource = R.drawable.selector_bg_r11_3_view_01
                    setPadding(dip(28),0,0,0)
                }.lparams(dip(170.7f), dip(70)) {
                    startToEnd = R.id.screen_guide_line_01
                    topToTop = R.id.screen_widget_id_16
                    bottomToBottom = R.id.screen_widget_id_16
                }

                // TODO : 이메일 입력 칸
                nonSpacingTextView("@") {
                    id = R.id.screen_widget_id_21
                    maxLines = 1
                    textSize = 28f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                    isSelected = false
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_20
                    marginStart = dip( 17.3f)
                    topToTop = R.id.screen_widget_id_16
                    bottomToBottom = R.id.screen_widget_id_16
                }

                domainInputView = editText {
                    id = R.id.screen_widget_id_22
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumBold
                    inputType = InputType.TYPE_CLASS_TEXT
                    maxLines = 1
                    hint = getString(R.string.sign_up_180)
                    backgroundResource = R.drawable.selector_bg_r11_3_view_01
                    setPadding(dip(28),0,dip(28),0)
                    isVisible = false
                }.lparams(dip(215.3f), dip(70)) {
                    startToEnd = R.id.screen_widget_id_21
                    marginStart = dip( 17.3f)
                    topToTop = R.id.screen_widget_id_16
                    bottomToBottom = R.id.screen_widget_id_16
                }

                domainSpinnerView = spinner {
                    id = R.id.screen_widget_id_23
                    val emailArray = resources.getStringArray(R.array.email_servers)
                    adapter = SpinnerItemAdapter(requireContext(),emailArray)

                    onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>?,view: View?,position: Int,id: Long) {
                            when(position) {
                                4 -> {
                                    domainSpinnerView.isVisible = false
                                    domainSpinnerArrowView.isVisible = false
                                    domainInputView.isVisible = true
                                    domainString = ""
                                }
                                else -> domainString = emailArray[position]
                                // TODO : 삼각형 아이콘이 필요함.
                                // TODO : edittext 에서 다시 Spinner 로 돌아가는 방법이 있는지?
                            }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {
                        }
                    }
                    stateListAnimator = null
                    backgroundResource = R.drawable.shape_r11_3_dbdddf
                    gravity = Gravity.CENTER_VERTICAL
                    setPadding(dip(28),0,dip(50f),0)
                }.lparams(dip(215.3f), dip(70)) {
                    startToEnd = R.id.screen_widget_id_21
                    marginStart = dip( 17.3f)
                    topToTop = R.id.screen_widget_id_16
                    bottomToBottom = R.id.screen_widget_id_16
                    horizontalBias = 0.0f
                }

                domainSpinnerArrowView = imageView(R.drawable.vector_triangle) {
                    rotation = 90f
                }.lparams(wrapContent,wrapContent) {
                    setMargins(0,0,dip(22),0)
                    topToTop = R.id.screen_widget_id_23
                    bottomToBottom = R.id.screen_widget_id_23
                    endToEnd = R.id.screen_widget_id_23
                }

                agreeCheckBox = checkBox {
                    id = R.id.screen_widget_id_17
                    backgroundResource = R.drawable.selector_sign_agree
                    buttonDrawable = null
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(22f)
                    topToBottom = R.id.screen_widget_id_15
                    startToStart = R.id.screen_widget_id_04
                }

                nonSpacingTextView(R.string.sign_up_110) {
                    id = R.id.screen_widget_id_18
                    maxLines = 1
                    textSize = 18.7f
                    typeface = Font.kopubDotumMedium
                    textColorResource = R.color.x_929597
                }.lparams(wrapContent, wrapContent) {
                    marginStart = dip(17.3f)
                    startToEnd = R.id.screen_widget_id_17
                    topToTop = R.id.screen_widget_id_17
                    bottomToBottom = R.id.screen_widget_id_17
                    horizontalBias = 0.0f
                }

                button(R.string.sign_up_120) {
                    backgroundResource = R.drawable.selector_bg_r6_7_btn_01
                    textSize = 20.0f
                    textColorResource = R.color.x_a8a8a8
                    //setTextColor(AppCompatResources.getColorStateList(owner.requireContext(), R.color.selector_r10_btn_text_01))
                    typeface = Font.kopubDotumMedium
                    gravity = Gravity.CENTER
                    stateListAnimator = null
                    onDebounceClick {
                        showPolicyPopup()
                    }
                }.lparams(wrapContent, 0) {
                    marginStart = dip(19.3f)
                    startToEnd = R.id.screen_widget_id_18
                    topToTop = R.id.screen_widget_id_17
                    bottomToBottom = R.id.screen_widget_id_17
                }
            }.lparams(dip(0.0f), dip(0.0f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_03
                endToEnd = PARENT_ID
                bottomToTop = R.id.screen_widget_id_19
                verticalBias = 0.0f
                horizontalBias = 0.0f
            }

            button(R.string.sign_up_130) {
                id = R.id.screen_widget_id_19
                backgroundResource = R.drawable.selector_bg_r40_btn_02
                textSize = 36.0f
                setTextColor(AppCompatResources.getColorStateList(owner.requireContext(), R.color.selector_r10_btn_text_01))
                typeface = Font.kopubDotumMedium
                gravity = Gravity.CENTER
                stateListAnimator = null
                onDebounceClick {
                    if(checkInputDatas()) {
                        showSignUpDataPopUp()
                    }
                }
            }.lparams(dip(473.3f), dip(76.0f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(24f)
            }
        }
    }.view

    private fun checkInputDatas():Boolean {
        fun EditText.isAvailable(textCount:Int = 0) = text.toString().trim().length >= textCount

        fun String.checkBirth() = when {
            isEmpty() -> false
            else -> DateTimeFormat.forPattern(DTP_YYYYMMDD).parseDateTime(this).isBeforeNow
        }

        fun String.checkPhone()= when {
            isEmpty() -> false
            else -> Pattern.compile("\\b01(?:0|1|[6-9])(?:\\d{3}|\\d{4})\\d{4}\\b")
                .matcher(this)
                .matches()
        }

        fun String.checkEmail()= Patterns.EMAIL_ADDRESS.matcher(this).matches()

        domainInputView.takeIf {
            it.isVisible
        }?.run {
            domainString = domainInputView.text.toString()
        }
        emailString = "${emailInputView.text}@${domainString}".takeIf {
            it.checkEmail()
        }

        when {
            !nameInputView.isAvailable(2) -> R.string.sign_up_notifications_010
            curringCreateUser == null -> R.string.sign_up_notifications_020
            !birthInputView.isAvailable(8) or
            !birthInputView.text.toString().checkBirth() -> R.string.sign_up_notifications_030
            !phoneInputView.isAvailable(11) or
            !phoneInputView.text.toString().checkPhone() -> R.string.sign_up_notifications_040
//            !emailString.checkEmail() -> R.string.sign_up_notifications_050
            !agreeCheckBox.isChecked -> R.string.sign_up_notifications_060
            else -> null
        }?.let { msgResId ->
            alert(requireContext(),msgResId)
            return false
        }

        return true
    }

    private fun showSignUpDataPopUp() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                maxWidth(literal = dip(1000.0f))
                customView(
                    view = SignUpDataPopComponent<MaterialDialog>(
                        data = curringCreateUser!!.invoke(
                            nameInputView.text.toString().trim(),
                            birthInputView.text.toString().trim(),
                            phoneInputView.text.toString().trim(),
                            emailString
                        ),
                        onConfirm = { user ->
                            dismiss()
                            //showPolicyPopup()
                            signUp(user)
                        },
                        onCancel = {
                            dismiss()
                        }
                    ).createView(
                        AnkoContext.create(requireContext(), this)
                    ).also { v ->
                        (v as? ConstraintLayout)?.layoutParams =
                            FrameLayout.LayoutParams(dip(1000.0f), dip(689.3f))
                    },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }

    private fun showPolicyPopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                maxWidth(literal = dip(800.0f))
                customView(
                    view = PolicyPopComponent<MaterialDialog>(){
                        dismiss()
                    }.createView(
                        AnkoContext.create(requireContext(), this)
                    ).also { v ->
                        (v as? ConstraintLayout)?.layoutParams =
                            FrameLayout.LayoutParams(dip(800.0f), wrapContent)
//                            FrameLayout.LayoutParams(dip(800.0f), dip(689.3f))
                    },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }

    private fun signUp(data:SignUpData) {
        fun executeError(th: Throwable) {
            when (th) {
                is SocketTimeoutException -> R.string.api_error_msg_050
                is HttpExceptionWrapper   -> {
                    Timber.e("--> Http status code[${th.httpError.status}], message[${th.httpError.message}]")
                    th.httpError.message.let { code ->
                        when (XomnifitHeader.ResultCode.of(code)) {
                            XomnifitHeader.ResultCode.ERROR_ALREADY_REGISTERED_USER        -> R.string.api_error_msg_100
                            else                                                  -> {
                                Timber.e(th)
                                R.string.api_error_msg_090
                            }
                        }
                    }
                }
                else                      -> {
                    Timber.e(th)
                    R.string.api_error_msg_090
                }
            }.let { messageResId ->
                runOnUiThread {
                    alert(requireContext(),messageResId)
                }
            }
        }

        WebApi.userSignUp(
            requireContext(),
            this@SignUpScreen,
            data,
            { user ->
                runOnUiThread {
                    performBackPressed()
                }
            },
            { th ->
                th.printStackTrace()
                executeError(th)
            }
        )
    }
}

class SpinnerItemAdapter(
    ctx: Context,
    val items: Array<String>
) : ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, items) {
    private val ankoContext = AnkoContext.createReusable(ctx, this)

    override fun getView(position: Int,convertView: View?,parent: ViewGroup): View {
        return convertView ?: ListItem(items[position]).createView(ankoContext)
    }

    internal class ListItem(val msg:String) : AnkoComponent<SpinnerItemAdapter> {

        override fun createView(ui: AnkoContext<SpinnerItemAdapter>) = ui.apply {
            textView {
                text = msg
                textSize = 26.7f
                typeface = Font.kopubDotumBold
                textColorResource = R.color.x_353739
                lines = 1
                ellipsize = TextUtils.TruncateAt.MARQUEE
            }
        }.view

        private fun getHolder(convertView: View): SpinnerItemHolder {
            return (convertView.tag as? SpinnerItemHolder) ?: SpinnerItemHolder(convertView as TextView).apply {
                convertView.tag = this
            }
        }

        inner class SpinnerItemHolder(val textView:TextView)
    }
}