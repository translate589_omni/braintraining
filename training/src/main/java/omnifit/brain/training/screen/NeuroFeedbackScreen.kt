package omnifit.brain.training.screen

import android.animation.LayoutTransition
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import at.favre.lib.dali.Dali
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.R
import omnifit.brain.training.component.*
import omnifit.brain.training.helper.ServiceResultHelper
import omnifit.brain.training.view.headsetStateVisualizer
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.common.*
import omnifit.commons.omnifitbrain.Error
import omnifit.commons.omnifitbrain.useOmnifitBrain
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import omnifit.commons.peripheral.common.toMultipleElectroencephalogram
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NeuroFeedbackScreen(val content: AudioPlayer.AudioSource) : UIScreen() {
    private var trainingTimeView: TextView? = null
    private var analyzingView: View? = null
    private var resultErrorView: View? = null
    private var buttonPlay: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            layoutTransition = LayoutTransition()

            // 배경
            imageView {
                into(content.thumbnail as? Drawable)
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }
            // 불투명 처리
            imageView {
                backgroundColorResource = R.color.x_000000_op40
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_wt_2) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            // 그래프
            headsetStateVisualizer(icon = R.drawable.ic_headset_wave) {
                id = R.id.screen_widget_id_02
                bindVisualized(owner, Bindable.isHeadsetStateVisualized)
            }.lparams(matchParent, wrapContent) {
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(134.7f)
            }

            // 측정시간
            trainingTimeView = textView("00:00") {
                id = R.id.screen_widget_id_03
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textColorResource = R.color.x_ffffff
                textSize = 93.0f
                typeface = Font.latoRegular
            }.lparams(0, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_01
                topMargin = dip(192.7f)
                endToEnd = PARENT_ID
            }

            buttonPlay = imageView(R.drawable.selector_btn_play_and_stop) {
                onDebounceClick {
                    when(isTraining.value) {
                        true -> {
                            performBackPressed()
                        }
                        false -> {
                            startTraining()
                        }
                    }
                }
                bindPlayButtonDuration(owner,isTraining)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(22)
            }

            analyzingView = AnalyzingViewComponent<NeuroFeedbackScreen>(ServiceCategory.NEURO_FEEDBACK)
                .createView(AnkoContext.create(requireContext(), this@NeuroFeedbackScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_04
                    v.isVisible = false
                    addView(v)
                }

            resultErrorView = ResultErrorViewComponent<NeuroFeedbackScreen>(ServiceCategory.NEURO_FEEDBACK)
                .createView(AnkoContext.create(requireContext(), this@NeuroFeedbackScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_05
                    v.isVisible = false
                    addView(v)
                }
        }
    }.view

    @ExperimentalUnsignedTypes
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        content.source.takeUnless {
            it is String
        }?.let {
            AudioPlayer.observePlaybackStatus(this) { state ->
                when (state) {
                    is AudioPlayer.PlaybackState.Prepare  -> Unit
                    is AudioPlayer.PlaybackState.Start    -> initEffectSounds { observeBrainFeedback() }
                    is AudioPlayer.PlaybackState.Playing  -> Unit
                    is AudioPlayer.PlaybackState.Pause    -> Unit
                    is AudioPlayer.PlaybackState.Resume   -> Unit
                    is AudioPlayer.PlaybackState.Stop     -> Unit
                    is AudioPlayer.PlaybackState.Complete -> Unit
                    is AudioPlayer.PlaybackState.Error    -> Unit
                    else                                  -> Unit
                }
            }
        }

        // 최초 팝업
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                hideSoftKeyboard()
                maxWidth(literal = dip(514.0f))
                customView(
                    view = TrainingStartGuidePopComponent<MaterialDialog>(
                        R.string.meditation_content_screen_050,
                        R.string.meditation_content_screen_060
                    )
                        .createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(333.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                    startTraining()
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseEffectSound()
    }

    @ExperimentalUnsignedTypes
    override fun onBackPressed(): Boolean {
        if(isTraining.value) {
            requireStopWithValidTraining { isStop ->
                if (isStop) stopTraining()
            }
            return true
        }

        if (analyzingView?.isVisible == true) {
            alert(
                requireContext(),
                R.string.meditation_content_screen_030,
                R.color.x_000000_op60
            )
            return true
        }

        useOmnifitBrain {
            cancelMeasurement()
        }

        stopTraining()
        return super.onBackPressed()
    }

    private var trainingElapsedTimeFinalizer: Disposable? = null
    private var trainingElapsedTime: Long = 0L
    @ExperimentalUnsignedTypes
    private fun observeTrainingElapsedTimeChange() {
        if (trainingElapsedTimeFinalizer == null) {
            trainingElapsedTimeFinalizer = Observable.interval(0L, 1L, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    content.source.takeUnless{
                        it is String
                    }?.let {
                        playMusic()
                    } ?: run {
                        initEffectSounds { observeBrainFeedback() }
                    }
                }
                .doFinally {
                    updateTrainingTime(0L)
                    trainingElapsedTimeFinalizer = null
                }
                .subscribe(
                    { t ->
                        trainingElapsedTime = t
                        // 60분이 넘은 경우 종료
                        if(t >= 60.minutesToSeconds()) {
                            // TODO : 음성 메세지가 나오는 중 화면 전환을 하면?
                            effectCalmEndSound()
                            stopTraining()
                            showStopTrainingWhen1HourDialog()
                        }
                        updateTrainingTime(t)
                    },
                    { e ->
                        e.printStackTrace()
                    }
                )
        }
    }

    private fun updateTrainingTime(seconds: Long) {
        trainingTimeView?.text = seconds.secondsToMillis().millisToFormatString(DTP_MM_SS)
    }

    private val isTraining: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    @ExperimentalUnsignedTypes
    private fun startTraining() {
        isTraining.value = true

        observeTrainingElapsedTimeChange()
    }

    private fun stopTraining() {
        isTraining.value = false
        trainingElapsedTimeFinalizer?.dispose()
        content.source.takeUnless{
            it is String
        }?.let {
            stopMusic()
        }
    }

    private fun showStopTrainingWhen1HourDialog() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BasicPopComponent<MaterialDialog>(
                        context.getString(R.string.meditation_content_screen_040),
                        context.getString(R.string.meditation_content_screen_080),
                        Gravity.CENTER,
                        false,
                        buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                            override fun onPositive() {
                                dismiss()
                                useOmnifitBrain {
                                    stopMeasurement()
                                }
                            }

                            override fun onNegative() {
                            }
                        }
                    )
                        .createView(AnkoContext.create(context, this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                        },
                    noVerticalPadding = true
                )
                cornerRadius(15.0f)
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                }
            }
    }

    @ExperimentalUnsignedTypes
    private fun playMusic() {
        AudioPlayer.play(
            requireContext(),
            this@NeuroFeedbackScreen,
            listOf(
                content.apply {
                    repeatable = true
                }
            ),
            playMode = AudioPlayer.PlayMode.RANDOM_PLAY
        )
    }

    private fun stopMusic() {
        AudioPlayer.stop()
    }

    @ExperimentalUnsignedTypes
    private fun observeBrainFeedback() {
        useOmnifitBrain {
            isNotMeasuring {
                startMeasurement {
                    onStart {
                        Timber.v("[두뇌안정훈련] --> 측정시작")
                    }
                    onMeasure { m ->
                        Timber.v("[두뇌안정훈련] --> 측정진행[${m.elapsedTime.secondsToFormatString(DTP_MM_SS)}]")
                        // TODO(부착상태확인)
//                        alert(requireContext(), R.string.notifications_020)
                        Timber.v("Test : ${m.result}")

                        m.result.takeUnless{
                            it.isEmpty()
                        }?.let {
                            when (it[0].leftRelaxationIndicatorValue >= 7.0) {
                                true  -> effectCalmLevelSound()
                                false -> stopEffectSound()
                            }
                        }
                    }
                    onFinish {
                        Timber.v("[두뇌안정훈련] --> 측정종료")
                        runOnUiThread {
                            processingToResult(
                                content.sequence,
                                trainingElapsedTime.toInt(),
                                it.result.toMultipleElectroencephalogram()
                            )
                        }
                    }
                    onCancel { _, error ->
                        Timber.v("[두뇌안정훈련] --> 측정취소")
                        when (error) {
                            Error.OMNIFIT_BRAIN_HEADSET_UNLINKED -> {
                                showBrainDisconnectErrorPopup()
                            }
                            else                                 -> Unit
                        }
                    }
                }
            }
        }
    }

    private fun requireValidMeasurement(): Boolean {
        return trainingElapsedTime >= 3.minutesToSeconds()
    }

    @ExperimentalUnsignedTypes
    private fun requireStopWithValidTraining(block: (Boolean) -> Unit): Boolean {
        return requireValidMeasurement()
            .also { valid ->
                MaterialDialog(requireContext())
                    .show {
                        hideNavigation()
                        customView(
                            view = BasicPopComponent<MaterialDialog>(
                                context.getString(
                                    if (valid) R.string.meditation_content_screen_040
                                    else R.string.common_050
                                ),
                                context.getString(
                                    if (valid) R.string.meditation_content_screen_070
                                    else R.string.common_060
                                ),
                                Gravity.CENTER,
                                buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                                    override fun onPositive() {
                                        block(true)
                                        useOmnifitBrain {
                                            when (valid) {
                                                true -> stopMeasurement()
                                                else -> performBackPressed()
                                            }
                                        }
                                        dismiss()
                                    }

                                    override fun onNegative() {
                                        block(false)
                                        dismiss()
                                    }
                                }
                            )
                                .createView(AnkoContext.create(context, this))
                                .also { v ->
                                    (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                                },
                            noVerticalPadding = true
                        )
                        cornerRadius(15.0f)
                        cancelable(false)
                        cancelOnTouchOutside(false)
                        setOnDismissListener {
                        }
                    }
            }
    }

    private fun checkValidResult(
        result: MultipleElectroencephalogram,
        block: () -> Unit
    ): Boolean {
        return result
            .meanRelaxationIndicatorValues
            .any {
                it > 0.0
            }
            .also { isValid ->
                when {
                    isValid -> block()
                    else    -> showResultErrorView()
                }
            }
    }

    private fun processingToResult(
        sequence: Int,
        elapsedTime: Int,
        result: MultipleElectroencephalogram
    ) {
        checkValidResult(result) {
            showAnalyzingView()
            ServiceResultHelper.createResult(
                context = requireContext(),
                user = currentUser,
                category = ServiceCategory.NEURO_FEEDBACK,
                contentSequence = sequence,
                elapsedTime = elapsedTime,
                result = result
            ) { timeTaken, _result ->
                Timber.d("[두뇌안정훈련] --> 서비스 결과 생성 ID : ${_result.id}, 소요시간 : ${timeTaken.millisToFormatString(DTP_SS_SSS)}")
                Single.timer(1L, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .bindUntilEvent(this, Lifecycle.Event.ON_DESTROY)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            runOnUiThread {
                                screenTo(ServiceResultScreen(_result), R.anim.slide_hold, R.anim.slide_exit_right)
                            }
                        },
                        { e -> Timber.d(e) }
                    )
            }
        }
    }

    private val blurBackgroundDrawable: BitmapDrawable?
        get() = this.view
            ?.run {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    .also { bmp ->
                        draw(Canvas(bmp))
                    }
            }
            ?.let { bmp ->
                Dali.create(context)
                    .load(bmp)
                    .blurRadius(25)
                    .concurrent()
                    .skipCache()
                    .get()
            }

    private fun showAnalyzingView() {
        analyzingView?.backgroundDrawable = blurBackgroundDrawable
        analyzingView?.isVisible = true
        analyzingView?.bringToFront()
    }

    private fun showResultErrorView() {
        resultErrorView?.backgroundDrawable = blurBackgroundDrawable
        resultErrorView?.isVisible = true
    }

    private fun showBrainDisconnectErrorPopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BrainDisconnectPopComponent<MaterialDialog>(
                        onConfirm = { screenTo(MainScreen()) }
                    ).createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(633.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }

    /*
   ╔═══════════════════════════════════════════════════════════════════════════
   ║
   ║ ⥥ SOUND POOL
   ║
   ╚═══════════════════════════════════════════════════════════════════════════
   */

    private var isEffectSoundLoadCompleted: Boolean = false

    private fun initEffectSounds(onComplete: () -> Unit) {
        if (isEffectSoundLoadCompleted) {
            onComplete()
            return
        }
        effectSound.setOnLoadCompleteListener { _, _, status ->
            isEffectSoundLoadCompleted = status == 0
            if (isEffectSoundLoadCompleted) {
                onComplete()
            }
        }
        effectSoundId
    }

    private val effectSound: SoundPool by lazy {
        SoundPool.Builder()
            .setMaxStreams(1)
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            .build()
    }

    private val effectSoundId: Int by lazy {
        effectSound.load(requireContext(), R.raw.effect_calm, 1)
    }

    private val endSoundId: Int by lazy {
        effectSound.load(requireContext(), R.raw.effect_ai_1, 1)
    }


    private var playingEffectSoundId: Int = 0
    private var playingEffectSoundStreamId: Int = 0

    private fun effectCalmLevelSound() {
        if (playingEffectSoundId != effectSoundId) {
            playingEffectSoundId = effectSoundId
            playingEffectSoundStreamId = effectSound.play(playingEffectSoundId, 0.4f, 0.4f, 0, -1, 1f)
        }
    }

    private fun effectCalmEndSound() {
        playingEffectSoundStreamId = effectSound.play(playingEffectSoundId, 0.4f, 0.4f, 0, -1, 1f)
    }

    private fun stopEffectSound() {
        if (playingEffectSoundId != 0) {
            playingEffectSoundId = 0
            effectSound.stop(playingEffectSoundStreamId)
        }
    }

    private fun releaseEffectSound() {
        effectSound.release()
    }

    private fun TextView.bindDuration(
        owner: LifecycleOwner,
        bindable: BaseObservableProperty<Int>
    ) {
        bindSomething(owner,bindable) { offSet ->
            context.runOnUiThread {
                text = (content.duration + offSet).millisToFormatString(DTP_MM_SS)
            }
        }
    }

    private fun ImageView.bindPlayButtonDuration(
        owner: LifecycleOwner,
        bindable: BaseObservableProperty<Boolean>
    ) {
        bindSomething(owner,bindable) { bool ->
            context.runOnUiThread {
                isActivated = bool
            }
        }
    }
}