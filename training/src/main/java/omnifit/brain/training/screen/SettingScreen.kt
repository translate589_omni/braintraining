package omnifit.brain.training.screen

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import omnifit.brain.training.*
import omnifit.brain.training.component.AppUpdatePopComponent
import omnifit.brain.training.component.BasicPopComponent
import omnifit.brain.training.component.HeadsetSettingPopComponent
import omnifit.brain.training.component.SynchronizableDataPopComponent
import omnifit.brain.training.db.model.Administrator
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.http.ServiceConfiguration
import omnifit.brain.training.util.decryptAes256
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.common.BaseObservableProperty
import omnifit.commons.omnifitbrain.Headset
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import kotlin.properties.Delegates

class SettingScreen : UIScreen() {

    private var dataSyncCount: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            dataSyncCountView?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(String.format(getString(R.string.setting_screen_130), n), Html.FROM_HTML_OPTION_USE_CSS_COLORS)
            }
            else {
                Html.fromHtml(String.format(getString(R.string.setting_screen_130), n))
            }
        }
    }

    private var corpName: TextView? = null
    private var administratorName: TextView? = null
    private var appUpdate: Button? = null
    private var dataSyncCountView: TextView? = null
    private var dataSyncUpload: Button? = null

    @ExperimentalUnsignedTypes
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            backgroundResource = R.drawable.shape_bg_main

            view {
                id = R.id.screen_widget_id_01
                backgroundResource = R.drawable.layer_bg_common_header
            }.lparams(dip(0.0f), dip(81.0f)) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            textView(R.string.setting_screen_010) {
                id = R.id.screen_widget_id_02
                textSize = 33.3f
                textColorResource = R.color.x_000000
                typeface = Font.kopubDotumMedium
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(0, 0) {
                topToTop = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
            }

            constraintLayout {
                guideline {
                    id = R.id.screen_guide_line_01
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideBegin = dip(37.0f)
                }

                guideline {
                    id = R.id.screen_guide_line_02
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideEnd = dip(37.0f)
                }

                guideline {
                    id = R.id.screen_guide_line_03
                }.lparams(0, 0) {
                    orientation = ConstraintLayout.LayoutParams.VERTICAL
                    guideBegin = dip(640.0f)
                }

                // 관리자 정보
                nonSpacingTextView(R.string.setting_screen_020) {
                    id = R.id.screen_widget_id_03
                    maxLines = 1
                    textSize = 22.0f
                    textColorResource = R.color.x_9ea3a7
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_guide_line_01
                    topToTop = PARENT_ID
                }

                view {
                    id = R.id.screen_widget_id_04
                    backgroundResource = R.drawable.shape_r10_b1_d8e1e6_ffffff
                }.lparams(dip(0.0f), dip(81.0f)) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(14.0f)
                    endToStart = R.id.screen_guide_line_02
                }

                textView(R.string.setting_screen_030) {
                    id = R.id.screen_widget_id_05
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_04
                    startToStart = R.id.screen_widget_id_04
                    marginStart = dip(33.0f)
                }

                corpName = textView {
                    maxLines = 1
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_05
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_04
                    endToStart = R.id.screen_guide_line_03
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_04
                    horizontalBias = 0.0f
                }

                textView(R.string.setting_screen_040) {
                    id = R.id.screen_widget_id_06
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_04
                    startToEnd = R.id.screen_guide_line_03
                    marginStart = dip(33.0f)
                }

                administratorName = textView {
                    id = R.id.screen_widget_id_08
                    maxLines = 1
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_06
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_04
                    endToStart = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_04
                    horizontalBias = 0.0f
                }

                // 측정 기기
                nonSpacingTextView(R.string.setting_screen_050) {
                    id = R.id.screen_widget_id_08
                    maxLines = 1
                    textSize = 22.0f
                    textColorResource = R.color.x_9ea3a7
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_04
                    topMargin = dip(17.0f)
                }

                view {
                    id = R.id.screen_widget_id_09
                    backgroundResource = R.drawable.shape_r10_b1_d8e1e6_ffffff
                }.lparams(dip(0.0f), dip(81.0f)) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_08
                    topMargin = dip(14.0f)
                    endToStart = R.id.screen_guide_line_02
                }

                textView(R.string.setting_screen_060) {
                    id = R.id.screen_widget_id_10
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_09
                    bottomToBottom = R.id.screen_widget_id_09
                    startToStart = R.id.screen_widget_id_09
                    marginStart = dip(33.0f)
                }

                textView {
                    maxLines = 1
                    textSize = 26.7f
                    textResource = R.string.headset_setting_060
                    typeface = Font.kopubDotumMedium
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_headset_name_text))
                    bindHeadsetName(owner, Bindable.handlingHeadset)
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_10
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_09
                    endToStart = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_09
                    horizontalBias = 0.0f
                }

                button(R.string.setting_screen_070) {
                    id = R.id.screen_widget_id_11
                    backgroundResource = R.drawable.selector_bg_r26_66_btn_01
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r26_66_btn_text_01))
                    textSize = 22.66f
                    stateListAnimator = null
                    isAllCaps = false
                    onDebounceClick { openHeadsetSetting() }
                }.lparams(dip(126.7f), dip(53.3f)) {
                    topToTop = R.id.screen_widget_id_09
                    bottomToBottom = R.id.screen_widget_id_09
                    endToEnd = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                }

                checkBox {
                    id = R.id.screen_widget_id_12
                    isClickable = false
                    backgroundResource = R.drawable.selector_ico_setting_bt_on_off
                    buttonDrawable = null
                    bindChecked(owner, Bindable.isHeadsetLinked)
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_09
                    bottomToBottom = R.id.screen_widget_id_09
                    endToStart = R.id.screen_widget_id_11
                    marginEnd = dip(19.0f)
                }

                progressBar {
                    indeterminateDrawable = CircularProgressDrawable(context).apply {
                        strokeWidth = dip(2.0f).toFloat()
                        backgroundColor = Color.TRANSPARENT
                        setColorSchemeColors(ContextCompat.getColor(context, R.color.x_2da3f5))
                    }
                    bindVisible(owner, Bindable.isHeadsetLinking)
                }.lparams(dip(0.0f), dip(0.0f)) {
                    startToStart = R.id.screen_widget_id_12
                    topToTop = R.id.screen_widget_id_12
                    endToEnd = R.id.screen_widget_id_12
                    bottomToBottom = R.id.screen_widget_id_12
                }

                // 앱 정보
                nonSpacingTextView(R.string.setting_screen_080) {
                    id = R.id.screen_widget_id_13
                    maxLines = 1
                    textSize = 22.0f
                    textColorResource = R.color.x_9ea3a7
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_09
                    topMargin = dip(17.0f)
                }

                view {
                    id = R.id.screen_widget_id_14
                    backgroundResource = R.drawable.shape_r10_b1_d8e1e6_ffffff
                }.lparams(dip(0.0f), dip(81.0f)) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_13
                    topMargin = dip(14.0f)
                    endToStart = R.id.screen_guide_line_02
                }

                textView(R.string.setting_screen_090) {
                    id = R.id.screen_widget_id_15
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_14
                    bottomToBottom = R.id.screen_widget_id_14
                    startToStart = R.id.screen_widget_id_13
                    marginStart = dip(33.0f)
                }

                textView {
                    maxLines = 1
                    text = getAppVersion(owner.requireContext())
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_15
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_14
                    endToStart = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_14
                    horizontalBias = 0.0f
                }

                appUpdate = button(R.string.setting_screen_100) {
                    backgroundResource = R.drawable.selector_bg_r26_66_btn_02
                    textSize = 22.66f
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r26_66_btn_text_02))
                    stateListAnimator = null
                    isAllCaps = false
                    isEnabled = false
                    onDebounceClick { openAppUpdateDialog() }
                }.lparams(dip(126.0f), dip(53.0f)) {
                    topToTop = R.id.screen_widget_id_14
                    bottomToBottom = R.id.screen_widget_id_14
                    endToEnd = R.id.screen_guide_line_03
                    marginEnd = dip(27.0f)
                }

                textView(R.string.setting_screen_160) {
                    id = R.id.screen_widget_id_20
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_14
                    bottomToBottom = R.id.screen_widget_id_14
                    startToEnd = R.id.screen_guide_line_03
                    marginStart = dip(33.0f)
                }

                textView {
                    id = R.id.screen_widget_id_21
                    maxLines = 1
                    textResource = R.string.app_name
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_20
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_14
                    endToStart = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_14
                    horizontalBias = 0.0f
                }

                // 동기화
                nonSpacingTextView(R.string.setting_screen_110) {
                    id = R.id.screen_widget_id_16
                    maxLines = 1
                    textSize = 22.0f
                    textColorResource = R.color.x_9ea3a7
                    typeface = Font.kopubDotumBold
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_14
                    topMargin = dip(17.0f)
                }

                view {
                    id = R.id.screen_widget_id_17
                    backgroundResource = R.drawable.shape_r10_b1_d8e1e6_ffffff
                }.lparams(dip(0.0f), dip(81.0f)) {
                    startToStart = R.id.screen_guide_line_01
                    topToBottom = R.id.screen_widget_id_16
                    topMargin = dip(14.0f)
                    endToStart = R.id.screen_guide_line_02
                }

                textView(R.string.setting_screen_120) {
                    id = R.id.screen_widget_id_18
                    maxLines = 1
                    includeFontPadding = false
                    textSize = 26.0f
                    textColorResource = R.color.x_353739
                    typeface = Font.kopubDotumBold
                }.lparams(dip(185.0f), wrapContent) {
                    topToTop = R.id.screen_widget_id_17
                    bottomToBottom = R.id.screen_widget_id_17
                    startToStart = R.id.screen_widget_id_17
                    marginStart = dip(33.0f)
                }

                dataSyncCountView = textView {
                    maxLines = 1
                    textSize = 26.7f
                    textColorResource = R.color.x_848484
                    typeface = Font.kopubDotumMedium
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_18
                    marginStart = dip(27.0f)
                    topToTop = R.id.screen_widget_id_17
                    endToStart = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                    bottomToBottom = R.id.screen_widget_id_17
                    horizontalBias = 0.0f
                }

                dataSyncUpload = button(R.string.setting_screen_140) {
                    backgroundResource = R.drawable.selector_bg_r26_66_btn_01
                    textSize = 22.66f
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r26_66_btn_text_01))
                    stateListAnimator = null
                    isAllCaps = false
                    onDebounceClick { openSynchronizableData() }
                }.lparams(dip(126), dip(53)) {
                    topToTop = R.id.screen_widget_id_17
                    bottomToBottom = R.id.screen_widget_id_17
                    endToEnd = R.id.screen_guide_line_02
                    marginEnd = dip(27.0f)
                }
            }.lparams(dip(0.0f), dip(0.0f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_01
                topMargin = dip(22.0f)
                endToEnd = PARENT_ID
                bottomToTop = R.id.screen_widget_id_19
                verticalBias = 0.0f
                horizontalBias = 0.0f
            }

            button(R.string.setting_screen_150) {
                id = R.id.screen_widget_id_19
                backgroundResource = R.drawable.selector_bg_r10_btn_01
                textSize = 36.0f
                setTextColor(AppCompatResources.getColorStateList(owner.requireContext(), R.color.selector_r10_btn_text_01))
                typeface = Font.kopubDotumMedium
                gravity = Gravity.CENTER
                stateListAnimator = null
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(dip(0.0f), dip(80.0f)) {
                startToStart = PARENT_ID
                marginStart = dip(37.0f)
                endToEnd = PARENT_ID
                marginEnd = dip(37.0f)
                bottomToBottom = PARENT_ID
                bottomMargin = dip(23.3f)
            }
        }
    }.view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(Administrator.get(requireContext())) {
            corpName?.text = corporationName.decryptAes256()
            administratorName?.text = name.decryptAes256()
        }

        updateAppVersion()
        uploadableBadgeCount()
    }

    private fun uploadableBadgeCount() {
        dataSyncCount = Measurement.countByUploadable()
        if (dataSyncCount > 0) {
            dataSyncUpload?.isEnabled = true
            Bindable.uploadableBadgeCount.value = dataSyncCount.toString()
        }
        else {
            dataSyncUpload?.isEnabled = false
        }
    }

    private fun updateAppVersion() {
//        if (BuildConfig.DEBUG) {
//            appUpdate?.isEnabled = true
//            return
//        }

        ServiceConfiguration.get(requireContext()).run {
            getAppUpdateConfigurationListThatRequireUpdate()?.let {
                appUpdate?.isEnabled = it.requiredUpdatable == "Y" && (getAppVersion(requireContext()).compareTo(it.appVersion)) < 0
            } ?: runOnUiThread {
                appUpdate?.isEnabled = false
            }
        }
    }

    private fun openSynchronizableData() {
        MaterialDialog(requireContext()).show {
            hideNavigation()
            hideSoftKeyboard()
            maxWidth(literal = dip(900.0f))
            customView(view = SynchronizableDataPopComponent<MaterialDialog>().createView(AnkoContext.create(requireContext(), this)).also { v ->
                (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(900.0f), FrameLayout.LayoutParams.WRAP_CONTENT)
            }, noVerticalPadding = true)
            cancelable(false)
            cancelOnTouchOutside(false)
            setOnDismissListener {
                uploadableBadgeCount()
            }
        }
    }

    @ExperimentalUnsignedTypes
    private fun openHeadsetSetting() {
        MaterialDialog(requireContext()).show {
            hideNavigation()
            hideSoftKeyboard()
            maxWidth(literal = dip(672.0f))
            customView(view = HeadsetSettingPopComponent<MaterialDialog>().createView(AnkoContext.create(requireContext(), this)).also { v ->
                (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(672.0f), FrameLayout.LayoutParams.WRAP_CONTENT)
            }, noVerticalPadding = true)
            cancelable(false)
            cancelOnTouchOutside(false)
            setOnDismissListener {

            }
        }
    }

    private fun openAppUpdateDialog() {
        requireDataSynchronizationCompleted {
            MaterialDialog(requireContext())
                .show {
                    hideNavigation()
                    customView(
                        view = BasicPopComponent<MaterialDialog>(
                            context.getString(R.string.app_update_screen_010),
                            context.getString(R.string.app_update_screen_080),
                            Gravity.CENTER,
                            false,
                            object : BasicPopComponent.ButtonActionListener {
                                override fun onPositive() {
                                    dismiss()
                                }

                                override fun onNegative() {
                                    dismiss()
                                }
                            }
                        )
                            .createView(AnkoContext.create(context, this))
                            .also { v ->
                                (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                            },
                        noVerticalPadding = true
                    )
                    cornerRadius(15.0f)
                    cancelable(false)
                    cancelOnTouchOutside(false)
                    setOnDismissListener {
                    }
                }
        }
            .takeIf {
                it
            }
            ?.run {
                MaterialDialog(requireContext()).show {
                    hideNavigation()
                    hideSoftKeyboard()
                    maxWidth(literal = dip(672.0f))
                    customView(view = AppUpdatePopComponent<MaterialDialog>().createView(AnkoContext.create(requireContext(), this)).also { v ->
                        (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(672.0f), FrameLayout.LayoutParams.WRAP_CONTENT)
                    }, noVerticalPadding = true)
                    cancelable(false)
                    cancelOnTouchOutside(false)
                    setOnDismissListener {}
                }
            }
    }

    private fun requireDataSynchronizationCompleted(block: () -> Unit): Boolean =
        (Bindable.uploadableBadgeCount.value.toInt() > 0)
            .not()
            .also { isCompleted ->
                if (!isCompleted) block()
            }

    private fun <T : TextView> T.bindHeadsetName(
        owner: LifecycleOwner,
        bindable: BaseObservableProperty<Headset>,
        @StringRes format: Int = -1
    ) {
        fun bind(v: Headset) {
            context.runOnUiThread {
                text = v.name
                isSelected = !v.isEmpty
            }
        }

        val valueChangeListener: (Headset) -> Unit = { s ->
            bind(s)
        }

        bind(bindable.value)
        bindable.addValueChangeListener(valueChangeListener)
        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun unbind() {
                bindable.removeValueChangeListener(valueChangeListener)
                owner.lifecycle.removeObserver(this)
            }
        })
    }
}