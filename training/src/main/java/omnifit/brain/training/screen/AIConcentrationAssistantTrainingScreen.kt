package omnifit.brain.training.screen

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.lifecycle.Lifecycle
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import at.favre.lib.dali.Dali
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.R
import omnifit.brain.training.algorithm.ServiceAlgorithm
import omnifit.brain.training.component.*
import omnifit.brain.training.helper.*
import omnifit.brain.training.view.headsetStateVisualizer
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.binauralbeatgenerator.BinauralBeatGenerator
import omnifit.commons.common.*
import omnifit.commons.omnifitbrain.Error
import omnifit.commons.omnifitbrain.useOmnifitBrain
import omnifit.commons.peripheral.common.Eyes
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import omnifit.commons.peripheral.common.toMultipleElectroencephalogram
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.asSequence
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.runOnUiThread
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class AIConcentrationAssistantTrainingScreen : UIScreen() {
    private var trainingTimeView: TextView? = null
    private var analyzingView: View? = null
    private var resultErrorView: View? = null
    private var musicBar: View? = null
    private var music: BaseObservableProperty<Pair<Int, AudioPlayer.AudioSource>>? = null

    @ExperimentalUnsignedTypes
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            id = R.id.screen_widget_id_root_container
            backgroundResource = R.drawable.bg_ai_concentration_assistant_training

            // 이전으로 버튼
            imageButton {
                id = R.id.screen_widget_id_01
                backgroundResource = R.drawable.selector_return_back_wt_2
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(42.7f)
                topToTop = PARENT_ID
                topMargin = dip(34.0f)
            }

            // 타이틀
            textView(R.string.ai_concentration_assistant_training_screen_010) {
                id = R.id.screen_widget_id_06
                typeface = Font.kopubDotumBold
                textSize = 26.7f
                textColorResource = R.color.x_ffffff
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            headsetStateVisualizer(icon = R.drawable.ic_headset_wave) {
                id = R.id.screen_widget_id_07
                bindVisualized(owner, Bindable.isHeadsetStateVisualized)
            }.lparams(matchParent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(134.7f)
            }

            view {
                id = R.id.screen_widget_id_20
                backgroundResource = R.drawable.shape_prog_ai_mode_bg
            }.lparams(dip(141.3f), dip(141.3f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_07
                topMargin = dip(38.0f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 집중도우미 진행 프로그래스 ----------------------------------------
            progressBar {
                id = R.id.screen_widget_id_15
                indeterminateDrawable = CircularProgressDrawable(context)
                    .apply {
                        strokeWidth = dip(6.0f).toFloat()
                        setColorSchemeColors(
                            ContextCompat.getColor(context, R.color.x_009de6),
                            ContextCompat.getColor(context, R.color.x_4d5eb8),
                            ContextCompat.getColor(context, R.color.x_5867c1)
                        )
                    }
                isVisible = false
                bindVisible(this@AIConcentrationAssistantTrainingScreen, isTraining)
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = R.id.screen_widget_id_20
                topToTop = R.id.screen_widget_id_20
                endToEnd = R.id.screen_widget_id_20
                bottomToBottom = R.id.screen_widget_id_20
            }

            imageView(R.drawable.level_list_prog) {
                id = R.id.screen_widget_id_16
                scaleType = ImageView.ScaleType.FIT_CENTER
                bindImageLevel(this@AIConcentrationAssistantTrainingScreen, soundType)
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_20
                topToTop = R.id.screen_widget_id_20
                endToEnd = R.id.screen_widget_id_20
                bottomToBottom = R.id.screen_widget_id_20
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }
            //------------------------------------------------------------------

            trainingTimeView = textView("00:00:00") {
                id = R.id.screen_widget_id_04
                typeface = Font.latoRegular
                textSize = 73.3f
                textColorResource = R.color.x_ffffff
                lines = 1
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_20
                topToBottom = R.id.screen_widget_id_20
//                topMargin = dip(48.7f)
                topMargin = dip(29.7f)
                endToEnd = R.id.screen_widget_id_20
                horizontalBias = 0.5f
            }


            // 집중도우미 시작/종료
            imageView {
                id = R.id.screen_widget_id_05
                backgroundResource = R.drawable.selector_ai_concentration_assistant_training_onoff
                bindActivated(this@AIConcentrationAssistantTrainingScreen, isTraining)
                onDebounceClick {
                    when (isActivated) {
                        true  -> requireStopWithValidTraining { isStopped ->
                            if (isStopped) {
                                stopTraining()
                            }
                        }
                        false -> checkHeadsetRequirements {
                            startTraining()
                        }
                    }
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_20
                topToBottom = R.id.screen_widget_id_20
                topMargin = dip(178.0f)
                endToEnd = R.id.screen_widget_id_20
                horizontalBias = 0.5f
            }

            // AI MODE ---------------------------------------------------------

            checkBox {
                id = R.id.screen_widget_id_08
                backgroundResource = R.drawable.selector_ai_mode
                buttonDrawable = null
                isChecked = isAIModeEnabled.value
                onCheckedChange { _, isChecked ->
                    isAIModeEnabled.value = isChecked
                    when {
                        isChecked -> {
                            soundType.value = (parent as View).find<RadioGroup>(R.id.screen_widget_id_09)
                                .checkedRadioButtonId
                                .let { id ->
                                    when (id) {
                                        R.id.screen_widget_id_10 -> SOUND_TYPE_NORMAL
                                        R.id.screen_widget_id_11 -> SOUND_TYPE_NATURE
                                        else                     -> SOUND_TYPE_BEAT
                                    }
                                }
                            openMusicBar()
                        }
                        else      -> {
                            soundType.value = SOUND_TYPE_OFF
                            closeMusicBar()
                        }
                    }
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_07
                topToBottom = R.id.screen_widget_id_07
                topMargin = dip(141.3f)
            }

            // 훈련 가이드 안내 팝업 버튼
            imageButton(R.drawable.selector_ai_concentration_assistant_training_guide) {
                id = R.id.screen_widget_id_18
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(4.7f))
                onDebounceClick {
                    openTrainingGuidePopup()
                }
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_widget_id_08
//                marginStart = dip(4.7f)
                topToTop = R.id.screen_widget_id_08
                bottomToTop = R.id.screen_widget_id_08
            }


            radioGroup {
                id = R.id.screen_widget_id_09
                orientation = LinearLayout.VERTICAL
                gravity = Gravity.CENTER
                // 일반음
                radioButton {
                    id = R.id.screen_widget_id_10
                    backgroundResource = R.drawable.selector_normal_sound
                    buttonDrawable = null
                    isChecked = true
                    bindEnabled(this@AIConcentrationAssistantTrainingScreen, isAIModeEnabled)
                }.lparams(wrapContent, wrapContent)
                // 자연음
                radioButton {
                    id = R.id.screen_widget_id_11
                    backgroundResource = R.drawable.selector_nature_sound
                    buttonDrawable = null
                    bindEnabled(this@AIConcentrationAssistantTrainingScreen, isAIModeEnabled)
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(10.7f)
                }
                // 비트음
                radioButton {
                    id = R.id.screen_widget_id_12
                    backgroundResource = R.drawable.selector_beat_sound
                    buttonDrawable = null
                    bindEnabled(this@AIConcentrationAssistantTrainingScreen, isAIModeEnabled)
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(10.7f)
                }

                onCheckedChange { _, checkedId ->
                    soundType.value = when (checkedId) {
                        R.id.screen_widget_id_10 -> SOUND_TYPE_NORMAL
                        R.id.screen_widget_id_11 -> SOUND_TYPE_NATURE
                        else                     -> SOUND_TYPE_BEAT
                    }
                    if (isTraining.value) {
                        playMusic()
                    }
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_08
                topToBottom = R.id.screen_widget_id_08
                topMargin = dip(16.7f)
                endToEnd = R.id.screen_widget_id_08
            }

            // 서비스 알고리즘(바이노럴비트) 모니터링 뷰
//            textView {
//                typeface = Font.latoRegular
//                textSize = 16.0f
//                textColor = Color.YELLOW
//                lines = 1
//                includeFontPadding = false
//                gravity = Gravity.END
//                bindText(owner, Bindable.algorithmMonitorLog)
//            }.lparams(wrapContent, wrapContent) {
//                topToTop = PARENT_ID
//                endToEnd = PARENT_ID
//            }

            musicBar = MusicBarViewComponent<AIConcentrationAssistantTrainingScreen>()
                .also {
                    music = it.music
                }
                .createView(AnkoContext.create(requireContext(), this@AIConcentrationAssistantTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_17
                    v.isVisible = false
                    addView(v)
                }

            analyzingView = AnalyzingViewComponent<AIConcentrationAssistantTrainingScreen>(ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@AIConcentrationAssistantTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_13
                    v.isVisible = false
                    addView(v)
                }

            resultErrorView = ResultErrorViewComponent<AIConcentrationAssistantTrainingScreen>(ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING)
                .createView(AnkoContext.create(requireContext(), this@AIConcentrationAssistantTrainingScreen))
                .also { v ->
                    v.id = R.id.screen_widget_id_14
                    v.isVisible = false
                    addView(v)
                }
        }
    }.view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        BinauralBeatGenerator.prepare(
            beatFrequency = 13.0,
            // 음량은 0f ~ 1f 0.25는 기본값
            volume = 0.04f
        )

        AudioPlayer.observePlaybackStatus(this) { state ->
            when (state) {
                is AudioPlayer.PlaybackState.Prepare  -> {
                    music?.value = soundType.value to state.audioSource
                }
                is AudioPlayer.PlaybackState.Start    -> initEffectSounds { }
                is AudioPlayer.PlaybackState.Playing  -> Unit
                is AudioPlayer.PlaybackState.Pause    -> Unit
                is AudioPlayer.PlaybackState.Resume   -> Unit
                is AudioPlayer.PlaybackState.Stop     -> Unit
                is AudioPlayer.PlaybackState.Complete -> Unit
                is AudioPlayer.PlaybackState.Error    -> Unit
                else                                  -> Unit
            }
        }
    }

    @ExperimentalUnsignedTypes
    override fun onBackPressed(): Boolean {
        if(isTraining.value) {
            requireStopWithValidTraining { isStop ->
                if (isStop) stopTraining()
            }
            return true
        }

        if (analyzingView?.isVisible == true) {
            alert(
                requireContext(),
                R.string.ai_concentration_assistant_training_screen_040,
                R.color.x_000000_op60
            )
            return true
        }

        useOmnifitBrain {
            cancelMeasurement()
        }
        stopTraining()
        return super.onBackPressed()
    }

    private val normalSoundContents: List<AudioPlayer.AudioSource>
        get() = ContentHelper
            .getAINormalSoundContents()
            .asSequence()
            .toList()
    private val natureSoundContents: List<AudioPlayer.AudioSource>
        get() = ContentHelper
            .getAINatureSoundContents()
            .asSequence()
            .toList()

    // TEST
//    private val contents: List<AudioPlayer.AudioSource>
//        get() = listOf(
//            AudioPlayer.AudioSource(
//                title = "[테스트 음악] SRG_SLEEP_01",
//                source = R.raw.srg_sleep_01,
//                duration = 30.secondsToMillis(),
//                order = 1
//            ),
//            AudioPlayer.AudioSource(
//                title = "[테스트 음악] SRG_SLEEP_02",
//                source = R.raw.srg_sleep_02,
//                duration = 30.secondsToMillis(),
//                order = 2
//            ),
//            AudioPlayer.AudioSource(
//                title = "[테스트 음악] SRG_SLEEP_03",
//                source = R.raw.srg_sleep_03,
//                duration = 30.secondsToMillis(),
//                order = 3
//            ),
//            AudioPlayer.AudioSource(
//                title = "[테스트 음악] SRG_SLEEP_04",
//                source = R.raw.srg_sleep_04,
//                duration = 30.secondsToMillis(),
//                order = 4
//            )
//        )

    private val isTraining: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    private val isAIModeEnabled: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(true)
    }

    private val soundType: BaseObservableProperty<Int> by lazy {
        BaseObservableProperty(SOUND_TYPE_NORMAL)
    }

    private val isMusicBarOpened: Boolean
        get() = musicBar?.isVisible == true

    private var optionalCumulativeUsageTime: Int = 0

    @ExperimentalUnsignedTypes
    private fun startTraining() {
        isTraining.value = true
        observeTrainingElapsedTimeChange()
        openMusicBar()
    }

    private fun stopTraining() {
        isTraining.value = false
        trainingElapsedTimeFinalizer?.dispose()
        closeMusicBar()
    }

    private fun musicBarTransition() {
        TransitionManager.beginDelayedTransition(
            find(R.id.screen_widget_id_root_container),
            Slide(Gravity.BOTTOM).addTarget(R.id.screen_widget_id_17)
                .addListener(object : Transition.TransitionListener {
                    override fun onTransitionEnd(transition: Transition) {
                        transition.removeListener(this)
                    }

                    override fun onTransitionResume(transition: Transition) {}
                    override fun onTransitionPause(transition: Transition) {}
                    override fun onTransitionCancel(transition: Transition) {}
                    override fun onTransitionStart(transition: Transition) {}
                })
        )
    }

    @ExperimentalUnsignedTypes
    private fun openMusicBar() {
        if (isTraining.value
            && isAIModeEnabled.value
            && !isMusicBarOpened
        ) {
            musicBarTransition()
            musicBar?.isVisible = true
            playMusic()
        }
    }

    private fun closeMusicBar() {
        if (isMusicBarOpened) {
            musicBarTransition()
            musicBar?.isVisible = false
            stopMusic()
        }
    }

    @ExperimentalUnsignedTypes
    private fun playMusic() {
        when (soundType.value) {
            SOUND_TYPE_NORMAL -> normalSoundContents
            SOUND_TYPE_NATURE -> natureSoundContents
            else              -> emptyList()
        }
            .takeIf { sources ->
                sources.isNotEmpty()
            }
            ?.let { sources ->
                AudioPlayer.play(
                    requireContext(),
                    this@AIConcentrationAssistantTrainingScreen,
                    sources,
                    Random.nextInt(0, sources.size),
                    AudioPlayer.PlayMode.RANDOM_PLAY
                )
            }
            ?: keepQuiet()

        if (!BinauralBeatGenerator.isGenerating) {
            Bindable.algorithmMonitorLog.value = "바이노럴비트 발생 시작 --> 바이노럴비트 [13Hz]"
            BinauralBeatGenerator.setBeatFrequency(13.0)
            BinauralBeatGenerator.generate()
        }
    }

    private fun keepQuiet() {
        AudioPlayer.stop()
        music?.value = soundType.value to AudioPlayer.AudioSource(source = Any())
    }

    private fun stopMusic() {
        BinauralBeatGenerator.stop()
        AudioPlayer.stop()
    }

    private var trainingElapsedTimeFinalizer: Disposable? = null
    private var trainingElapsedTime: Long = 0L

    @ExperimentalUnsignedTypes
    private fun observeTrainingElapsedTimeChange() {
        if (trainingElapsedTimeFinalizer == null) {
            trainingElapsedTimeFinalizer = Observable.interval(0L, 1L, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    observeBrainFeedback()
                }
                .doFinally {
                    updateTrainingTime(0L)
                    trainingElapsedTimeFinalizer = null
                }
                .subscribe(
                    { t ->
                        trainingElapsedTime = t
                        if (isMusicBarOpened && t > 0L) {
                            ++optionalCumulativeUsageTime
                        }

                        // 60분이 넘은 경우 종료
                        if(t >= 60.minutesToSeconds()) {
                            // TODO : 음성 메세지가 나오는 중 화면 전환을 하면?
                            effectCalmEndSound()
                            stopTraining()
                            showStopTrainingWhen1HourDialog()
                        }
                        updateTrainingTime(t)
                    },
                    { e ->
                        e.printStackTrace()
                    }
                )
        }
    }

    private fun updateTrainingTime(seconds: Long) {
        trainingTimeView?.text = seconds.secondsToMillis().millisToFormatString(DTP_HH_MM_SS)
    }

    @ExperimentalUnsignedTypes
    private fun observeBrainFeedback() {
        useOmnifitBrain {
            isNotMeasuring {
                startMeasurement(eyesState = Eyes.State.OPENED) {

                    var cumulativeCount = 1
                    var cumulativeMeanConcentrationIndicatorValue = 0.0

                    onStart {
                        Timber.v("[AI집중도우미] --> 측정시작")
                    }
                    onMeasure { m ->
                        Timber.v("[AI집중도우미] --> 측정진행[${m.elapsedTime.secondsToFormatString(DTP_MM_SS)}]")
                        m.result
                            .whenNotEmpty {
                                first()
                                    .takeIf {
                                        it.concentrationIndicatorValue >= 0.0
                                    }
                                    ?.concentrationIndicatorValue
                                    ?.let { v ->
                                        cumulativeMeanConcentrationIndicatorValue = cumulativeMeanConcentrationIndicatorValue.cumulativeAverage(v, cumulativeCount++)
                                        ServiceAlgorithm.exchangeBinauralBeatGeneration(m.elapsedTime, cumulativeMeanConcentrationIndicatorValue)
                                    }
                                    ?: checkHeadsetWeared()
                            }
                    }
                    onFinish {
                        Timber.v("[AI집중도우미] --> 측정종료")
                        runOnUiThread {
                            processingToResult(-1, trainingElapsedTime.toInt(), optionalCumulativeUsageTime, it.result.toMultipleElectroencephalogram())
                        }
                    }
                    onCancel { _, error ->
                        Timber.v("[AI집중도우미] --> 측정취소")
                        when (error) {
                            Error.OMNIFIT_BRAIN_HEADSET_UNLINKED -> {
                                showBrainDisconnectErrorPopup()
                            }
                            else                                 -> Unit
                        }
                    }
                }
            }
        }
    }

    private fun requireValidMeasurement(): Boolean {
        return trainingElapsedTime >= 15.minutesToSeconds()
    }

    /**
     * 인공지능 집중 도우미 종료시 훈련 진행 유효성 확인
     *
     * @param block 확인 후 처리부
     * @return true : 훈련 중지, false : 훈련 지속 - 유효성 여부에 따라 결과 페이지 이동 혹은 시작 페이지 이동
     */
    @ExperimentalUnsignedTypes
    private fun requireStopWithValidTraining(block: (Boolean) -> Unit): Boolean {
        return requireValidMeasurement()
            .also { valid ->
                MaterialDialog(requireContext())
                    .show {
                        hideNavigation()
                        customView(
                            view = BasicPopComponent<MaterialDialog>(
                                context.getString(R.string.ai_concentration_assistant_training_screen_010),
                                when (valid) {
                                    true -> context.getString(R.string.ai_concentration_assistant_training_screen_060)
                                    else -> context.getString(
                                        R.string.ai_concentration_assistant_training_screen_050,
                                        trainingElapsedTime.toInt().secondsToFormatString(DTP_HH_MM_SS)
                                    )
                                },
                                Gravity.CENTER,
                                buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                                    override fun onPositive() {
                                        block(true)
                                        useOmnifitBrain {
                                            when (valid) {
                                                true -> stopMeasurement()
                                                else -> performBackPressed()
                                            }
                                        }
                                        dismiss()
                                    }

                                    override fun onNegative() {
                                        block(false)
                                        dismiss()
                                    }
                                }
                            )
                                .createView(AnkoContext.create(context, this))
                                .also { v ->
                                    (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                                },
                            noVerticalPadding = true
                        )
                        cornerRadius(15.0f)
                        cancelable(false)
                        cancelOnTouchOutside(false)
                        setOnDismissListener {
                        }
                    }
            }
    }

    private fun showStopTrainingWhen1HourDialog() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BasicPopComponent<MaterialDialog>(
                        context.getString(R.string.ai_concentration_assistant_training_screen_010),
                        context.getString(R.string.ai_concentration_assistant_training_screen_150),
                        Gravity.CENTER,
                        false,
                        buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                            override fun onPositive() {
                                dismiss()
                                useOmnifitBrain {
                                    stopMeasurement()
                                }
                            }

                            override fun onNegative() {
                            }
                        }
                    )
                        .createView(AnkoContext.create(context, this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(295.5f), FrameLayout.LayoutParams.WRAP_CONTENT)

                        },
                    noVerticalPadding = true
                )
                cornerRadius(15.0f)
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                }
            }
    }

    private fun checkValidResult(
        result: MultipleElectroencephalogram,
        block: () -> Unit
    ): Boolean {
        return result
            .concentrationIndicatorValues
            .any {
                it > 0.0
            }
            .also { isValid ->
                when {
                    isValid -> block()
                    else    -> showResultErrorView()
                }
            }
    }

    @SuppressLint("CheckResult")
    private fun processingToResult(
        sequence: Int,
        elapsedTime: Int,
        optionalTime: Int,
        result: MultipleElectroencephalogram
    ) {
        checkValidResult(result) {
            showAnalyzingView()
            ServiceResultHelper.createResult(
                context = requireContext(),
                user = currentUser,
                category = ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING,
                contentSequence = sequence,
                elapsedTime = elapsedTime,
                optionalTime = optionalTime,
                result = result
            ) { timeTaken, _result ->
                Timber.d("[AI집중도우미] --> 서비스 결과 생성 ID : ${_result.id}, 소요시간 : ${timeTaken.millisToFormatString(DTP_SS_SSS)}")
                Single.timer(1L, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .bindUntilEvent(this, Lifecycle.Event.ON_DESTROY)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            runOnUiThread {
                                screenTo(ServiceResultScreen(_result), R.anim.slide_hold, R.anim.slide_exit_right)
                            }
                        },
                        { e -> Timber.d(e) }
                    )
            }
        }
    }

    private val blurBackgroundDrawable: BitmapDrawable?
        get() = this.view
            ?.run {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                    .also { bmp ->
                        draw(Canvas(bmp))
                    }
            }
            ?.let { bmp ->
                Dali.create(context)
                    .load(bmp)
                    .blurRadius(25)
                    .concurrent()
                    .skipCache()
                    .get()
            }

    private fun showAnalyzingView() {
        analyzingView?.backgroundDrawable = blurBackgroundDrawable
        analyzingView?.isVisible = true
        analyzingView?.bringToFront()
    }

    private fun showResultErrorView() {
        resultErrorView?.backgroundDrawable = blurBackgroundDrawable
        resultErrorView?.isVisible = true
    }

    private fun showBrainDisconnectErrorPopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BrainDisconnectPopComponent<MaterialDialog>(
                        onConfirm = { screenTo(MainScreen()) }
                    ).createView(AnkoContext.create(requireContext(), this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(514.0f), dip(633.3f))
                        },
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }


    private fun openTrainingGuidePopup() {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                maxWidth(literal = dip(980.0f))
                customView(
                    view = AIConcentrationAssistantTrainingGuidePopComponent<MaterialDialog>()
                        .createView(AnkoContext.create(requireContext(), this))
//                        .also { v ->
//                            (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(977.3f), dip(565.3f))
//                        },
                    ,
                    noVerticalPadding = true
                )
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {}
            }
    }

    private var isEffectSoundLoadCompleted: Boolean = false

    private fun initEffectSounds(onComplete: () -> Unit) {
        if (isEffectSoundLoadCompleted) {
            onComplete()
            return
        }
        effectSound.setOnLoadCompleteListener { _, _, status ->
            isEffectSoundLoadCompleted = status == 0
            if (isEffectSoundLoadCompleted) {
                onComplete()
            }
        }
        effectSoundId
    }

    private val effectSound: SoundPool by lazy {
        SoundPool.Builder()
            .setMaxStreams(1)
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            .build()
    }

    private val effectSoundId: Int by lazy {
        effectSound.load(requireContext(), R.raw.effect_ai_1, 1)
    }

    private var playingEffectSoundId: Int = 0
    private var playingEffectSoundStreamId: Int = 0

    private fun effectCalmEndSound() {
        if (playingEffectSoundId != effectSoundId) {
            playingEffectSoundId = effectSoundId
            playingEffectSoundStreamId = effectSound.play(playingEffectSoundId, 1f, 1f, 0, 0, 1f)
        }
    }

    private fun stopEffectSound() {
        if (playingEffectSoundId != 0) {
            playingEffectSoundId = 0
            effectSound.stop(playingEffectSoundStreamId)
        }
    }

    private fun releaseEffectSound() {
        effectSound.release()
    }
}