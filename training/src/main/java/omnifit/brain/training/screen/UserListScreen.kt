package omnifit.brain.training.screen

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import omnifit.brain.training.*
import omnifit.brain.training.component.SynchronizableDataPopComponent
import omnifit.brain.training.component.UserItemComponent
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.db.model.User
import omnifit.brain.training.http.WebApi
import omnifit.brain.training.util.HangulInitialSoundHelper
import omnifit.brain.training.view.NonSpacingTextView
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.anko.AnkoBindableComponentAdapter
import omnifit.commons.anko.AnkoBindableComponentHolder
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onEditorAction
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import java.util.*
import kotlin.properties.Delegates

/*
    TODO : UserList 의 Header 에 해당 하는 애를 Component 안에 넣고 Type 을 줘서 생성하게 하자.
     메디슨 처럼. 지금처럼 하면 뷰 위치 정하기 매우 어렵다.
 */

class UserListScreen : UIScreen() {

    private var userCount: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            userCountView?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(String.format(getString(R.string.user_list_020), n), Html.FROM_HTML_OPTION_USE_CSS_COLORS)
            }
            else {
                Html.fromHtml(String.format(getString(R.string.user_list_020), n))
            }
        }
    }

    private val userList: MutableList<User> by lazy {
        User.findAll(sortField = User.SortField.REGISTRATION_DATE, isDescending = true).toMutableList().also { list ->
            userCount = list.size
        }
    }

    private val userListAdapter by lazy {
        UserListAdapter(mutableListOf(), this@UserListScreen)
    }

    private var refreshList: ImageButton? = null
    private var refreshProgress: ProgressBar? = null
    private var uploadableCount: TextView? = null
    private var userCountView: NonSpacingTextView? = null
    private var inputSearchNameView: EditText? = null
    private var userListView: RecyclerView? = null
    private var noUserView: NonSpacingTextView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            backgroundResource = R.drawable.shape_bg_main

            // 처음으로
            imageButton(R.drawable.selector_return_back) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    screenTo(LoginMenuScreen())
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(32f)
            }

            // 리스트 가져오기
            refreshList = imageButton(R.drawable.selector_list_back) {
                id = R.id.screen_widget_id_02
                backgroundColor = Color.TRANSPARENT
                onDebounceClick { refreshUserList() }
            }.lparams(wrapContent, wrapContent) {
                endToEnd = PARENT_ID
                marginEnd = dip(34f)
                topToTop = PARENT_ID
                topMargin = dip(32f)
            }
            // 연동 중 프로그래스
            refreshProgress = progressBar {
                indeterminateDrawable = CircularProgressDrawable(context).apply {
                    strokeWidth = dip(2.0f).toFloat()
                    backgroundColor = Color.TRANSPARENT
                    setColorSchemeColors(ContextCompat.getColor(context, R.color.x_2da3f5))
                }
            }.lparams(dip(45.0f), dip(45.0f)) {
                topToTop = R.id.screen_widget_id_02
                startToStart = R.id.screen_widget_id_02
                marginStart = dip(30.5f)
                bottomToBottom = R.id.screen_widget_id_02
            }

            // 타이틀
            textView(R.string.user_list_010) {
                id = R.id.screen_widget_id_03
                includeFontPadding = false
                lines = 1
                textColorResource = R.color.x_000000
                textSize = 39.7f
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_02
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_02
            }

            // 데이터 동기화
            imageButton {
                id = R.id.screen_widget_id_04
                backgroundResource = R.drawable.selector_sync_upload
                onDebounceClick {
                    // TODO : 동기화 처리 해야 함
                    if (Measurement.countByUploadable() > 0) openSynchronizableData()
                    else this@UserListScreen.alert(context, R.string.sync_screen_080, gravity = Gravity.CENTER)
                }
            }.lparams(wrapContent, wrapContent) {
                topToBottom = R.id.screen_widget_id_02
                topMargin = dip(13.0f)
                endToEnd = PARENT_ID
                marginEnd = dip(38.7f)
                bottomToBottom = R.id.screen_widget_id_05
            }
            // 데이터 동기화 목록 갯수 Badge
            uploadableCount = textView {
                backgroundResource = R.drawable.ic_upload_badeg
                setPadding(3, 0, 3, 0)
                gravity = Gravity.CENTER
                includeFontPadding = false
                textColorResource = R.color.x_ffffff
                textSize = 17.3f
                typeface = Font.kopubDotumBold
                if (Measurement.countByUploadable() > 0) bindText(owner, Bindable.uploadableBadgeCount)
                else visibility = View.INVISIBLE
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_04
                marginStart = dip(38.0f)
                topToBottom = R.id.screen_widget_id_02
                topMargin = dip(22.0f)
            }

            // 회원검색
            imageButton {
                id = R.id.screen_widget_id_05
                //backgroundResource = R.drawable.selector_search
                backgroundResource = R.drawable.btn_search
                setPadding(0,0,dip(55), 0)
                onDebounceClick {
                    // TODO : 회원 검색 해야 함. 현재 데이터에는 회원의 이름이 없음.
                    hideSoftKeyboard()
                    hideNavigation()
                    clearFocus()
                    searchUser(inputSearchNameView?.text.toString())
                }
            }.lparams(dip(254.7f), dip(82.7f)) {
                topToBottom = R.id.screen_widget_id_02
                endToStart = R.id.screen_widget_id_04
                marginEnd = dip(20.7f)
            }

            inputSearchNameView = editText {
                backgroundColor = Color.TRANSPARENT
                imeOptions = EditorInfo.IME_ACTION_SEARCH
                inputType = EditorInfo.TYPE_TEXT_VARIATION_PERSON_NAME
                singleLine = true
                includeFontPadding = false
                textColorResource = R.color.x_72777b
                textSize = 26.0f

                addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        s?.run {
                            searchUser(s.toString())
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    }
                })
                // yuumz: EditText에서 검색버튼 클릭시 호출
                onEditorAction { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        hideSoftKeyboard()
                        hideNavigation()
                        clearFocus()
                        searchUser(inputSearchNameView?.text.toString())
                    }
                }
            }.lparams(dip(200.0f), dip(50.0f)) {
                startToStart = R.id.screen_widget_id_05
                marginStart = dip(30.0f)
                topToTop = R.id.screen_widget_id_05
                bottomToBottom = R.id.screen_widget_id_05
            }

            // 총 회원 수
            userCountView = nonSpacingTextView {
                includeFontPadding = false
                textColorResource = R.color.x_9196a5
                textSize = 22.3f
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_06
                marginStart = dip(33.3f)
                bottomToTop = R.id.screen_widget_id_06
                bottomMargin = dip(22.7f)
            }

            // 회원리스트배경
            view {
                id = R.id.screen_widget_id_06
                backgroundResource = R.drawable.layer_list_bg_user_list
            }.lparams(dip(1205.3f), dip(594f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }

            // 리스트 타이틀
            // No.
            textView(R.string.user_list_040) {
                id = R.id.screen_widget_id_07
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_ffffff
                textSize = 30.0f
                typeface = Font.kopubDotumBold
            }.lparams(dip(100.0f), dip(80.0f)) {
                startToStart = R.id.screen_widget_id_06
                marginStart = dip(89.2f)
                topToTop = R.id.screen_widget_id_06
            }
            // 이름
            textView(R.string.user_list_050) {
                id = R.id.screen_widget_id_08
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_ffffff
                textSize = 30.0f
                typeface = Font.kopubDotumBold
            }.lparams(dip(175.0f), dip(80.0f)) {
                startToEnd = R.id.screen_widget_id_07
                marginStart = dip(40.0f)
                topToTop = R.id.screen_widget_id_07
                bottomToBottom = R.id.screen_widget_id_07
            }
            // 성별
            textView(R.string.user_list_060) {
                id = R.id.screen_widget_id_09
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_ffffff
                textSize = 30.0f
                typeface = Font.kopubDotumBold
            }.lparams(dip(100.0f), dip(80.0f)) {
                startToEnd = R.id.screen_widget_id_08
                marginStart = dip(40.0f)
                topToTop = R.id.screen_widget_id_07
                bottomToBottom = R.id.screen_widget_id_07
            }
            // 생년월일
            textView(R.string.user_list_070) {
                id = R.id.screen_widget_id_10
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_ffffff
                textSize = 30.0f
                typeface = Font.kopubDotumBold
            }.lparams(dip(250.0f), dip(80.0f)) {
                startToEnd = R.id.screen_widget_id_09
                marginStart = dip(40.0f)
                topToTop = R.id.screen_widget_id_07
                bottomToBottom = R.id.screen_widget_id_07
            }
            // 핸드폰
            textView(R.string.user_list_080) {
                id = R.id.screen_widget_id_11
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_ffffff
                textSize = 30.0f
                typeface = Font.kopubDotumBold
            }.lparams(dip(250.0f), dip(80.0f)) {
                endToEnd = R.id.screen_widget_id_06
                marginEnd = dip(90.3f)
                topToTop = R.id.screen_widget_id_07
                bottomToBottom = R.id.screen_widget_id_07
            }

            // 리스트
            userListView = recyclerView {
                id = R.id.screen_widget_id_12
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
                    setDrawable(ContextCompat.getDrawable(context, R.drawable.shape_divider_default)!!)
                })
                adapter = userListAdapter
            }.lparams(dip(0.0f), dip(0.0f)) {
                startToStart = R.id.screen_widget_id_06
                topToTop = R.id.screen_widget_id_06
                topMargin = dip(80.0f)
                endToEnd = R.id.screen_widget_id_06
                bottomToBottom = R.id.screen_widget_id_06
                bottomMargin = dip(10.0f)
                verticalBias = 0.0f
            }

            // 회원이 없을 경우
            noUserView = nonSpacingTextView {
                textResource = R.string.user_list_090
                textColorResource = R.color.x_b2bac2
                textSize = 30.7f
                isVisible = userList.isEmpty()
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_06
                topToTop = R.id.screen_widget_id_06
                topMargin = dip(132.0f)
                endToEnd = R.id.screen_widget_id_06
            }

        }.apply {
            updateRefreshList(true)
        }
    }.view

    @ExperimentalUnsignedTypes
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (userList.isEmpty()) {
            true -> noUserView?.isVisible = true
            false -> updateListData(userList)
        }

        initUploadableBadgeCount()
        checkHeadsetLink(context)
    }

    private fun initUploadableBadgeCount() {
        if (Measurement.countByUploadable() > 0) {
            Bindable.uploadableBadgeCount.value = Measurement.countByUploadable().toString()
        }
        else {
            uploadableCount?.visibility = View.INVISIBLE
        }
    }

    private fun refreshUserList() {
        updateRefreshList(false)
        // yuumz: 리스트 다시 가져오기는 서버연동 처리 이후 경우에 따라서 처리 되어야함.
        inputSearchNameView?.takeIf {
            it.text.isNotEmpty()
        }?.text?.clear()

        WebApi.getUsers(requireContext(), this@UserListScreen, onComplete = { users ->
            User.findAll(sortField = User.SortField.REGISTRATION_DATE, isDescending = true).toMutableList().also { user ->
                userList.clear()
                userList.addAll(user)
                updateListData(userList)
            }
        }, onError = { e ->
            // 디비에서 가져오기
            with(userList) {
                updateListData(this)
            }
        })
    }

    private fun searchUser(searchTarget: String) {
        mutableListOf<User>().also { _user ->
            var charText = searchTarget.toLowerCase(Locale.getDefault())
            // yuumz: 검색어가 비었을 경우 전체 리스트를 띄워준다.
            if (charText.isEmpty()) {
                _user.addAll(userList)
            }
            else {
                for (user in userList) {
                    val iniName = HangulInitialSoundHelper.getHangulInitialSound(user.name, charText)
                    if (iniName.indexOf(charText) >= 0) { // 초성검색어가 있으면 해당 데이터 리스트에 추가
                        _user.add(user)
                    }
                    else if (user.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                        _user.add(user)
                    }
                }
            }
            updateListData(_user)
        }
    }

    private fun updateListData(user: MutableList<User>) {
        runOnUiThread {
            when (user.isEmpty()) {
                true -> {
                    noUserView?.isVisible = true
                    userListAdapter.clear()
                }
                false -> {
                    noUserView?.isVisible = false
                    userListAdapter.updateData(user)
                }
            }
            userCount = user.size
            updateRefreshList(true)
        }
    }

    private fun openSynchronizableData() {
        MaterialDialog(requireContext()).show {
            hideNavigation()
            hideSoftKeyboard()
            maxWidth(literal = dip(900.0f))
            customView(view = SynchronizableDataPopComponent<MaterialDialog>().createView(AnkoContext.create(requireContext(), this)).also { v ->
                (v as? ConstraintLayout)?.layoutParams = FrameLayout.LayoutParams(dip(900.0f), FrameLayout.LayoutParams.WRAP_CONTENT)
            }, noVerticalPadding = true)
            cancelable(false)
            cancelOnTouchOutside(false)
            setOnDismissListener {
                initUploadableBadgeCount()
            }
        }
    }

    private fun updateRefreshList(isRefresh: Boolean) {
        refreshList?.isEnabled = isRefresh
        refreshProgress?.isInvisible = isRefresh
    }
}

class UserListAdapter constructor(items: MutableList<User>, owner: UserListScreen) : AnkoBindableComponentAdapter<MutableList<User>, UserListScreen, UserListAdapter.ComponentHolder>(items, owner) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(UserItemComponent(parent.context, owner)) { _, user ->
            owner.currentUser = user
            owner.hideSoftKeyboard()
            owner.hideNavigation()
            owner.screenTo(MainScreen())
        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun updateData(data: MutableList<User>) {
        clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }

    inner class ComponentHolder(component: AnkoBindableComponent<User, UserListScreen>, action: (AnkoBindableComponent<User, UserListScreen>, User) -> Unit) : AnkoBindableComponentHolder<User, UserListScreen>(component) {
        init {
            itemView.onDebounceClick {
                action(component, items[adapterPosition])
            }
        }
    }
}