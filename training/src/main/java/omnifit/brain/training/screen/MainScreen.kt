package omnifit.brain.training.screen

import android.graphics.Color
import android.graphics.Outline
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.res.ResourcesCompat
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.training.*
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.common.DTP_YYYYMMDD
import omnifit.commons.common.DTP_YYYY_MM_DD
import omnifit.commons.common.DTP_YYYY__MM__DD
import omnifit.commons.common.toFormatString
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onLongClick
import org.jetbrains.anko.support.v4.UI

class MainScreen : UIScreen() {

    private var studentName: TextView? = null
    private var studentBirth: TextView? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = UI {
        constraintLayout {
            backgroundResource = R.drawable.bg_main

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_wt_2) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    performBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(36.7f)
                topToTop = PARENT_ID
                topMargin = dip(31.3f)
            }

            // 타이틀
            imageView(R.drawable.ic_logo) {
                id = R.id.screen_widget_id_02
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
            }

            // 밧데리잔량
            constraintLayout {
                id = R.id.screen_widget_id_03
                backgroundResource = R.drawable.shape_r28_op15_000000

                // 헤드셋 이미지
                imageView(R.drawable.ic_headset_wt) {
                    id = R.id.screen_widget_id_04
                }.lparams(wrapContent, wrapContent) {
                    startToStart = PARENT_ID
                    marginStart = dip(25.3f)
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                }
                // 헤드셋 밧데리 잔량
                textView {
                    id = R.id.screen_widget_id_05
                    lines = 1
                    includeFontPadding = false
                    textColorResource = R.color.x_ffffff
                    textSize = 21.3f
                    typeface = Font.latoRegular
                    bindBatteryLevel(owner, Bindable.batteryLevel)
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_04
                    endToEnd = PARENT_ID
                    topToTop = PARENT_ID
                    bottomToBottom = PARENT_ID
                    horizontalBias = 0.3f
                    verticalBias = 0.5f
                }
            }.lparams(dip(137.3f), dip(56.0f)) {
                topToTop = R.id.screen_widget_id_06
                endToStart = R.id.screen_widget_id_06
                marginEnd = dip(28.7f)
                bottomToBottom = R.id.screen_widget_id_06
            }

            // 설정
            imageButton(R.drawable.selector_setting_wt) {
                id = R.id.screen_widget_id_06
                scaleType = ImageView.ScaleType.CENTER
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
//                            openHeadsetSetting()
                    screenTo(SettingScreen())
                }
            }.lparams(wrapContent, wrapContent) {
                endToEnd = PARENT_ID
                marginEnd = dip(36.7f)
                topToTop = R.id.screen_widget_id_01
                bottomToBottom = R.id.screen_widget_id_01
            }

//            // 기준 포인트
            space {
                id = R.id.screen_widget_id_24
            }.lparams(dip(27.3f), dip(27.3f)) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(335.3f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 유저정보
            imageView(R.drawable.ic_profile_bg) {
                id = R.id.screen_widget_id_07
            }.lparams(wrapContent, wrapContent) {
                endToStart = R.id.screen_widget_id_24
                topToTop = R.id.screen_widget_id_25
                bottomToBottom = R.id.screen_widget_id_25
            }
//
//            imageView(R.drawable.shape_r26_7_ffffff) {
//                id = R.id.screen_widget_id_07
//            }.lparams(dip(561.3f), dip(199.3f)) {
//                endToStart = R.id.screen_widget_id_21
//                bottomToTop = R.id.screen_widget_id_21
//            }

            imageView(R.drawable.ic_user) {
                id = R.id.screen_widget_id_08
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_widget_id_07
                marginStart = dip(27.3f)
                topToTop = R.id.screen_widget_id_07
                bottomToBottom = R.id.screen_widget_id_07
            }

            // 이름
            studentName = textView {
                id = R.id.screen_widget_id_10
                typeface = Font.kopubDotumBold
                textSize = 45.3f
                textColorResource = R.color.x_2e2e2e
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_widget_id_08
                topToTop = R.id.screen_widget_id_08
                bottomToBottom = R.id.screen_widget_id_08
            }
            // 님 (1982/03/05)
            studentBirth = textView(
                Spanner().apply {
                    val birth = currentUser.birthday.takeIf {
                        it.isNotEmpty()
                    }?.toFormatString(DTP_YYYYMMDD, DTP_YYYY_MM_DD) ?: "0000.00.00"

                    val gender = when (currentUser.genderSignature) {
                        "F" -> context.getString(R.string.user_list_110)
                        else -> context.getString(R.string.user_list_100)
                    }

                    context.getString(R.string.main_screen_010,birth,gender).split('|').let { strings ->
                        val sp1 = 29
                        val sp2 = 21
                        append(strings[0], Spans.sizeSP(sp1))
                        append(strings[1], Spans.sizeSP(sp2))
                    }
                }.trim()
            ) {
                typeface = Font.kopubDotumMedium
                textSize = 24.0f
                textColorResource = R.color.x_a7a7a7
                lines = 1
                typeface = Font.mediumByLanguage()
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                marginStart = dip(3.0f)
                startToEnd = R.id.screen_widget_id_10
//                bottomToBottom = R.id.screen_widget_id_10
                baselineToBaseline = R.id.screen_widget_id_10
            }

            imageButton {
                id = R.id.screen_widget_id_25
                backgroundResource = R.drawable.selector_ai_concentration_assistant_training_enter
                onDebounceClick {
                    screenTo(AIConcentrationAssistantTrainingScreen())
                }
//                onLongClick {
//                    ServiceResult.findLastCategory(ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING.name)?.let { data ->
//                        screenTo(ServiceResultScreen(data), R.anim.slide_hold, R.anim.slide_exit_right)
//                    }
//                }
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_widget_id_24
                bottomToTop = R.id.screen_widget_id_24
            }

            // 집중력 게임
            constraintLayout {
                id = R.id.screen_widget_id_12
                onDebounceClick {
                    screenTo(ConcentrationTrainingContentListScreen())
                }
//                onLongClick {
//                    ServiceResult.findLastCategory(ServiceCategory.CONCENTRATION_TRAINING.name)?.let { data ->
//                        screenTo(ServiceResultScreen(data), R.anim.slide_hold, R.anim.slide_exit_right)
//                    }
//                }

                imageView {
                    id = R.id.screen_widget_id_13
                    imageResource = R.drawable.img_game
                    clipToOutline = true
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    outlineProvider = object : ViewOutlineProvider() {
                        override fun getOutline(view: View?, outline: Outline?) {
                            outline?.setRoundRect(0, 0, view!!.width, (view.height + 26.7f).toInt(), 26.7f)
                        }
                    }
                }.lparams(matchParent, dip(239)) {
                    topToTop = PARENT_ID
                    startToStart = PARENT_ID
                }

                imageView(R.drawable.selector_start) {
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(104.0f)
                    startToStart = R.id.screen_widget_id_13
                    topToTop = R.id.screen_widget_id_13
                    endToEnd = R.id.screen_widget_id_13
                }

                imageView(R.drawable.shape_r26_7_ffffff_bl_br) {
                    id = R.id.screen_widget_id_14
                }.lparams(matchConstraint, dip(162.0f)) {
                    startToStart = R.id.screen_widget_id_13
                    endToEnd = R.id.screen_widget_id_13
                    topToBottom = R.id.screen_widget_id_13
                }

                imageView(R.drawable.ic_game) {
                    id = R.id.screen_widget_id_26
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = R.id.screen_widget_id_14
                    marginEnd = dip(23.8f)
                    bottomToBottom = R.id.screen_widget_id_14
                    bottomMargin = dip(103.3f)
                }

                nonSpacingTextView(R.string.main_screen_040) {
                    id = R.id.screen_widget_id_15
                    typeface = Font.kopubDotumBold
                    textSize = 28f
                    textColorResource = R.color.x_000000
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_14
                    marginStart = dip(34)
                    topToTop = R.id.screen_widget_id_26
                    topMargin = dip(12f)
                }

                nonSpacingTextView(R.string.main_screen_050) {
                    typeface = Font.kopubDotumMedium
                    textSize = 17.3f
                    textColorResource = R.color.x_2e2e2e_op60
                    setLineSpacing(sp(25.3f).toFloat(),0f)
                    includeFontPadding = false
                }.lparams(0, 0) {
                    setMargins(0,dip(16.7f),dip(16.7f),dip(26))
                    topToBottom = R.id.screen_widget_id_15
                    startToStart = R.id.screen_widget_id_15
                    bottomToBottom = R.id.screen_widget_id_14
                    endToStart = R.id.screen_widget_id_26
                }
            }.lparams(dip(364.7f), dip(401.3f)) {
//                bottomMargin = dip(44.7f)
                topMargin = dip(26.7f)
                marginStart = dip(66)
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_07
//                bottomToBottom = PARENT_ID
            }

            // 힐링 뮤직
            constraintLayout {
                id = R.id.screen_widget_id_16
                onDebounceClick {
                    screenTo(BrainStabilityTrainingContentListScreen())
                }
//                onLongClick {
//                    ServiceResult.findLastCategory(ServiceCategory.BRAIN_STABILITY_TRAINING.name)?.let { data ->
//                        screenTo(ServiceResultScreen(data), R.anim.slide_hold, R.anim.slide_exit_right)
//                    }
//                }

                imageView {
                    id = R.id.screen_widget_id_17
                    imageResource = R.drawable.img_healing
                    outlineProvider = object : ViewOutlineProvider() {
                        override fun getOutline(view: View?, outline: Outline?) {
                            outline?.setRoundRect(0, 0, view!!.width, (view.height + 26.7f).toInt(), 26.7f)
                        }
                    }
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    clipToOutline = true
                }.lparams(matchParent, dip(239.0f)) {
                    topToTop = PARENT_ID
                    startToStart = PARENT_ID
                }

                imageView(R.drawable.selector_start) {
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(104.0f)
                    startToStart = R.id.screen_widget_id_17
                    topToTop = R.id.screen_widget_id_17
                    endToEnd = R.id.screen_widget_id_17
                }

                imageView(R.drawable.shape_r26_7_ffffff_bl_br) {
                    id = R.id.screen_widget_id_18
                }.lparams(matchConstraint, dip(162.0f)) {
                    bottomToBottom = PARENT_ID
                    startToStart = PARENT_ID
                    endToEnd = PARENT_ID
                }

                nonSpacingTextView(R.string.main_screen_060) {
                    id = R.id.screen_widget_id_19
                    typeface = Font.kopubDotumBold
                    textSize = 28f
                    textColorResource = R.color.x_000000
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_18
                    marginStart = dip(34)
                    topToTop = R.id.screen_widget_id_27
                    topMargin = dip(12f)
                }

                nonSpacingTextView(R.string.main_screen_070) {
                    typeface = Font.kopubDotumMedium
                    textSize = 17.3f
                    textColorResource = R.color.x_2e2e2e_op60
                    setLineSpacing(sp(25.3f).toFloat(),0f)
                    includeFontPadding = false
                }.lparams(0, 0) {
                    setMargins(0,dip(16.7f),dip(16.7f),dip(26))
                    topToBottom = R.id.screen_widget_id_19
                    startToStart = R.id.screen_widget_id_19
                    bottomToBottom = R.id.screen_widget_id_18
                    endToStart = R.id.screen_widget_id_27
                }

                imageView(R.drawable.ic_healing) {
                    id = R.id.screen_widget_id_27
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = R.id.screen_widget_id_18
                    marginEnd = dip(23.8f)
                    bottomToBottom = R.id.screen_widget_id_18
                    bottomMargin = dip(103.3f)
                }
            }.lparams(dip(364.7f), dip(401.3f)) {
                marginStart = dip(30.2f)
                topToTop = R.id.screen_widget_id_12
                startToEnd = R.id.screen_widget_id_12
                bottomToBottom = R.id.screen_widget_id_12
            }

            // 뉴로 피드백 명상
            constraintLayout {
                id = R.id.screen_widget_id_20
                onDebounceClick {
                    //screenTo(HealingContentListScreen())
                    screenTo(NeuroFeedbackContentListScreen())
                }
//                onLongClick {
//                    ServiceResult.findLastCategory(ServiceCategory.NEURO_FEEDBACK.name)?.let { data ->
//                        screenTo(ServiceResultScreen(data), R.anim.slide_hold, R.anim.slide_exit_right)
//                    }
//                }

                imageView {
                    id = R.id.screen_widget_id_21
                    imageResource = R.drawable.img_therapy
                    outlineProvider = object : ViewOutlineProvider() {
                        override fun getOutline(view: View?, outline: Outline?) {
                            outline?.setRoundRect(0, 0, view!!.width, (view.height + 26.7f).toInt(), 26.7f)
                        }
                    }
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    clipToOutline = true
                }.lparams(matchParent, dip(239.0f)) {
                    topToTop = PARENT_ID
                    startToStart = PARENT_ID
                }

                imageView(R.drawable.selector_start) {
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(104.0f)
                    startToStart = R.id.screen_widget_id_21
                    topToTop = R.id.screen_widget_id_21
                    endToEnd = R.id.screen_widget_id_21
                }

                imageView(R.drawable.shape_r26_7_ffffff_bl_br) {
                    id = R.id.screen_widget_id_22
                }.lparams(matchConstraint, dip(162.0f)) {
                    bottomToBottom = PARENT_ID
                    startToStart = PARENT_ID
                    endToEnd = PARENT_ID
                }

                nonSpacingTextView(R.string.main_screen_080) {
                    id = R.id.screen_widget_id_23
                    typeface = Font.kopubDotumBold
                    textSize = 28f
                    textColorResource = R.color.x_000000
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_22
                    marginStart = dip(34)
                    topToTop = R.id.screen_widget_id_28
                    topMargin = dip(12f)
                }

                nonSpacingTextView(R.string.main_screen_090) {
                    typeface = Font.kopubDotumMedium
                    textSize = 17.3f
                    textColorResource = R.color.x_2e2e2e_op60
                    setLineSpacing(sp(25.3f).toFloat(),0f)
                    includeFontPadding = false
                }.lparams(0, 0) {
                    setMargins(0,dip(16.7f),dip(16.7f),dip(26))
                    topToBottom = R.id.screen_widget_id_23
                    startToStart = R.id.screen_widget_id_23
                    bottomToBottom = R.id.screen_widget_id_22
                    endToStart = R.id.screen_widget_id_28
                }

                imageView(R.drawable.ic_therapy) {
                    id = R.id.screen_widget_id_28
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = R.id.screen_widget_id_22
                    marginEnd = dip(23.8f)
                    bottomToBottom = R.id.screen_widget_id_22
                    bottomMargin = dip(103.3f)

                }
            }.lparams(dip(364.7f), dip(401.3f)) {
                marginStart = dip(30.2f)
                topToTop = R.id.screen_widget_id_12
                startToEnd = R.id.screen_widget_id_16
                bottomToBottom = R.id.screen_widget_id_12
            }
        }.apply {
            updateStudent()
        }
    }.view

    @ExperimentalUnsignedTypes
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkHeadsetLink(context)
    }

    override fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    @Suppress("CheckResult")
    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
    }

    private fun updateStudent() {
        studentName?.text = if (currentUser.name.isEmpty()) getString(R.string.app_name) else currentUser.name
    }
}