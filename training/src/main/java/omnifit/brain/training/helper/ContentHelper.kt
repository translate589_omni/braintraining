package omnifit.brain.training.helper

import android.content.ContentResolver
import android.content.Context
import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.util.SparseArray
import com.squareup.moshi.Json
import omnifit.brain.training.BuildConfig
import omnifit.brain.training.R
import omnifit.brain.training.db.Content
import omnifit.brain.training.db.model.GameContent
import omnifit.brain.training.db.model.MusicContent
import omnifit.brain.training.db.model.MusicContent.Companion.SIGNATURE_CATEGORY_NEURO_FEED_BACK_SOUND
import omnifit.brain.training.db.model.MusicContent.Companion.SIGNATURE_CATEGORY_NORMAL_SOUND
import omnifit.brain.training.db.model.MusicContent.Companion.TYPE_AI_NATURE_SOUND
import omnifit.brain.training.db.model.MusicContent.Companion.TYPE_AI_NORMAL_SOUND
import omnifit.brain.training.util.encryptAes256
import omnifit.commons.audioplayer.AudioPlayer
import org.apache.commons.io.FilenameUtils
import java.io.InputStream

object ContentHelper {

    private val AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE: SparseArray<SparseArray<AudioPlayer.AudioSource>> = SparseArray()
    private val BRAIN_STABILITY_TRAINING_CONTENT_CACHE: SparseArray<AudioPlayer.AudioSource> = SparseArray()
    private val NEURO_FEED_BACK_CONTENT_CACHE: SparseArray<AudioPlayer.AudioSource> = SparseArray()

    @Suppress("CheckResult")
    fun initLoad(
        context: Context,
        onLoad: (Int) -> Unit
    ) {
        clearCaches()

        try {
            // 두뇌 안정 훈련
            MusicContent.findNormalSound()
                .map { mc ->
                    BRAIN_STABILITY_TRAINING_CONTENT_CACHE
                        .put(mc.sequence, mc.createAudioSource(context))
                }

            MusicContent.findNeuroFeedbackSound()
                .map { mc ->
                    NEURO_FEED_BACK_CONTENT_CACHE
                        .put(mc.sequence, mc.createAudioSource(context))
                }
                .run {
                    NEURO_FEED_BACK_CONTENT_CACHE.put(
                        Int.MAX_VALUE,
                        AudioPlayer.AudioSource(
                            sequence = Int.MAX_VALUE,
                            thumbnail = getThumbnailWithImageFileName(context,"img_neuro_bg_06.png"),
                            albumTitle = context.getString(R.string.meditation_content_no_sound_01),
                            title = context.getString(R.string.meditation_content_no_sound_02),
                            source = "",
                            duration = 0,
                            order = 0
                        )
                    )
                }

            // 집중 도우미
            MusicContent.findAISound()
                .map { mc ->
                    when (mc.typeSignature) {
                        MusicContent.SIGNATURE_TYPE_AI_NORMAL_SOUND -> {
                            run {
                                AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE[TYPE_AI_NORMAL_SOUND]
                                    ?: AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE
                                        .run {
                                            put(TYPE_AI_NORMAL_SOUND, SparseArray())
                                            get(TYPE_AI_NORMAL_SOUND)
                                        }
                            }
                                .put(mc.sequence, mc.createAudioSource(context))
                        }
                        MusicContent.SIGNATURE_TYPE_AI_NATURE_SOUND -> {
                            run {
                                AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE[TYPE_AI_NATURE_SOUND]
                                    ?: AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE
                                        .run {
                                            put(TYPE_AI_NATURE_SOUND, SparseArray())
                                            get(TYPE_AI_NATURE_SOUND)
                                        }
                            }
                                .put(mc.sequence, mc.createAudioSource(context))
                        }
                    }
                }
        }
        catch (e: Exception) {
            e.printStackTrace()
        }
        finally {
            onLoad(102)
        }
    }

    fun getNormalSoundContents(): SparseArray<AudioPlayer.AudioSource> = BRAIN_STABILITY_TRAINING_CONTENT_CACHE
    fun getNeuroFeedbackSoundContents(): SparseArray<AudioPlayer.AudioSource> = NEURO_FEED_BACK_CONTENT_CACHE
    fun getAINormalSoundContents(): SparseArray<AudioPlayer.AudioSource> = AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE[TYPE_AI_NORMAL_SOUND]
    fun getAINatureSoundContents(): SparseArray<AudioPlayer.AudioSource> = AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE[TYPE_AI_NATURE_SOUND]

    private fun clearCaches() {
        AI_CONCENTRATION_ASSISTANT_CONTENT_CACHE.clear()
        BRAIN_STABILITY_TRAINING_CONTENT_CACHE.clear()
        NEURO_FEED_BACK_CONTENT_CACHE.clear()
    }
}

fun MusicContent.createAudioSource(context: Context): AudioPlayer.AudioSource = AudioPlayer.AudioSource(
    sequence = sequence,
    thumbnail = when(categorySignature) {
        SIGNATURE_CATEGORY_NORMAL_SOUND -> getThumbnail(context)
        SIGNATURE_CATEGORY_NEURO_FEED_BACK_SOUND -> getThumbnailWithImageFileName(context,imageFileName)
        else                                             -> null
    },
    albumTitle = description,
    title = title,
    source = getSourceUri(context),
    duration = duration(context),
    order = order
)

fun GameContent.getSourcePath(): String = "file:///android_asset/game/$sequence/index.html"

fun MusicContent.getSourceUri(context: Context): Uri =
    when(categorySignature) {
        SIGNATURE_CATEGORY_NEURO_FEED_BACK_SOUND -> FilenameUtils.getBaseName("nf_$fileName")
        else -> FilenameUtils.getBaseName(fileName)
    }.let { fileName ->
        Uri.parse("${ContentResolver.SCHEME_ANDROID_RESOURCE}://${BuildConfig.APPLICATION_ID}/raw/${FilenameUtils.getBaseName(fileName)}")
    }

fun MusicContent.duration(context: Context): Long = with(MediaMetadataRetriever()) {
    try {
        setDataSource(context, getSourceUri(context))
        extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
    }
    finally {
        release()
    }
}

fun Content.getThumbnail(context: Context): Drawable = Drawable.createFromStream(imageStreamFromAssets(context), null)

fun Content.imageStreamFromAssets(context: Context): InputStream = context.assets.open(
    "${assetDirectoryName(url.replaceExtensionToPNG())}/${FilenameUtils.getName(url.replaceExtensionToPNG())}"
)

fun getThumbnailWithImageFileName(context: Context, imageFileName: String): Drawable = Drawable.createFromStream(imageStreamFromAssetsWithImageFileName(context,imageFileName), null)

fun imageStreamFromAssetsWithImageFileName(context: Context, imageFileName:String): InputStream = context.assets.open(
    "${assetDirectoryName(imageFileName.replaceExtensionToPNG())}/${FilenameUtils.getName(imageFileName.replaceExtensionToPNG())}"
)

fun String.replaceExtensionToPNG(): String = this.replace(FilenameUtils.getExtension(this), "webp")

private fun assetDirectoryName(filename: String): String {
    return when (FilenameUtils.getExtension(filename)) {
        "mp3" -> "audio"
        "mp4" -> "video"
        else  -> "image"
    }
}


const val SOUND_TYPE_NORMAL: Int = 0
const val SOUND_TYPE_NATURE: Int = 1
const val SOUND_TYPE_BEAT: Int = 2
const val SOUND_TYPE_OFF: Int = 3

data class SignUpData(
    @Json(name = "user_nm") val name:String,
    @Json(name = "gender") val gender:String,
    @Json(name = "birthday") val birthDate:String,
    @Json(name = "phone") val phoneNumber:String,
    @Json(name = "email") val email:String?
) {
    companion object {
        enum class Gender(val code: String) {
            MALE("M"),
            FEMALE("F")
        }

        fun create(
            name:String,
            gender:Gender,
            birthDate:String,
            phoneNumber:String,
            email:String?
        ) = SignUpData(
            name.encryptAes256(),
            gender.code,
            birthDate.encryptAes256(),
            phoneNumber.encryptAes256(),
            email?.encryptAes256()
        )
    }
}