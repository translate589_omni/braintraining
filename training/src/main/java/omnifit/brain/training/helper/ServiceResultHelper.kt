package omnifit.brain.training.helper

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.vicpin.krealmextensions.save
import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.db.model.User
import omnifit.brain.training.http.AI322Vo
import omnifit.brain.training.http.WebApi
import omnifit.commons.common.realm.save
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import timber.log.Timber

object ServiceResultHelper {

    fun createResult(
        context: Context,
        user: User,
        category: ServiceCategory,
        contentSequence: Int,
        elapsedTime: Int,
        optionalTime: Int = 0,
        result: MultipleElectroencephalogram,
        callback: (Long, ServiceResult) -> Unit
    ) {
        doAsync {
            val timeTaken = System.currentTimeMillis()
            Measurement.create(user.id, elapsedTime, result) { m ->
                ServiceResult.create(m.no, user.id, category, contentSequence, elapsedTime, optionalTime, m) { sr ->
                    m.save(
                        {
                            targetSignature = sr.id
                            //isUploaded = category == ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING
                        },
                        { m ->
                            uiThread {
                                callback(System.currentTimeMillis() - timeTaken, sr)
                            }
                            uploadServiceResult(context, sr)
                        }
                    )
                }
            }
        }
    }

    private fun uploadServiceResult(
        context: Context,
        result: ServiceResult
    ) {
        WebApi.uploadServiceResult(
            context,
            context as LifecycleOwner,
            AI322Vo.create(result),
            {
                Timber.e("측정 결과 전송 완료[$it]")
                Measurement.findByNo(result.measurementNo)
                    ?.apply {
                        isUploaded = true
                    }
                    ?.save()
            },
            { Timber.e("측정 결과 전송 실패[$it]") }
        )
    }
}