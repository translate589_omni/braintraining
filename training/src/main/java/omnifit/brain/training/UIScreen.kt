package omnifit.brain.training

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import omnifit.brain.training.component.BasicPopComponent
import omnifit.brain.training.db.model.User
import omnifit.brain.training.screen.SettingScreen
import omnifit.commons.common.whenNot
import omnifit.commons.omnifitbrain.useOmnifitBrain
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.dip
import timber.log.Timber
import kotlin.system.exitProcess

abstract class UIScreen : Fragment(), UIFrame.OnFocusListenable,
    UIFrame.OnPlaybackStatusChangeListener {

    private var screenFrame: UIFrame? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        screenFrame = context as? UIFrame
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (nextAnim == 0) {
            onScreenTransitionEnd(this, enter)
            return super.onCreateAnimation(transit, enter, nextAnim)
        }
        return AnimationUtils.loadAnimation(requireContext(), nextAnim).run {
            view?.let { v ->
                v.setLayerType(View.LAYER_TYPE_HARDWARE, null)
                setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        if (nextAnim == R.anim.slide_enter_right) {
                            ViewCompat.setTranslationZ(v, 1.0f)
                        }
                        onScreenTransitionStart(this@UIScreen, enter)
                    }

                    override fun onAnimationRepeat(animation: Animation) {
                    }

                    override fun onAnimationEnd(animation: Animation) {
                        v.setLayerType(View.LAYER_TYPE_NONE, null)
                        if (nextAnim == R.anim.slide_enter_right) {
                            Handler(Looper.getMainLooper()).post {
                                ViewCompat.setTranslationZ(v, 0.0f)
                            }
                        }
                        onScreenTransitionEnd(this@UIScreen, enter)
                    }
                })
            }
            AnimationSet(true).apply { addAnimation(this@run) }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
        }
    }

    open fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    open fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
    }

    override fun onResume() {
        super.onResume()
        onUserLeave = false
    }

    @Volatile
    var onUserLeave: Boolean = false

    open fun onUserLeaveHint() {
        onUserLeave = true
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    open fun onHomePressed() {

    }

    open fun onRecentAppsPressed() {

    }

    override fun onPlaybackStatusChanged(status: Int, curr: Int, duration: Int) {
    }

    var currentUser: User
        set(value) {
            screenFrame?.currentUser = value
        }
        get() = screenFrame?.currentUser ?: User()

    var isAdminLogin: Boolean
        set(value) {
            screenFrame?.isAdminLogin = value
        }
        get() = screenFrame?.isAdminLogin ?: false

    fun performBackPressed() {
        screenFrame?.onBackPressed()
    }

    fun checkHeadsetRequirements(
        notWearedMessage: (() -> Unit)? = null,
        required: () -> Unit = {}
    ): Boolean {
        return (checkHeadsetLinked() && checkHeadsetWeared(notWearedMessage))
            .also { if (it) required() }
    }

    fun checkHeadsetLinked(): Boolean {
        return useOmnifitBrain {
            isNotLinked {
                screenTo(SettingScreen())
            }.not()
        }
    }

    fun checkHeadsetWeared(notWearedMessage: (() -> Unit)? = null): Boolean {
        return useOmnifitBrain {
            isNotWeared {
                notWearedMessage
                    ?.invoke()
                    ?: alert(requireContext(), R.string.notifications_020, R.color.x_000000_op60)
            }.not()
        }
    }

    protected fun appFinish(): Boolean {
        useOmnifitBrain {
            stopLink()
        }

        requireActivity().run {
                (application as? GeniusApplication)?.close()
                finishAndRemoveTask()
            }

        exitProcess(0)
    }

    fun hideNavigation() {
        screenFrame?.hideNavigation()
    }

    fun hideSoftKeyboard() {
        screenFrame?.hideSoftKeyboard()
    }

    fun alert(
        context: Context,
        messageRes: Int,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = screenFrame?.alert(context, messageRes, colorRes, gravity)

    fun alert(
        context: Context,
        message: String,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = screenFrame?.alert(context, message, colorRes, gravity)

    protected val fadeIn: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
    }

    protected val fadeOut: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
    }

    @ExperimentalUnsignedTypes
    protected fun checkHeadsetLink(context: Context?) {
        activity?.useOmnifitBrain {
            Bindable.handlingHeadset.value = getMostRecentlyUsedHeadset()
            Timber.v("[GENIUS ${this@UIScreen::class.java.simpleName}] --> ${Bindable.handlingHeadset.value}")
            isNotLinked {
                isNotLinking {
                    Bindable
                        .handlingHeadset
                        .value
                        .isEmpty
                        .whenNot {
                            Bindable.isHeadsetLinking.value = true
                            startLink(Bindable.handlingHeadset.value) {
                                onSuccess { _headset ->
                                    // TODO : 헤드셋 링크 성공 후 처리가 필요한 경우 여기에...
                                    Bindable.handlingHeadset.value = _headset
                                    Bindable.isHeadsetLinking.value = false
                                }
                                onFailure { _, _ ->
                                    // TODO : 헤드셋 링크 실패 후 처리가 필요한 경우 여기에...
                                    Bindable.isHeadsetLinking.value = false
                                }
                            }
                        }
                }
            }
        }
    }

    fun showBasicPopDialog(
        title:String,
        msg:String,
        onPositiveAction:() -> Unit = {},
        onNegativeAction: () -> Unit = {}
    ) {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BasicPopComponent<MaterialDialog>(
                        title,
                        msg,
                        buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                            override fun onPositive() {
                                onPositiveAction()
                                dismiss()
                            }

                            override fun onNegative() {
                                onNegativeAction()
                                dismiss()
                            }
                        }
                    )
                        .createView(AnkoContext.create(context, this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams =
                                FrameLayout.LayoutParams(
                                    dip(295.5f),
                                    FrameLayout.LayoutParams.WRAP_CONTENT
                                )
                        },
                    noVerticalPadding = true
                )
                cornerRadius(15.0f)
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                }
            }
    }
}