package omnifit.brain.training.db.model

import com.squareup.moshi.Json
import com.vicpin.krealmextensions.count
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import omnifit.commons.common.json.moshi.AllowedEmptyIntField

open class Code(
    @Json(name = "cd") @PrimaryKey open var signature: String = "",
    @Json(name = "cd_nm") open var label: String = "",
    @Json(name = "cd_desc") open var explanation: String = "",
    @Json(name = "cd_sort") @AllowedEmptyIntField open var order: Int = -1
) : RealmObject() {
    open var groupSignature: String = "${signature.replace("""[^a-zA-Z]""".toRegex(), "")}000"

    companion object {
        const val FIELD_NAME_SIGNATURE: String = "signature"

        fun of(signature: String): Code? = queryFirst { equalTo(FIELD_NAME_SIGNATURE, signature) }

        fun isEmpty(): Boolean {
            return count<Code>() == 0L
        }
    }
}