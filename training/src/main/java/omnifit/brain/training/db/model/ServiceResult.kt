package omnifit.brain.training.db.model

import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.queryLast
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import omnifit.brain.training.R
import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.algorithm.ServiceAlgorithm
import omnifit.brain.training.algorithm.toDoubleArray
import omnifit.commons.algorithm.Algorithm
import omnifit.commons.algorithm.Indicator
import omnifit.commons.algorithm.name
import omnifit.commons.common.realm.save
import omnifit.commons.common.secondsToMillis
import timber.log.Timber
import java.util.*

open class ServiceResult(
    @PrimaryKey
    // ID_PREFIX + yyyyMMddHHmmss
    open var id: String = "",
    open var measurementNo: String = "",
    open var begin: Date = Date(),
    open var end: Date = Date(),
    open var userId: String = "",
    open var category: String = "",
    open var contentSequence: Int = -1,
    open var indicatorValues: String = "[]",
    open var score: Int = 0,
    open var resultSignature: String = "",
    open var elapsedTime: Long = 0,
    open var effectiveTime: Long = 0,
    open var effectiveTimeRatio: Int = 0,
    open var optionalTime: Long = 0
) : RealmObject() {
    companion object {
        const val ID_PREFIX_AI_CONCENTRATION_TRAINING: String = "ACT"
        const val ID_PREFIX_CONCENTRATION_TRAINING: String = "CT"
        const val ID_PREFIX_BRAIN_STABILITY_TRAINING: String = "BST"
        const val ID_PREFIX_NEURO_FEEDBACK: String = "NF"
        const val FIELD_NAME_ID: String = "id"
        const val FIELD_NAME_CATEGORY: String = "category"

        fun create(
            _measurementNo: String,
            _userId: String,
            _category: ServiceCategory,
            _contentSequence: Int,
            _elapsedTime: Int,
            _optionalTime: Int = 0,
            _measurement: Measurement,
            cb: (ServiceResult) -> Unit = {}
        ) = ServiceResult()
            .save(
                {
                    id = "${
                    when (_category) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> ID_PREFIX_AI_CONCENTRATION_TRAINING
                        ServiceCategory.CONCENTRATION_TRAINING              -> ID_PREFIX_CONCENTRATION_TRAINING
                        ServiceCategory.BRAIN_STABILITY_TRAINING            -> ID_PREFIX_BRAIN_STABILITY_TRAINING
                        ServiceCategory.NEURO_FEEDBACK                      -> ID_PREFIX_NEURO_FEEDBACK
                    }
                    }${_measurement.id}"
                    measurementNo = _measurementNo
                    begin = _measurement.begin
                    end = _measurement.end
                    userId = _userId
                    category = _category.name
                    contentSequence = _contentSequence
                    indicatorValues = when (_category) {
                        ServiceCategory.NEURO_FEEDBACK -> _measurement.leftRelaxationIndicatorValues
                        ServiceCategory.BRAIN_STABILITY_TRAINING -> _measurement.meanHighBetaIndicatorValues
                        else                           -> _measurement.concentrationIndicatorValues
                    }
                    elapsedTime = _elapsedTime.secondsToMillis()
                    optionalTime = _optionalTime.secondsToMillis()
                    effectiveTime = Algorithm.computeEffectiveTime(indicatorValues.toDoubleArray(), ServiceAlgorithm.EFFECTIVE_TIME_THRESHOLD)
                    Timber.d("==> 측정 총 시간 : $elapsedTime, 집중 시간 : $effectiveTime")
                    effectiveTimeRatio = ServiceAlgorithm.computeEffectiveTimeRatio(effectiveTime, elapsedTime)
                    score = ServiceAlgorithm.convEffectiveTimeRatioToScore(effectiveTimeRatio)
                    resultSignature = Algorithm.indicatorOf<Indicator.Content.Result>(score.toDouble()).name ?: ""
                }, cb
            )

        fun findById(id: String) = queryFirst<ServiceResult> {
            equalTo(FIELD_NAME_ID, id)
        }

        // Translate589 테스트용으로 추가
        fun findLast() = queryLast<ServiceResult>()

        fun findLastCategory(category:String) = queryLast<ServiceResult> {
            equalTo(FIELD_NAME_CATEGORY, category)
        }
    }
}