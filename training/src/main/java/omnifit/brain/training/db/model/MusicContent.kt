package omnifit.brain.training.db.model

import com.squareup.moshi.Json
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import omnifit.brain.training.db.Content
import omnifit.commons.common.json.moshi.BooleanField

open class MusicContent(
    @Json(name = "music_no") @PrimaryKey open var sequence: Int = -1,
    @Json(name = "play_order") open var order: Int = -1,
    @Json(name = "music_title") open var title: String = "",
    @Json(name = "music_desc") open var description: String = "",
    @Json(name = "music_ctg_cd") open var categorySignature: String = "",
    @Json(name = "music_type_cd") open var typeSignature: String = "",
    @Json(name = "music_file_nm") open var fileName: String = "",
    @Json(name = "music_file_type") open var fileTypeSignature: String = "",
    @Json(name = "music_file_size") open var fileSize: Long = 0L,
    @Json(name = "music_url") override var url: String = "",
    @Json(name = "img_file_nm") open var imageFileName: String = "",
    @Json(name = "img_url") open var imageUrl: String = "",
    @Json(name = "use_yn") @BooleanField open var isUsed: Boolean = true
) : RealmObject(), Content {
    companion object {
        const val FIELD_NAME_SEQUENCE: String = "sequence"
        const val FIELD_NAME_ORDER: String = "order"
        const val FIELD_NAME_CATEGORY_SIGNATURE: String = "categorySignature"
        const val FIELD_NAME_TYPE_SIGNATURE: String = "typeSignature"

        const val SIGNATURE_CATEGORY_NORMAL_SOUND: String = "AC001"
        const val SIGNATURE_CATEGORY_AI_SOUND: String = "AC002"
        const val SIGNATURE_CATEGORY_NEURO_FEED_BACK_SOUND: String = "AC003"
        const val SIGNATURE_TYPE_AI_NORMAL_SOUND: String = "GP001"
        const val SIGNATURE_TYPE_AI_NATURE_SOUND: String = "GP002"
        const val SIGNATURE_TYPE_NORMAL_STRESS_SOUND: String = "GP003"
        const val SIGNATURE_TYPE_NEURO_FEED_BACK_SOUND: String = "GP004"

        const val CATEGORY_NORMAL_SOUND: Int = 0
        const val CATEGORY_AI_SOUND: Int = 1
        const val TYPE_AI_NORMAL_SOUND: Int = 0
        const val TYPE_AI_NATURE_SOUND: Int = 1

        fun findBySequence(sequence: Int): MusicContent? = queryFirst {
            equalTo(FIELD_NAME_SEQUENCE, sequence)
        }

        fun findAll(): List<MusicContent> = queryAll()

        fun findNormalSound(): List<MusicContent> = query {
            equalTo(FIELD_NAME_CATEGORY_SIGNATURE, SIGNATURE_CATEGORY_NORMAL_SOUND)
        }

        fun findNeuroFeedbackSound(): List<MusicContent> = query {
            equalTo(FIELD_NAME_CATEGORY_SIGNATURE, SIGNATURE_CATEGORY_NEURO_FEED_BACK_SOUND)
        }

        fun findAISound(): List<MusicContent> = query {
            equalTo(FIELD_NAME_CATEGORY_SIGNATURE, SIGNATURE_CATEGORY_AI_SOUND)
        }

        fun findAINormalSound(): List<MusicContent> = query {
            equalTo(FIELD_NAME_CATEGORY_SIGNATURE, SIGNATURE_CATEGORY_AI_SOUND)
                .and()
                .equalTo(FIELD_NAME_TYPE_SIGNATURE, SIGNATURE_TYPE_AI_NORMAL_SOUND)
        }

        fun findAINatureSound(): List<MusicContent> = query {
            equalTo(FIELD_NAME_CATEGORY_SIGNATURE, SIGNATURE_CATEGORY_AI_SOUND)
                .and()
                .equalTo(FIELD_NAME_TYPE_SIGNATURE, SIGNATURE_TYPE_AI_NATURE_SOUND)
        }
    }
}