package omnifit.brain.training.db.model

import android.content.Context
import androidx.core.content.edit
import com.squareup.moshi.Json
import omnifit.brain.training.requireSharedPreferences
import omnifit.brain.training.util.decryptAes256
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.json.toJson

/**
 * 상담사
 *
 * @property serviceId
 * @property corporationId
 * @property adminAccessToken
 * @property adminRefreshToken
 * @property adminTokenExpire
 * @property id
 * @property name (encrypt)
 * @property corporationName (encrypt)
 * @property userMembershipMethod
 * @property waitMode
 * @property smsMode
 * @property emailMode
 * @property printMode
 * @property serviceCategoryId
 * @property serviceCategoryType
 * @property serviceCategoryMemberType
 * @property serviceCategorySurveyType
 * @property apiUrl
 */
data class Administrator(
    @Json(name = "svc_id") var serviceId: String = "",
    @Json(name = "corp_id") var corporationId: String = "",
    @Json(name = "admin_access_token") var adminAccessToken: String = "",
    @Json(name = "admin_refresh_token") var adminRefreshToken: String = "",
    @Json(name = "admin_token_expire") var adminTokenExpire: String = "",
    @Json(name = "counsel_id") var id: String = "",
    @Json(name = "counsel_nm") var name: String = "",
    @Json(name = "corp_nm") var corporationName: String = "",
    @Json(name = "user_scrb_mthd") var userMembershipMethod: String = "",
    @Json(name = "wait_mode") var waitMode: String = "",
    @Json(name = "sms_mode") var smsMode: String = "",
    @Json(name = "email_mode") var emailMode: String = "",
    @Json(name = "print_mode") var printMode: String = "",
    @Json(name = "svc_ctg_id") var serviceCategoryId: String = "",
    @Json(name = "svc_ctg_type") var serviceCategoryType: String = "",
    @Json(name = "svc_ctg_member") var serviceCategoryMemberType: String = "",
    @Json(name = "svc_ctg_rsch") var serviceCategorySurveyType: String = "",
    @Json(name = "api_url") var apiUrl: String = ""
) {
    var loginId: String = ""
    var loginPw: String = ""

    val decryptName: String get() = name.decryptAes256()
    val decryptCorporationName: String get() = corporationName.decryptAes256()

    fun set(context: Context) {
        toJson().let { json ->
            requireSharedPreferences(context).edit {
                putString(PREF_NAME, json)
            }
        }
    }

    companion object {
        const val PREF_NAME: String = "Administrator"

        fun get(context: Context): Administrator {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    Administrator().fromJson(json)
                }
        }

        fun clear(context: Context) {
            Administrator().set(context)
        }
    }

    override fun toString(): String {
        return "Teacher(" +
                "\nserviceId='$serviceId', " +
                "\ncorporationId='$corporationId', " +
                "\nadminAccessToken='$adminAccessToken', " +
                "\nadminRefreshToken='$adminRefreshToken', " +
                "\nadminTokenExpire='$adminTokenExpire', " +
                "\nid='$id', " +
                "\nname='$decryptName', " +
                "\ncorporationName='$decryptCorporationName', " +
                "\nuserMembershipMethod='$userMembershipMethod', " +
                "\nwaitMode='$waitMode', " +
                "\nsmsMode='$smsMode', " +
                "\nemailMode='$emailMode', " +
                "\nprintMode='$printMode', " +
                "\nserviceCategoryId='$serviceCategoryId', " +
                "\nserviceCategoryType='$serviceCategoryType', " +
                "\nserviceCategoryMemberType='$serviceCategoryMemberType', " +
                "\nserviceCategorySurveyType='$serviceCategorySurveyType', " +
                "\napiUrl='$apiUrl'" +
                ")\n"
    }
}