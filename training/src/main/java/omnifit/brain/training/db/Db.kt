package omnifit.brain.training.db

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.annotations.RealmModule
import omnifit.brain.training.config.AppConfig.Companion.REALM_FILE_NAME
import omnifit.brain.training.config.AppConfig.Companion.REALM_SCHEMA_VERSION

object Db {
    fun init(context: Context) {
        Realm.init(context)
        RealmConfiguration.Builder()
            .name(REALM_FILE_NAME)
            .schemaVersion(REALM_SCHEMA_VERSION)
            .modules(BrainGeniusDbModule())
//            .migration { realm, o, n -> }
            .compactOnLaunch()
            .deleteRealmIfMigrationNeeded()
            .build()
            .run {
                Realm.setDefaultConfiguration(this)
            }
    }
}

@RealmModule(allClasses = true)
class BrainGeniusDbModule