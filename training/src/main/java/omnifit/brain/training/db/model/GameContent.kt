package omnifit.brain.training.db.model

import com.squareup.moshi.Json
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import omnifit.brain.training.db.Content
import omnifit.commons.common.json.moshi.BooleanField

open class GameContent(
    @Json(name = "game_no") @PrimaryKey open var sequence: Int = -1,
    @Json(name = "play_order") open var order: Int = -1,
    @Json(name = "game_title") open var title: String = "",
    @Json(name = "game_desc") open var description: String = "",
    @Json(name = "game_type_cd") open var typeSignature: String = "",
    @Json(name = "game_file_nm") open var fileName: String = "",
    @Json(name = "game_file_type") open var fileTypeSignature: String = "",
    @Json(name = "game_file_size") open var fileSize: Long = 0L,
    @Json(name = "game_url") override var url: String = "",
    @Json(name = "img_file_nm") open var imageFileName: String = "",
    @Json(name = "img_url") open var imageUrl: String = "",
    @Json(name = "use_yn") @BooleanField open var isUsed: Boolean = true
) : RealmObject(), Content {
    companion object {
        const val FIELD_NAME_SEQUENCE: String = "sequence"

        fun findBySequence(sequence: Int): GameContent? = queryFirst {
            equalTo(FIELD_NAME_SEQUENCE, sequence)
        }

        fun findAll(): List<GameContent> = queryAll()
    }
}