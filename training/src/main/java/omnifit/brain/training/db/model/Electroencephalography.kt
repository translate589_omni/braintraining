package omnifit.brain.training.db.model

//open class Electroencephalography(
//    @PrimaryKey
//    open var id: String = "", // yyyyMMddHHmmss
//    open var no: String = UUID.randomUUID().toString(),
//    open var begin: Date = Date(),
//    open var end: Date = Date(),
//    open var studentId: String = "",
//    open var targetSignature: String = "", // 측정데이터의 대상 ID
//    open var leftThetaIndicatorValues: String = "[]",
//    open var meanLeftThetaIndicatorValue: Double = 0.0,
//    open var rightThetaIndicatorValues: String = "[]",
//    open var meanRightThetaIndicatorValue: Double = 0.0,
//    open var meanThetaIndicatorValues: String = "[]",
//    open var meanThetaIndicatorValue: Double = 0.0,
//    open var leftAlphaIndicatorValues: String = "[]",
//    open var meanLeftAlphaIndicatorValue: Double = 0.0,
//    open var rightAlphaIndicatorValues: String = "[]",
//    open var meanRightAlphaIndicatorValue: Double = 0.0,
//    open var meanAlphaIndicatorValues: String = "[]",
//    open var meanAlphaIndicatorValue: Double = 0.0,
//    open var leftLowBetaIndicatorValues: String = "[]",
//    open var meanLeftLowBetaIndicatorValue: Double = 0.0,
//    open var rightLowBetaIndicatorValues: String = "[]",
//    open var meanRightLowBetaIndicatorValue: Double = 0.0,
//    open var meanLowBetaIndicatorValues: String = "[]",
//    open var meanLowBetaIndicatorValue: Double = 0.0,
//    open var leftMiddleBetaIndicatorValues: String = "[]",
//    open var meanLeftMiddleBetaIndicatorValue: Double = 0.0,
//    open var rightMiddleBetaIndicatorValues: String = "[]",
//    open var meanRightMiddleBetaIndicatorValue: Double = 0.0,
//    open var meanMiddleBetaIndicatorValues: String = "[]",
//    open var meanMiddleBetaIndicatorValue: Double = 0.0,
//    open var leftHighBetaIndicatorValues: String = "[]",
//    open var meanLeftHighBetaIndicatorValue: Double = 0.0,
//    open var rightHighBetaIndicatorValues: String = "[]",
//    open var meanRightHighBetaIndicatorValue: Double = 0.0,
//    open var meanHighBetaIndicatorValues: String = "[]",
//    open var meanHighBetaIndicatorValue: Double = 0.0,
//    open var leftGammaIndicatorValues: String = "[]",
//    open var meanLeftGammaIndicatorValue: Double = 0.0,
//    open var rightGammaIndicatorValues: String = "[]",
//    open var meanRightGammaIndicatorValue: Double = 0.0,
//    open var meanGammaIndicatorValues: String = "[]",
//    open var meanGammaIndicatorValue: Double = 0.0,
//    open var concentrationIndicatorValues: String = "[]",
//    open var meanConcentrationIndicatorValue: Double = 0.0,
//    open var leftRelaxationIndicatorValues: String = "[]",
//    open var meanLeftRelaxationIndicatorValue: Double = 0.0,
//    open var rightRelaxationIndicatorValues: String = "[]",
//    open var meanRightRelaxationIndicatorValue: Double = 0.0,
//    open var meanRelaxationIndicatorValues: String = "[]",
//    open var meanRelaxationIndicatorValue: Double = 0.0,
//    open var balanceIndicatorValues: String = "[]",
//    open var meanBalanceIndicatorValue: Double = 0.0,
//    open var leftSef90Hzs: String = "[]",
//    open var meanLeftSef90Hz: Double = 0.0,
//    open var rightSef90Hzs: String = "[]",
//    open var meanRightSef90Hz: Double = 0.0,
//    open var meanSef90Hzs: String = "[]",
//    open var meanSef90HzIndicatorValues: String = "[]",
//    open var meanSef90Hz: Double = 0.0,
//    open var meanLeftPowerSpectrum: String = "[]",
//    open var meanRightPowerSpectrum: String = "[]",
//    open var meanPowerSpectrum: String = "[]",
//    open var meanLeftRhythmDistribution: String = "[]",
//    open var meanRightRhythmDistribution: String = "[]",
//    open var meanRhythmDistribution: String = "[]",
//    open var uploaded: Boolean = false
//) : RealmObject() {
//    companion object {
//        const val FIELD_NAME_ID: String = "id"
//        const val FIELD_NAME_BEGIN_DATE: String = "begin"
//        const val FIELD_NAME_UPLOADED: String = "uploaded"
//
//        fun create(
//            studentId: String,
//            elapsedTime: Int,
//            electroencephalographies: List<ELECTROENCEPHALOGRAPHY>,
//            callback: (Electroencephalography) -> Unit = {}
//        ) = Electroencephalography().save(
//            {
//                DateTime.now().let { end ->
//                    end.minusSeconds(elapsedTime).let { begin ->
//                        this.id = begin.toString(DTP_YYYYMMDDHHMMSS)
//                        this.begin = begin.toDate()
//                        this.end = end.toDate()
//                        this.studentId = studentId
//                    }
//                }
//                with(SparseArray<DoubleArray>()) {
//                    fun ELECTROENCEPHALOGRAPHY.byKey(key: Int): Double {
//                        return when (key) {//@formatter:off
//                            RESULT_ITEM_L_THETA -> leftThetaIndicatorValue
//                            RESULT_ITEM_R_THETA -> rightThetaIndicatorValue
//                            RESULT_ITEM_L_ALPHA -> leftAlphaIndicatorValue
//                            RESULT_ITEM_R_ALPHA -> rightAlphaIndicatorValue
//                            RESULT_ITEM_L_LBETA -> leftLowBetaIndicatorValue
//                            RESULT_ITEM_R_LBETA -> rightLowBetaIndicatorValue
//                            RESULT_ITEM_L_MBETA -> leftMiddleBetaIndicatorValue
//                            RESULT_ITEM_R_MBETA -> rightMiddleBetaIndicatorValue
//                            RESULT_ITEM_L_HBETA -> leftHighBetaIndicatorValue
//                            RESULT_ITEM_R_HBETA -> rightHighBetaIndicatorValue
//                            RESULT_ITEM_L_GAMMA -> leftGammaIndicatorValue
//                            RESULT_ITEM_R_GAMMA -> rightGammaIndicatorValue
//                            RESULT_ITEM_CONCENTRATION -> concentrationIndicatorValue
//                            RESULT_ITEM_L_RELAXATION -> leftRelaxationIndicatorValue
//                            RESULT_ITEM_R_RELAXATION -> rightRelaxationIndicatorValue
//                            RESULT_ITEM_BALANCE -> balanceIndicatorValue
//                            RESULT_ITEM_L_SEF90HZ -> leftSef90Hz
//                            RESULT_ITEM_R_SEF90HZ -> rightSef90Hz
//                            else -> 0.0
//                        }//@formatter:on
//                    }
//                    electroencephalographies.forEachIndexed { index, electroencephalography ->
//                        //@formatter:off
//                        get(RESULT_ITEM_L_THETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_THETA)
//                        ) ?: put(
//                            RESULT_ITEM_L_THETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_THETA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_THETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_THETA)
//                        ) ?: put(
//                            RESULT_ITEM_R_THETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_THETA)
//                                )
//                            })
//                        get(RESULT_ITEM_L_ALPHA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_ALPHA)
//                        ) ?: put(
//                            RESULT_ITEM_L_ALPHA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_ALPHA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_ALPHA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_ALPHA)
//                        ) ?: put(
//                            RESULT_ITEM_R_ALPHA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_ALPHA)
//                                )
//                            })
//                        get(RESULT_ITEM_L_LBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_LBETA)
//                        ) ?: put(
//                            RESULT_ITEM_L_LBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_LBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_LBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_LBETA)
//                        ) ?: put(
//                            RESULT_ITEM_R_LBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_LBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_L_MBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_MBETA)
//                        ) ?: put(
//                            RESULT_ITEM_L_MBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_MBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_MBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_MBETA)
//                        ) ?: put(
//                            RESULT_ITEM_R_MBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_MBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_L_HBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_HBETA)
//                        ) ?: put(
//                            RESULT_ITEM_L_HBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_HBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_HBETA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_HBETA)
//                        ) ?: put(
//                            RESULT_ITEM_R_HBETA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_HBETA)
//                                )
//                            })
//                        get(RESULT_ITEM_L_GAMMA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_GAMMA)
//                        ) ?: put(
//                            RESULT_ITEM_L_GAMMA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_GAMMA)
//                                )
//                            })
//                        get(RESULT_ITEM_R_GAMMA)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_GAMMA)
//                        ) ?: put(
//                            RESULT_ITEM_R_GAMMA,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_GAMMA)
//                                )
//                            })
//                        get(RESULT_ITEM_CONCENTRATION)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_CONCENTRATION)
//                        ) ?: put(
//                            RESULT_ITEM_CONCENTRATION,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_CONCENTRATION)
//                                )
//                            })
//                        get(RESULT_ITEM_L_RELAXATION)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_RELAXATION)
//                        ) ?: put(
//                            RESULT_ITEM_L_RELAXATION,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_RELAXATION)
//                                )
//                            })
//                        get(RESULT_ITEM_R_RELAXATION)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_RELAXATION)
//                        ) ?: put(
//                            RESULT_ITEM_R_RELAXATION,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_RELAXATION)
//                                )
//                            })
//                        get(RESULT_ITEM_BALANCE)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_BALANCE)
//                        ) ?: put(
//                            RESULT_ITEM_BALANCE,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_BALANCE)
//                                )
//                            })
//                        get(RESULT_ITEM_L_SEF90HZ)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_L_SEF90HZ)
//                        ) ?: put(
//                            RESULT_ITEM_L_SEF90HZ,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_L_SEF90HZ)
//                                )
//                            })
//                        get(RESULT_ITEM_R_SEF90HZ)?.set(
//                            index,
//                            electroencephalography.byKey(RESULT_ITEM_R_SEF90HZ)
//                        ) ?: put(
//                            RESULT_ITEM_R_SEF90HZ,
//                            DoubleArray(electroencephalographies.size).apply {
//                                set(
//                                    index,
//                                    electroencephalography.byKey(RESULT_ITEM_R_SEF90HZ)
//                                )
//                            })
//                        //@formatter:on
//                    }
//                    //@formatter:off
//                    leftThetaIndicatorValues = get(RESULT_ITEM_L_THETA, doubleArrayOf()).toJson()
//                    meanLeftThetaIndicatorValue =
//                        get(RESULT_ITEM_L_THETA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightThetaIndicatorValues = get(RESULT_ITEM_R_THETA, doubleArrayOf()).toJson()
//                    meanRightThetaIndicatorValue =
//                        get(RESULT_ITEM_R_THETA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanThetaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_THETA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_THETA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanThetaIndicatorValue =
//                        doubleArrayOf().fromJson(meanThetaIndicatorValues).ignoreNegativeAverage()
//                    leftAlphaIndicatorValues = get(RESULT_ITEM_L_ALPHA, doubleArrayOf()).toJson()
//                    meanLeftAlphaIndicatorValue =
//                        get(RESULT_ITEM_L_ALPHA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightAlphaIndicatorValues = get(RESULT_ITEM_R_ALPHA, doubleArrayOf()).toJson()
//                    meanRightAlphaIndicatorValue =
//                        get(RESULT_ITEM_R_ALPHA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanAlphaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_ALPHA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_ALPHA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanAlphaIndicatorValue =
//                        doubleArrayOf().fromJson(meanAlphaIndicatorValues).ignoreNegativeAverage()
//                    leftLowBetaIndicatorValues = get(RESULT_ITEM_L_LBETA, doubleArrayOf()).toJson()
//                    meanLeftLowBetaIndicatorValue =
//                        get(RESULT_ITEM_L_LBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightLowBetaIndicatorValues = get(RESULT_ITEM_R_LBETA, doubleArrayOf()).toJson()
//                    meanRightLowBetaIndicatorValue =
//                        get(RESULT_ITEM_R_LBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanLowBetaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_LBETA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_LBETA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanLowBetaIndicatorValue =
//                        doubleArrayOf().fromJson(meanLowBetaIndicatorValues).ignoreNegativeAverage()
//                    leftMiddleBetaIndicatorValues =
//                        get(RESULT_ITEM_L_MBETA, doubleArrayOf()).toJson()
//                    meanLeftMiddleBetaIndicatorValue =
//                        get(RESULT_ITEM_L_MBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightMiddleBetaIndicatorValues =
//                        get(RESULT_ITEM_R_MBETA, doubleArrayOf()).toJson()
//                    meanRightMiddleBetaIndicatorValue =
//                        get(RESULT_ITEM_R_MBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanMiddleBetaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_MBETA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_MBETA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanMiddleBetaIndicatorValue =
//                        doubleArrayOf().fromJson(meanMiddleBetaIndicatorValues)
//                            .ignoreNegativeAverage()
//                    leftHighBetaIndicatorValues = get(RESULT_ITEM_L_HBETA, doubleArrayOf()).toJson()
//                    meanLeftHighBetaIndicatorValue =
//                        get(RESULT_ITEM_L_HBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightHighBetaIndicatorValues =
//                        get(RESULT_ITEM_R_HBETA, doubleArrayOf()).toJson()
//                    meanRightHighBetaIndicatorValue =
//                        get(RESULT_ITEM_R_HBETA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanHighBetaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_HBETA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_HBETA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanHighBetaIndicatorValue =
//                        doubleArrayOf().fromJson(meanHighBetaIndicatorValues)
//                            .ignoreNegativeAverage()
//                    leftGammaIndicatorValues = get(RESULT_ITEM_L_GAMMA, doubleArrayOf()).toJson()
//                    meanLeftGammaIndicatorValue =
//                        get(RESULT_ITEM_L_GAMMA, doubleArrayOf()).ignoreNegativeAverage()
//                    rightGammaIndicatorValues = get(RESULT_ITEM_R_GAMMA, doubleArrayOf()).toJson()
//                    meanRightGammaIndicatorValue =
//                        get(RESULT_ITEM_R_GAMMA, doubleArrayOf()).ignoreNegativeAverage()
//                    meanGammaIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_GAMMA, doubleArrayOf()),
//                        get(RESULT_ITEM_R_GAMMA, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanGammaIndicatorValue =
//                        doubleArrayOf().fromJson(meanGammaIndicatorValues).ignoreNegativeAverage()
//                    concentrationIndicatorValues =
//                        get(RESULT_ITEM_CONCENTRATION, doubleArrayOf()).toJson()
//                    meanConcentrationIndicatorValue =
//                        get(RESULT_ITEM_CONCENTRATION, doubleArrayOf()).ignoreNegativeAverage()
//                    leftRelaxationIndicatorValues =
//                        get(RESULT_ITEM_L_RELAXATION, doubleArrayOf()).toJson()
//                    meanLeftRelaxationIndicatorValue =
//                        get(RESULT_ITEM_L_RELAXATION, doubleArrayOf()).ignoreNegativeAverage()
//                    rightRelaxationIndicatorValues =
//                        get(RESULT_ITEM_R_RELAXATION, doubleArrayOf()).toJson()
//                    meanRightRelaxationIndicatorValue =
//                        get(RESULT_ITEM_R_RELAXATION, doubleArrayOf()).ignoreNegativeAverage()
//                    meanRelaxationIndicatorValues = arrayOf(
//                        get(RESULT_ITEM_L_RELAXATION, doubleArrayOf()),
//                        get(RESULT_ITEM_R_RELAXATION, doubleArrayOf())
//                    ).columnAverage().toJson()
//                    meanRelaxationIndicatorValue =
//                        doubleArrayOf().fromJson(meanRelaxationIndicatorValues)
//                            .ignoreNegativeAverage()
//                    balanceIndicatorValues = get(RESULT_ITEM_BALANCE, doubleArrayOf()).toJson()
//                    meanBalanceIndicatorValue =
//                        get(RESULT_ITEM_BALANCE, doubleArrayOf()).ignoreNegativeAverage()
//                    leftSef90Hzs = get(RESULT_ITEM_L_SEF90HZ, doubleArrayOf()).toJson()
//                    rightSef90Hzs = get(RESULT_ITEM_R_SEF90HZ, doubleArrayOf()).toJson()
//                    //@formatter:on
//                }
//                // 신규 좌우뇌균형도 서비스 알고리즘으로 교체
//                electroencephalographies.map {
//                    arrayOf(
//                        it.leftPowerSpectrum,
//                        it.rightPowerSpectrum
//                    ).columnAverage().average().let { avg ->
//                        if (avg > 0.0) BrainBalanceIndicator.calc11piValueFromPowerSpectrum(
//                            it.leftPowerSpectrum,
//                            it.rightPowerSpectrum
//                        )
//                        else avg
//                    }
//                }.let { `11pivs` ->
//                    balanceIndicatorValues = `11pivs`.toDoubleArray().toJson()
//                    meanBalanceIndicatorValue = `11pivs`.toDoubleArray().ignoreNegativeAverage()
//                }
//
//                electroencephalographies.filter { it.leftPowerSpectrum.average() > 0.0 }
//                    .map { it.leftPowerSpectrum }
//                    .toTypedArray()
//                    .columnAverage()
//                    .let { _meanLeftPowerSpectrum ->
//                        meanLeftPowerSpectrum = _meanLeftPowerSpectrum.toJson()
//                        meanLeftSef90Hz = _meanLeftPowerSpectrum.toSef90Hz(true)
//                        meanLeftRhythmDistribution =
//                            _meanLeftPowerSpectrum.toRhythmDistribution().toJson()
//                    }
//
//                electroencephalographies.filter { it.rightPowerSpectrum.average() > 0.0 }
//                    .map { it.rightPowerSpectrum }
//                    .toTypedArray()
//                    .columnAverage()
//                    .let { _meanRightPowerSpectrum ->
//                        meanRightPowerSpectrum = _meanRightPowerSpectrum.toJson()
//                        meanRightSef90Hz = _meanRightPowerSpectrum.toSef90Hz(true)
//                        meanRightRhythmDistribution =
//                            _meanRightPowerSpectrum.toRhythmDistribution().toJson()
//                    }
//
//                electroencephalographies.filter { it.leftPowerSpectrum.average() > 0.0 && it.rightPowerSpectrum.average() > 0.0 }
//                    .map {
//                        arrayOf(
//                            it.leftPowerSpectrum,
//                            it.rightPowerSpectrum
//                        ).columnAverage()
//                    }
//                    .toTypedArray()
//                    .columnAverage()
//                    .let { _meanPowerSpectrum ->
//                        meanPowerSpectrum = _meanPowerSpectrum.toJson()
//                        meanSef90Hz = _meanPowerSpectrum.toSef90Hz(true)
//                        meanRhythmDistribution = _meanPowerSpectrum.toRhythmDistribution().toJson()
//                    }
//
//                electroencephalographies.map {
//                    arrayOf(
//                        it.leftPowerSpectrum,
//                        it.rightPowerSpectrum
//                    ).columnAverage()
//                }.let {
//                    meanSef90Hzs = it.map { meanPowerSpectrum ->
//                        meanPowerSpectrum.average().let { avg ->
//                            if (avg > 0.0) meanPowerSpectrum.toSef90Hz(true)
//                            else avg
//                        }
//                    }.toDoubleArray().toJson()
//                    meanSef90HzIndicatorValues = it.map { meanPowerSpectrum ->
//                        meanPowerSpectrum.average().let { avg ->
//                            if (avg > 0.0) transformSef90HzTo11piValue(
//                                meanPowerSpectrum.toSef90Hz(
//                                    true
//                                )
//                            )
//                            else avg
//                        }
//                    }.toDoubleArray().toJson()
//                }
//            }, callback
//        )
//
//        fun findById(id: String) = queryFirst<Electroencephalography> { equalTo(FIELD_NAME_ID, id) }
//
//        fun findByDate(
//            date: Date,
//            sort: Sort = Sort.DESCENDING
//        ): List<Electroencephalography> {
//            return querySorted(FIELD_NAME_BEGIN_DATE, sort) {
//                beginsWith(FIELD_NAME_ID, date.toLocalDate().toString(DTP_YYYYMMDD))
//            }
//        }
//
//        fun findByDateAsFlowable(
//            date: Date,
//            sort: Sort = Sort.DESCENDING
//        ): Flowable<Electroencephalography> {
//            return querySortedAsSingle<Electroencephalography>(FIELD_NAME_BEGIN_DATE, sort) {
//                beginsWith(FIELD_NAME_ID, date.toLocalDate().toString(DTP_YYYYMMDD))
//            }.subscribeOn(Schedulers.io())
//                .flattenAsFlowable { it }
//        }
//
//        fun findAllByUploadable(sort: Sort = Sort.DESCENDING): List<Electroencephalography> {
//            return querySorted(FIELD_NAME_BEGIN_DATE, sort) {
//                equalTo(FIELD_NAME_UPLOADED, false)
//            }
//        }
//
//        fun findAllByUploadableAsync(
//            sort: Sort = Sort.DESCENDING,
//            callback: (List<Electroencephalography>) -> Unit
//        ) {
//            doAsync {
//                findAllByUploadable(sort).run {
//                    uiThread { callback(this) }
//                }
//            }
//        }
//
//        fun countOfUploadable(): Int {
//            return query<Electroencephalography> { equalTo(FIELD_NAME_UPLOADED, false) }.count()
//        }
//
//        fun findLast(): Electroencephalography? {
//            return queryLast()
//        }
//    }
//
//    /*
//    ╔═══════════════════════════════════════════════════════════════════════════
//    ║
//    ║ ⥥
//    ║
//    ╚═══════════════════════════════════════════════════════════════════════════
//    */
//
//    private fun DoubleArray.ignoreNegativeAverage(): Double {
//        return filter { it.isAvailable() }.average()
//    }
//
//}
//
//typealias ELECTROENCEPHALOGRAPHY = omnifit.sdk.omnifitbrain.Electroencephalography