package omnifit.brain.training.db.model

import com.squareup.moshi.Json
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class GroupCode(
    @Json(name = "cd_grp") @PrimaryKey open var signature: String = "",
    @Json(name = "cd_grp_nm") open var label: String = "",
    @Json(name = "code_arr") open var codeList: RealmList<Code>? = null
) : RealmObject() {
    companion object {
        const val FIELD_NAME_SIGNATURE: String = "signature"

        fun of(signature: String): Code? = queryFirst { equalTo(FIELD_NAME_SIGNATURE, signature) }
    }
}