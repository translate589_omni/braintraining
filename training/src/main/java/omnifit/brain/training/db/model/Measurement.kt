package omnifit.brain.training.db.model

import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.querySorted
import io.realm.RealmObject
import io.realm.Sort
import io.realm.annotations.PrimaryKey
import omnifit.commons.common.DTP_YYYYMMDDHHMMSS
import omnifit.commons.common.json.toJson
import omnifit.commons.common.realm.save
import omnifit.commons.peripheral.common.MultipleElectroencephalogram
import org.jetbrains.anko.doAsync
import org.joda.time.DateTime
import java.util.*

open class Measurement(
    @PrimaryKey
    open var id: String = "", // yyyyMMddHHmmss
    open var no: String = UUID.randomUUID().toString(),
    open var begin: Date = Date(),
    open var end: Date = Date(),
    open var userId: String = "",
    open var targetSignature: String = "", // 측정데이터의 대상 ID
    open var leftThetaIndicatorValues: String = "[]",
    open var meanLeftThetaIndicatorValue: Double = 0.0,
    open var rightThetaIndicatorValues: String = "[]",
    open var meanRightThetaIndicatorValue: Double = 0.0,
    open var meanThetaIndicatorValues: String = "[]",
    open var meanThetaIndicatorValue: Double = 0.0,
    open var leftAlphaIndicatorValues: String = "[]",
    open var meanLeftAlphaIndicatorValue: Double = 0.0,
    open var rightAlphaIndicatorValues: String = "[]",
    open var meanRightAlphaIndicatorValue: Double = 0.0,
    open var meanAlphaIndicatorValues: String = "[]",
    open var meanAlphaIndicatorValue: Double = 0.0,
    open var leftLowBetaIndicatorValues: String = "[]",
    open var meanLeftLowBetaIndicatorValue: Double = 0.0,
    open var rightLowBetaIndicatorValues: String = "[]",
    open var meanRightLowBetaIndicatorValue: Double = 0.0,
    open var meanLowBetaIndicatorValues: String = "[]",
    open var meanLowBetaIndicatorValue: Double = 0.0,
    open var leftMiddleBetaIndicatorValues: String = "[]",
    open var meanLeftMiddleBetaIndicatorValue: Double = 0.0,
    open var rightMiddleBetaIndicatorValues: String = "[]",
    open var meanRightMiddleBetaIndicatorValue: Double = 0.0,
    open var meanMiddleBetaIndicatorValues: String = "[]",
    open var meanMiddleBetaIndicatorValue: Double = 0.0,
    open var leftHighBetaIndicatorValues: String = "[]",
    open var meanLeftHighBetaIndicatorValue: Double = 0.0,
    open var rightHighBetaIndicatorValues: String = "[]",
    open var meanRightHighBetaIndicatorValue: Double = 0.0,
    open var meanHighBetaIndicatorValues: String = "[]",
    open var meanHighBetaIndicatorValue: Double = 0.0,
    open var leftGammaIndicatorValues: String = "[]",
    open var meanLeftGammaIndicatorValue: Double = 0.0,
    open var rightGammaIndicatorValues: String = "[]",
    open var meanRightGammaIndicatorValue: Double = 0.0,
    open var meanGammaIndicatorValues: String = "[]",
    open var meanGammaIndicatorValue: Double = 0.0,
    open var concentrationIndicatorValues: String = "[]",
    open var meanConcentrationIndicatorValue: Double = 0.0,
    open var leftRelaxationIndicatorValues: String = "[]",
    open var meanLeftRelaxationIndicatorValue: Double = 0.0,
    open var rightRelaxationIndicatorValues: String = "[]",
    open var meanRightRelaxationIndicatorValue: Double = 0.0,
    open var meanRelaxationIndicatorValues: String = "[]",
    open var meanRelaxationIndicatorValue: Double = 0.0,
    open var unbalanceIndicatorValues: String = "[]",
    open var meanUnbalanceIndicatorValue: Double = 0.0,
    open var leftPowerSpectrums: String = "[]",
    open var rightPowerSpectrums: String = "[]",
    open var leftSef90Hzs: String = "[]",
    open var meanLeftSef90Hz: Double = 0.0,
    open var rightSef90Hzs: String = "[]",
    open var meanRightSef90Hz: Double = 0.0,
    open var meanSef90Hzs: String = "[]",
    open var meanSef90HzIndicatorValues: String = "[]",
    open var meanSef90Hz: Double = 0.0,
    open var meanLeftPowerSpectrum: String = "[]",
    open var meanRightPowerSpectrum: String = "[]",
    open var meanPowerSpectrum: String = "[]",
    open var meanLeftRhythmDistribution: String = "[]",
    open var meanRightRhythmDistribution: String = "[]",
    open var meanRhythmDistribution: String = "[]",
    open var isUploaded: Boolean = false
) : RealmObject() {
    companion object {
        const val FIELD_NAME_ID: String = "id"
        const val FIELD_NAME_NO: String = "no"
        const val FIELD_NAME_BEGIN_DATE: String = "begin"
        const val FIELD_NAME_IS_UPLOADED: String = "isUploaded"

        fun create(
            userId: String,
            elapsedTime: Int,
            electroencephalogram: MultipleElectroencephalogram,
            callback: (Measurement) -> Unit = {}
        ) = Measurement()
            .save(
                {
                    DateTime.now()
                        .let { end ->
                            end.minusSeconds(elapsedTime)
                                .let { begin ->
                                    this.id = begin.toString(DTP_YYYYMMDDHHMMSS)
                                    this.begin = begin.toDate()
                                    this.end = end.toDate()
                                    this.userId = userId
                                }
                        }
                    leftThetaIndicatorValues = electroencephalogram.leftThetaIndicatorValuesString
                    meanLeftThetaIndicatorValue = electroencephalogram.meanLeftThetaIndicatorValue
                    rightThetaIndicatorValues = electroencephalogram.rightThetaIndicatorValuesString
                    meanRightThetaIndicatorValue = electroencephalogram.meanRightThetaIndicatorValue
                    meanThetaIndicatorValues = electroencephalogram.meanThetaIndicatorValues.toJson()
                    meanThetaIndicatorValue = electroencephalogram.meanThetaIndicatorValue
                    leftAlphaIndicatorValues = electroencephalogram.leftAlphaIndicatorValuesString
                    meanLeftAlphaIndicatorValue = electroencephalogram.meanLeftAlphaIndicatorValue
                    rightAlphaIndicatorValues = electroencephalogram.rightAlphaIndicatorValuesString
                    meanRightAlphaIndicatorValue = electroencephalogram.meanRightAlphaIndicatorValue
                    meanAlphaIndicatorValues = electroencephalogram.meanAlphaIndicatorValues.toJson()
                    meanAlphaIndicatorValue = electroencephalogram.meanAlphaIndicatorValue
                    leftLowBetaIndicatorValues = electroencephalogram.leftLowBetaIndicatorValuesString
                    meanLeftLowBetaIndicatorValue = electroencephalogram.meanLeftLowBetaIndicatorValue
                    rightLowBetaIndicatorValues = electroencephalogram.rightLowBetaIndicatorValuesString
                    meanRightLowBetaIndicatorValue = electroencephalogram.meanRightLowBetaIndicatorValue
                    meanLowBetaIndicatorValues = electroencephalogram.meanLowBetaIndicatorValues.toJson()
                    meanLowBetaIndicatorValue = electroencephalogram.meanLowBetaIndicatorValue
                    leftMiddleBetaIndicatorValues = electroencephalogram.leftMiddleBetaIndicatorValuesString
                    meanLeftMiddleBetaIndicatorValue = electroencephalogram.meanLeftMiddleBetaIndicatorValue
                    rightMiddleBetaIndicatorValues = electroencephalogram.rightMiddleBetaIndicatorValuesString
                    meanRightMiddleBetaIndicatorValue = electroencephalogram.meanRightMiddleBetaIndicatorValue
                    meanMiddleBetaIndicatorValues = electroencephalogram.meanMiddleBetaIndicatorValues.toJson()
                    meanMiddleBetaIndicatorValue = electroencephalogram.meanMiddleBetaIndicatorValue
                    leftHighBetaIndicatorValues = electroencephalogram.leftHighBetaIndicatorValuesString
                    meanLeftHighBetaIndicatorValue = electroencephalogram.meanLeftHighBetaIndicatorValue
                    rightHighBetaIndicatorValues = electroencephalogram.rightHighBetaIndicatorValuesString
                    meanRightHighBetaIndicatorValue = electroencephalogram.meanRightHighBetaIndicatorValue
                    meanHighBetaIndicatorValues = electroencephalogram.meanHighBetaIndicatorValues.toJson()
                    meanHighBetaIndicatorValue = electroencephalogram.meanHighBetaIndicatorValue
                    leftGammaIndicatorValues = electroencephalogram.leftGammaIndicatorValuesString
                    meanLeftGammaIndicatorValue = electroencephalogram.meanLeftGammaIndicatorValue
                    rightGammaIndicatorValues = electroencephalogram.rightGammaIndicatorValuesString
                    meanRightGammaIndicatorValue = electroencephalogram.meanRightGammaIndicatorValue
                    meanGammaIndicatorValues = electroencephalogram.meanGammaIndicatorValues.toJson()
                    meanGammaIndicatorValue = electroencephalogram.meanGammaIndicatorValue
                    concentrationIndicatorValues = electroencephalogram.concentrationIndicatorValuesString
                    meanConcentrationIndicatorValue = electroencephalogram.meanConcentrationIndicatorValue
                    leftRelaxationIndicatorValues = electroencephalogram.leftRelaxationIndicatorValuesString
                    meanLeftRelaxationIndicatorValue = electroencephalogram.meanLeftRelaxationIndicatorValue
                    rightRelaxationIndicatorValues = electroencephalogram.rightRelaxationIndicatorValuesString
                    meanRightRelaxationIndicatorValue = electroencephalogram.meanRightRelaxationIndicatorValue
                    meanRelaxationIndicatorValues = electroencephalogram.meanRelaxationIndicatorValues.toJson()
                    meanRelaxationIndicatorValue = electroencephalogram.meanRelaxationIndicatorValue
                    unbalanceIndicatorValues = electroencephalogram.unbalanceIndicatorOnNonCognitiveValuesString
                    meanUnbalanceIndicatorValue = electroencephalogram.meanUnbalanceIndicatorOnNonCognitiveValue
                    leftPowerSpectrums = electroencephalogram.leftPowerSpectrums.map { it.value }.toTypedArray().toJson()
                    rightPowerSpectrums = electroencephalogram.rightPowerSpectrums.map { it.value }.toTypedArray().toJson()
                },
                callback
            )

        fun findByNo(no: String): Measurement? = queryFirst {
            equalTo(FIELD_NAME_NO, no)
        }

        fun findByUploadable(sort: Sort = Sort.DESCENDING): List<Measurement> = querySorted(FIELD_NAME_BEGIN_DATE, sort) {
            equalTo(FIELD_NAME_IS_UPLOADED, false)
        }

        fun findByUploadableAsync(
            sort: Sort = Sort.DESCENDING,
            block: (List<Measurement>) -> Unit
        ) {
            doAsync {
                findByUploadable(sort)
                    .let(block)
            }
        }

        fun countByUploadable(sort: Sort = Sort.DESCENDING): Int = findByUploadable(sort).count()
    }
}