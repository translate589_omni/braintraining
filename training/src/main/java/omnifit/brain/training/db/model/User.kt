package omnifit.brain.training.db.model

import com.squareup.moshi.Json
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import omnifit.brain.training.util.decryptAes256
import omnifit.commons.common.json.moshi.YyyyMMddHHmmssFormatField
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

open class User(
    @Json(name = "user_id") @PrimaryKey open var id: String = "",
    @Json(name = "user_nm") open var name: String = "",
    @Json(name = "gender") open var genderSignature: String = "",
    @Json(name = "birthday") open var birthday: String = "",
    @Json(name = "phone") open var phone: String = "",
    @Json(name = "local_group") open var citySignature: String = "",
    @Json(name = "job") open var jobSignature: String = "",
    @Json(name = "job_nm") open var workName: String = "",
    @Json(name = "email") open var email: String = "",
    @Json(name = "class_nm") open var className: String = "",
    @Json(name = "level") open var studentLevel: String = "",
    @Json(name = "reg_dt") @YyyyMMddHHmmssFormatField open var registrationDate: Date = Date()
)  : RealmObject() {
    enum class SortField {
        NAME, GENDER, BIRTHDAY, WORK_NAME, CLASS_NAME, STUDENT_LEVEL, REGISTRATION_DATE
    }

    companion object {
        const val FIELD_NAME_ID: String = "id"

        private fun User.decryptFields(): User {
            name = name.decryptAes256()
            birthday = birthday.decryptAes256()
            phone = phone.decryptAes256()
            workName = workName.decryptAes256()
            email = email.decryptAes256()
            return this
        }

        fun findById(id: String?): User? {
            return queryFirst<User> { equalTo(FIELD_NAME_ID, id) }?.decryptFields()
        }

        fun findByIdAsync(
                id: String?,
                callback: (User?) -> Unit
        ) {
            doAsync {
                findById(id).run {
                    uiThread { callback(this) }
                }
            }
        }

        fun findAll(
                sortField: SortField = SortField.NAME,
                isDescending: Boolean = false,
                isDecrypted: Boolean = true
        ): List<User> {
            return query<User> {
            }.filter { it.name.isNotEmpty() }.let {
                if (isDecrypted) {
                    it.map { student ->
                        student.decryptFields()
                    }
                }
                else it
            }.let { students ->
                when (sortField) {
                    SortField.NAME              -> if (isDescending) students.sortedByDescending { student -> student.name } else students.sortedBy { student -> student.name }
                    SortField.GENDER            -> if (isDescending) students.sortedByDescending { student -> student.genderSignature } else students.sortedBy { student -> student.genderSignature }
                    SortField.BIRTHDAY          -> if (isDescending) students.sortedByDescending { student -> student.birthday } else students.sortedBy { student -> student.birthday }
                    SortField.WORK_NAME         -> if (isDescending) students.sortedByDescending { student -> student.workName } else students.sortedBy { student -> student.workName }
                    SortField.CLASS_NAME        -> if (isDescending) students.sortedByDescending { student -> student.className } else students.sortedBy { student -> student.className }
                    SortField.STUDENT_LEVEL     -> if (isDescending) students.sortedByDescending { student -> student.studentLevel } else students.sortedBy { student -> student.studentLevel }
                    SortField.REGISTRATION_DATE -> if (isDescending) students.sortedByDescending { student -> student.registrationDate } else students.sortedBy { student -> student.registrationDate }
                }
            }
        }

        fun findAllClassName(): List<String> {
            return queryAll<User>()
                    .map { student -> student.className }
                    .distinct()
                    .sorted()
        }

        fun findAllClassNameAsync(callback: (List<String>) -> Unit) {
            doAsync {
                findAllClassName().run {
                    uiThread { callback(this) }
                }
            }
        }
    }

    override fun toString(): String {
        return "Student(id='$id', name='$name', genderSignature='$genderSignature', birthday='$birthday', phone='$phone', citySignature='$citySignature', jobSignature='$jobSignature', workName='$workName', email='$email', className='$className', registrationDate=$registrationDate)"
    }

}