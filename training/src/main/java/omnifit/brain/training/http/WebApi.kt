package omnifit.brain.training.http

import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.InternetObservingSettings
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import com.vicpin.krealmextensions.deleteAll
import com.vicpin.krealmextensions.executeTransaction
import com.vicpin.krealmextensions.save
import com.vicpin.krealmextensions.saveAll
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Headers.Companion.toHeaders
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okio.buffer
import okio.sink
import omnifit.brain.training.BuildConfig
import omnifit.brain.training.R
import omnifit.brain.training.db.model.*
import omnifit.brain.training.helper.SignUpData
import omnifit.brain.training.http.WebApiService.Companion.CODE_BASE_CODE_DATA
import omnifit.brain.training.http.WebApiService.Companion.CODE_GAME_CONTENT_LIST_DATA
import omnifit.brain.training.http.WebApiService.Companion.CODE_MUSIC_CONTENT_LIST_DATA
import omnifit.brain.training.http.WebApiService.Companion.CODE_PRIVACY_POLICY_DATA
import omnifit.brain.training.http.WebApiService.Companion.CODE_TERMS_OF_SERVICE_DATA
import omnifit.brain.training.util.encryptAes256
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.json.toJson
import omnifit.commons.http.retrofit.HttpService
import omnifit.commons.http.retrofit.error.HttpExceptionWrapper
import omnifit.commons.omnifitbrain.Headset
import org.jetbrains.anko.doAsync
import retrofit2.HttpException
import timber.log.Timber
import java.io.File
import java.util.*

object WebApi {

    private val BASE_DATA_API: Map<String, Single<*>> by lazy {
        mapOf(
            CODE_BASE_CODE_DATA to requireApiService().postAI002(),
            CODE_PRIVACY_POLICY_DATA to requireApiService().getPrivacyPolicy(),
            CODE_TERMS_OF_SERVICE_DATA to requireApiService().getTermsOfService(),
//            Pair(CODE_PLAYBACK_CONTENT_DATA_YOUTH, requireApiService().getPlaybackContentList()),
//            Pair(CODE_APP_CONTENT_DATA, requireApiService().getAppContentList()),
//            Pair(CODE_GRADE_DATA, requireApiService().getGradeList()),
//            Pair(CODE_NOTICE_DATA, requireApiService().getNoticeList()),
//            Pair(CODE_QNA_DATA, requireApiService().getQnAList())
            CODE_GAME_CONTENT_LIST_DATA to requireApiService().postAI318(),
            CODE_MUSIC_CONTENT_LIST_DATA to requireApiService().postAI320()
        )
    }

    fun init(context: Context) {
        apiService = create(context)
    }

    private const val PARAM_ADMIN_LOGIN_ID: String = "admin_lgin_id"
    private const val PARAM_ADMIN_LOGIN_PW: String = "admin_lgin_pwd"

    /**
     * 상담사 로그인.
     *
     * @param T
     * @param owner lifecycleOwner
     * @param id teacher id
     * @param pw teacher pw (non encrypt)
     * @param onComplete request completed
     * @param onError request error
     */
    @Suppress("CheckResult")
    @ExperimentalUnsignedTypes
    fun <T : LifecycleOwner> loginAdministrator(
        context: Context,
        owner: T,
        id: String,
        pw: String,
        onComplete: (Administrator) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) {
                    requireApiService().postAI026(
                        mapOf(
                            PARAM_ADMIN_LOGIN_ID to id.trim(),
                            PARAM_ADMIN_LOGIN_PW to pw.trim().encryptAes256()
                        )
                    )
                }
                else Single.error { NetworkErrorException() }
            }
            .subscribe(
                { cc ->
                    cc.loginId = id
                    cc.loginPw = pw.trim().encryptAes256()
                    cc.set(context)
                    setXomnifitHeader(context) {
                        serviceId = cc.serviceId
                        corporationId = cc.corporationId
                        adminLoginId = id
                        adminAccessToken = cc.adminAccessToken
                        serviceCategoryId = cc.serviceCategoryId
                        serviceCategoryType = cc.serviceCategoryType
                    }
                    onComplete(cc)
                },
                { e ->
                    when (e) {
                        is HttpException -> onError(HttpExceptionWrapper(e))
                        else             -> onError(e)
                    }
                }
            )
    }

    @Suppress("CheckResult")
    @ExperimentalUnsignedTypes
    fun <T : LifecycleOwner> userSignUp(
        context: Context,
        owner: T,
        signUpData: SignUpData,
        onComplete: (User) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val PARAM_USER_INFO = "user_info"

        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) {
                    requireApiService().postAI201(
                        mutableMapOf(PARAM_USER_INFO to signUpData)
                    )
                }
                else Single.error { NetworkErrorException() }
            }
            .subscribe(
                { r ->
                    /*
                        Translate589 userId, userName, gender, email, regDt 만 내려와서 회원 가입 후
                         userList 화면으로 이동하면 데이터가 없어 앱이 죽음.
                         임시로 user 데이터에 추가함.
                    */
                    executeTransaction {
                        r.birthday = signUpData.birthDate
                        r.phone = signUpData.phoneNumber
                        r.save()
                    }
                    onComplete(r)
                },
                { e ->
                    when (e) {
                        is HttpException -> onError(HttpExceptionWrapper(e))
                        else             -> onError(e)
                    }
                }
            )
    }

    private const val PARAM_OS_TYPE: String = "os_type"

    @Suppress("CheckResult")
    fun <T : LifecycleOwner> requireBaseData(
        context: Context,
        owner: T,
        on: (Triple<Float, ApiConfiguration?, ServiceConfiguration?>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap { isConnected ->
                if (isConnected) requireApiService().postAI001(mapOf(PARAM_OS_TYPE to "OT001"))
                else Single.error { NetworkErrorException() }
            }
            .toFlowable()
            .flatMap { receivedServiceConfiguration ->
                ServiceConfiguration.get(context)
                    .also {
                        Timber.v("Saved service configuration --> $it")
                    }
                    .getApiConfigurationListThatRequireUpdate(receivedServiceConfiguration.apiConfigurationList!!)
                    .let { apiConfigurationListToUpdate ->
                        if (apiConfigurationListToUpdate.isNotEmpty()) {
                            apiConfigurationListToUpdate.mapIndexed { index, apiConfigurationToUpdate ->

                                    fun postProgress(): Triple<Float, ApiConfiguration, ServiceConfiguration> {
                                        return Triple(
                                            ((index + 1).toFloat() / apiConfigurationListToUpdate.size.toFloat() * 100.0f),
                                            apiConfigurationToUpdate,
                                            receivedServiceConfiguration
                                        )
                                    }

                                    BASE_DATA_API[apiConfigurationToUpdate.apiSignature]?.map { rsp ->
                                        when (apiConfigurationToUpdate.apiSignature) {
                                            CODE_BASE_CODE_DATA          -> {
                                                executeTransaction {
                                                    deleteAll<GroupCode>()
                                                    deleteAll<Code>()
                                                    (rsp as AI002).codeCategoryList.flatMap {
                                                        it.groupCodeList.asIterable()
                                                    }.saveAll()
                                                }
                                            }

                                            CODE_PRIVACY_POLICY_DATA -> {
                                                File(context.filesDir, "$CODE_PRIVACY_POLICY_DATA.html").let { f ->
                                                    if (f.exists()) f.delete()
                                                    (rsp as PolicyData).saveToFile(f)
                                                }
                                            }
                                            CODE_TERMS_OF_SERVICE_DATA -> {
                                                File(context.filesDir, "$CODE_TERMS_OF_SERVICE_DATA.html").let { f ->
                                                    if (f.exists()) f.delete()
                                                    (rsp as PolicyData).saveToFile(f)
                                                }
                                            }
                                            CODE_GAME_CONTENT_LIST_DATA  -> {
                                                executeTransaction {
                                                    deleteAll<GameContent>()
                                                    (rsp as AI318)
                                                        .contents
                                                        .saveAll()
                                                }
                                            }
                                            CODE_MUSIC_CONTENT_LIST_DATA -> {
                                                executeTransaction {
                                                    deleteAll<MusicContent>()
                                                    (rsp as AI320)
                                                        .contents
                                                        .saveAll()
                                                }
                                            }
                                            else                         -> Unit
                                        }
                                        postProgress()
                                    }
                                        ?: Single.just(postProgress())
                                            .doOnEvent { (_, apiConfiguration, serviceConfiguration), _ ->
                                                serviceConfiguration
                                                    .apiConfigurationList
                                                    ?.toMutableList()
                                                    ?.apply {
                                                        remove(apiConfiguration)
                                                        serviceConfiguration.apiConfigurationList = toList()
                                                    }
                                            }
                                }
                                .let {
                                    Single.concat(it)
                                }
                        }
                        else Flowable.just(Triple(100.0f, null, receivedServiceConfiguration))
                    }
            }
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { (progress, apiConfiguration, serviceConfiguration) ->
                    on(Triple(progress, apiConfiguration, serviceConfiguration))
                    if (progress == 100.0f) {
                        doAsync {
                            serviceConfiguration.set(context)
                        }
                    }
                },
                { e ->
                    Timber.e(e, "서비스 구성 업데이트 오류 : $e")
                    when (e) {
                        is NetworkErrorException -> {
                            ServiceConfiguration.get(context).apiConfigurationList?.run {
                                on(Triple(100.0f, null, null))
                            } ?: run { onError(UninitializedPropertyAccessException()) }
                        }
                        is HttpException         -> onError(HttpExceptionWrapper(e))
                        else                     -> onError(e)
                    }
                },
                { Timber.d("서비스 구성 업데이트 완료") }
            )
    }

    @SuppressLint("CheckResult")
    fun <O : LifecycleOwner> uploadServiceResult(
        context: Context,
        owner: O,
        param: AI322Vo,
        onComplete: (Map<String, String>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) {
                    requireApiService().postAI322(param)
                }
                else Single.error { NetworkErrorException() }
            }
            .subscribe(onComplete) { e ->
                when (e) {
                    is NoSuchElementException -> onComplete(emptyMap())
                    is HttpException          -> onError(HttpExceptionWrapper(e))
                    else                      -> onError(e)
                }
            }
    }

    @SuppressLint("CheckResult")
    fun <O : LifecycleOwner> uploadBeforeAfterResult(
        context: Context,
        owner: O,
        param: AI322Vo,
        onComplete: (Map<String, String>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) {
                        requireApiService().postAI322(param)
                }
                else Single.error { NetworkErrorException() }
            }
            .subscribe(onComplete) { e ->
                when (e) {
                    is NoSuchElementException -> onComplete(emptyMap())
                    is HttpException          -> onError(HttpExceptionWrapper(e))
                    else                      -> onError(e)
                }
            }
    }


    const val RSP_KEY_MEASUREMENT_ID: String = "KEY_MEASUREMENT_ID"

    /**
     * 서비스(게임, 힐링, 두뇌체크...) 결과 일괄 업로드
     */
    @ExperimentalUnsignedTypes
    @Suppress("CheckResult")
    fun <T : LifecycleOwner> bulkUploadServiceResult(
        context: Context,
        owner: T,
        params: List<Measurement>,
        onUploaded: (Map<String, String>) -> Unit,
        onError: (Throwable) -> Unit,
        onComplete: (List<Measurement>) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) Single.just(params)
                else Single.error { NetworkErrorException() }
            }
            .toFlowable()
            .flatMap {
                Single.concat(
                    params.map { param ->
                        ServiceResult.findById(param.targetSignature)
                            ?.let { r ->
                                requireApiService().postAI322(AI322Vo.create(r))
                                    .onErrorResumeNext { e ->
                                        if (e is HttpException
                                            && e.response().code() == 400
                                            && e.response().message() == XomnifitHeader.ResultCode.ERROR_DUPLICATE_UNIQUE_DATA.signature
                                        ) {
                                            Single.just(mapOf<String, String>().fromJson(e.response().errorBody()?.string() ?: "{}"))
                                        }
                                        else Single.error(e)
                                    }.map {
                                        param
                                            .apply {
                                                isUploaded = true
                                            }
                                            .save()
                                        // 업로드 결과에 해당 아이템 ID 추가
                                        it.plus(RSP_KEY_MEASUREMENT_ID to param.id)
                                    }
                            }
                    }
                )
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                onUploaded,
                { e ->
                    Timber.e(e, "서비스 결과 업로드 실패 : $e")
                    when (e) {
                        is NoSuchElementException -> onUploaded(emptyMap())
                        is HttpException          -> onError(HttpExceptionWrapper(e))
                        else                      -> onError(e)
                    }
                },
                {
                    onComplete(
                        params.filter {
                            it.isUploaded.not()
                        }
                    )
                }
            )
    }

    private const val PARAM_BELONGING_TYPE: String = "type"

    @Suppress("CheckResult")
    @ExperimentalUnsignedTypes
    fun <T : LifecycleOwner> getUsers(
        context: Context,
        owner: T,
        type: String = "R",
        onComplete: (List<User>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        ReactiveNetwork.checkInternetConnectivity(internetObservingSettingsByLocale(context))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .flatMap { isConnected ->
                if (isConnected) {
                    requireApiService().postAI408(mapOf(PARAM_BELONGING_TYPE to type))
                }
                else Single.error { NetworkErrorException() }
            }
            .subscribe(
                { r ->
                    executeTransaction {
                        deleteAll<User>()
                        (r as AI408).userList.saveAll()
                    }
                    onComplete(r.userList)
                },
                { e ->
                    when (e) {
                        is HttpException -> onError(HttpExceptionWrapper(e))
                        else             -> onError(e)
                    }
                }
            )
    }

    private fun internetObservingSettingsByLocale(context: Context): InternetObservingSettings {
        return when (Locale.getDefault().language) {
            // 중국 경우 Google http://clients3.google.com/generate_204 요청을 이용할 수 없는 관계로 내부 요청 이용.
            Locale.CHINA.language -> {
                InternetObservingSettings.builder()
                    .host(context.getString(R.string.no_content_host_url))
                    .port(443)
                    .build()
            }
            else                  -> InternetObservingSettings.create()
        }
    }

    /**
     * HTTP 헤더 정보 세팅
     */
    @ExperimentalUnsignedTypes
    inline fun setXomnifitHeader(
        context: Context,
        set: XomnifitHeader.() -> Unit
    ) {
        HttpService.getHeader(context, XomnifitHeader.PROPERTY_NAME)
            .let { header ->
                XomnifitHeader().fromJson(header)
            }
            .apply {
                set()
            }
            .toJson()
            .let { header ->
                HttpService.setHeader(context, Pair(XomnifitHeader.PROPERTY_NAME, header))
            }
    }

    private var apiService: WebApiService? = null

    @ExperimentalUnsignedTypes
    private fun create(context: Context): WebApiService {
        return if (HttpService.getHeaders(context).isEmpty()) {
            mutableMapOf(
                "Accept-Language" to "${Locale.getDefault().language}-${Locale.getDefault().country}",
                XomnifitHeader.PROPERTY_NAME to XomnifitHeader().toJson()
            )
        }
        else {
            setXomnifitHeader(context) {
                appVersion = BuildConfig.VERSION_NAME
                headsetSerialNo = Headset.get(context).serialNo
            }
            HttpService.getHeaders(context).apply {
                set("Accept-Language", "${Locale.getDefault().language}-${Locale.getDefault().country}")
            }
        }.let { headers ->
            HttpService.of(
                context = context,
                baseUrl = if (BuildConfig.DEBUG) context.getString(R.string.api_dev_host_url) else context.getString(R.string.api_host_url),
                headers = headers,
                requestInterceptor = Interceptor { chain ->
                    chain.proceed(
                        chain.request()
                            .also { request ->
                                request.url.encodedPath.let { path ->
                                    setXomnifitHeader(context) {
                                        apiSignature = path.substring(path.lastIndexOf('/') + 1)
                                    }
                                }
                            }
                            .newBuilder()
                            .headers(HttpService.getHeaders(context).toHeaders())
                            .build()
                    ).let { response ->
                        mapOf<String, String>().fromJson(response.header(XomnifitHeader.PROPERTY_NAME) ?: "{}")
                            .let { header ->
                                header["result_code"]
                                    ?.let { resultCode ->
                                        if (resultCode != XomnifitHeader.ResultCode.SUCCESS.signature) {
                                            Response.Builder()
                                                .code(400)
                                                .message(resultCode)
                                                .protocol(response.protocol)
                                                .request(response.request)
                                                .body(response.body)
                                                .build()
                                        }
                                        else response
                                    } ?: response
                            }
                    }
                }
            )
        }
    }

    private fun requireApiService(): WebApiService {
        return requireNotNull(apiService)
    }

    private fun <T : ResponseBody> T.saveToFile(path: File) = path.sink().buffer().use { buffer -> buffer.writeAll(source()) }
    private fun <T : PolicyData> T.saveToFile(path: File) = path.sink().buffer().use { buffer -> buffer.writeUtf8(content) }
}