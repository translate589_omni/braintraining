package omnifit.brain.training.http

import android.content.Context
import android.os.Build
import androidx.core.content.edit
import com.github.debop.kodatimes.toDateTime
import com.squareup.moshi.Json
import io.reactivex.Single
import omnifit.brain.training.*
import omnifit.brain.training.algorithm.toDoubleArray
import omnifit.brain.training.data.ComparisionData
import omnifit.brain.training.db.model.*
import omnifit.brain.training.helper.SignUpData
import omnifit.commons.algorithm.Algorithm
import omnifit.commons.algorithm.Indicator
import omnifit.commons.algorithm.code
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.json.moshi.YyyyMMddHHmmssFormatField
import omnifit.commons.common.json.toJson
import omnifit.commons.common.millisToSeconds
import omnifit.commons.common.roundToDecimal
import retrofit2.http.Body
import retrofit2.http.POST
import timber.log.Timber
import java.util.*

interface WebApiService {
    companion object {
        const val BASIC_UPDATE_DATE = "PT001"
        const val PRO_UPDATE_DATE = "PT002"
        const val MCB_GENIUS_UPDATE_DATE = "PT003"
        const val BRAIN_GENIUS_UPDATE_DATE = "PT004"

        const val CODE_SERVICE_CONFIGURATION_DATA: String = "DA000"  // 서비스 정보
        const val CODE_BASE_CODE_DATA: String = "DA001"              // 데이터 기반 코드
        const val CODE_PRIVACY_POLICY_DATA: String = "DA002"         // 개인정보 취급방침
        const val CODE_TERMS_OF_SERVICE_DATA: String = "DA003"       // 서비스 이용약관
        const val CODE_SURVEY_DATA: String = "DA004"                 // 설문
        const val CODE_WAIT_MODE_DATA: String = "DA005"              // 대기 모드
        const val CODE_MEASUREMENT_STATISTICS_DATA: String = "DA006" // 측정 평균 통계
        const val CODE_GAME_CONTENT_LIST_DATA: String = "DA007"      // 게임 목록
        const val CODE_MUSIC_CONTENT_LIST_DATA: String = "DA008"     // 음원 목록

        private val IGNORE_DATA_CODES: Set<String> = setOf(
//            CODE_PRIVACY_POLICY_DATA,
//            CODE_TERMS_OF_SERVICE_DATA,
            CODE_SURVEY_DATA,
            CODE_WAIT_MODE_DATA,
            CODE_MEASUREMENT_STATISTICS_DATA
        )

        fun isIgnorableApi(apiCodeText: String): Boolean {
            return IGNORE_DATA_CODES.contains(apiCodeText)
        }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ POST
    ║
    ╚═══════════════════════════════════════════════════════════════════════════════
    */

    @POST("/api/AI026")
    fun postAI026(@Body param: Map<String, String>): Single<Administrator>

    @POST("/api/AI001")
    fun postAI001(@Body param: Map<String, String>): Single<ServiceConfiguration>

    @POST("/api/AI002")
    fun postAI002(): Single<AI002>

    @POST("/api/AI007")
    fun getPrivacyPolicy(): Single<PolicyData>

    @POST("/api/AI008")
    fun getTermsOfService(): Single<PolicyData>

    @POST("/api/AI201")
    fun postAI201(@Body param: Map<String, SignUpData>): Single<User>

    @POST("/api/AI306")
    fun postAI306(@Body param: AI306Vo): Single<Map<String, String>>

    @POST("/api/AI318")
    fun postAI318(): Single<AI318>

    @POST("/api/AI322")
    fun postAI322(@Body param: AI322Vo): Single<Map<String, String>>

    @POST("/api/AI320")
    fun postAI320(): Single<AI320>

    @POST("/api/AI408")
    fun postAI408(@Body param: Map<String, String>): Single<AI408>
}

@ExperimentalUnsignedTypes
data class XomnifitHeader(
    @Json(name = "api_code") var apiSignature: String? = null,
    @Json(name = "svc_id") var serviceId: String? = null,
    @Json(name = "corp_id") var corporationId: String? = null,
    @Json(name = "admin_lgin_id") var adminLoginId: String? = null,
    @Json(name = "admin_access_token") var adminAccessToken: String? = null,
    @Json(name = "user_lgin_id") var userLoginId: String? = null,
    @Json(name = "access_token") var accessToken: String? = null,
    @Json(name = "phone_mac_addr") var phoneMacAddress: String = wifiMacAddress(),
    @Json(name = "phone_model") var phoneModel: String = Build.MODEL,
    @Json(name = "os_version") var appOsVersion: String = Build.VERSION.RELEASE,
    @Json(name = "app_package_name") var appPackageName: String = BuildConfig.APPLICATION_ID,
    @Json(name = "app_version") var appVersion: String = BuildConfig.VERSION_NAME,
    @Json(name = "svc_ctg_id") var serviceCategoryId: String? = null,
    @Json(name = "svc_ctg_type") var serviceCategoryType: String? = null,
    @Json(name = "serial_no") var headsetSerialNo: String? = null
) {
    companion object {
        const val PROPERTY_NAME: String = "X-omnifit"
    }

    enum class ResultCode(
        val signature: String,
        val message: String,
        val cause: String
    ) {
        //@formatter:off
        UNKNOWN("", "?", "?"),
        SUCCESS("00000", "성공.", "연동성공."),
        ERROR_ENCRYPTION_PROCESSING("98001", "암호화 처리 오류가 발생하였습니다.", "암호화 오류."),
        ERROR_PROCESSING("99999", "처리중 오류가 발생하였습니다.", "알 수 없는 오류."),
        M1001("M1001", "처리에 필요한 정보가 부족합니다.", "공통 필수 입력정보 부족."),
        M1002("M1002", "처리에 필요한 정보가 부족합니다.", "전달 json 포멧검증 중 오류 발생."),
        M1003("M1003", "접속경로를 다시 확인하여 주십시오.", "존재하지 않은 업체코드 정보로 접속 시도 함."),
        M1004("M1004", "다시 시도하여 주십시오.", "암/복호화 실패 시."),
        M1005("M1005", "접속경로가 바르지 않습니다.", "CSRF 체크 오류."),
        M1006("M1006", "정보를 다시 입력하여 주십시오.", "인증번호 수신 핸드폰번호 포멧검증 실패."),
        M1007("M1007", "정보를 다시 입력하여 주십시오.", "인증번호 수신 이멩리 포멧검증 실패."),
        M1008("M1008", "입력정보가 바르지 않습니다.", "내담자 등록 시 파라미터 검증 실패."),
        M1009("M1009", "입력정보가 바르지 않습니다.", "내담자 수정 시 파라미터 검증 실패."),
        M1010("M1010", "입력정보가 바르지 않습니다.", "내담자 삭제 시 파라미터 검증 실패."),
        M1011("M1011", "입력정보가 바르지 않습니다.", "내담자 가족관계 등록/수정 시 파라미터 검증 실패."),
        M1012("M1012", "인증번호를 다시 입력하여 주십시오.", "인증번호 포멧검증 실패."),
        M1013("M1013", "처리에 필요한 정보가 바르지 않습니다.", "REQ 검증 시 오류발생."),
        M1014("M1014", "입력정보가 바르지 않습니다.", "파라미터 json 파싱 오류."),
        M1015("M1015", "입력정보가 바르지 않습니다.", "요청 파라미터 값이 형식이 맞지 않음."),
        ERROR_UNAVAILABLE_ACCESS_TOKEN("M2001", "다시 로그인하여 주십시오.", "access_token 인증 실패."),
        M2002("M2002", "다시 로그인하여 주십시오.", "access_token 기간만료."),
        M2003("M2003", "다시 로그인하여 주십시오.", "refresh_token 인증 실패."),
        M2004("M2004", "다시 로그인하여 주십시오.", "지정업체에 존재하지 않는 아이디."),
        M2005("M2005", "고객센터로 문의하여 주십시오.", "(M2005) 미납으로 서비스 중지 (인증불가)."),
        ERROR_NONEXISTENT_ID("M2006", "일치하는 회원정보가 없습니다.", "존재하지 않는 ID 일 때."),  // <-----
        ERROR_MISMATCH_PW("M2007", "일치하는 회원정보가 없습니다.", "PW가 일치하지 않을 때."),   // <-----
        M2008("M2008", "접속할 수 없는 메뉴입니다.", "권한 없는 메뉴 접속 시도 시."),
        M2009("M2009", "정보가 부족합니다.", "존재하지 않는 서비스코드 정보."),
        M2010("M2010", "정보가 부족합니다.", "존재하지 않는 업체코드 정보."),
        M2011("M2011", "로그인 정보가 존재하지 않습니다.", "로그아웃 처리 대상 정보가 존재하지 않음."),
        M2012("M2012", "일치하는 정보가 없습니다.", "일치하는 핸드폰 번호가 없음."),
        M2013("M2013", "일치하는 정보가 없습니다.", "일치하는 이메일 번호가 없음."),
        M2014("M2014", "일치하는 정보가 없습니다.", "핸드폰번호+이메일이 일치하징지 않음."),
        M2015("M2015", "로그인 실패로 잠금처리 되었습니다.", "로그인 실패에 다른 일정 시간 로그인 잠금 처리 됨."), // <-----
        M3001("M3001", "요청하신 정보가 존재하지 않습니다.", "요청 정보가 존재하지 않을 때."),
        M3002("M3002", "요청하신 정보가 존재하지 않습니다.", "내담자 목록 조회 실패."),
        M3003("M3003", "3분 후에 다시 시도하여 주십시오.", "3분이내 3회 초과 SMS 인증번호 발송 요청 시."),
        M3004("M3004", "3분 후에 다시 시도하여 주십시오.", "3분이내 3회 초과 이메일 인증번호 발송 요청 시."),
        M3005("M3005", "새로운 인증번호를 요청하여 주십시오!", "10분 초과 후 인증번호 인증 시도 시."),
        M3006("M3006", "일치하는 회원정보가 없습니다.", "아이디 찾기 회원정보 불일치 시."),
        M3007("M3007", "인증번호를 다시 입력하여 주십시오.", "아이디 찾기 인증번호 불일치 시."),
        M3008("M3008", "요청하신 정보가 존재하지 않습니다.", "access_token 일치 정보가 존재하지 않을 시."),
        M3009("M3009", "정보가 부족합니다.", "거치형 상담사 조회 시 정보 부족."),
        ERROR_INVALID_ARGUMENT("M3010", "처리에 필요한 정보가 바르지 않습니다.", "파라미터 검증 실패."),
        M3011("M3011", "처리 중 오류가 발생하였습니다.", "DB 업데이트가 0건 됨."),
        M3012("M3012", "등록이 불가능한 디바이스입니다.", "디바이스 연결 시 분실/중지/삭제의 경우 연결 실패."),
        M3013("M3013", "다른 사용자가 사용중인 디바이스입니다.", " 1:1매칭타입 디바이스이나 타인 사용중으로 연결 실패."),
        M3014("M3014", "디바이스 정보가 바르지 않습니다.", "MAC 또는 시리얼번호가 바르지 않습니다."),
        M3015("M3015", "파일을 찾을수 없습니다.", "파일 찾을수 없음."),
        M3016("M3016", "이미 탈퇴처리 되어 있습니다.", "이미 탈퇴처리 되어 있습니다."),
        M4001("M4001", "일치하는 정보가 존재하지 않습니다.", "입력정보 불일치로 인한 access token 갱신 실패."),
        M4002("M4002", "일치하는 정보가 존재하지 않습니다.", "입력정보 불일치로 인한 상담사 access token 갱신 실패."),
        M4003("M4003", "입력정보가 부족합니다.", "성별/핸드폰/생년월일 필수 입력정보 부족."),
        M4004("M4004", "일치하는 정보가 존재하지 않습니다.", "디바이스 연결 시 MAC_ADDR 존재하지 않음."),
        M4005("M4005", "일치하는 정보가 존재하지 않습니다.", "설문/검사 템플릿 조회 정보가 존재하지 않음."),
        M4006("M4006", "일치하는 정보가 존재하지 않습니다.", "설문/검사 항목 조회 정보가 존재하지 않음."),
        M4007("M4007", "일치하는 정보가 존재하지 않습니다.", "설문/검사결과 상세내역이 존재하지 않음."),
        ERROR_DUPLICATE_UNIQUE_DATA("M4008", "중복값이 존재합니다.", "중복된 유일값입니다."),
        M4009("M4009", "처리중 오류가 발생하였습니다.", "반영실패 하였습니다."),
        ERROR_ALREADY_REGISTERED_USER("M4010", "이미 내담자 등록이 되어 있습니다.", "내담자 등록정보 데이터가 존재합니다."),
        M5001("M5001", "없음.", "없음."),
        M6001("M6001", "접속이 지연되고 있습니다.", "DB 연결 실패."),
        M6002("M6002", "DB처리 중 오류가 발생하였습니다.", "SQL 예외 오류 발생."),
        M7001("M7001", "서비스 점검 중 입니다.	", "시스템 점검 및 장애 시."),
        M7002("M7002", "다시 접속하여 주십시오.", "encryptPassword 처리 중 Exception 발생."),
        M8001("M8001", "접속이 지연되고 있습니다.", "연동 시스템 TimeOut 발생 시."),
        M8002("M8002", "접속이 지연되고 있습니다.", "연동 시스템으로 부터 답변이 없을 시."),
        ERROR_RESPONSE_EMPTY_ENTITY("E0001", "응답 항목이 비어있습니다.", ""),
        ERROR_DB_MISMATCH_PW("E0002", "일치하는 회원정보가 없습니다.", ""),
        ERROR_REQUEST_FAILURE("E0003", "API 서버 요청 실패 했습니다.", "");
        //@formatter:on

        companion object {
            fun of(signature: String): ResultCode? {
                return values().find { rc -> rc.signature == signature }
            }
        }
    }

    override fun toString(): String {
        return "XomnifitHeader(" +
                "\napiSignature=$apiSignature, " +
                "\nserviceId=$serviceId, " +
                "\ncorporationId=$corporationId, " +
                "\nteacherLoginId=$adminLoginId, " +
                "\nadminAccessToken=$adminAccessToken, " +
                "\nuserLoginId=$userLoginId, " +
                "\naccessToken=$accessToken, " +
                "\nphoneMacAddress='$phoneMacAddress', " +
                "\nphoneModel='$phoneModel', " +
                "\nappOsVersion='$appOsVersion', " +
                "\nappPackageName='$appPackageName', " +
                "\nappVersion='$appVersion', " +
                "\nserviceCategoryId=$serviceCategoryId, " +
                "\nserviceCategoryType=$serviceCategoryType" +
                ")\n"
    }
}

/**
 * TODO
 *
 * @property appOsType
 * @property appUpdateConfigurationList
 * @property apiConfigurationList
 */
data class ServiceConfiguration(
    @Json(name = "os_type") var appOsType: String = "",
    @Json(name = "app_info") var appUpdateConfigurationList: List<AppUpdateConfiguration>? = null,
    @Json(name = "data_last_ver") var apiConfigurationList: List<ApiConfiguration>? = null
) {
    fun set(context: Context) {
        toJson().let { json ->
            requireSharedPreferences(context).edit {
                putString(PREF_NAME, json)
            }
        }
    }

    /**
     * 앱 업데이트 항목 가져오기
     */
    fun getAppUpdateConfigurationListThatRequireUpdate(): AppUpdateConfiguration? {
        return appUpdateConfigurationList?.find { n ->
            WebApiService.BRAIN_GENIUS_UPDATE_DATE == n.appSignature
        }
    }

    /**
     * 업데이트가 필요한 API 구성 목록 가져 오기.
     */
    fun getApiConfigurationListThatRequireUpdate(n: List<ApiConfiguration>): List<ApiConfiguration> {
        return mutableListOf<ApiConfiguration>()
            .apply {
                apiConfigurationList
                    ?.let {
                        it.intersect(n)
                            .zip(n)
                            .map { (o, n) ->
                                if (!WebApiService.isIgnorableApi(o.apiSignature) && o.updateDate.toDateTime().isBefore(n.updateDate.toDateTime())) add(o)
                            }
                        n.subtract(it)
                            .forEach { _n ->
                                add(_n)
                            }
                    } ?: addAll(n.filter { _n -> !WebApiService.isIgnorableApi(_n.apiSignature) }
                )
            }
    }

    companion object {
        const val PREF_NAME: String = "ServiceConfiguration"

        fun get(context: Context): ServiceConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    ServiceConfiguration().fromJson(json)
                }
        }

        fun clear(context: Context) {
            ServiceConfiguration().set(context)
        }
    }

    override fun toString(): String {
        return "ServiceConfiguration(" +
                "\nappOsType='$appOsType', " +
                "\nappUpdateConfigurationList=$appUpdateConfigurationList, " +
                "\napiConfigurationList=$apiConfigurationList" +
                ")\n"
    }
}

data class AppUpdateConfiguration(
    @Json(name = "app_update_type") var requiredUpdatable: String = "",
    @Json(name = "app_type") var appSignature: String = "",
    @Json(name = "ver_info") var appVersion: String = "",
    @Json(name = "app_desc") var appExplanation: String = "",
    @Json(name = "app_down_url") var appDownloadUrl: String = ""
) {
    override fun toString(): String {
        return "ServiceConfiguration(" +
                "\nrequiredUpdatable='$requiredUpdatable', " +
                "\nappSignature='$appSignature', " +
                "\nappVersion='$appVersion', " +
                "\nappExplanation='$appExplanation', " +
                "\nappDownloadUrl='$appDownloadUrl', " +
                ")\n"
    }
}

data class ApiConfiguration(
    @Json(name = "data_type") var apiSignature: String = "",
    @Json(name = "last_updday") @YyyyMMddHHmmssFormatField var updateDate: Date = Date()
) {
    override fun toString(): String {
        return "ApiConfiguration(" +
                "\napiSignature='$apiSignature', " +
                "\nupdateDate=$updateDate" +
                ")\n"
    }
}

data class AI002(
    @Json(name = "code_ver") @YyyyMMddHHmmssFormatField var codeVersion: Date = Date(),
    @Json(name = "code_ctg_arr") var codeCategoryList: List<CodeCategory> = emptyList()
)

data class PolicyData(
    @Json(name = "info") var content: String = "",
    @Json(name = "info_ver") @YyyyMMddHHmmssFormatField var infoVersion: Date = Date()
)

data class CodeCategory(
    @Json(name = "cd_ctg") var signature: String = "",
    @Json(name = "cd_ctg_nm") var label: String = "",
    @Json(name = "code_grp_arr") var groupCodeList: List<GroupCode> = emptyList()
)

data class AI318(
    @Json(name = "games") var contents: List<GameContent> = emptyList()
)

data class AI320(
    @Json(name = "musics") var contents: List<MusicContent> = emptyList()
)

data class AI408(
    @Json(name = "user_info_arr") var userList: List<User> = emptyList()
)

data class AI306Vo(
    val neuro_measure_no: String,
    val user_id: String,
    @YyyyMMddHHmmssFormatField
    val start_time: Date,
    @YyyyMMddHHmmssFormatField
    val end_time: Date,
    val measure_time: String,
    val left_theta: String,
    val left_alpha: String,
    val left_lbeta: String,
    val left_mbeta: String,
    val left_hbeta: String,
    val left_gamma: String,
    val right_theta: String,
    val right_alpha: String,
    val right_lbeta: String,
    val right_mbeta: String,
    val right_hbeta: String,
    val right_gamma: String,
    val avg_theta: String,
    val avg_alpha: String,
    val avg_lbeta: String,
    val avg_mbeta: String,
    val avg_hbeta: String,
    val avg_gamma: String,
    val mental: String,
    val mental_cd: String,
    val left_sef: String,
    val right_sef: String,
    val left_sef_cd: String,
    val right_sef_cd: String,
    val left_sef_graph: String,
    val right_sef_graph: String,
    val balance_brdfixval: String,
    val balance_cd: String,
    val concentration_cd: String,
    val concentration_graph: String,
    val concentration_type: String,
    val stress: String,
    val stress_cd: String,
    // 제공 안함
    val raw_data: String = "",
    val concentration: String,
    val balance: String,
    val avg_sef_graph: String,
    val ratio_sef: String,
    val brain_score: String,
    val balance_ratio: String,
    val measure_ctg_type: String
) {
    companion object {
        fun create(
            user: User,
            electroencephalography: Measurement
        ): AI306Vo {
//            val leftPowerSpectrum = doubleArrayOf().fromJson(electroencephalography.meanLeftPowerSpectrum)
//            val rightPowerSpectrum = doubleArrayOf().fromJson(electroencephalography.meanRightPowerSpectrum)
//            return AI306Vo(//@formatter:off
//                neuro_measure_no = electroencephalography.no,
//                user_id = electroencephalography.studentId,
//                start_time = electroencephalography.begin,
//                end_time = electroencephalography.end,
//                measure_time = Seconds.secondsBetween(
//                    DateTime(electroencephalography.begin),
//                    DateTime(electroencephalography.end)
//                ).seconds.toString(),
//                left_theta = electroencephalography.meanLeftThetaIndicatorValue.downToDecimal().toString(),
//                left_alpha = electroencephalography.meanLeftAlphaIndicatorValue.downToDecimal().toString(),
//                left_lbeta = electroencephalography.meanLeftLowBetaIndicatorValue.downToDecimal().toString(),
//                left_mbeta = electroencephalography.meanLeftMiddleBetaIndicatorValue.downToDecimal().toString(),
//                left_hbeta = electroencephalography.meanLeftHighBetaIndicatorValue.downToDecimal().toString(),
//                left_gamma = electroencephalography.meanLeftGammaIndicatorValue.downToDecimal().toString(),
//                right_theta = electroencephalography.meanRightThetaIndicatorValue.downToDecimal().toString(),
//                right_alpha = electroencephalography.meanRightAlphaIndicatorValue.downToDecimal().toString(),
//                right_lbeta = electroencephalography.meanRightLowBetaIndicatorValue.downToDecimal().toString(),
//                right_mbeta = electroencephalography.meanRightMiddleBetaIndicatorValue.downToDecimal().toString(),
//                right_hbeta = electroencephalography.meanRightGammaIndicatorValue.downToDecimal().toString(),
//                right_gamma = electroencephalography.meanLeftThetaIndicatorValue.downToDecimal().toString(),
//                avg_theta = electroencephalography.meanThetaIndicatorValue.downToDecimal().toString(),
//                avg_alpha = electroencephalography.meanAlphaIndicatorValue.downToDecimal().toString(),
//                avg_lbeta = electroencephalography.meanLowBetaIndicatorValue.downToDecimal().toString(),
//                avg_mbeta = electroencephalography.meanMiddleBetaIndicatorValue.downToDecimal().toString(),
//                avg_hbeta = electroencephalography.meanHighBetaIndicatorValue.downToDecimal().toString(),
//                avg_gamma = electroencephalography.meanGammaIndicatorValue.downToDecimal().toString(),
//                mental = electroencephalography.meanSef90Hz.downToDecimal().toString(),
//                mental_cd = BrainActivityIndicator.of(
//                    transformSef90HzTo11piValue(
//                        electroencephalography.meanSef90Hz
//                    )
//                ).codeSignature,
//                left_sef = electroencephalography.meanLeftSef90Hz.downToDecimal().toString(),
//                right_sef = electroencephalography.meanRightSef90Hz.downToDecimal().toString(),
//                left_sef_cd = BrainActivityIndicator.of(
//                    transformSef90HzTo11piValue(
//                        electroencephalography.meanLeftSef90Hz
//                    )
//                ).codeSignature,
//                right_sef_cd = BrainActivityIndicator.of(
//                    transformSef90HzTo11piValue(
//                        electroencephalography.meanRightSef90Hz
//                    )
//                ).codeSignature,
//                left_sef_graph = DoubleArray(128) { 0.0 }.apply {
//                    System.arraycopy(
//                        leftPowerSpectrum,
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        this,
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        Ocwh20Client.EFFECTIVE_POWER_SPECTRUM_SIZE - Ocwh20Client.THETA_BEGIN_INDEX
//                    )
//                }.toJson().removeSquareBrackets(),
//                right_sef_graph = DoubleArray(128) { 0.0 }.apply {
//                    System.arraycopy(
//                        rightPowerSpectrum,
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        this,
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        Ocwh20Client.EFFECTIVE_POWER_SPECTRUM_SIZE - Ocwh20Client.THETA_BEGIN_INDEX
//                    )
//                }.toJson().removeSquareBrackets(),
//                balance_brdfixval = BrainBalanceIndicator.transformBalanceToBrdfixval(
//                    electroencephalography.meanBalanceIndicatorValue
//                ).toString(),
//                balance_cd = BrainBalanceIndicator.of(electroencephalography.meanBalanceIndicatorValue).codeSignature,
//                concentration_cd = BrainConcentrationIndicator.of(electroencephalography.meanConcentrationIndicatorValue).codeSignature,
//                concentration_graph = doubleArrayOf().fromJson(electroencephalography.concentrationIndicatorValues).map { v -> if (v < 0.0) 0.0 else v }.toDoubleArray().toJson().removeSquareBrackets(),
//                concentration_type = BrainConcentrationTypeIndicator.of(
//                    doubleArrayOf().fromJson(
//                        electroencephalography.concentrationIndicatorValues
//                    )
//                ).codeSignature,
//                stress = electroencephalography.meanHighBetaIndicatorValue.downToDecimal().toString(),
//                stress_cd = BrainStressIndicator.of(electroencephalography.meanHighBetaIndicatorValue).codeSignature,
//                raw_data = "",
//                concentration = electroencephalography.meanConcentrationIndicatorValue.downToDecimal().toString(),
//                balance = electroencephalography.meanBalanceIndicatorValue.downToDecimal().toString(),
//                avg_sef_graph = DoubleArray(Ocwh20Client.EFFECTIVE_POWER_SPECTRUM_SIZE) { 0.0 }.apply {
//                    System.arraycopy(
//                        doubleArrayOf().fromJson(electroencephalography.meanPowerSpectrum),
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        this,
//                        Ocwh20Client.THETA_BEGIN_INDEX,
//                        Ocwh20Client.EFFECTIVE_POWER_SPECTRUM_SIZE - Ocwh20Client.THETA_BEGIN_INDEX
//                    )
//                }.toJson().removeSquareBrackets(),
//                ratio_sef = doubleArrayOf().fromJson(electroencephalography.meanRhythmDistribution).let {
//                    it.copyOfRange(
//                        1,
//                        it.size
//                    )
//                }.toJson().removeSquareBrackets(),
//                brain_score = ((BrainConcentrationIndicator.analyzeBrainHealthScore(
//                    electroencephalography.meanConcentrationIndicatorValue
//                )
//                        + BrainActivityIndicator.analyzeBrainHealthScoreByHz(electroencephalography.meanSef90Hz)
//                        + BrainStressIndicator.analyzeBrainHealthScore(electroencephalography.meanHighBetaIndicatorValue)
//                        + BrainBalanceIndicator.analyzeBrainHealthScore(electroencephalography.meanBalanceIndicatorValue)) / 4.0).roundToInt().toString(),
//                balance_ratio = BrainBalanceIndicator.calcPowerSpectrumRatio(
//                    leftPowerSpectrum,
//                    rightPowerSpectrum
//                ).first.toString(),
//                measure_ctg_type = when {
//                    electroencephalography.targetSignature.startsWith(Healing.ID_PREFIX) -> MEASURE_TYPE_HEALING
//                    electroencephalography.targetSignature.startsWith(Game.ID_PREFIX) -> MEASURE_TYPE_GAME
//                    else -> MEASURE_TYPE_BRAIN_CHECK
//                }
//            )//@formatter:on

            TODO("수정된 알고리즘으로 데이터 맵핑 다시")
        }
    }
}

sealed class Vo

// 전후 비교 값 업로드 API
data class AI322Vo(
    val measure_no:String,
    val measure_type:String,
    val conts_no:Int,
    val user_id:String,
    @YyyyMMddHHmmssFormatField
    val start_time:Date,
    @YyyyMMddHHmmssFormatField
    val end_time:Date,
    val graph:String,
    val score:Int,
    val tot_trn_time:Int,
    val focus_trn_time:Int,
    val ai_mode_time:Int,
    val bf_val:Double,
    val bf_cd:String,
    val bf_cctt_type:String = "",
    val af_val:Double,
    val af_cd:String,
    val af_cctt_type:String = "",
    val focus_rto:Double,
    val tst_rts_cd:String,
    val chg_rto:Double
) : Vo() {
    companion object {
        enum class MeasureType(val code:String) {
            CONCENTRATION_GAME("ME001"),
            HEALING_MUSIC("ME002"),
            NEURO_FEEDBACK("ME003"),
            AI("ME004")
        }

        fun create(result: ServiceResult): AI322Vo {
            return ComparisionData.create(result).let { comparisionData ->

                // TODO : 인디케이터 코드를 확인해야 한다.

                val indicatorCode = { v:Double ->
                    when(ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> Algorithm.indicatorOf<Indicator.EEG.Concentration>(v)
                        ServiceCategory.CONCENTRATION_TRAINING              -> Algorithm.indicatorOf<Indicator.EEG.Concentration>(v)
                        ServiceCategory.BRAIN_STABILITY_TRAINING            -> Algorithm.indicatorOf<Indicator.EEG.Stress>(10 - v)
                        ServiceCategory.NEURO_FEEDBACK                      -> Algorithm.indicatorOf<Indicator.EEG.Relaxation>(v)
                    }.code ?: "AN000"
                }

                val optionalIndicator = { v:DoubleArray ->
                    when(ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING,
                        ServiceCategory.CONCENTRATION_TRAINING -> Algorithm.indicatorOf<Indicator.EEG.ConcentrationType>(v)
                        else -> null
                    }?.code ?: ""
                }

                // 설계에 따라 기존 GC000 , MU000 를 사용하지 않고 TS로 사용
                val resultCode = Algorithm.indicatorOf<Indicator.Content.Result>(result.resultSignature)
                    .code
                    ?: "TS000"

                AI322Vo(
                    measure_no = result.measurementNo,
                    measure_type = when(ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> MeasureType.AI.code
                        ServiceCategory.CONCENTRATION_TRAINING              -> MeasureType.CONCENTRATION_GAME.code
                        ServiceCategory.BRAIN_STABILITY_TRAINING            -> MeasureType.HEALING_MUSIC.code
                        ServiceCategory.NEURO_FEEDBACK                      -> MeasureType.NEURO_FEEDBACK.code
                    },
                    conts_no = result.contentSequence,
                    user_id = result.userId,
                    start_time = result.begin,
                    end_time = result.end,
                    graph = result
                        .indicatorValues
                        .toDoubleArray()
                        .map { v ->
                            if (v < 0.0) 0.0
                            else v
                        }
                        .toDoubleArray()
                        .toJson()
                        .removeSquareBrackets(),
                    score = result.score,
                    tot_trn_time = result.elapsedTime.millisToSeconds(),
                    focus_trn_time = result.effectiveTime.millisToSeconds(),
                    ai_mode_time = when(ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> result.optionalTime.millisToSeconds()
                        else -> 0
                    },
                    bf_val = comparisionData.beforeAverage.roundToDecimal(1),
                    bf_cd = indicatorCode(comparisionData.beforeAverage),
                    bf_cctt_type = optionalIndicator(comparisionData.beforeSubList),
                    af_val = comparisionData.afterAverage.roundToDecimal(1),
                    af_cd = indicatorCode(comparisionData.afterAverage),
                    af_cctt_type = optionalIndicator(comparisionData.afterSubList),
                    focus_rto = result.effectiveTimeRatio.toDouble(),
                    tst_rts_cd = resultCode,
                    chg_rto = (comparisionData.comparisonRate * 10.0).roundToDecimal(1)
                    //chg_rto = comparisionData.comparisonRate //(comparisionData.comparisonRate * 100.0).roundToDecimal(1)
                )
            }
        }
    }
}