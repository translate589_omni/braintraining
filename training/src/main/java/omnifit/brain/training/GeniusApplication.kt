package omnifit.brain.training

import android.app.Application
import android.content.Context
import android.util.DisplayMetrics
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import omnifit.brain.training.db.Db
import omnifit.brain.training.db.model.User
import omnifit.commons.algorithm.Algorithm
import omnifit.commons.algorithm.SERVICE_TYPE_MINDCARE
import omnifit.commons.audioplayer.AudioPlayer
import org.jetbrains.anko.windowManager
import timber.log.Timber
import java.lang.ref.WeakReference

class GeniusApplication : Application() {

    private var contextReference: WeakReference<Context>? = null

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        Timber.plant(Timber.DebugTree())
    }

    override fun onCreate() {
        super.onCreate()
        contextReference = WeakReference(this)
        contextReference?.get()?.let { context ->
            initialize(context)
        }
    }

    private fun initialize(base: Context?) {
        base?.let { context ->
            requireDensity()
            Db.init(context)
            Font.init(context)
            AudioPlayer.init(context)
//            AppConfig.init(context) // 사용 이유 찾기
            //Algorithm.Settings.setServiceType(SERVICE_TYPE_BRAIN)
            Algorithm.Settings.setServiceType(SERVICE_TYPE_MINDCARE)
        } ?: throw IllegalArgumentException("Not attached base context")
    }

    var desiredDisplayMetrics: DisplayMetrics? = null

    /**
     * densityDpi :
     *
     * ldpi = 0.75 (120dpi)
     * mdpi = 1 (160dpi)
     * hdpi = 1.5 (240dpi)
     * xhdpi = 2.0 (320dpi)
     * xxhdpi = 3.0 (480dpi)
     * xxxhdpi = 4.0 (640dpi)
     */
    private fun requireDensity() {
        resources.displayMetrics.density.let { density ->
            when {
                density > 0.75f && density < 1.0f -> 1.0f
                density > 1.0f && density < 1.5f  -> 1.5f
                density > 1.5f && density < 2.0f  -> 2.0f
                density > 2.0f && density < 3.0f  -> 3.0f
                density > 3.0f && density < 4.0f  -> 4.0f
                else                              -> return
            }
        }.let(this::changeDensity)
    }

    private fun changeDensity(desiredDensity: Float) {
        DisplayMetrics().apply {
            windowManager.defaultDisplay.getMetrics(this)
            density = desiredDensity
            densityDpi = (desiredDensity * 160f).toInt()
            scaledDensity = desiredDensity
            xdpi = desiredDensity * 160f
            ydpi = desiredDensity * 160f
        }.let { metrics ->
            resources.displayMetrics.setTo(metrics)
            desiredDisplayMetrics = metrics
        }
    }

    fun close(block: () -> Unit = {}) {
        contextReference?.clear()
        block()
    }

    var currentUser: User = User()
}

@GlideModule
class GeniusGlideModule : AppGlideModule()

enum class ServiceCategory {
    AI_CONCENTRATION_ASSISTANT_TRAINING,
    CONCENTRATION_TRAINING,
    BRAIN_STABILITY_TRAINING,
    NEURO_FEEDBACK;

    companion object {
        fun of(n: Int): ServiceCategory = values()[n]
        fun of(name: String): ServiceCategory = valueOf(name)
    }
}

enum class ResultCategory {
    GAME, HEALING, MEDITATION, AI;

    companion object {
        fun of(n: Int): ResultCategory = values()[n]
    }
}