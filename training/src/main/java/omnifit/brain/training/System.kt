package omnifit.brain.training

import android.content.Context
import android.content.SharedPreferences
import java.net.NetworkInterface

private const val PREFS_NAME: String = "${BuildConfig.APPLICATION_ID}.prefs"

private var sharedPreferences: SharedPreferences? = null

fun requireSharedPreferences(context: Context): SharedPreferences = sharedPreferences
    ?: context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).also { sharedPreferences = it }

@ExperimentalUnsignedTypes
fun wifiMacAddress(): String {
    val unknown = "02:00:00:00:00:00"
    return try {
        NetworkInterface.getNetworkInterfaces()
                .asSequence()
                .find { it.name == "wlan0" }
                ?.hardwareAddress
                ?.asUByteArray()
                ?.joinToString(":") { it.toString(16).padStart(2, '0') }
//                ?.toUpperCase(Locale.getDefault())
                ?: unknown
    }
    catch (e: Exception) {
        e.printStackTrace()
        unknown
    }
}