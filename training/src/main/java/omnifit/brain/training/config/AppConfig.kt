package omnifit.brain.training.config

import android.content.Context
import androidx.core.content.edit
import omnifit.brain.training.BuildConfig
import omnifit.brain.training.requireSharedPreferences
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.json.toJson

data class AppConfig(
    var isExistsNotificationChannel: Boolean = false
) {
    fun set(context: Context) {
        toJson().let { json ->
            requireSharedPreferences(context)
                .edit {
                    putString(PREF_NAME, json)
                }
        }
    }

    companion object {
        private const val PREF_NAME: String = "AppConfig"
        private const val PREF_DEFAULT_VALUE: String = "{}"

        const val REALM_FILE_NAME = "${BuildConfig.APPLICATION_ID}.realm"
        const val REALM_SCHEMA_VERSION = 1L
        const val NOTIFICATION_CHANNEL_ID = "${BuildConfig.APPLICATION_ID}.notification"

        @Volatile
        private var config: AppConfig? = null

        fun set(
            context: Context,
            run: AppConfig.() -> Unit
        ) {
            get(context).apply(run).set(context)
        }

        fun get(context: Context): AppConfig {
            return config ?: requireSharedPreferences(context)
                .getString(PREF_NAME, PREF_DEFAULT_VALUE)!!
                .let { json ->
                    AppConfig().fromJson(json).also { config = it }
                }
        }

        fun init(context: Context) {
            set(context) {

            }
        }
    }
}