package omnifit.brain.training.common

// 코드 정의
const val SIGNATURE_UTS_HEALING_MUSIC: String          = "UTS003"
const val SIGNATURE_UTS_MEDITATION: String             = "UTS006"
const val SIGNATURE_UTS_MEDITATION_MBSR: String        = "UTS008"
const val SIGNATURE_UTS_MEDITATION_TRAINING: String    = "UTS009"

const val SIGNATURE_MUC_NORMAL: String = "MUC001"  // 일반음
const val SIGNATURE_MUC_NATURAL: String = "MUC002" // 자연음

const val SIGNATURE_BRAIN_CHECK: String = "CEN000"
const val SIGNATURE_GAME: String        = "GC000"
const val SIGNATURE_MUSIC: String       = "MU000"

const val SIGNATURE_MIC_BRAIN_STRESS: String = "BSC000"
const val SIGNATURE_MIC_RELAXATION: String = "RLX000"
const val SIGNATURE_MIC_BARIN_ACTIVITY: String = "BAL000"

const val SIGNATURE_FIL_IMAGE: String = "FIL001"
const val SIGNATURE_FIL_VIDEO: String = "FIL003"

const val MEASURE_TYPE_BRAIN_CHECK = "MS001"
const val MEASURE_TYPE_HEALING     = "MS002"
const val MEASURE_TYPE_GAME        = "MS003"

// 1분측정
const val MEASUREMENT_ITEM_BRAIN_STRESS: Int   = 0
const val MEASUREMENT_ITEM_CONCENTRATION: Int  = 1
const val MEASUREMENT_ITEM_BRAIN_ACTIVITY: Int = 2
const val MEASUREMENT_ITEM_BALANCE: Int        = 3

// 게임
const val ARGUMENTS_RESULT_ID: String               = "RESULT_ID"
const val ARGUMENTS_RESULT_TYPE: String             = "RESULT_TYPE"
const val SCREEN_ARGUMENTS_CONTENT_POSITION: String = "CONTENT_POSITION"

// 힐링
const val MINIMUM_HEALING_EFFECTIVE_VALUE_COUNT: Int = 60