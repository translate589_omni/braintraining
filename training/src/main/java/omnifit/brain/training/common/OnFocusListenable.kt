package omnifit.brain.training.common

interface OnFocusListenable {
    fun onWindowFocusChanged(hasFocus: Boolean)
}