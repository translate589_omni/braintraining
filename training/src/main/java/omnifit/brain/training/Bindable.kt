package omnifit.brain.training

import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import lt.neworld.spanner.Spanner
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.view.HeadsetStateVisualizer
import omnifit.commons.common.BaseObservableProperty
import omnifit.commons.omnifitbrain.Headset
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.runOnUiThread

object Bindable {
    val handlingHeadset:BaseObservableProperty<Headset> by lazy {
        BaseObservableProperty(Headset.empty())
    }

    val uploadableBadgeCount: BaseObservableProperty<String> by lazy {
        BaseObservableProperty(Measurement.countByUploadable().toString())
    }

    val isHeadsetLinked: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    val isHeadsetLinking: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    val batteryLevel: BaseObservableProperty<Int> by lazy {
        BaseObservableProperty(0)
    }

    val batteryLevelSpannableString: BaseObservableProperty<Spanner> by lazy {
        BaseObservableProperty(Spanner())
    }

    val isUserLeaved: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    val isHeadsetStateVisualized: BaseObservableProperty<Boolean> by lazy {
        BaseObservableProperty(false)
    }

    val algorithmMonitorLog:BaseObservableProperty<String> by lazy {
        BaseObservableProperty("")
    }
}

fun <T : View> T.bindActivated(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            isActivated = v
        }
    }
}

fun HeadsetStateVisualizer.bindVisualized(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>
) {
    bindSomething(owner,bindable) { bool ->
        context.runOnUiThread {
            when(bool) {
                true -> start()
                false -> stop()
            }
        }
    }
}

fun <T : TextView, PT> T.bindBatteryLevel(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<PT>
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            when (v) {
                is Int                    -> "$v%"
                is SpannableStringBuilder -> v
                else                      -> ""
            }.let { s ->
                text = if (Bindable.isHeadsetLinked.value) s else "-------"
            }
        }
    }
}

fun <T : View> T.bindEnabled(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>
) {
    bindSomething(owner,bindable) { bool ->
        context.runOnUiThread {
            isEnabled = bool
        }
    }
}

fun <T : CompoundButton> T.bindChecked(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>
) {
    bindSomething(owner,bindable) { bool ->
        context.runOnUiThread {
            isChecked = bool
        }
    }
}

fun <T : View> T.bindVisible(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>,
    isContrariwise: Boolean = false
) {
    bindSomething(owner,bindable) { bool ->
        context.runOnUiThread {
            isVisible = bool.xor(isContrariwise)
        }
    }
}

fun <T : ImageView, PT> T.bindImageLevel(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<PT>
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            when (v) {
                is Int     -> setImageLevel(v)
                is Boolean -> setImageLevel(if (v) 1 else 0)
                else       -> Unit
            }
        }
    }
}

fun <T : ImageView, R> T.bindToggleImage(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Boolean>,
    toggleAction: (Boolean) -> R
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            toggleAction(v)
                .let { r ->
                    when (r) {
                        is Int      -> imageResource = r
                        is Drawable -> setImageDrawable(r)
                        else        -> Unit
                    }
                }
        }
    }
}

fun <T : ProgressBar> T.bindProgress(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Int>
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            progress = v
        }
    }
}

fun <T : TextView> T.bindSpannableString(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<Spanner>
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            text = v
        }
    }
}

fun <T : TextView, PT> T.bindText(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<PT>,
    @StringRes format: Int = -1
) {
    bindSomething(owner,bindable) { v ->
        context.runOnUiThread {
            when (v) {
                is Int                    -> getString(v)
                is Long                   -> v.toString()
                is String                 -> v
                is SpannableStringBuilder -> v
                else                      -> ""
            }.let { s ->
                text = if (format != -1) getString(format, s) else s
            }
        }
    }
}

fun <T:View, K> T.bindSomething(
    owner: LifecycleOwner,
    bindable: BaseObservableProperty<K>,
    bindAction:(K) -> Unit = {}
) {
    val valueChangeListener: (K) -> Unit = { data ->
        bindAction(data)
    }

    bindAction(bindable.value)
    bindable.addValueChangeListener(valueChangeListener)
    owner.lifecycle.addObserver(object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun unbind() {
            bindable.removeValueChangeListener(valueChangeListener)
            owner.lifecycle.removeObserver(this)
        }
    })
}
