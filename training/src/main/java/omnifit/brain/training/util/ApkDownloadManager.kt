package omnifit.brain.training.util

import android.content.Context
import android.os.Environment
import com.liulishuo.okdownload.DownloadTask
import com.liulishuo.okdownload.SpeedCalculator
import com.liulishuo.okdownload.core.Util
import com.liulishuo.okdownload.core.breakpoint.BlockInfo
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo
import com.liulishuo.okdownload.core.cause.EndCause
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend
import timber.log.Timber
import java.io.File

class ApkDownloadManager(
    private val context: Context,
    private val url: String,
    private val fileName: String,
    private val progressUpdateInterval: Int,
    private val onTaskStart: (() -> Unit)?,
    private val onInfoReady: (() -> Unit)?,
    private val onConnectionStart: (() -> Unit)?,
    private val onConnectionStop: (() -> Unit)?,
    private val onProgressUpdate: ((Float) -> Unit)?,
    private val onTaskEnd: ((EndCause, String?) -> Unit)?
) {
    private fun getParentFile(context: Context): File = context.externalCacheDir ?: context.cacheDir

    private fun getParentFilePath(context: Context): String =
        context.externalCacheDir?.absolutePath ?: context.cacheDir.absolutePath

    private var downloadApkPath: String = ""

    private fun calcProgressPercent(offset: Long, length: Long): Float =
        offset.toFloat() / length.toFloat()

    private val task by lazy {
        downloadApkPath = "${getParentFilePath(context)}/download/apk"
        Timber.e("--> savePath = $downloadApkPath | fileName = $fileName")

        DownloadTask.Builder(url, File(downloadApkPath))
            .setFilename(fileName)
            .setMinIntervalMillisCallbackProcess(progressUpdateInterval)
            .setPassIfAlreadyCompleted(true)
            .build()
    }

    private val listener4WithSpeed by lazy {
        object : DownloadListener4WithSpeed() {
            var readableTotalLength = ""
            var totalLength = 0L

            override fun taskStart(task: DownloadTask) {
                onTaskStart?.invoke()
            }

            override fun infoReady(
                task: DownloadTask,
                info: BreakpointInfo,
                fromBreakpoint: Boolean,
                model: Listener4SpeedAssistExtend.Listener4SpeedModel
            ) {
                totalLength = info.totalLength
                readableTotalLength = Util.humanReadableBytes(info.totalLength, true)
                onInfoReady?.invoke()
                onProgressUpdate?.invoke(
                    calcProgressPercent(
                        info.totalOffset,
                        info.totalLength
                    )
                )
            }

            override fun connectStart(
                task: DownloadTask,
                blockIndex: Int,
                requestHeaderFields: MutableMap<String, MutableList<String>>
            ) {
                onConnectionStart?.invoke()
            }

            override fun connectEnd(
                task: DownloadTask,
                blockIndex: Int,
                responseCode: Int,
                responseHeaderFields: MutableMap<String, MutableList<String>>
            ) {
                onConnectionStop?.invoke()
            }

            override fun progress(
                task: DownloadTask,
                currentOffset: Long,
                taskSpeed: SpeedCalculator
            ) {
                "${Util.humanReadableBytes(
                    currentOffset,
                    true
                )} / $readableTotalLength (${taskSpeed.speed()})"

                onProgressUpdate?.invoke(calcProgressPercent(currentOffset, totalLength))
            }

            override fun taskEnd(
                task: DownloadTask,
                cause: EndCause,
                realCause: Exception?,
                taskSpeed: SpeedCalculator
            ) {
                Timber.e("--> filepath : ${task.file?.absolutePath ?: "not find"}")
                onTaskEnd?.invoke(cause, task.file?.absolutePath)
            }

            override fun progressBlock(
                task: DownloadTask,
                blockIndex: Int,
                currentBlockOffset: Long,
                blockSpeed: SpeedCalculator
            ) {
            }

            override fun blockEnd(
                task: DownloadTask,
                blockIndex: Int,
                info: BlockInfo?,
                blockSpeed: SpeedCalculator
            ) {
            }
        }
    }

    fun downloadApk() {
        task.enqueue(listener4WithSpeed)
    }

    data class Builder(
        private var context: Context,
        private var url: String,
        private var fileName: String? = null,
        private var progressUpdateInterval: Int? = null,
        private var onTaskStart: (() -> Unit)? = null,
        private var onInfoReady: (() -> Unit)? = null,
        private var onConnectionStart: (() -> Unit)? = null,
        private var onConnectionStop: (() -> Unit)? = null,
        private var onProgressUpdate: ((Float) -> Unit)? = null,
        private var onTaskEnd: ((EndCause, String?) -> Unit)? = null
    ) {
        private val defaultInterval = 30
//        private val defaultFileName = "file_${DateTime.now().toString(DTP_YYYYMMDDHHMMSS)}.omni"
        private val defaultFileName = "update.apk"

        fun setProgressUpdateInterval(interval: Int) =
            apply { this.progressUpdateInterval = interval }

        fun onTaskStart(event: (() -> Unit)) = apply { this.onTaskStart = event }
        fun onInfoReady(event: (() -> Unit)) = apply { this.onInfoReady = event }
        fun onConnectionStart(event: (() -> Unit)) = apply { this.onConnectionStart = event }
        fun onConnectionStop(event: (() -> Unit)) = apply { this.onConnectionStop = event }
        fun onProgressUpdate(event: ((Float) -> Unit)) = apply { this.onProgressUpdate = event }
        fun onTaskEnd(event: ((EndCause, String?) -> Unit)) = apply { this.onTaskEnd = event }

        fun build() = ApkDownloadManager(
            context,
            url,
            fileName ?: defaultFileName,
            progressUpdateInterval ?: defaultInterval,
            onTaskStart,
            onInfoReady,
            onConnectionStart,
            onConnectionStop,
            onProgressUpdate,
            onTaskEnd
        )
    }
}