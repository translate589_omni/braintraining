package omnifit.brain.training.util

import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * TODO
 *
 * @return
 */
fun String.encryptAes256(): String {
    return try {
        if (trim().isEmpty()) throw IllegalArgumentException()
        else Base64.encodeToString(encryptCipher.doFinal(trim().toByteArray()), Base64.NO_WRAP)
    }
    catch (e: Exception) {
        trim()
    }
}

/**
 * TODO
 *
 * @return
 */
fun String.decryptAes256(): String {
    return try {
        if (trim().isEmpty()) throw IllegalArgumentException()
        else String(decryptCipher.doFinal(Base64.decode(trim(), Base64.NO_WRAP))).let { decrypted ->
            if (decrypted.isNotEmpty()) decrypted else trim()
        }
    }
    catch (e: Exception) {
        trim()
    }
}

private val secretKeySpec: SecretKeySpec by lazy {
    SecretKeySpec(SECRET_KEY.toByteArray(), "AES")
}

private val algorithmParameterSpec: IvParameterSpec
    get() = IvParameterSpec(ByteArray(16))


private val encryptCipher: Cipher
    get() = Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
        init(Cipher.ENCRYPT_MODE, secretKeySpec, algorithmParameterSpec)
    }


private val decryptCipher: Cipher
    get() = Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
        init(Cipher.DECRYPT_MODE, secretKeySpec, algorithmParameterSpec)
    }


private const val SECRET_KEY: String = "e89160b73fa99e706bdd5bb07acedfcf"