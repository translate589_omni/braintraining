package omnifit.brain.ceragemui.utils

import kotlin.math.pow

/**
 * 트렌드 라인 그리는 파일
 * @author Translate589
 * @see http://blog.naver.com/PostView.nhn?blogId=moojigai07&logNo=120186757908
 */
object TrendLine {

    /**
     *  x 평균과 y 평균 구하는 함수
     */
    private fun calcAverage(array:List<Pair<Double,Double>>) : Pair<Double,Double> {
        var sumX = 0.0
        var sumY = 0.0
        array.forEach {
            sumX += it.first
            sumY += it.second
        }
        return Pair(
            sumX.div(array.size),
            sumY.div(array.size)
        )
    }

    /**
     * 기울기 구하는 함수
     */
    private fun calcInclination(
        pairList:List<Pair<Double,Double>>,
        averageXY: Pair<Double,Double>
    ): Double {
        /*
        // 분자 구하는 함수
        fun sigmaOfNumerator(): Double {
            var sum = 0f
            pairList.forEach {
                sum += (it.second - averageXY.second) * (it.first - averageXY.first)
            }
            return sum
        }

        // 분모 구하는 함수
        fun sigmaOfDenominator(): Double {
            var sum = 0.0
            pairList.forEach {
                sum += (it.first - averageXY.first).toDouble().pow(2.toDouble())
            }
            return sum.toDouble()
        }
        */

        var numerator = 0.0
        var denominator = 0.0

        pairList.forEach {
            numerator = numerator.plus(
                it.second
                    .minus(averageXY.second)
                    .times(it.first.minus(averageXY.first))
            )
            denominator = denominator.plus(
                it.first
                    .minus(averageXY.first)
                    .pow(2)
            )
        }

        return numerator.div(denominator)
    }

    /**
     * 절편 구하는 함수
     */
    private fun calcIntercept(averageXY: Pair<Double,Double>, inclination: Double)
            = averageXY.second.minus(inclination.times(averageXY.first))

    fun calcLinearEquation(list:List<Pair<Double,Double>>) =
        { xPoint: Double ->
            calcAverage(list).let { averageXY ->
                calcInclination(list,averageXY).let { inclination ->
                    val intercept = calcIntercept(averageXY,inclination)
                    print("TEST LOG --> averageXY = ${averageXY.first}, ${averageXY.second} inclination = $inclination / intercept = $intercept \n")
                    (xPoint)
                        .times(inclination)
                        .plus(intercept)
                }
            }
    }

    fun calcLinearEquationWithDoubleArray(list:List<Double>) = mutableListOf<Pair<Double,Double>>().apply {
        list.forEachIndexed { index, d ->
            add(Pair((index+1).toDouble(),d))
        }
    }.let { convertedList ->
        calcLinearEquation(convertedList)
    }
}

fun main() {
    val testData = listOf(
        Pair(10.0,71.0),
        Pair(20.0,45.0),
        Pair(30.0,24.0),
        Pair(40.0,8.0)
    )

    val 노주리혜 = listOf(
        4.759,
        4.233,
        5.317,
        5.033,
        5.133,
        5.133,
        5.267,
        5.15,
        5.033,
        5.45,
        5.417,
        4.783,
        5.4,
        5.15,
        5.35,
        5.1,
        5.067,
        5.583,
        4.933,
        5.833,
        5.05,
        5.233
    )
    val 박주영 = listOf(
        3.091,
        3.767,
        3.13,
        3.0,
        4.0,
        3.733,
        3.431,
        3.917,
        3.583,
        3.55,
        3.707,
        3.569,
        3.731,
        3.65,
        3.667,
        4.017,
        3.5
    )
    val 조수현 = listOf(
        3.386,
        4.617,
        4.517,
        4.65,
        4.69,
        4.1,
        4.339,
        4.556,
        5.179,
        4.379,
        4.304,
        4.2,
        3.966,
        4.85,
        4.379,
        4.333,
        4.054,
        2.333,
        3.935,
        4.87
    )
    val 유성미 = listOf(
        3.55,
        3.467,
        3.333,
        3.683,
        3.483,
        3.1,
        2.883,
        3.1,
        2.767,
        2.583,
        2.65,
        2.8,
        2.683,
        2.9,
        2.783,
        3.083,
        3.7,
        3.167
    )
    val 김신초 = listOf(
        3.7, 3.883, 3.431, 3.933, 4.233,
        4.017, 4.083, 3.733, 4.067, 3.883,
        3.95, 4.183, 4.017, 4.117, 4.083,
        4.317, 4.4, 4.367, 4.467, 4.717,
        5.0, 4.917, 4.917
    )

    val testList = mutableListOf<Int>().apply {
        for(i in 0..100) {
            add(i)
        }
    }


    val linearEquation = TrendLine.calcLinearEquationWithDoubleArray(박주영)
    val linearEquation2 = TrendLine.calcLinearEquationWithDoubleArray(노주리혜)
    val linearEquation3 = TrendLine.calcLinearEquationWithDoubleArray(조수현)
    val linearEquation4 = TrendLine.calcLinearEquationWithDoubleArray(유성미)
    val linearEquation5 = TrendLine.calcLinearEquationWithDoubleArray(김신초)

//    print("박주영 Trend 1 : ${linearEquation(1.0)}\n")
//    print("박주영 Trend ${박주영.size} : ${linearEquation(박주영.size.toDouble())}\n")
//    print("노주리혜 Trend 1 : ${linearEquation2(1.0)}\n")
//    print("노주리혜 Trend ${노주리혜.size} : ${linearEquation2(노주리혜.size.toDouble())}\n")
//    print("조수현 Trend 1 : ${linearEquation3(1.0)}\n")
//    print("조수현 Trend ${조수현.size} : ${linearEquation3(조수현.size.toDouble())}\n")
//    print("유성미 Trend 1 : ${linearEquation4(1.0)}\n")
//    print("유성미 Trend ${유성미.size} : ${linearEquation4(유성미.size.toDouble())}\n")
//    print("김신초 Trend 1 : ${linearEquation5(1.0)}\n")
//    print("김신초 Trend ${김신초.size} : ${linearEquation5(김신초.size.toDouble())}\n")

    print("before : ${testList.subList(0,30)}")
    print("after : ${testList.subList(testList.size - 30 , testList.size)}")

//    print("Test : ${linearEquation(1.0)}\n")
//    print("Test2 : ${linearEquation(35.0)}\n")
}