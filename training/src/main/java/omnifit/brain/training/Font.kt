package omnifit.brain.training

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan
import androidx.annotation.FontRes
import androidx.collection.SparseArrayCompat
import androidx.core.content.res.ResourcesCompat
import lt.neworld.spanner.SpanBuilder
import org.jetbrains.anko.doAsync
import java.util.*

object Font {
    private val typefaceCache: SparseArrayCompat<Typeface?> = SparseArrayCompat()
    fun init(c: Context) {
        doAsync {
            typefaceCache.put(R.font.kopub_dotum_bold, get(c, R.font.kopub_dotum_bold))
            typefaceCache.put(R.font.kopub_dotum_medium, get(c, R.font.kopub_dotum_medium))
            typefaceCache.put(R.font.lato_light, get(c, R.font.lato_light))
            typefaceCache.put(R.font.lato_regular, get(c, R.font.lato_regular))
            typefaceCache.put(R.font.lato_bold, get(c, R.font.lato_bold))
        }
    }

    val kopubDotumBold: Typeface? by lazy { typefaceCache[R.font.kopub_dotum_bold] }
    val kopubDotumMedium: Typeface? by lazy { typefaceCache[R.font.kopub_dotum_medium] }
    val latoLight: Typeface? by lazy { typefaceCache[R.font.lato_light] }
    val latoRegular: Typeface? by lazy { typefaceCache[R.font.lato_regular] }
    val latoBold: Typeface? by lazy { typefaceCache[R.font.lato_bold] }

    fun mediumByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> kopubDotumMedium
            else                  -> latoRegular
        }
    }

    fun boldByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> kopubDotumBold
            else                  -> latoBold
        }
    }

    private fun get(c: Context, @FontRes id: Int) = ResourcesCompat.getFont(c, id)

    class TypefaceSpan(private val typeface: Typeface?) : MetricAffectingSpan() {

        override fun updateDrawState(state: TextPaint) {
            apply(state)
        }

        override fun updateMeasureState(paint: TextPaint) {
            apply(paint)
        }

        private fun apply(paint: Paint) {
            val oldStyle = paint.typeface?.style ?: 0
            val fakeStyle = typeface?.style?.inv()?.and(oldStyle) ?: 0
            if (fakeStyle and Typeface.BOLD != 0) paint.isFakeBoldText = true
            if (fakeStyle and Typeface.ITALIC != 0) paint.textSkewX = -0.25f
            paint.typeface = typeface
        }
    }

    class CustomTypefaceSpan(private val typeface: Typeface?) : SpanBuilder {
        override fun build(): Any {
            return TypefaceSpan(typeface)
        }
    }
}