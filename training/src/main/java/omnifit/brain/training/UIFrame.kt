package omnifit.brain.training

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.collection.SparseArrayCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.db.model.User
import omnifit.brain.training.screen.LoginMenuScreen
import omnifit.commons.omnifitbrain.Link
import omnifit.commons.omnifitbrain.useOmnifitBrain
import omnifit.commons.peripheral.common.Electrode
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.inputMethodManager

class UIFrame : AppCompatActivity() {

    private var onPlaybackStatusChangeListener: OnPlaybackStatusChangeListener? = null
    private val keyEventReceiver: BroadcastReceiver by lazy {
        object : BroadcastReceiver() {
            val SYSTEM_DIALOG_REASON_KEY = "reason"
            val SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions"
            val SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps"
            val SYSTEM_DIALOG_REASON_HOME_KEY = "homekey"
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    Intent.ACTION_CLOSE_SYSTEM_DIALOGS -> {
                        when (intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY)) {
                            SYSTEM_DIALOG_REASON_HOME_KEY -> attachedScreen?.onHomePressed()
                            SYSTEM_DIALOG_REASON_RECENT_APPS -> attachedScreen?.onRecentAppsPressed()
                        }
                    }
                }
            }
        }
    }

    @ExperimentalUnsignedTypes
    @Suppress("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        registerReceiver(keyEventReceiver, IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
        super.onCreate(savedInstanceState)

        constraintLayout {
            // 메인 컨테이
            frameLayout {
                id = R.id.screen_container
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            bindStateObservers()
            //screenTo(LoginScreen(), R.anim.slide_hold, R.anim.slide_hold)
            screenTo(LoginMenuScreen(), R.anim.slide_hold, R.anim.slide_hold)
        }
    }

    @ExperimentalUnsignedTypes
    private fun bindStateObservers() {
        useOmnifitBrain {
            observeLinkStateChange()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .bindUntilEvent(this@UIFrame, Lifecycle.Event.ON_DESTROY)
                .subscribe(
                    { s ->
                        Bindable.isHeadsetLinked.value = s is Link.State.Linked
                    },
                    { e ->
                        e.printStackTrace()
                    }
                )

            observeElectrodeStateChange()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .bindUntilEvent(this@UIFrame, Lifecycle.Event.ON_DESTROY)
                .subscribe(
                    { s ->
                        Bindable.isHeadsetStateVisualized.value = s is Electrode.State.Attached
                    },
                    { e ->
                        e.printStackTrace()
                    }
                )

            observeBatteryLevelChange()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .bindUntilEvent(this@UIFrame, Lifecycle.Event.ON_DESTROY)
                .observeOn(AndroidSchedulers.mainThread())
                .bindBatteryLevelObserver {
                    onLevelChanged { level ->
                        Bindable.batteryLevel.value = level
//                        Bindable.batteryLevelSpannableString.value = Spanner()
//                            .apply {
//                                val sp1 = sp(24f)
//                                val sp2 = sp(17.6f)
//                                val font = Font.CustomTypefaceSpan(Font.mediumByLanguage())
//                                val color = ResourcesCompat.getColor(resources, R.color.x_000000, null)
//                                append("$level", Spans.sizeSP(sp1), Spans.custom(font), Spans.foreground(color))
//                                append("%", Spans.sizeSP(sp2), Spans.custom(font), Spans.foreground(color))
//                            }

                    }
                }
        }
    }

    private val attachedScreenCache: SparseArrayCompat<Class<out UIScreen>> = SparseArrayCompat()
    var attachedScreen: UIScreen? = null
    override fun onAttachFragment(fragment: Fragment) {
        (fragment as? UIScreen)?.let { screen ->
            attachedScreen?.let { screen ->
                attachedScreenCache.put(attachedScreenCache.size(), screen::class.java)
            }

            screen::class.java.let { cls ->
                if (attachedScreenCache.containsValue(cls)) {
                    attachedScreenCache.indexOfValue(cls).let { n ->
                        attachedScreenCache.removeAtRange(n, attachedScreenCache.size() - n)
                    }
                }
            }
            attachedScreen = screen
            onPlaybackStatusChangeListener = screen
        }
    }

    override fun onUserLeaveHint() {
        attachedScreen?.onUserLeaveHint()
    }

    override fun onDestroy() {
        unregisterReceiver(keyEventReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() {
        attachedScreen?.let { screen ->
            if (screen.onBackPressed()) return
            if (!attachedScreenCache.isEmpty) {
                attachedScreenCache[attachedScreenCache.size() - 1]
                    ?.newInstance()
                    ?.let { backScreen ->
                        screenTo(backScreen)
                    }
                return
            }
        }
        super.onBackPressed()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            keepScreenOn()
//            layoutNoLimits()
            hideNavigation()
            attachedScreen?.onWindowFocusChanged(hasFocus)
        }
    }

    var currentUser: User
        set(value) {
            (application as GeniusApplication).currentUser = value
        }
        get() = (application as GeniusApplication).currentUser

    var isAdminLogin: Boolean = false

    // TODO : 임시 토스트
    fun alert(
        context: Context,
        messageRes: Int,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = alert(context, context.getString(messageRes), colorRes, gravity)

    fun alert(
        context: Context,
        message: String,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = Toasty.custom(
        context,
        message,
        null,
        ContextCompat.getColor(context, colorRes),
        Toast.LENGTH_SHORT,
        false,
        true
    ).apply { setGravity(gravity, 0, 0) }.show()

    private fun keepScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun layoutNoLimits() {
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    fun hideNavigation() {
        window.decorView.systemUiVisibility = DEFAULT_SYSTEM_UI_FLAG
    }

    fun hideSoftKeyboard() {
        val view = currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    companion object {
        const val ID_NAME_NAVIGATION_BAR_HEIGHT: String = "navigation_bar_height"
        const val DEF_TYPE_DIMEN: String = "dimen"
        const val DEF_PACKAGE_ANDROID: String = "android"

        const val DEFAULT_SYSTEM_UI_FLAG: Int =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

        val EMPTY_DRAWABLE: Drawable? = null

        const val PLAYBACK_STATUS_START: Int = 0
        const val PLAYBACK_STATUS_PLAYING: Int = 1
        const val PLAYBACK_STATUS_COMPLETE: Int = 2
    }

    interface OnFocusListenable {
        fun onWindowFocusChanged(hasFocus: Boolean)
    }

    interface OnPlaybackStatusChangeListener {
        fun onPlaybackStatusChanged(status: Int, curr: Int, duration: Int)
    }
}