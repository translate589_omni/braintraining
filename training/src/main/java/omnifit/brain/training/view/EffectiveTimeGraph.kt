package omnifit.brain.training.view

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.view.View
import android.view.ViewManager
import androidx.core.content.ContextCompat
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.commons.common.millisToSeconds
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip
import org.jetbrains.anko.sp
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class EffectiveTimeGraph(context: Context) : View(context) {

    var totalTime: Long by Delegates.observable(100L) { _, o, n ->
        if (1L.millisToSeconds() <= n) {
            postInvalidate()
        }
        // hym: 0의 경우 초기값이 세팅이 안됨
        else if (n <= 0L) {
            totalTime = 100L
            postInvalidate()
        }
    }

    var effectiveTime: Long by Delegates.observable(2L) { _, o, n ->
        if (1L.millisToSeconds() <= n) {
            postInvalidate()
        }
        // hym: 0의 경우 초기값이 세팅이 안됨
        else if(n <= 0L) {
            effectiveTime =  2L
            postInvalidate()
        }
    }

    var effectiveRatio: Int = 0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        drawDeepTrainingHBar(canvas)
        drawTotalTime(canvas)
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)

    private fun drawDeepTrainingHBar(c: Canvas) {
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = Color.parseColor("#ffe3e3e3")
        refPoint[IDX_DEEP_TRAINING_HBAR]!!.run {
            refSize[IDX_DEEP_TRAINING_HBAR]!!.run {
                c.drawRect(x, y, x + w, y + h, paint) // Progress bar bg
                paint.color = Color.parseColor("#ff3091f9")
                PointF(x + calcStableTimeProgressWidth(), y + h / 2.0f).let { cp ->
                    c.drawRect(x, y, cp.x, y + h, paint) // Progress bar
                    paint.color = Color.parseColor("#ff1b7ae2")
                    c.drawCircle(cp.x, cp.y, refSize[IDX_DEEP_TRAINING_HBAR_DOT]!!.w / 2.0f, paint) // Dot outer
                    paint.color = Color.WHITE
                    c.drawCircle(cp.x, cp.y, refSize[IDX_DEEP_TRAINING_HBAR_DOT]!!.w / 3.0f, paint) // Dot inner
                    refPoint[IDX_DEEP_TRAINING_RATIO_BALLOON]!!.run {
                        refSize[IDX_DEEP_TRAINING_RATIO_BALLOON]!!.run {
                            ContextCompat.getDrawable(context, R.drawable.bg_balloon)?.apply {
                                setBounds(
                                    (cp.x - w / 2.0f).toInt(),
                                    y.toInt(),
                                    (cp.x - w / 2.0f).toInt() + w,
                                    y.toInt() + h
                                ) // Ratio balloon
                            }?.draw(c)
                            textPaint.color = Color.WHITE
                            textPaint.textAlign = Paint.Align.CENTER
                            textPaint.textSize = sp(14.7f).toFloat()
                            textPaint.typeface = Font.latoRegular
                            calcStableTimeRatio().run {
                                Timber.d("--> 깊은 뇌파율 : $this")
                                val bound = Rect()
                                textPaint.getTextBounds(this, 0, this.length, bound)
                                c.drawText(
                                    this,
                                    cp.x,
                                    y + dip(9.0f).toFloat() + bound.height() + bound.bottom,
                                    textPaint
                                )
                                drawAlignCenterBound(
                                    c,
                                    bound,
                                    PointF(cp.x, y + dip(9.0f).toFloat() + bound.height()),
                                    paint
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    // Align.RIGHT 이라 역으로 그려야함.
    private fun drawTotalTime(c: Canvas) {
        textPaint.color = Color.parseColor("#992e2e2e")
        textPaint.textAlign = Paint.Align.RIGHT
        textPaint.textSize = sp(14.0f).toFloat()
        textPaint.typeface = Font.kopubDotumMedium
        val bound = Rect()
        refPoint[IDX_TOTAL_TIME]!!.run {
            textPaint.getTextBounds(context.getString(SECONDS), 0, context.getString(SECONDS).length, bound)
            c.drawText(context.getString(SECONDS), x, y - bound.bottom, textPaint)
            var xoffset = bound.width() + calcCharSpacingQuater(context.getString(SECONDS), bound)
            ((TimeUnit.MILLISECONDS.toSeconds(totalTime) % 60L) % 1000L).toString().run {
                textPaint.textSize = sp(16.0f).toFloat()
                textPaint.typeface = Font.latoRegular
                textPaint.getTextBounds(this, 0, this.length, bound)
                c.drawText(this, x - xoffset, y - bound.bottom, textPaint)
                xoffset += bound.width() + calcCharSpacingHalf(this, bound)
            }
            textPaint.textSize = sp(14.0f).toFloat()
            textPaint.typeface = Font.kopubDotumMedium
            textPaint.getTextBounds(context.getString(MINUTES), 0, context.getString(MINUTES).length, bound)
            c.drawText(context.getString(MINUTES), x - xoffset, y - bound.bottom, textPaint)
            xoffset += bound.width() + calcCharSpacingQuater(context.getString(MINUTES), bound)
            (TimeUnit.MILLISECONDS.toMinutes(totalTime) % 60L).toString().run {
                textPaint.textSize = sp(16.0f).toFloat()
                textPaint.typeface = Font.latoRegular
                textPaint.getTextBounds(this, 0, this.length, bound)
                c.drawText(this, x - xoffset, y - bound.bottom, textPaint)
                xoffset += bound.width() + calcCharSpacingHalf(this, bound)
            }
            textPaint.textSize = sp(14.0f).toFloat()
            textPaint.typeface = Font.kopubDotumMedium
            textPaint.getTextBounds(context.getString(HOUR), 0, context.getString(HOUR).length, bound)
            c.drawText(context.getString(HOUR), x - xoffset, y - bound.bottom, textPaint)
            xoffset += bound.width() + calcCharSpacingQuater(context.getString(HOUR), bound)
            TimeUnit.MILLISECONDS.toHours(totalTime).toString().run {
                textPaint.textSize = sp(16.0f).toFloat()
                textPaint.typeface = Font.latoRegular
                textPaint.getTextBounds(this, 0, this.length, bound)
                c.drawText(this, x - xoffset, y - bound.bottom, textPaint)
                xoffset += bound.width() + calcCharSpacingHalf(this, bound)
            }
            textPaint.textSize = sp(14.0f).toFloat()
            textPaint.typeface = Font.kopubDotumMedium
            textPaint.getTextBounds(context.getString(TOTAL), 0, context.getString(TOTAL).length, bound)
            c.drawText(context.getString(TOTAL), x - xoffset -5, y - bound.bottom, textPaint)
        }
    }

    companion object {
        const val TOTAL = R.string.content_deep_time
        const val HOUR = R.string.content_deep_hour
        const val MINUTES = R.string.content_deep_minutes
        const val SECONDS = R.string.content_deep_seconds

        const val IDX_DEEP_TRAINING_RATIO_BALLOON = 4
        const val IDX_DEEP_TRAINING_HBAR = 5
        const val IDX_DEEP_TRAINING_HBAR_DOT = 6
        const val IDX_TOTAL_TIME = 7
    }

    private val refPoint = hashMapOf(
        Pair(IDX_DEEP_TRAINING_RATIO_BALLOON, PointF(dip(10.7f).toFloat(), dip(0.0f).toFloat()) ),
        Pair(IDX_DEEP_TRAINING_HBAR, PointF(dip(42.7f).toFloat(), dip(40.7f).toFloat())),
        Pair(IDX_TOTAL_TIME, PointF(dip(334.7f).toFloat(), dip(65.0f).toFloat()))
    )

    private val refSize = hashMapOf(
        Pair(IDX_DEEP_TRAINING_RATIO_BALLOON,Size(dip(64.0f), dip(42.0f))),
        Pair(IDX_DEEP_TRAINING_HBAR, Size(dip(292.7f), dip(4.0f))),
        Pair(IDX_DEEP_TRAINING_HBAR_DOT, Size(dip(14.3f), dip(14.3f)))
    )


    private fun calcStableTimeProgressWidth(): Float {
        return refSize[IDX_DEEP_TRAINING_HBAR]!!.run {
            Timber.d("--> 총 시간  :  $totalTime  /  깊은 뇌파 시간 : $effectiveTime / w : ${this.w.toFloat()}")
            effectiveTime.toFloat() * this.w.toFloat() / totalTime.toFloat()
        }
    }

    private fun calcStableTimeRatio(): String {
//        return if (stableRatio <= 0.0F) "0%"
//        else stableRatio.toString().plus("%")
        return if (effectiveRatio <= 0.0F) "0%"
        else effectiveRatio.toString().plus("%")
    }

    private fun calcCharSpacingHalf(text: String, bound: Rect): Float = bound.width() / (text.length.toFloat() * 2.0f)

    private fun calcCharSpacingQuater(text: String, bound: Rect): Float = bound.width() / (text.length.toFloat() * 4.0f)

    private fun drawAlignCenterBound(c: Canvas, r: Rect, pf: PointF, p: Paint) {
        p.style = Paint.Style.STROKE
        p.color = Color.RED
    }
}

data class Size(val w: Int, val h: Int)

inline fun ViewManager.effectiveTimeGraph(
    theme: Int = 0,
    init: EffectiveTimeGraph.() -> Unit
): EffectiveTimeGraph {
    return ankoView({ EffectiveTimeGraph(it) }, theme, init)
}
