package omnifit.brain.training.view

import android.os.SystemClock
import android.view.View
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

fun View.onDebounceClick(
    context: CoroutineContext = Dispatchers.Main,
    debounceTime: Long = 600L,
    handler: suspend CoroutineScope.(v: View?) -> Unit
) {
    setOnClickListener(object : View.OnClickListener {
        private var clickedTime: Long = 0L
        override fun onClick(v: View?) {
            GlobalScope.launch(context, CoroutineStart.DEFAULT) {
                if (SystemClock.elapsedRealtime() - clickedTime > debounceTime) {
                    handler(v)
                }
                clickedTime = SystemClock.elapsedRealtime()
            }
        }
    })
}