package omnifit.brain.training.view

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.view.View
import android.view.ViewManager
import android.view.animation.LinearInterpolator
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip

class RotateLoading constructor(context: Context) : View(context) {
    private var loadingPaint: Paint? = null
    private var loadingRectF: RectF? = null
    private var shadowRectF: RectF? = null
    private var topDegree = 10
    private var bottomDegree = 190
    private var arc: Float = 0.toFloat()
    private var strokeWidth: Int = 0
    private var changeBigger = true
    private var shadowPosition: Int = 0
    private var isStart = false
    private var color: Int = 0
    private var speedOfDegree: Int = 0
    private var speedOfArc: Float = 0.toFloat()

    init {
        color = Color.WHITE
//        strokeWidth = dip(DEFAULT_WIDTH)
        shadowPosition = dip(DEFAULT_SHADOW_POSITION)
        speedOfDegree = DEFAULT_SPEED_OF_DEGREE
        speedOfArc = (speedOfDegree / 4).toFloat()
        loadingPaint = Paint()
        loadingPaint?.color = color
        loadingPaint?.isAntiAlias = true
        loadingPaint?.style = Paint.Style.STROKE
        loadingPaint?.strokeWidth = strokeWidth.toFloat()
        loadingPaint?.strokeCap = Paint.Cap.ROUND
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        arc = 10.0f
        loadingRectF = RectF((2 * strokeWidth).toFloat(), (2 * strokeWidth).toFloat(), (w - 2 * strokeWidth).toFloat(), (h - 2 * strokeWidth).toFloat())
        shadowRectF = RectF(
            (2 * strokeWidth + shadowPosition).toFloat(),
            (2 * strokeWidth + shadowPosition).toFloat(),
            (w - 2 * strokeWidth + shadowPosition).toFloat(),
            (h - 2 * strokeWidth + shadowPosition).toFloat()
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (!isStart) {
            return
        }

//        loadingPaint!!.color = Color.parseColor("#1a000000")
//        canvas.drawArc(shadowRectF!!, topDegree.toFloat(), arc, false, loadingPaint!!)
//        canvas.drawArc(shadowRectF!!, bottomDegree.toFloat(), arc, false, loadingPaint!!)

//        loadingPaint!!.color = color
        canvas.drawArc(loadingRectF!!, topDegree.toFloat(), arc, false, loadingPaint!!)
        canvas.drawArc(loadingRectF!!, bottomDegree.toFloat(), arc, false, loadingPaint!!)

        topDegree += speedOfDegree
        bottomDegree += speedOfDegree
        if (topDegree > 360) {
            topDegree -= 360
        }
        if (bottomDegree > 360) {
            bottomDegree -= 360
        }

        if (changeBigger) {
            if (arc < 160) {
                arc += speedOfArc
                invalidate()
            }
        }
        else {
            if (arc > speedOfDegree) {
                arc -= 2 * speedOfArc
                invalidate()
            }
        }
        if (arc >= 160 || arc <= 10) {
            changeBigger = !changeBigger
            invalidate()
        }
    }

    fun setStrokeWidth(width: Int) {
        strokeWidth = width
        loadingPaint?.strokeWidth = strokeWidth.toFloat()
    }

    fun getStrokeWidth(): Int {
        return strokeWidth
    }

    fun setLoadingColor(color: Int) {
        this.color = color
        loadingPaint!!.color = color
    }

    fun getLoadingColor(): Int {
        return color
    }

    fun start() {
        startAnimator()
        isStart = true
        invalidate()
    }

    fun stop() {
        stopAnimator()
        invalidate()
    }

    fun isStart(): Boolean {
        return isStart
    }

    private fun startAnimator() {
        val scaleXAnimator = ObjectAnimator.ofFloat(this, "scaleX", 0.0f, 1.0f).apply {
            duration = 300
            interpolator = LinearInterpolator()
        }
        val scaleYAnimator = ObjectAnimator.ofFloat(this, "scaleY", 0.0f, 1.0f).apply {
            duration = 300
            interpolator = LinearInterpolator()
        }
        AnimatorSet().apply {
            playTogether(scaleXAnimator, scaleYAnimator)
        }.start()
    }

    private fun stopAnimator() {
        val scaleXAnimator = ObjectAnimator.ofFloat(this, "scaleX", 1.0f, 0.0f).apply {
            duration = 300
            interpolator = LinearInterpolator()
        }
        val scaleYAnimator = ObjectAnimator.ofFloat(this, "scaleY", 1.0f, 0.0f).apply {
            duration = 300
            interpolator = LinearInterpolator()
        }
        AnimatorSet().apply {
            playTogether(scaleXAnimator, scaleYAnimator)
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                }

                override fun onAnimationEnd(animation: Animator) {
                    isStart = false
                }

                override fun onAnimationCancel(animation: Animator) {
                }

                override fun onAnimationRepeat(animation: Animator) {
                }
            })
        }.start()
    }

    companion object {
        private const val DEFAULT_WIDTH = 6
        private const val DEFAULT_SHADOW_POSITION = 2
        private const val DEFAULT_SPEED_OF_DEGREE = 10
    }
}

inline fun ViewManager.rotateLoading(theme: Int = 0, init: RotateLoading.() -> Unit): RotateLoading {
    return ankoView({ RotateLoading(it) }, theme, init)
}