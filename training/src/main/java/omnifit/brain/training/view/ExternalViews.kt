package omnifit.brain.training.view

import android.view.ViewManager
import androidx.viewpager2.widget.ViewPager2
import com.github.mikephil.charting.charts.LineChart
import com.google.android.material.tabs.TabLayout
import com.hanks.htextview.fade.FadeTextView
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple
import org.jetbrains.anko.custom.ankoView

inline fun ViewManager.lineChart(theme: Int = 0, init: LineChart.() -> Unit): LineChart {
    return ankoView({ LineChart(it) }, theme, init)
}

inline fun ViewManager.shapeRipple(theme: Int = 0, init: ShapeRipple.() -> Unit): ShapeRipple {
    return ankoView({ ShapeRipple(it) }, theme, init)
}

inline fun ViewManager.fadeTextView(theme: Int = 0, init: FadeTextView.() -> Unit): FadeTextView {
    return ankoView({ FadeTextView(it) }, theme, init)
}

inline fun ViewManager.materialTabLayout(theme: Int = 0, init: TabLayout.() -> Unit): TabLayout {
    return ankoView({ TabLayout(it) }, theme, init)
}

inline fun ViewManager.viewPager2(theme: Int = 0, init: ViewPager2.() -> Unit): ViewPager2 {
    return ankoView({ ViewPager2(it) }, theme, init)
}