package omnifit.brain.training.view

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewManager
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip
import java.nio.DoubleBuffer
import java.util.concurrent.Future
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.math.sin

class HeadsetStateVisualizer(
    context: Context,
    @DrawableRes _icon: Int,
    @DrawableRes vararg _frame: Int
) : View(context) {

    var visualizerOffset: Float = dip(47.75f).toFloat()

    private var icon: Drawable? = null
    private var frame: Array<Drawable?> = Array(_frame.size) { null }

    init {
        isEnabled = false
        isActivated = false
        icon = AppCompatResources.getDrawable(context, _icon)
            ?.apply {
                setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            }
        if (_frame.isNotEmpty()) {
            _frame.mapIndexed { idx, res ->
                frame[idx] = AppCompatResources.getDrawable(context, res)
                    ?.apply {
                        setBounds(0, 0, intrinsicWidth, intrinsicHeight)
                    }
            }
        }
        icon?.let {
            if (visualizerOffset == 0.0f) {
                visualizerOffset = it.intrinsicWidth.toFloat()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(
            when (MeasureSpec.getMode(widthMeasureSpec)) {
                MeasureSpec.AT_MOST     -> icon?.intrinsicWidth ?: 0
                MeasureSpec.EXACTLY     -> MeasureSpec.getSize(widthMeasureSpec)
                MeasureSpec.UNSPECIFIED -> widthMeasureSpec
                else                    -> widthMeasureSpec
            },
            icon?.intrinsicHeight ?: 0
        )
    }

    override fun onDraw(canvas: Canvas) {
        rendering(canvas)
        invalidate()
    }

    fun start() {
        isEnabled = true
    }

    fun stop() {
        isEnabled = false
    }

    fun effectOn() {
        isActivated = true
    }

    fun effectOff() {
        isActivated = false
        p = 0.0f
    }

    private var renderingFinalizer: Disposable? = null
    private val synchronizer: Lock = ReentrantLock()
    private var renderingWorker: Future<Unit>? = null

    companion object {
        const val FRAME_RATE: Long = 16L // 60fps
        const val DEFAULT_AMPLITUDE_SIZE: Double = 8.0
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private val xfermodeSize: Float = dip(37.50f).toFloat()
    private val xfermodePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                color = Color.TRANSPARENT
                xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
            }
    }

    private val drawingWaveBitmap: Bitmap by lazy {
        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    }

    private val drawingWavePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                strokeWidth = dip(2.0f).toFloat()
                color = Color.parseColor("#80ffffff") // 알파값 50%
                style = Paint.Style.STROKE
            }
    }

    private val drawingWavePath: Path by lazy {
        Path()
    }

    private val drawingWaveStartOffset: PointF by lazy {
        PointF(
            icon?.intrinsicWidth?.div(2.0f) ?: 0.0f,
            icon?.intrinsicHeight?.div(2.0f) ?: 0.0f
        )
    }

    private var amplitudeSize: Double = height.toDouble()
        set(value) {
            field = when {
                value > height                 -> height.toDouble()
                value < DEFAULT_AMPLITUDE_SIZE -> DEFAULT_AMPLITUDE_SIZE
                else                           -> value
            }
        }

    /**
     * 진폭
     */
    private val drawingWaveAmplitude: Double
        get() = height / amplitudeSize

    /**
     * 주기
     */
    private val drawingWavePeriod: Int = 2

    /**
     * 기준선
     */
    private val drawingWaveV0: Int
        get() = height / 2

    /**
     * 0 ~ 360도 정현파 변위
     */
    private val referenceSineWave: DoubleBuffer by lazy {
        DoubleBuffer.wrap(
            DoubleArray(361) { angle ->
                sin(Math.toRadians(angle.toDouble()))
            }.apply {
                reverse()
            }
        )
    }

    /**
     *
     */
    private val drawingWaveVelocity: Int = 7

    private fun rendering(c: Canvas) {
        drawWave(c)
        if (checkEffectable()) drawEffect(c)
        else drawIcon(c)
    }

    private var drawingWaveCache: IntArray? = null
    private fun drawWave(canvas: Canvas) {
        checkEnabled()

//        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR) //i::SurfaceView, TextureView 에서만

        drawingWaveCache = drawingWaveCache
            ?.rightShift(drawingWaveVelocity)
            ?.apply {
                for (e in drawingWaveVelocity - 1 downTo 0) {
                    if (referenceSineWave.position() == referenceSineWave.capacity()) {
                        referenceSineWave.rewind()
                    }
                    this[e] = drawingWaveV0 - (drawingWaveAmplitude * referenceSineWave.get()).toInt()
                }
            } ?: IntArray(width / drawingWavePeriod) { drawingWaveV0 }

        drawingWavePath.reset()
        drawingWavePath.let { path ->
            drawingWaveCache?.forEachIndexed { x, y ->
                if (x == 0) {
                    path.moveTo(x.toFloat() + drawingWaveStartOffset.x, y.toFloat())
                }
                else {
                    path.lineTo((x.toFloat() * drawingWavePeriod) + drawingWaveStartOffset.x, y.toFloat())
                }
            }

            drawingWaveBitmap.let { b ->
                b.eraseColor(Color.TRANSPARENT)
                Canvas(b).let { c ->
                    // 사인파 그리기
                    c.drawPath(path, drawingWavePaint)

                    // 아이콘 투명 영역의 사인파 라인 가리기
                    icon?.let { d ->
                        with(
                            PointF(
                                (d.intrinsicWidth - visualizerOffset) / 2,
                                (d.intrinsicHeight - visualizerOffset) / 2
                            )
                        ) {
                            c.drawOval(x, y, x + visualizerOffset, y + visualizerOffset, xfermodePaint)
                        }
                    }
                }
                canvas.drawBitmap(b, 0.0f, 0.0f, null)
            }
        }
    }

    private fun checkEnabled() {
        if (isEnabled) {
            if (amplitudeSize == 0.0) {
                amplitudeSize = height.toDouble()
            }
            if (amplitudeSize > DEFAULT_AMPLITUDE_SIZE) {
                amplitudeSize -= amplitudeSize * 0.08
            }
        }
        else {
            if (amplitudeSize < height) {
                amplitudeSize += 1
            }
        }
    }

    private fun drawIcon(canvas: Canvas) {
        icon?.draw(canvas)
    }

    private fun checkEffectable(): Boolean {
        return isActivated && frame.isNotEmpty()
    }

    private var p: Float = 0.0f
    private var delay: Float = 2.5f
    private fun drawEffect(canvas: Canvas) {
        if (frame.isNotEmpty()) {
            frame[(p++ / delay).toInt()]?.draw(canvas)
            if (p == frame.size * delay) p = 0.0f
        }
    }

    private fun IntArray.rightShift(n: Int): IntArray {
        return IntArray(n) { 0 } + sliceArray(0..size - (n + 1))
    }
}

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

inline fun ViewManager.headsetStateVisualizer(
    theme: Int = 0,
    @DrawableRes icon: Int = 0,
    @DrawableRes vararg frame: Int,
    init: HeadsetStateVisualizer.() -> Unit
): HeadsetStateVisualizer {
    return ankoView({ HeadsetStateVisualizer(it, icon, *frame) }, theme, init)
}