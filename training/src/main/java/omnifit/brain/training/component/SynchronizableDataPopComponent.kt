package omnifit.brain.training.component

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.setPadding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Bindable
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIFrame
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.http.WebApi
import omnifit.brain.training.http.WebApi.RSP_KEY_MEASUREMENT_ID
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.anko.AnkoBindableComponentAdapter
import omnifit.commons.anko.AnkoBindableComponentHolder
import omnifit.commons.http.retrofit.error.HttpExceptionWrapper
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView
import retrofit2.HttpException
import timber.log.Timber

class SynchronizableDataPopComponent<in T : MaterialDialog> : AnkoComponent<T> {

    private var synchronizableProgress: ProgressBar? = null
    private var uploadableDataView: RecyclerView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.sync_screen_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 29.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(62.7f)
                topToTop = PARENT_ID
                topMargin = dip(53.3f)
            }

            // 닫기
            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(30.0f))
                onDebounceClick {
                    if (isUploading()) (context as? UIFrame)?.alert(context, R.string.sync_screen_070, gravity = Gravity.CENTER)
                    else owner.dismiss()
                }
            }.lparams(dip(136), dip(136)) {
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            view {
                id = R.id.dialog_widget_01_id_10
                backgroundColorResource = R.color.x_eaeaea
            }.lparams(matchConstraint,dip(53.3f)) {
                topToBottom = R.id.dialog_widget_01_id_02
            }

            // no
            textView(R.string.sync_screen_020) {
                id = R.id.dialog_widget_01_id_03
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textSize = 22.0f
                textColorResource = R.color.x_353739
            }.lparams(dip(0.0f), wrapContent) {
                startToStart = R.id.dialog_widget_01_id_01
                topToBottom = R.id.dialog_widget_01_id_02
                topMargin = dip(32.0f)
                endToStart = R.id.dialog_widget_01_id_04
                horizontalBias = 0.5f
                horizontalWeight = 1f
            }

            // 이름
            textView(R.string.sync_screen_030) {
                id = R.id.dialog_widget_01_id_04
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textSize = 22.0f
                textColorResource = R.color.x_353739
            }.lparams(dip(0.0f), wrapContent) {
                startToEnd = R.id.dialog_widget_01_id_03
                topToTop = R.id.dialog_widget_01_id_03
                endToStart = R.id.dialog_widget_01_id_05
                bottomToBottom = R.id.dialog_widget_01_id_03
                horizontalBias = 0.5f
                horizontalWeight = 3f
            }

            // 측정타입
            textView(R.string.sync_screen_040) {
                id = R.id.dialog_widget_01_id_05
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textSize = 22.0f
                textColorResource = R.color.x_353739
            }.lparams(dip(0.0f), wrapContent) {
                startToEnd = R.id.dialog_widget_01_id_04
                topToTop = R.id.dialog_widget_01_id_03
                endToStart = R.id.dialog_widget_01_id_06
                bottomToBottom = R.id.dialog_widget_01_id_03
                horizontalBias = 0.5f
                horizontalWeight = 5f
            }

            // 측정일
            textView(R.string.sync_screen_050) {
                id = R.id.dialog_widget_01_id_06
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textSize = 22.0f
                textColorResource = R.color.x_353739
            }.lparams(dip(0.0f), wrapContent) {
                startToEnd = R.id.dialog_widget_01_id_05
                topToTop = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_02
                bottomToBottom = R.id.dialog_widget_01_id_03
                horizontalBias = 0.5f
                horizontalWeight = 3f
            }

            uploadableDataView = recyclerView {
                id = R.id.dialog_widget_01_id_07
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
                    setDrawable(ContextCompat.getDrawable(context, R.drawable.shape_divider_default)!!)
                })
                Measurement.findByUploadableAsync { uploadables ->
                    setHasFixedSize(true)                  // 정적 리스트 데이터 성능용
                    setItemViewCacheSize(uploadables.size) // 정적 리스트 캐시 사이즈
                    adapter = SynchronizedListAdapter(uploadables.toMutableList(), owner)
                }
            }.lparams(dip(0.0f), dip(377.3f)) {
                startToStart = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_06
                topToBottom = R.id.dialog_widget_01_id_04
                topMargin = dip(20.0f)
            }

            synchronizableProgress = horizontalProgressBar {
                id = R.id.dialog_widget_01_id_08
                isIndeterminate = true
                indeterminateTintList = AppCompatResources.getColorStateList(context, R.color.x_2da3f5)
                indeterminateTintMode = PorterDuff.Mode.SRC_ATOP
                isInvisible = true
            }.lparams(dip(0.0f), wrapContent) {
                startToStart = R.id.dialog_widget_01_id_07
                endToEnd = R.id.dialog_widget_01_id_07
                topToBottom = R.id.dialog_widget_01_id_07
                topMargin = dip(10.0f)
            }

            button(R.string.sync_screen_060) {
                id = R.id.dialog_widget_01_id_09
                backgroundResource = R.drawable.selector_bg_r10_btn_03
                textSize = 32.0f
                textColorResource = R.color.x_ffffff
                typeface = Font.kopubDotumMedium
                stateListAnimator = null
                onDebounceClick {
                    if (isUploading()) (context as? UIFrame)?.alert(context, R.string.sync_screen_070, gravity = Gravity.CENTER)
                    else doUploading(
                        context,
                        { owner.dismiss() },
                        // TODO : 동기화 진행중 중단에 따른 UI 처리가 필요한 경우 여기서...
                        { e -> }
                    )
                }
            }.lparams(dip(0.0f), dip(74.3f)) {
                startToStart = R.id.dialog_widget_01_id_08
                endToEnd = R.id.dialog_widget_01_id_08
                topToBottom = R.id.dialog_widget_01_id_08
                horizontalBias = 0.5f
            }

            view {}.lparams(dip(5.0f), dip(30.0f)) {
                topToBottom = R.id.dialog_widget_01_id_09
            }
        }
    }

    @ExperimentalUnsignedTypes
    private fun doUploading(
        context: Context,
        onComplete: (List<Measurement>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        (uploadableDataView?.adapter as? SynchronizedListAdapter)
            ?.let { adapter ->
                showUploadingProgress()
                WebApi.bulkUploadServiceResult(
                    context,
                    context as LifecycleOwner,
                    adapter.items,
                    { rsp ->
                        rsp[RSP_KEY_MEASUREMENT_ID]
                            ?.let { id ->
                                Handler().post {
                                    adapter
                                        .items
                                        .indexOfFirst { m ->
                                            m.id == id
                                        }
                                        .let { position ->
                                            uploadableDataView?.smoothScrollToPosition(position)
                                            adapter.items.size.minus(position.inc())
                                                .let { badgeCount ->
                                                    Bindable.uploadableBadgeCount.value = badgeCount.toString()
                                                }
                                        }
                                }
                            }
                    },
                    { e ->
                        hideUploadingProgress()
                        when (e) {
                            is NoSuchElementException -> onComplete(emptyList())
                            is HttpException          -> onError(HttpExceptionWrapper(e))
                            else                      -> onError(e)
                        }
                    },
                    {
                        Timber.e("--> 동기화 완료 후 리스트 : $it")
                        onComplete(it)
                    }
                )
            }
    }

    private fun showUploadingProgress() {
        synchronizableProgress?.isInvisible = false
    }

    private fun hideUploadingProgress() {
        synchronizableProgress?.isInvisible = true
    }

    private fun isUploading(): Boolean {
        return !(synchronizableProgress?.isInvisible ?: true)
    }
}

class SynchronizedListAdapter constructor(
    items: MutableList<Measurement>,
    owner: MaterialDialog
) : AnkoBindableComponentAdapter<MutableList<Measurement>, MaterialDialog, SynchronizedListAdapter.ComponentHolder>(
    items,
    owner
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(SynchronizableDataItemComponent(parent.context, owner))
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<Measurement, MaterialDialog>
    ) : AnkoBindableComponentHolder<Measurement, MaterialDialog>(component)
}