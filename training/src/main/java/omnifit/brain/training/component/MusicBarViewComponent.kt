package omnifit.brain.training.component

import android.animation.LayoutTransition
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIScreen
import omnifit.brain.training.helper.SOUND_TYPE_NATURE
import omnifit.brain.training.helper.SOUND_TYPE_NORMAL
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.common.BaseObservableProperty
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onTouch

class MusicBarViewComponent<in T : UIScreen> : AnkoComponent<T> {
    private var summary: TextView? = null
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.screen_inner_widget_01_id_root_container
            layoutTransition = LayoutTransition()
                .apply {
                    enableTransitionType(LayoutTransition.CHANGING)
                }
            backgroundColorResource = R.color.x_000000_op15
            onTouch(returnValue = true) { _, _ ->
            }

            summary = textView {
                id = R.id.screen_inner_widget_01_id_01
                includeFontPadding = false
                setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_music_bar_on, 0, 0, 0)
                compoundDrawablePadding = dip(18.7f)
                gravity = Gravity.CENTER_VERTICAL
                bindMusic(owner, music)
            }.lparams(matchConstraint, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(42.7f)
                topToTop = PARENT_ID
                bottomToBottom = PARENT_ID
                verticalBias = 0.5f
            }

        }.applyRecursively { v ->
            (v as? ConstraintLayout)?.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, dip(70.7f)).apply {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }
        }
    }

    val music: BaseObservableProperty<Pair<Int, AudioPlayer.AudioSource>> by lazy {
        BaseObservableProperty(SOUND_TYPE_NORMAL to AudioPlayer.AudioSource(source = Any()))
    }

    private fun <T : TextView> T.bindMusic(
        owner: LifecycleOwner,
        bindable: BaseObservableProperty<Pair<Int, AudioPlayer.AudioSource>>
    ) {
        fun bind(v: Pair<Int, AudioPlayer.AudioSource>) {
            context.runOnUiThread {
                text = ""
                postDelayed(
                    {
                        text = Spanner()
                            .apply {
                                val sp_1 = 19
                                val sp_2 = 16
                                val typeface_1 = Font.CustomTypefaceSpan(Font.kopubDotumBold)
                                val typeface_2 = Font.CustomTypefaceSpan(Font.kopubDotumMedium)
                                val color_1 = ResourcesCompat.getColor(resources, R.color.x_ffffff, null)
                                val color_2 = ResourcesCompat.getColor(resources, R.color.x_9a9a9a, null)
                                val (mode, source) = v
                                getString(
                                    R.string.ai_concentration_assistant_training_screen_100,
                                    when (mode) {
                                        SOUND_TYPE_NORMAL -> getString(R.string.ai_concentration_assistant_training_screen_070)
                                        SOUND_TYPE_NATURE -> getString(R.string.ai_concentration_assistant_training_screen_080)
                                        else              -> getString(R.string.ai_concentration_assistant_training_screen_090)
                                    },
                                    source.title
                                )
                                    .split('|')
                                    .let { strings ->
                                        append(strings[0], Spans.sizeSP(sp_1), Spans.custom(typeface_1), Spans.foreground(color_1))
                                        append(strings[1], Spans.sizeSP(sp_1), Spans.custom(typeface_2), Spans.foreground(color_1))
                                        append(strings[2], Spans.sizeSP(sp_2), Spans.custom(typeface_2), Spans.foreground(color_2))
                                    }
                            }
                    }, 300L // LayoutTransition.CHANGING's default duration is 300L
                )
            }
        }

        val valueChangeListener: (Pair<Int, AudioPlayer.AudioSource>) -> Unit = { v ->
            bind(v)
        }

        bind(bindable.value)
        bindable.addValueChangeListener(valueChangeListener)
        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun unbind() {
                bindable.removeValueChangeListener(valueChangeListener)
                owner.lifecycle.removeObserver(this)
            }
        })
    }
}