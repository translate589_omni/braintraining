package omnifit.brain.training.component

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.setPadding
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class AIConcentrationAssistantTrainingGuidePopComponent<in T : MaterialDialog> : AnkoComponent<T> {

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.ai_concentration_assistant_training_screen_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumBold
                textSize = 33.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_04
                topToTop = PARENT_ID
                topMargin = dip(52.0f)
            }

            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(10.0f))
                onDebounceClick {
                    owner.dismiss()
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = PARENT_ID
                topMargin = dip(36.7f - 10.0f)
                endToEnd = PARENT_ID
                marginEnd = dip(44.7f - 10.0f)
            }

            imageView {
                id = R.id.dialog_widget_01_id_03
                backgroundResource = R.drawable.stress_center_01
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_04
                topToTop = PARENT_ID
                topMargin = dip(129.3f)
            }

            textView(R.string.ai_concentration_assistant_training_screen_160) {
                id = R.id.dialog_widget_01_id_10
                textSize = 25.3f
                textColorResource = R.color.x_1c1c1c
                typeface = Font.kopubDotumMedium
                setLineSpacing(sp(44).toFloat(),0f)
            }.lparams(wrapContent, wrapContent) {
                marginStart = dip(34)
                startToEnd = R.id.dialog_widget_01_id_03
                topToTop = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_03
            }

            textView(R.string.ai_concentration_assistant_training_screen_170) {
                textSize = 16.7f
                textColorResource = R.color.x_898989
                typeface = Font.kopubDotumMedium
            }.lparams(0, wrapContent) {
                topMargin = dip(14)
                startToStart = R.id.dialog_widget_01_id_10
                endToEnd = R.id.dialog_widget_01_id_10
                topToBottom = R.id.dialog_widget_01_id_10
            }

            view {
                id = R.id.dialog_widget_01_id_04
                backgroundResource = R.drawable.shape_r40_f8f8f8
            }.lparams(dip(888.0f), dip(167.3f)) {
                startToStart = PARENT_ID
                marginStart = dip(46.0f)
                topToTop = PARENT_ID
                topMargin = dip(344.0f)
                endToEnd = PARENT_ID
                marginEnd = dip(46.0f)
                bottomToBottom = PARENT_ID
                bottomMargin = dip(54.0f)
            }

            imageView(R.drawable.ic_ai_concentration_assistant_training_guide_mode_1) {
                id = R.id.dialog_widget_01_id_05
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_04
                marginStart = dip(36.7f)
                topToTop = R.id.dialog_widget_01_id_04
                topMargin = dip(16.0f)
            }

            imageView(R.drawable.ic_ai_concentration_assistant_training_guide_mode_2) {
                id = R.id.dialog_widget_01_id_06
            }.lparams(wrapContent, wrapContent) {
                endToEnd = R.id.dialog_widget_01_id_04
                marginEnd = dip(12.7f)
                bottomToBottom = R.id.dialog_widget_01_id_04
            }

            nonSpacingTextView(R.string.ai_concentration_assistant_training_screen_120) {
                id = R.id.dialog_widget_01_id_07
                typeface = Font.kopubDotumBold
                textSize = 36.0f
                textColorResource = R.color.x_2fa1f6
                includeFontPadding = false
                lines = 1
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.dialog_widget_01_id_05
                marginStart = dip(22.0f)
                topToTop = R.id.dialog_widget_01_id_05
                bottomToBottom = R.id.dialog_widget_01_id_05
                verticalBias = 0.5f
            }

            nonSpacingTextView(R.string.ai_concentration_assistant_training_screen_130) {
                id = R.id.dialog_widget_01_id_08
                typeface = Font.kopubDotumMedium
                textSize = 21.0f
                textColorResource = R.color.x_0f568a
                letterSpacing = -0.01f
                includeFontPadding = false
                lines = 1
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_05
                topToBottom = R.id.dialog_widget_01_id_05
                topMargin = dip(12.7f)
            }

            nonSpacingTextView(R.string.ai_concentration_assistant_training_screen_140) {
                id = R.id.dialog_widget_01_id_09
                typeface = Font.kopubDotumMedium
                textSize = 16.7f
                textColorResource = R.color.x_898989
                includeFontPadding = false
                lines = 1
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_05
                topToBottom = R.id.dialog_widget_01_id_05
                topMargin = dip(46.7f)
            }
        }
    }
}