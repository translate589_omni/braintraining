package omnifit.brain.training.component

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_PACKED
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.UIScreen
import omnifit.brain.training.algorithm.ServiceAlgorithm
import omnifit.brain.training.db.model.GameContent
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.helper.ContentHelper
import omnifit.brain.training.view.effectiveTimeGraph
import omnifit.brain.training.view.lineChart
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.commons.algorithm.Algorithm
import omnifit.commons.algorithm.Indicator
import omnifit.commons.common.DTP_HH_MM_SS
import omnifit.commons.common.DTP_MM_SS
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.millisToFormatString
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

class ResultSummaryComponent<T : UIScreen>(
    val result: ServiceResult
) : AnkoComponent<T> {

    private val resultIndicator: Indicator.Content.Result by lazy {
        Algorithm.indicatorOf<Indicator.Content.Result>(result.score.toDouble())
    }

    private var totalScoreLabel: String = ""
    private var graphLegend: String = ""
    private var cautionMessage: String = ""
    private var effectiveTrainingTimeLabel: String = ""
    private var contentTitle: String = ""

    @DrawableRes
    private var serviceCategoryIconResource: Int = -1
    private var totalTrainingTime: Long = 0L
    private var optionalTrainingTime: Long = 0L
    private var effectiveTrainingTime: Long = 0L
    private var effectiveTrainingRatio: Int = 0

    private var graphTitle:String = ""
    private var graphXLabelHigh: String = ""
    private var graphXLabelLow: String = ""

    private lateinit var scrollGuideView:ImageView

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        with(owner.requireContext()) {
            when (ServiceCategory.of(result.category)) {
                ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> {
//                    serviceCategoryLabel = getString(R.string.main_screen_031)
//                    serviceCategoryIconResource = R.drawable.ic_result_con
                    totalScoreLabel = getString(R.string.content_result_screen_023)
                    graphLegend = getString(R.string.content_result_screen_091)
                    cautionMessage = getString(R.string.content_result_screen_121)
                    effectiveTrainingTimeLabel = getString(R.string.content_result_screen_110)
                    optionalTrainingTime = result.optionalTime
//                    graphXLabelHigh = getString(R.string.content_result_screen_170)
//                    graphXLabelLow = getString(R.string.content_result_screen_180)
                    graphTitle = getString(R.string.content_result_screen_081)
                }
                ServiceCategory.CONCENTRATION_TRAINING              -> {
//                    serviceCategoryLabel = getString(R.string.main_screen_040)
                    serviceCategoryIconResource = R.drawable.ic_result_con
                    totalScoreLabel = getString(R.string.content_result_screen_021)
                    graphLegend = getString(R.string.content_result_screen_090)
                    cautionMessage = getString(R.string.content_result_screen_120)
                    effectiveTrainingTimeLabel = getString(R.string.content_result_screen_110)
                    graphTitle = getString(R.string.content_result_screen_080)
                    contentTitle = GameContent.findBySequence(result.contentSequence)?.title ?: ""
                }
                ServiceCategory.BRAIN_STABILITY_TRAINING            -> {
//                    serviceCategoryLabel = getString(R.string.main_screen_060)
                    serviceCategoryIconResource = R.drawable.ic_result_hea
                    totalScoreLabel = getString(R.string.content_result_screen_020)
                    graphLegend = getString(R.string.content_result_screen_093)
                    cautionMessage = getString(R.string.content_result_screen_120)
                    effectiveTrainingTimeLabel = getString(R.string.content_result_screen_111)
//                    graphXLabelHigh = getString(R.string.content_result_screen_170)
//                    graphXLabelLow = getString(R.string.content_result_screen_180)
                    graphTitle = getString(R.string.content_result_screen_080)
                    contentTitle = ContentHelper.getNormalSoundContents().get(result.contentSequence).albumTitle
                }
                ServiceCategory.NEURO_FEEDBACK                      -> {
//                    serviceCategoryLabel = getString(R.string.main_screen_080)
                    serviceCategoryIconResource = R.drawable.ic_result_therapy
                    totalScoreLabel = getString(R.string.content_result_screen_022)
                    graphLegend = getString(R.string.content_result_screen_092)
                    cautionMessage = getString(R.string.content_result_screen_120)
                    effectiveTrainingTimeLabel = getString(R.string.content_result_screen_112)
//                    graphXLabelHigh = getString(R.string.content_result_screen_190)
//                    graphXLabelLow = getString(R.string.content_result_screen_200)
                    graphTitle = getString(R.string.content_result_screen_080)
                    contentTitle = ContentHelper.getNeuroFeedbackSoundContents().get(result.contentSequence).albumTitle
                }
            }
            totalTrainingTime = result.elapsedTime
            effectiveTrainingTime = result.effectiveTime
            effectiveTrainingRatio = result.effectiveTimeRatio
        }

        constraintLayout {
            // 종합점수 레이어
            nonSpacingTextView(totalScoreLabel) {
                id = R.id.screen_inner_widget_01_id_04
                typeface = Font.kopubDotumBold
                textSize = 37.7f
                letterSpacing = -0.01f
                textColorResource = R.color.x_2e2e2e
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                marginStart = dip(80.7f)
                startToStart = PARENT_ID
                topToTop = PARENT_ID
            }

            cardView {
                id = R.id.screen_inner_widget_01_id_03
                cardElevation = dip(5.0f).toFloat()
                backgroundResource = R.drawable.shape_r26_7_ffffff

                constraintLayout {
                    // 종합 점수 타이틀
                    backgroundResource = R.drawable.shape_r26_7_ffffff

                    // 명상 종합 점수 등급
                    nonSpacingTextView(resultIndicator.label) {
                        id = R.id.screen_inner_widget_01_id_05
                        typeface = Font.latoRegular
                        textSize = 76.0f
                        textColor = getIndicatorColor()
                        letterSpacing = -0.03f
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToTop = PARENT_ID
                        topMargin = dip(54f)
                        endToEnd = PARENT_ID
                    }

                    // 명상 종합 점수 라벨
                    textView(R.string.content_result_screen_070) {
                        id = R.id.screen_inner_widget_01_id_06
                        typeface = Font.latoRegular
                        textSize = 22.7f
                        textColorResource = R.color.x_abafb9
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_inner_widget_01_id_05
                        topMargin = dip(38.7f)
                        endToStart = R.id.screen_inner_widget_01_id_07
                        horizontalChainStyle = CHAIN_PACKED
                        horizontalBias = 0.5f
                    }

                    // 명상 종합 점수
                    nonSpacingTextView(result.score.toString()) {
                        id = R.id.screen_inner_widget_01_id_07
                        typeface = Font.latoRegular
                        textSize = 40.0f
                        textColorResource = R.color.x_232323
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_01_id_06
                        marginStart = dip(16.0f)
                        endToEnd = PARENT_ID
                        bottomToBottom = R.id.screen_inner_widget_01_id_06
                    }

                    constraintLayout {
                        backgroundResource = R.drawable.shape_bg_result_component

                        //----------------------------------------------------------

                        val excellent = Indicator.Content.Result.Excellent()

                        // 등급 1 하이라이트
                        constraintLayout {
                            id = R.id.screen_inner_widget_01_id_08
                            backgroundColor = excellent.highlightIfResultMatched()

                            imageView(R.drawable.ic_footnote) {
                                isVisible = excellent.isResultMatched()
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(16.7f)
                                topToTop = PARENT_ID
                                bottomToBottom = PARENT_ID
                            }
                        }.lparams(matchConstraint, dip(37.3f)) {
                            startToStart = PARENT_ID
                            marginStart = dip(0.2f)
                            topToTop = PARENT_ID
                            topMargin = dip(30.0f)
                            endToEnd = PARENT_ID
                            marginEnd = dip(0.2f)
                        }

                        // 등급 1 라벨
                        nonSpacingTextView(excellent.label) {
                            id = R.id.screen_inner_widget_01_id_09
                            typeface = excellent.labelTypeface()
                            textSize = 21.0f
                            textColorResource = excellent.labelColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_08
                            marginStart = dip(54.7f)
                            topToTop = R.id.screen_inner_widget_01_id_08
                            bottomToBottom = R.id.screen_inner_widget_01_id_08
                        }

                        // 등급 1 점수 범위
                        nonSpacingTextView(excellent.printRange()) {
                            typeface = Font.latoRegular
                            textSize = 22.0f
                            textColorResource = excellent.rangeColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_09
                            marginStart = dip(160.0f)
                            topToTop = R.id.screen_inner_widget_01_id_09
                            bottomToBottom = R.id.screen_inner_widget_01_id_09
                        }

                        //----------------------------------------------------------

                        val veryGood = Indicator.Content.Result.VeryGood()

                        // 등급 2 하이라이트
                        constraintLayout {
                            id = R.id.screen_inner_widget_01_id_10
                            backgroundColor = veryGood.highlightIfResultMatched()

                            imageView(R.drawable.ic_footnote) {
                                isVisible = veryGood.isResultMatched()
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(16.7f)
                                topToTop = PARENT_ID
                                bottomToBottom = PARENT_ID
                            }
                        }.lparams(matchConstraint, dip(37.3f)) {
                            startToStart = PARENT_ID
                            marginStart = dip(0.2f)
                            topToBottom = R.id.screen_inner_widget_01_id_08
                            endToEnd = PARENT_ID
                            marginEnd = dip(0.2f)
                        }

                        // 등급 2 라벨
                        nonSpacingTextView(veryGood.label) {
                            id = R.id.screen_inner_widget_01_id_11
                            typeface = veryGood.labelTypeface()
                            textSize = 21.0f
                            textColorResource = veryGood.labelColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_10
                            marginStart = dip(54.7f)
                            topToTop = R.id.screen_inner_widget_01_id_10
                            bottomToBottom = R.id.screen_inner_widget_01_id_10
                        }

                        // 등급 2 점수 범위
                        nonSpacingTextView(veryGood.printRange()) {
                            typeface = Font.latoRegular
                            textSize = 22.0f
                            textColorResource = veryGood.rangeColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_11
                            marginStart = dip(160.0f)
                            topToTop = R.id.screen_inner_widget_01_id_11
                            bottomToBottom = R.id.screen_inner_widget_01_id_11
                        }

                        //----------------------------------------------------------

                        val good = Indicator.Content.Result.Good()

                        // 등급 3 하이라이트
                        constraintLayout {
                            id = R.id.screen_inner_widget_01_id_12
                            backgroundColor = good.highlightIfResultMatched()

                            imageView(R.drawable.ic_footnote) {
                                isVisible = good.isResultMatched()
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(16.7f)
                                topToTop = PARENT_ID
                                bottomToBottom = PARENT_ID
                            }
                        }.lparams(matchConstraint, dip(37.3f)) {
                            startToStart = PARENT_ID
                            marginStart = dip(0.2f)
                            topToBottom = R.id.screen_inner_widget_01_id_10
                            endToEnd = PARENT_ID
                            marginEnd = dip(0.2f)
                        }

                        // 등급 3 라벨
                        nonSpacingTextView(good.label) {
                            id = R.id.screen_inner_widget_01_id_13
                            typeface = good.labelTypeface()
                            textSize = 21.0f
                            textColorResource = good.labelColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_12
                            marginStart = dip(54.7f)
                            topToTop = R.id.screen_inner_widget_01_id_12
                            bottomToBottom = R.id.screen_inner_widget_01_id_12
                        }

                        // 등급 3 점수 범위
                        nonSpacingTextView(good.printRange()) {
                            typeface = Font.latoRegular
                            textSize = 22.0f
                            textColorResource = good.rangeColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_13
                            marginStart = dip(160.0f)
                            topToTop = R.id.screen_inner_widget_01_id_13
                            bottomToBottom = R.id.screen_inner_widget_01_id_13
                        }

                        //----------------------------------------------------------

                        val notGood = Indicator.Content.Result.NotGood()

                        // 등급 4 하이라이트
                        constraintLayout {
                            id = R.id.screen_inner_widget_01_id_14
                            backgroundColor = notGood.highlightIfResultMatched()

                            imageView(R.drawable.ic_footnote) {
                                isVisible = notGood.isResultMatched()
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(16.7f)
                                topToTop = PARENT_ID
                                bottomToBottom = PARENT_ID
                            }
                        }.lparams(matchConstraint, dip(37.3f)) {
                            startToStart = PARENT_ID
                            marginStart = dip(0.2f)
                            topToBottom = R.id.screen_inner_widget_01_id_12
                            endToEnd = PARENT_ID
                            marginEnd = dip(0.2f)
                        }

                        // 등급 4 라벨
                        nonSpacingTextView(notGood.label) {
                            id = R.id.screen_inner_widget_01_id_15
                            typeface = notGood.labelTypeface()
                            textSize = 21.0f
                            textColorResource = notGood.labelColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_14
                            marginStart = dip(54.7f)
                            topToTop = R.id.screen_inner_widget_01_id_14
                            bottomToBottom = R.id.screen_inner_widget_01_id_14
                        }

                        // 등급 4 점수 범위
                        nonSpacingTextView(notGood.printRange()) {
                            typeface = Font.latoRegular
                            textSize = 22.0f
                            textColorResource = notGood.rangeColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_15
                            marginStart = dip(160.0f)
                            topToTop = R.id.screen_inner_widget_01_id_15
                            bottomToBottom = R.id.screen_inner_widget_01_id_15
                        }

                        //----------------------------------------------------------

                        val bad = Indicator.Content.Result.Bad()

                        // 등급 5 하이라이트
                        constraintLayout {
                            id = R.id.screen_inner_widget_01_id_16
                            backgroundColor = bad.highlightIfResultMatched()

                            imageView(R.drawable.ic_footnote) {
                                isVisible = bad.isResultMatched()
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(16.7f)
                                topToTop = PARENT_ID
                                bottomToBottom = PARENT_ID
                            }
                        }.lparams(matchConstraint, dip(37.3f)) {
                            startToStart = PARENT_ID
                            marginStart = dip(0.2f)
                            topToBottom = R.id.screen_inner_widget_01_id_14
                            endToEnd = PARENT_ID
                            marginEnd = dip(0.2f)
                        }

                        // 등급 5 라벨
                        nonSpacingTextView(bad.label) {
                            id = R.id.screen_inner_widget_01_id_17
                            typeface = bad.labelTypeface()
                            textSize = 21.0f
                            textColorResource = bad.labelColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_16
                            marginStart = dip(54.7f)
                            topToTop = R.id.screen_inner_widget_01_id_16
                            bottomToBottom = R.id.screen_inner_widget_01_id_16
                        }

                        // 등급 5 점수 범위
                        nonSpacingTextView(bad.printRange()) {
                            typeface = Font.latoRegular
                            textSize = 22.0f
                            textColorResource = bad.rangeColorResource()
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_01_id_17
                            marginStart = dip(160.0f)
                            topToTop = R.id.screen_inner_widget_01_id_17
                            bottomToBottom = R.id.screen_inner_widget_01_id_17
                        }

                        //----------------------------------------------------------

                    }.lparams(dip(338.7f), dip(245.3f)) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_inner_widget_01_id_06
                        topMargin = dip(32f)
                        endToEnd = PARENT_ID
                    }

                }.lparams(matchParent, matchParent)
            }.lparams(dip(413.3f), dip(484.7f)) {
                startToStart = PARENT_ID
                marginStart = dip(78f)
                topToBottom = R.id.screen_inner_widget_01_id_04
                topMargin = dip(26.7f)
            }

            // 학습 중 뇌파변화
            cardView {
                id = R.id.screen_inner_widget_01_id_18
                cardElevation = dip(5.0f).toFloat()
                backgroundResource = R.drawable.shape_r26_7_6c80ff_2ba4f5

                constraintLayout {
                    // 타이틀
                    nonSpacingTextView(graphTitle) {
                        id = R.id.screen_inner_widget_01_id_19
                        typeface = Font.kopubDotumBold
                        letterSpacing = -0.1f
                        textSize = 22.7f
                        textColorResource = R.color.x_ffffff
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        marginStart = dip(38.7f)
                        topToTop = PARENT_ID
                        topMargin = dip(38.0f)
                    }

                    // 깊은 뇌파 구간 구분색
                    view {
                        backgroundColorResource = R.color.x_4ab6ff
                    }.lparams(dip(22.7f), dip(4.7f)) {
                        topToTop = R.id.screen_inner_widget_01_id_20
                        endToStart = R.id.screen_inner_widget_01_id_20
                        marginEnd = dip(10.7f)
                        bottomToBottom = R.id.screen_inner_widget_01_id_20
                    }

                    // 깊은 뇌파 구간 타이틀
                    nonSpacingTextView(graphLegend) {
                        id = R.id.screen_inner_widget_01_id_20
                        typeface = Font.kopubDotumMedium
                        textSize = 16.0f
                        textColorResource = R.color.x_ffffff
                        setLineSpacing(sp(60).toFloat(), 0f)
                        includeFontPadding = false
                        lines = 1
                    }.lparams(wrapContent, wrapContent) {
                        topToTop = R.id.screen_inner_widget_01_id_19
                        endToEnd = PARENT_ID
                        marginEnd = dip(41.3f)
                        bottomToBottom = R.id.screen_inner_widget_01_id_19
                    }

                    imageView {
                        id = R.id.screen_inner_widget_01_id_32
                        backgroundResource = R.drawable.ic_graph_line
                        rotation = when(ServiceCategory.of(result.category)) {
                            ServiceCategory.BRAIN_STABILITY_TRAINING -> 180f
                            else -> 0f
                        }
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = R.id.screen_inner_widget_01_id_19
                        topToBottom = R.id.screen_inner_widget_01_id_19
                        topMargin = dip(16.0f)
                        endToEnd = R.id.screen_inner_widget_01_id_20
                    }

                    // 라인차트
                    lineChart {
                        id = R.id.screen_inner_widget_01_id_30

                        setViewPortOffsets(0.0f, 0.0f, 0.0f, 0.0f)
                        setDrawGridBackground(false)
                        description.isEnabled = false
                        legend.isEnabled = false
                        setPinchZoom(false)
                        setScaleEnabled(false)

                        setTouchEnabled(true)
                        isDragXEnabled = true
                        isDragYEnabled = false

                        axisRight.isEnabled = false

                        xAxis.setDrawAxisLine(false)
                        xAxis.setDrawLabels(false)
                        xAxis.setDrawGridLines(false)

                        axisLeft.setDrawAxisLine(false)
                        axisLeft.setDrawLabels(false)
                        axisLeft.setDrawGridLines(false)
//                        axisLeft.labelCount = 15
                        axisLeft.labelCount = 11
                        axisLeft.axisMaximum = 10.0f
//                        axisLeft.axisMinimum = -4.0f
                        axisLeft.axisMinimum = 0f

                        data = LineData(generateLineData(result.indicatorValues) { x -> setScaleMinima(x, 0.0f) })
                    }.lparams(0, 0) {
                        startToStart = R.id.screen_inner_widget_01_id_32
                        endToEnd = R.id.screen_inner_widget_01_id_32
                        topToTop = R.id.screen_inner_widget_01_id_32
                        bottomToBottom = R.id.screen_inner_widget_01_id_32
                    }

//                    nonSpacingTextView(graphXLabelHigh) {
//                        id = R.id.screen_inner_widget_01_id_32
//                        typeface = Font.kopubDotumMedium
//                        textSize = 13.3f
//                        textColorResource = R.color.x_ffffff_op40
//                        letterSpacing = -0.05f
//                        includeFontPadding = false
//                        lines = 1
//                    }.lparams(wrapContent, wrapContent) {
//                        startToStart = R.id.screen_inner_widget_01_id_19
//                        topToTop = R.id.screen_inner_widget_01_id_30
//                        topMargin = dip(8.3f)
//                    }
//
//                    nonSpacingTextView(graphXLabelLow) {
//                        id = R.id.screen_inner_widget_01_id_33
//                        typeface = Font.kopubDotumMedium
//                        textSize = 13.3f
//                        textColorResource = R.color.x_ffffff_op40
//                        letterSpacing = -0.05f
//                        includeFontPadding = false
//                        lines = 1
//                    }.lparams(wrapContent, wrapContent) {
//                        startToStart = R.id.screen_inner_widget_01_id_32
//                        bottomToTop = R.id.screen_inner_widget_01_id_34
//                        bottomMargin = dip(8.3f)
//                    }
//
//                    nonSpacingTextView(R.string.content_result_screen_210) {
//                        id = R.id.screen_inner_widget_01_id_34
//                        typeface = Font.kopubDotumMedium
//                        textSize = 13.3f
//                        textColorResource = R.color.x_ffffff_op40
//                        letterSpacing = -0.05f
//                        includeFontPadding = false
//                    }.lparams(wrapContent, wrapContent) {
//                        startToStart = R.id.screen_inner_widget_01_id_32
//                        bottomToBottom = R.id.screen_inner_widget_01_id_30
//                    }

                    // 스크롤 가이드 아이콘
                    scrollGuideView = imageView(R.drawable.baseline_touch_app_white_36) {
                        id = R.id.screen_inner_widget_01_id_31
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = R.id.screen_inner_widget_01_id_30
                        topToTop = R.id.screen_inner_widget_01_id_30
                        endToEnd = R.id.screen_inner_widget_01_id_30
                        bottomToBottom = R.id.screen_inner_widget_01_id_30
                        horizontalBias = 0.5f
                        verticalBias = 0.5f
                    }
                }.lparams(matchParent, matchParent)
            }.lparams(dip(681.3f), dip(223.3f)) {
                startToEnd = R.id.screen_inner_widget_01_id_03
                marginStart = dip(28.0f)
                topToTop = R.id.screen_inner_widget_01_id_03
            }

            // 학습 전체 시간
            cardView {
                id = R.id.screen_inner_widget_01_id_21
                cardElevation = dip(5.0f).toFloat()
                backgroundResource = R.drawable.shape_r26_7_ffffff

                constraintLayout {
                    // 타이틀
                    nonSpacingTextView(R.string.content_result_screen_100) {
                        id = R.id.screen_inner_widget_01_id_22
                        typeface = Font.kopubDotumBold
                        letterSpacing = -0.1f
                        textSize = 22.7f
                        textColorResource = R.color.x_afb3c0
                        includeFontPadding = false
                        lines = 1
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        marginStart = dip(32.0f)
                        topToTop = PARENT_ID
                        topMargin = dip(36.7f)
                    }

                    // 전체 학습 시간
                    nonSpacingTextView(totalTrainingTime.millisToFormatString(DTP_HH_MM_SS)) {
                        id = R.id.screen_inner_widget_01_id_23
                        typeface = Font.latoBold
                        textSize = when (ServiceCategory.of(result.category)) {
                            ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> 40f
                            else                                                -> 48f
                        }
                        textColorResource = R.color.x_2e2e2e
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_inner_widget_01_id_22
                        topMargin = dip(
                            when (ServiceCategory.of(result.category)) {
                                ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> 17.65f
                                else                                                -> 35.3f
                            }
                        )
                        endToEnd = PARENT_ID
                    }

                    when (ServiceCategory.of(result.category)) {
                        ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> {
                            // 타이틀
                            nonSpacingTextView(R.string.ai_concentration_assistant_training_screen_110) {
                                id = R.id.screen_inner_widget_01_id_25
                                typeface = Font.kopubDotumBold
                                letterSpacing = -0.1f
                                textSize = 22.7f
                                textColorResource = R.color.x_afb3c0
                                includeFontPadding = false
                                lines = 1
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_01_id_22
                                topToBottom = R.id.screen_inner_widget_01_id_22
                                topMargin = dip(75.35f)
                            }

                            // AI 모드 이용 시간
                            nonSpacingTextView(optionalTrainingTime.millisToFormatString(DTP_HH_MM_SS)) {
                                id = R.id.screen_inner_widget_01_id_26
                                typeface = Font.latoBold
                                textSize = 40.0f
                                textColorResource = R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                topToBottom = R.id.screen_inner_widget_01_id_25
                                topMargin = dip(17.65f)
                                endToEnd = PARENT_ID
                            }
                        }
                        else                                                -> {
                            // 콘텐츠 타이틀
                            textView(contentTitle) {
                                id = R.id.screen_inner_widget_01_id_24
                                typeface = Font.kopubDotumMedium
                                textSize = 17.0f
                                textColorResource = R.color.x_2e2e2e_op60
                                includeFontPadding = false
                                gravity = Gravity.CENTER_VERTICAL
                                setCompoundDrawablesWithIntrinsicBounds(serviceCategoryIconResource, 0, 0, 0)
                                compoundDrawablePadding = dip(7.3f)
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_01_id_22
                                bottomToBottom = PARENT_ID
                                bottomMargin = dip(34.7f)
                            }
                        }
                    }
                }.lparams(matchParent, matchParent)
            }.lparams(dip(294.7f), dip(233.3f)) {
                startToStart = R.id.screen_inner_widget_01_id_18
                bottomToBottom = R.id.screen_inner_widget_01_id_03
            }

            // 깊은 학습 시간
            cardView {
                id = R.id.screen_inner_widget_01_id_26
                cardElevation = dip(5.0f).toFloat()
                backgroundResource = R.drawable.shape_r26_7_ffffff

                constraintLayout {
                    // 타이틀
                    nonSpacingTextView(effectiveTrainingTimeLabel) {
                        id = R.id.screen_inner_widget_01_id_27
                        typeface = Font.kopubDotumBold
                        letterSpacing = -0.1f
                        textSize = 22.7f
                        textColorResource = R.color.x_afb3c0
                        includeFontPadding = false
                        lines = 1
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        marginStart = dip(32.0f)
                        topToTop = PARENT_ID
                        topMargin = dip(30.7f)
                    }

                    // 명상시간
                    nonSpacingTextView(effectiveTrainingTime.millisToFormatString(DTP_MM_SS)) {
                        id = R.id.screen_inner_widget_01_id_28
                        typeface = Font.latoBold
                        textSize = 48.0f
                        textColorResource = R.color.x_2e2e2e
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_inner_widget_01_id_27
                        topMargin = dip(35.3f)
                        endToEnd = PARENT_ID
                    }

                    effectiveTimeGraph {
                        totalTime = totalTrainingTime
                        effectiveTime = effectiveTrainingTime
                        effectiveRatio = effectiveTrainingRatio
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_inner_widget_01_id_28
                        topMargin = dip(15.0f)
                        endToEnd = PARENT_ID
                    }

                }.lparams(matchParent, matchParent)
            }.lparams(dip(359.3f), 0) {
                endToEnd = R.id.screen_inner_widget_01_id_18
                topToTop = R.id.screen_inner_widget_01_id_21
                bottomToBottom = R.id.screen_inner_widget_01_id_21
            }

            // 주의 사항
            imageView(R.drawable.ic_information) {
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.screen_inner_widget_01_id_29
                endToStart = R.id.screen_inner_widget_01_id_29
                marginEnd = dip(10.7f)
                bottomToBottom = R.id.screen_inner_widget_01_id_29
            }

            nonSpacingTextView(cautionMessage) {
                id = R.id.screen_inner_widget_01_id_29
                typeface = Font.kopubDotumMedium
                textSize = 18.7f
                textColorResource = R.color.x_9196a5
                letterSpacing = -0.1f
                includeFontPadding = false
                lines = 1
            }.lparams(wrapContent, wrapContent) {
                topToBottom = R.id.screen_inner_widget_01_id_26
                topMargin = dip(32.7f)
                endToEnd = R.id.screen_inner_widget_01_id_26
            }
        }
    }

    fun startScrollAnimation() {
        scrollGuideView.let { icon ->
            AnimatorSet()
                .apply {
                    playSequentially(
                        ObjectAnimator.ofFloat(icon, "translationX", 0.0f, icon.width / 2.0f, 0.0f, icon.width / -2.0f, 0.0f)
                            .apply {
                                interpolator = LinearInterpolator()
                                repeatCount = 2
                                duration = 1000L
                            },
                        ObjectAnimator.ofFloat(icon, "alpha", 1.0f, 0.0f)
                            .apply {
                                interpolator = AccelerateInterpolator()
                                duration = 600L
                            }
                    )
                }
                .start()
        }
    }

    @ColorInt
    private fun Indicator.Content.Result.highlightIfResultMatched(): Int = when (this.isResultMatched()) {
        true -> getIndicatorColor()
        else -> Color.TRANSPARENT
    }

    private fun Indicator.Content.Result.isResultMatched(): Boolean =
        this.value == resultIndicator.value

    private fun Indicator.Content.Result.printRange(): String =
        "${ServiceAlgorithm.CONTENT_RESULT_SCORE_RANGES[this.value].first} ~ ${ServiceAlgorithm.CONTENT_RESULT_SCORE_RANGES[this.value].last}"

    private fun Indicator.Content.Result.labelTypeface(): Typeface? = when (this.isResultMatched()) {
        true -> Font.latoBold
        else -> Font.latoRegular
    }

    @ColorRes
    private fun Indicator.Content.Result.labelColorResource(): Int = when (this.isResultMatched()) {
        true -> R.color.x_ffffff
        else -> R.color.x_2e2e2e
    }

    @ColorRes
    private fun Indicator.Content.Result.rangeColorResource(): Int = when (this.isResultMatched()) {
        true -> R.color.x_ffffff
        else -> R.color.x_9196a5
    }

    private fun generateLineData(indicatorValues: String, setting: (Float) -> Unit): LineDataSet {
        return indicatorValues.let { vs ->
            doubleArrayOf().fromJson(vs).apply {
                setting(size.toFloat().div(45.0f))
            }
        }.mapIndexed { idx, v ->
            Entry(
                idx.toFloat(),
                when {
                    v >= 0.0 -> v.toFloat()
                    else     -> v.times(2.0).toFloat() // 미착용 표현 // TODO : 이 프로젝트 에서는 음수 값을 처리하지 않는데 0으로 해야하는거 아닌가?
                }
            )
        }.let {
            LineDataSet(it, null).apply {
                mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                cubicIntensity = 0.2f
                setDrawCircles(false)
                setDrawHighlightIndicators(false)
                setDrawValues(false)
                color = Color.WHITE
            }
        }
    }

    private fun getIndicatorColor(): Int {

        return when(resultIndicator.value) {
            0 -> Color.parseColor("#FFFF5F5F")
            1 -> Color.parseColor("#FFFFA800")
            2 -> Color.parseColor("#FFA3C535")
            3 -> Color.parseColor("#FF29BFD0")
            else -> Color.parseColor("#FF5A98ED")
        }
    }
}