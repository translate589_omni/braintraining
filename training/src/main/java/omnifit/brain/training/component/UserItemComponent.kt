package omnifit.brain.training.component

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIScreen
import omnifit.brain.training.db.model.User
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.common.DTP_YYYYMMDD
import omnifit.commons.common.DTP_YYYY__MM__DD
import omnifit.commons.common.toFormatString
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class UserItemComponent<T : UIScreen>(
    context: Context,
    owner: T
) : AnkoBindableComponent<User, T>(context, owner) {

    private var no: TextView? = null
    private var name: TextView? = null
    private var gender: TextView? = null
    private var birth: TextView? = null
    private var phoneNumber: TextView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            lparams(matchParent, wrapContent)

            no = textView {
                id = R.id.item_widget_01_id_01
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 26.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(100.0f), dip(80.0f)) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                marginStart = dip(89.2f)
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            }
            // 이름
            name = textView {
                id = R.id.item_widget_01_id_02
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 26.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(175.0f), dip(80.0f)) {
                startToEnd = R.id.item_widget_01_id_01
                marginStart = dip(40.0f)
                topToTop = R.id.item_widget_01_id_01
                bottomToBottom = R.id.item_widget_01_id_01
            }
            // 성별
            gender = textView {
                id = R.id.item_widget_01_id_03
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 26.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(100.0f), dip(80.0f)) {
                startToEnd = R.id.item_widget_01_id_02
                marginStart = dip(40.0f)
                topToTop = R.id.item_widget_01_id_01
                bottomToBottom = R.id.item_widget_01_id_01
            }
            // 생년월일
            birth = textView {
                id = R.id.item_widget_01_id_04
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 26.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(250.0f), dip(80.0f)) {
                startToEnd = R.id.item_widget_01_id_03
                marginStart = dip(40.0f)
                topToTop = R.id.item_widget_01_id_01
                bottomToBottom = R.id.item_widget_01_id_01
            }
            // 폰 번호
            phoneNumber = textView {
                id = R.id.item_widget_01_id_05
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 26.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(250.0f), dip(80.0f)) {
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                marginEnd = dip(90.3f)
                topToTop = R.id.item_widget_01_id_01
                bottomToBottom = R.id.item_widget_01_id_01
            }
        }
    }

    override fun bind(data: User, position: Int) {
        super.bind(data, position)

        no?.text = (position + 1).toString()
        name?.text = data.name
        gender?.text = if (data.genderSignature == "F") context.getString(R.string.user_list_110)
        else context.getString(R.string.user_list_100)
        birth?.text = data.birthday.toFormatString(DTP_YYYYMMDD, DTP_YYYY__MM__DD)
        phoneNumber?.text = data.phone
    }
}