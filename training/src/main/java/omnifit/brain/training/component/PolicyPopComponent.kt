package omnifit.brain.training.component

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.http.WebApiService.Companion.CODE_PRIVACY_POLICY_DATA
import omnifit.brain.training.http.WebApiService.Companion.CODE_TERMS_OF_SERVICE_DATA
import omnifit.brain.training.view.*
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.anko.AnkoBindableComponentAdapter
import omnifit.commons.anko.AnkoBindableComponentHolder
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

class PolicyPopComponent<in T : MaterialDialog> (
    val onConfirm: () -> Unit = {}
) : AnkoComponent<T> {

    private lateinit var termsView:ViewPager2
    private lateinit var tabLayout:TabLayout

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.policy_pop_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 29.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                topToTop = PARENT_ID
                startToStart = PARENT_ID
                topMargin = dip(52.7f)
                marginStart = dip(62)
            }

            // 닫기
            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(34.7f),dip(49.3f),dip(59.3f),dip(44.7f))
                onDebounceClick { owner.dismiss() }
            }.lparams(dip(136), dip(136)) {
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            tabLayout = materialTabLayout {
                id = R.id.dialog_widget_01_id_03
                tabGravity = TabLayout.GRAVITY_FILL
                tabMode = TabLayout.MODE_FIXED
                tabRippleColor = null



                backgroundColorResource = R.color.x_e9e9e9
                setSelectedTabIndicatorColor(ContextCompat.getColor(context,R.color.x_000000))
                addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                    override fun onTabReselected(p0: TabLayout.Tab?) {
                    }

                    override fun onTabUnselected(p0: TabLayout.Tab?) {
                    }

                    override fun onTabSelected(p0: TabLayout.Tab?) {
                        p0?.run {
                            termsView.post {
                                termsView.setCurrentItem(position,false)
                            }
                        }
                    }
                })
            }.lparams(matchConstraint, dip(60)) {
                marginStart = dip(60)
                marginEnd = dip(60)
                topToBottom = R.id.dialog_widget_01_id_02
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
            }

            view {
                backgroundColorResource = R.color.x_b1b1b1
            }.lparams(dip(1.3f),dip(25.3f)) {
                topToTop = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_03
                startToStart = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_03
            }

            termsView = viewPager2 {
                id = R.id.dialog_widget_01_id_04
                layoutTransition = LayoutTransition()
                orientation = ViewPager2.ORIENTATION_HORIZONTAL
                isUserInputEnabled = false
                adapter = PolicyAdapter(
                    mutableListOf(
                        "file:///${context.filesDir}/${CODE_PRIVACY_POLICY_DATA}.html",
                        "file:///${context.filesDir}/${CODE_TERMS_OF_SERVICE_DATA}.html"
                    ),
                    owner
                )
            }.lparams(0, dip(240)) {
                startToStart = R.id.dialog_widget_01_id_03
                topToBottom = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_03
                bottomToTop = R.id.dialog_widget_01_id_05
            }

            // TODO : WebView or TextView with Scroll

            button(R.string.policy_pop_040) {
                id = R.id.dialog_widget_01_id_05
                backgroundResource = R.drawable.selector_bg_r40_btn_01
                textSize = 32.0f
                typeface = Font.kopubDotumMedium
                textColorResource = R.color.x_ffffff
                stateListAnimator = null
                onDebounceClick {
                    onConfirm()
                }
            }.lparams(0, dip(76f)) {
                topMargin = dip(28.7f)
                bottomMargin = dip(39.3f)
                verticalBias = 0f
                startToStart = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_03
                topToBottom = R.id.dialog_widget_01_id_04
                bottomToBottom = PARENT_ID
            }
        }.apply {
            TabLayoutMediator(tabLayout, termsView, TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                tab.customView = NonSpacingTextView(context).apply {
                    textResource = when (position) {
                        0    -> R.string.policy_pop_020
                        else -> R.string.policy_pop_030
                    }
                    typeface = Font.boldByLanguage()
                    textSize = 20f
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_policy_tab))
                    letterSpacing = -0.05f
                    includeFontPadding = false
                    gravity = Gravity.CENTER
                    isDuplicateParentStateEnabled = true
                }
            }).attach()
        }
    }


    class PolicyAdapter constructor(
        items: MutableList<String>,
        owner: MaterialDialog
    ) : AnkoBindableComponentAdapter<MutableList<String>, MaterialDialog, PolicyAdapter.ComponentHolder>(
        items,
        owner
    ) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
            return ComponentHolder(PolicyItemComponent(parent.context, owner))
        }

        override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
            holder.component.bind(items[position], position)
        }

        override fun getItemCount(): Int {
            return 2
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        inner class ComponentHolder(
            component: AnkoBindableComponent<String, MaterialDialog>
        ) : AnkoBindableComponentHolder<String, MaterialDialog>(component)
    }

    class PolicyItemComponent<T : MaterialDialog>(
        context: Context,
        owner: T
    ) : AnkoBindableComponent<String, T>(context, owner) {
        private var webView: WebView? = null

        override fun createView(ui: AnkoContext<T>): View = with(ui) {
            constraintLayout {
                webView = webView {

                }.lparams(matchParent,matchParent)
            }
        }.also { v ->
            v.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT)
        }

        override fun bind(data: String, position: Int) {
            super.bind(data, position)
            webView?.loadUrl(data)
        }
    }

//    private fun createPolicyTab(context: Context, messageResId:Int): View {
//        return NonSpacingTextView(context).apply {
//            text = context.getString(messageResId)
//            typeface = Font.boldByLanguage()
//            textSize = 20f
//            setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_policy_tab))
//            letterSpacing = -0.05f
//            includeFontPadding = false
//            gravity = Gravity.CENTER
//            isDuplicateParentStateEnabled = true
//        }
//    }
}