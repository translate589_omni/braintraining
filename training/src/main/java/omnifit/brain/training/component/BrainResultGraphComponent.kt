package omnifit.brain.training.component

import android.view.Gravity
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.commons.common.roundToDecimal
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class BrainResultGraphComponent<in T>(
    private val result: Int,
    private val indicatorValue:Double,
    private val indicatorResourceData:IndicatorResourceData,
    private val pinResId: Int = R.drawable.ic_pin_01,
    private val labelResIdList: Array<Int> = arrayOf()
) : AnkoComponent<T> {

    private var label: Array<Int> = arrayOf()

    private var pingWidth: Float = 80.6f
    //private var graphPinBgStartMargin: Float = 57.3f - 12f + 1.3f
    private var graphPinBgStartMargin: Float = pingWidth - 13.5f
    private var graphLineBg: Int = 0
    private var graphLineTopMargin = 4.7f
    private var graphProgressHeight = 6.3f
    private var levelLayoutTopMargin = 7.8f
    private var levelTextSize = 10.5f
    private var levelTextLineSpacing = levelTextSize + 2.1f
    private var levelTextLines = 2

    init {
        label = labelResIdList
        graphPinBgStartMargin += (result * pingWidth)
        graphLineBg = R.drawable.ic_graphline_bg
    }

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.brain_result_graph_id_root_container

            // TODO : 아이콘 오른쪽에 값을 나타내는 뷰가 필요함.

            imageView(indicatorResourceData.pinBgResId) {
                id = R.id.brain_result_graph_id_01
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.brain_result_graph_id_02
                marginStart = dip(graphPinBgStartMargin)
                topToTop = PARENT_ID
            }

            imageView(pinResId) {
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.brain_result_graph_id_01
                topToTop = R.id.brain_result_graph_id_01
            }

            nonSpacingTextView(
                "(${indicatorValue.roundToDecimal(1)})"
            ) {
                textSize = 24f
                textColorResource = indicatorResourceData.activateTextColorResId
            }.lparams(wrapContent, wrapContent) {
                setMargins(dip(11.3f),dip(4f),0,0)
                startToEnd = R.id.brain_result_graph_id_01
                topToTop = R.id.brain_result_graph_id_01
            }

            imageView(graphLineBg) {
                id = R.id.brain_result_graph_id_02
            }.lparams(matchParent, wrapContent) {
                setMargins(dip(22.3f),0,dip(22.5f),0)
                startToStart = PARENT_ID
                topToBottom = R.id.brain_result_graph_id_01
                topMargin = dip(graphLineTopMargin)
                endToEnd = PARENT_ID
            }

            horizontalProgressBar {
                progressDrawable = ContextCompat.getDrawable(context, indicatorResourceData.progressBgResId)
                max = 100
                progress = (result + 1) * 20
            }.lparams(0, dip(graphProgressHeight)) {
                startToStart = R.id.brain_result_graph_id_02
                endToEnd = R.id.brain_result_graph_id_02
                bottomToBottom = R.id.brain_result_graph_id_02
            }

            linearLayout {
                textView {
                    text = resources.getString(label[0])
                    typeface = Font.boldByLanguage()
                    textSize = levelTextSize
                    textColorResource = getIndicatorColor(0)
                    includeFontPadding = false
                    lines = levelTextLines
                    gravity = Gravity.END
                    setLineSpacing(sp(levelTextLineSpacing).toFloat(),0f)
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[1])
                    typeface = Font.boldByLanguage()
                    textSize = levelTextSize
                    textColorResource = getIndicatorColor(1)
                    includeFontPadding = false
                    lines = levelTextLines
                    gravity = Gravity.END
                    setLineSpacing(sp(levelTextLineSpacing).toFloat(),0f)
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[2])
                    typeface = Font.boldByLanguage()
                    textSize = levelTextSize
                    textColorResource = getIndicatorColor(2)
                    includeFontPadding = false
                    lines = levelTextLines
                    gravity = Gravity.END
                    setLineSpacing(sp(levelTextLineSpacing).toFloat(),0f)
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[3])
                    typeface = Font.boldByLanguage()
                    textSize = levelTextSize
                    textColorResource = getIndicatorColor(3)
                    includeFontPadding = false
                    lines = levelTextLines
                    gravity = Gravity.END
                    setLineSpacing(sp(levelTextLineSpacing).toFloat(),0f)
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[4])
                    typeface = Font.boldByLanguage()
                    textSize = levelTextSize
                    textColorResource = getIndicatorColor(4)
                    includeFontPadding = false
                    lines = levelTextLines
                    gravity = Gravity.END
                    setLineSpacing(sp(levelTextLineSpacing).toFloat(),0f)
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }
            }.lparams(dip(0.0f), wrapContent) {
                orientation = ConstraintLayout.LayoutParams.HORIZONTAL
                startToStart = R.id.brain_result_graph_id_02
                topToBottom = R.id.brain_result_graph_id_02
                topMargin = dip(levelLayoutTopMargin)
                endToEnd = R.id.brain_result_graph_id_02
            }
        }
    }

    private fun getIndicatorColor(position:Int) = when(result) {
        position -> indicatorResourceData.activateTextColorResId
        else -> R.color.x_2e2e2e_op60
    }

    enum class IndicatorResourceData(
        val progressBgResId:Int,
        val pinBgResId:Int,
        val activateTextColorResId:Int
    ) {
        VERY_LOW(
            R.drawable.layer_list_result_graph_very_low,
            R.drawable.ic_pin_bg_03,
            R.color.x_fb1b5a
        ),
        LOW(
            R.drawable.layer_list_result_graph_low,
            R.drawable.ic_pin_bg_02,
            R.color.x_fe793f
        ),
        NORMAL(
            R.drawable.layer_list_result_graph_normal,
            R.drawable.ic_pin_bg_04,
            R.color.x_46eb85
        ),
        HIGH(
            R.drawable.layer_list_result_graph_high,
            R.drawable.ic_pin_bg_05,
            R.color.x_29bfcf
        ),
        VERY_HIGH(
            R.drawable.layer_list_result_graph_very_high,
            R.drawable.ic_pin_bg_01,
            R.color.x_006cff
        )
    }

}