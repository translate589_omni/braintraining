package omnifit.brain.training.component

import android.graphics.Color
import android.view.Gravity
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class BasicPopComponent<in T : MaterialDialog> constructor(
    private val title: String,
    private val message: String,
    private val messageGravity: Int = Gravity.START,
    private val isUseNegative: Boolean = true,
    private val buttonActionListener: ButtonActionListener
) : AnkoComponent<MaterialDialog> {
    override fun createView(ui: AnkoContext<MaterialDialog>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 22.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                text = title
            }.lparams(0, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(50.7f)
                endToEnd = PARENT_ID
                marginEnd = dip(60.7f)
                topToTop = PARENT_ID
                topMargin = dip(52.7f)
            }

            // 메세지
            nonSpacingTextView {
                id = R.id.dialog_widget_01_id_02
                typeface = Font.kopubDotumMedium
                textSize = 28f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                gravity = messageGravity
                text = message
            }.lparams(dip(0.0f), wrapContent) {
                startToStart = R.id.dialog_widget_01_id_01
                topToBottom = R.id.dialog_widget_01_id_01
                topMargin = dip(30.7f)
                endToEnd = R.id.dialog_widget_01_id_01
            }

            // 취소
            imageButton(R.drawable.selector_cancel) {
                id = R.id.dialog_widget_01_id_03
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    buttonActionListener.onNegative()
                }
                isVisible = isUseNegative
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.dialog_widget_01_id_04
                endToStart = R.id.dialog_widget_01_id_04
                bottomToBottom = R.id.dialog_widget_01_id_04
            }

            // 확인
            imageButton(R.drawable.selector_confirm_01) {
                id = R.id.dialog_widget_01_id_04
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    buttonActionListener.onPositive()
                }
            }.lparams(wrapContent, wrapContent) {
                topToBottom = R.id.dialog_widget_01_id_02
                topMargin = dip(54.7f)
                endToEnd = R.id.dialog_widget_01_id_02
                bottomToBottom = PARENT_ID
                bottomMargin = dip(30.0f)
            }
        }
    }

    interface ButtonActionListener {
        fun onPositive()
        fun onNegative()
    }
}