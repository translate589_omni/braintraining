package omnifit.brain.training.component

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import jp.wasabeef.glide.transformations.CropSquareTransformation
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import omnifit.brain.training.GlideApp
import omnifit.brain.training.R
import omnifit.brain.training.UIScreen
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.audioplayer.AudioPlayer
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.support.v4.dip

class BrainAudioTrainingContentItemComponent<T : UIScreen>(
    context: Context,
    owner: T
) : AnkoBindableComponent<AudioPlayer.AudioSource, T>(context, owner) {

    companion object {
        const val CORNER_RADIUS: Float = 26.7f
    }

    var content: CardView? = null
    var contentThumbnail: ImageView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        cardView {
            radius = dip(CORNER_RADIUS).toFloat()
            preventCornerOverlap = true
            setCardBackgroundColor(Color.TRANSPARENT)
            foreground = context.getDrawable(R.drawable.selector_card_fg)
            isSoundEffectsEnabled = true
            isClickable = true

            contentThumbnail = imageView {
                scaleType = ImageView.ScaleType.CENTER_CROP
            }.lparams(matchParent, matchParent)
        }.applyRecursively { v ->
            when (v) {
                is CardView -> {
                    content = v
                    v.layoutParams = FrameLayout.LayoutParams(dip(514.7f), dip(500.0f))
                }
            }
        }

    }

    override fun bind(data: AudioPlayer.AudioSource, position: Int) {
        super.bind(data, position)
        content?.tag = data
        contentThumbnail
            ?.let { thumbnail ->
                GlideApp.with(owner)
                    .load(data.thumbnail)
                    .transform(
                        MultiTransformation(
                            RoundedCornersTransformation(owner.dip(CORNER_RADIUS), 0),
                            CropSquareTransformation()
                        )
                    )
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(thumbnail)
            }
    }
}