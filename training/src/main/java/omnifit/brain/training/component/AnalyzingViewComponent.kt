package omnifit.brain.training.component

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.UIScreen
import omnifit.brain.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch

class AnalyzingViewComponent<in T : UIScreen>(val category: ServiceCategory) : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.screen_inner_widget_01_id_root_container
            backgroundColorResource = R.color.x_000000_op60
            onTouch(returnValue = true) { _, _ ->
            }

            nonSpacingTextView {
                id = R.id.screen_inner_widget_01_id_01
                typeface = Font.kopubDotumBold
                textResource = when (category) {
                    ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> R.string.ai_concentration_assistant_training_screen_020
                    ServiceCategory.CONCENTRATION_TRAINING              -> R.string.game_content_screen_010
                    ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.string.healing_music_content_screen_010
                    ServiceCategory.NEURO_FEEDBACK                      -> R.string.meditation_content_screen_010
                }
                textSize = 43.0f
                textColorResource = R.color.x_ffffff
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(196.0f)
                endToEnd = PARENT_ID
            }

            progressBar {
                id = R.id.screen_inner_widget_01_id_02
                indeterminateDrawable = CircularProgressDrawable(context).apply {
                    strokeWidth = dip(10.8f).toFloat()
                    backgroundColor = Color.TRANSPARENT
                    setColorSchemeColors(ContextCompat.getColor(context, R.color.x_2da3f5))
                }
            }.lparams(dip(295.3f), dip(295.3f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_inner_widget_01_id_01
                topMargin = dip(56.7f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            textView {
                id = R.id.screen_inner_widget_01_id_03
                gravity = Gravity.CENTER
                textResource = when (category) {
                    ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> R.string.ai_concentration_assistant_training_screen_030
                    ServiceCategory.CONCENTRATION_TRAINING   -> R.string.game_content_screen_020
                    ServiceCategory.BRAIN_STABILITY_TRAINING -> R.string.healing_music_content_screen_020
                    ServiceCategory.NEURO_FEEDBACK -> R.string.meditation_content_screen_020
                }
                letterSpacing = -0.03f
                textSize = 25.3f
                textColorResource = R.color.x_ffffff
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_02
                topToTop = R.id.screen_inner_widget_01_id_02
                endToEnd = R.id.screen_inner_widget_01_id_02
                bottomToBottom = R.id.screen_inner_widget_01_id_02
            }

        }.applyRecursively { v ->
            (v as? ConstraintLayout)?.layoutParams = ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }
}