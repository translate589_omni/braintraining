package omnifit.brain.training.component

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.db.model.Measurement
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.db.model.User
import omnifit.commons.anko.AnkoBindableComponent
import omnifit.commons.common.DTP_YYYY_MM_DD_HH_MM
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.joda.time.DateTime

class SynchronizableDataItemComponent<T : MaterialDialog>(
    context: Context,
    owner: T
) : AnkoBindableComponent<Measurement, T>(context, owner) {

    private var no: TextView? = null
    private var name: TextView? = null
    private var measurementType: TextView? = null
    private var measurementDate: TextView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            lparams(matchParent, wrapContent)

            no = textView {
                id = R.id.item_widget_01_id_01
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 22.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(0.0f), dip(64.0f)) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToStart = R.id.item_widget_01_id_02
                horizontalBias = 0.5f
                horizontalWeight = 1f
            }
            // 이름
            name = textView {
                id = R.id.item_widget_01_id_02
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 22.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(0.0f), dip(64.0f)) {
                startToEnd = R.id.item_widget_01_id_01
                topToTop = R.id.item_widget_01_id_01
                endToStart = R.id.item_widget_01_id_03
                horizontalBias = 0.5f
                horizontalWeight = 3f
            }
            // 측정타입
            measurementType = textView {
                id = R.id.item_widget_01_id_03
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 22.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(0.0f), dip(64.0f)) {
                startToEnd = R.id.item_widget_01_id_02
                topToTop = R.id.item_widget_01_id_01
                endToStart = R.id.item_widget_01_id_04
                horizontalBias = 0.5f
                horizontalWeight = 5f
            }
            // 측정일시
            measurementDate = textView {
                id = R.id.item_widget_01_id_04
                gravity = Gravity.CENTER
                includeFontPadding = false
                lines = 1
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                textColorResource = R.color.x_000000
                textSize = 22.7f
                typeface = Font.kopubDotumMedium
            }.lparams(dip(0.0f), dip(64.0f)) {
                startToEnd = R.id.item_widget_01_id_03
                topToTop = R.id.item_widget_01_id_01
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
                horizontalWeight = 3f
            }
        }
    }

    override fun bind(data: Measurement, position: Int) {
        super.bind(data, position)
        no?.text = (position + 1).toString()
        User.findByIdAsync(data.userId) { student ->
            name?.text = student?.name ?: ""
        }
        measurementType?.text = when {
            data.targetSignature.startsWith(ServiceResult.ID_PREFIX_CONCENTRATION_TRAINING)    -> owner.context.getString(R.string.sync_screen_100)
            data.targetSignature.startsWith(ServiceResult.ID_PREFIX_BRAIN_STABILITY_TRAINING)  -> owner.context.getString(R.string.sync_screen_110)
            data.targetSignature.startsWith(ServiceResult.ID_PREFIX_NEURO_FEEDBACK)            -> owner.context.getString(R.string.sync_screen_120)
            data.targetSignature.startsWith(ServiceResult.ID_PREFIX_AI_CONCENTRATION_TRAINING) -> owner.context.getString(R.string.sync_screen_130)
            else                                                                               -> ""
        }
        measurementDate?.text = DateTime(data.begin).toString(DTP_YYYY_MM_DD_HH_MM)
    }
}