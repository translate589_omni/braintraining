package omnifit.brain.training.component

import android.graphics.LinearGradient
import android.graphics.Shader
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_PACKED
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.ServiceCategory
import omnifit.brain.training.UIScreen
import omnifit.brain.training.algorithm.ServiceAlgorithm
import omnifit.brain.training.data.ComparisionData
import omnifit.brain.training.db.model.Code
import omnifit.brain.training.db.model.ServiceResult
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.commons.algorithm.*
import omnifit.commons.common.roundToDecimal
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import timber.log.Timber
import kotlin.math.abs

// 전 후 값을 받아야 할까? 아님 별도의 모델?
class ResultComparisonComponent<T : UIScreen>(
    val result: ServiceResult
) : AnkoComponent<T> {

    private val comparisionData: ComparisionData

    private val measureLabelList : Array<Int>

    private val categoryNameResId:Int
    private val beforeConcentrationType: String
    private val afterConcentrationType: String
    private val isReverse: Boolean
    private val rate: Double
    private val isSuccess: Boolean
    private val isKeepRelax: Boolean

    init {
        comparisionData = ComparisionData.create(result).apply {
            Timber.d("ResultComparisonComponent init value : $beforeScore, $afterScore, $beforeAverage, $afterAverage, $comparisonRate")
        }

        beforeConcentrationType = getConcentrationTypeLabel(result.category,comparisionData.beforeSubList)
        afterConcentrationType = getConcentrationTypeLabel(result.category,comparisionData.afterSubList)

        isReverse = ServiceCategory.of(result.category) == ServiceCategory.BRAIN_STABILITY_TRAINING
        rate = when(isReverse) {
            true -> abs(comparisionData.comparisonRate)
            false -> comparisionData.comparisonRate
        }.let { comparisionData ->
            (comparisionData * 10).roundToDecimal(1)
        }

        isSuccess = rate > 0
        isKeepRelax = checkKeepRelax()

        categoryNameResId = when(ServiceCategory.of(result.category)) {
            ServiceCategory.AI_CONCENTRATION_ASSISTANT_TRAINING -> R.string.content_result_screen_023
            ServiceCategory.CONCENTRATION_TRAINING              -> R.string.content_result_screen_021
            ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.string.content_result_screen_020
            ServiceCategory.NEURO_FEEDBACK                      -> R.string.content_result_screen_022
        }

        measureLabelList = measurementItemLabel()
    }

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            nonSpacingTextView(R.string.content_result_screen_082) {
                id = R.id.screen_inner_widget_02_id_01
                typeface = Font.kopubDotumBold
                textSize = 37.7f
                letterSpacing = -0.01f
                textColorResource = R.color.x_2e2e2e
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                marginStart = dip(80.7f)
                startToStart = PARENT_ID
                topToTop = PARENT_ID
            }

            cardView {
                id = R.id.screen_inner_widget_02_id_02
                cardElevation = dip(5.0f).toFloat()
                backgroundResource = R.drawable.shape_r26_7_96a6ff_63bcfa

                constraintLayout {
                    nonSpacingTextView (
                        when {
                            isSuccess -> String.format(context.getString(R.string.content_result_screen_160),context.getString(categoryNameResId))
                            isKeepRelax -> {
                                when (ServiceCategory.of(result.category)) {
                                    ServiceCategory.NEURO_FEEDBACK           -> R.string.content_result_screen_143
                                    ServiceCategory.CONCENTRATION_TRAINING   -> R.string.content_result_screen_141
                                    ServiceCategory.BRAIN_STABILITY_TRAINING -> R.string.content_result_screen_142
                                    else                                     -> -1
                                }.let { stateMsgResId ->
                                    String.format(context.getString(R.string.content_result_screen_140),context.getString(stateMsgResId))
                                }
                            }
                            else -> String.format(owner.requireContext().getString(R.string.content_result_screen_150),context.getString(categoryNameResId))
                        }
                    ) {
                        id = R.id.screen_inner_widget_02_id_15
                        textSize = 33.3f
                        textColorResource = R.color.x_232323
                    }.lparams(wrapContent, wrapContent) {
                        topMargin = dip(11.3f)
                        marginStart = dip(16.7f)
                        startToStart = PARENT_ID
                        topToTop = R.id.screen_inner_widget_02_id_16
                        if(!isSuccess) {
                            endToEnd = PARENT_ID
                        } else {
                            horizontalChainStyle = CHAIN_PACKED
                            endToStart = R.id.screen_inner_widget_02_id_13
                        }
                    }

                    nonSpacingTextView {
                        id = R.id.screen_inner_widget_02_id_13
                        textSize = 53.3f
                        textColorResource = R.color.x_232323
                        isVisible = isSuccess

                        when ((rate.toInt() - rate) == 0.0) {
                            true -> "${rate.toInt()}%"
                            false -> "$rate%"
                        }.let { msg ->
                            text = msg
                            val shaderWidth = paint.measureText(msg)
                            paint.shader = LinearGradient(
                                0f, 0f, shaderWidth, lineHeight.toFloat(),
                                ContextCompat.getColor(owner.requireContext(), R.color.x_6c80ff),
                                ContextCompat.getColor(owner.requireContext(), R.color.x_2ba4f5),
                                Shader.TileMode.REPEAT
                            )
                        }
                    }.lparams(wrapContent, wrapContent) {
                        marginStart = dip(20)
                        startToEnd = R.id.screen_inner_widget_02_id_15
                        endToStart = R.id.screen_inner_widget_02_id_14
                        bottomToBottom = R.id.screen_inner_widget_02_id_15
                    }

                    nonSpacingTextView (
                        when(ServiceCategory.of(result.category)) {
                            ServiceCategory.CONCENTRATION_TRAINING              -> R.string.content_result_screen_172
                            ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.string.content_result_screen_173
                            ServiceCategory.NEURO_FEEDBACK                      -> R.string.content_result_screen_171
                            else -> null
                        }?.let { msgResId ->
                            String.format(
                                owner.requireContext().getString(R.string.content_result_screen_170),
                                owner.requireContext().getString(msgResId)
                            )
                        }
                    ) {
                        id = R.id.screen_inner_widget_02_id_14
                        textSize = 33.3f
                        textColorResource = R.color.x_232323
                        isVisible = isSuccess
                    }.lparams(wrapContent, wrapContent) {
                        marginStart = dip(14)
                        startToEnd = R.id.screen_inner_widget_02_id_13
                        endToEnd = PARENT_ID
                        bottomToBottom = R.id.screen_inner_widget_02_id_15
                    }

                    imageView(R.drawable.ic_quotes_open) {
                        id = R.id.screen_inner_widget_02_id_16
                    }.lparams(wrapContent, wrapContent) {
                        marginEnd = dip(17.3f)
                        topMargin = dip(49.3f)
                        topToTop = PARENT_ID
                        endToStart = R.id.screen_inner_widget_02_id_15
                    }

                    imageView(R.drawable.ic_quotes_close) {
                    }.lparams(wrapContent, wrapContent) {
                        marginStart = dip(14f)
                        topToTop = R.id.screen_inner_widget_02_id_16
                        startToEnd = when(isSuccess) {
                            true -> R.id.screen_inner_widget_02_id_14
                            false -> R.id.screen_inner_widget_02_id_15
                        }
                    }
                }.lparams(matchParent, matchParent)
            }.lparams(dip(1122f), dip(138.7f)) {
                setMargins(dip(78),dip(27.3f),dip(80),0)
                endToEnd = PARENT_ID
                startToStart = PARENT_ID
                topToBottom = R.id.screen_inner_widget_02_id_01
            }

            constraintLayout {
                id = R.id.screen_inner_widget_02_id_17
                cardView {
                    id = R.id.screen_inner_widget_02_id_03
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_r26_7_ffffff

                    constraintLayout {
                        // before 뷰 구성
                        nonSpacingTextView(R.string.content_result_screen_083) {
                            id = R.id.screen_inner_widget_02_id_04
                            textSize = 24f
                            typeface = Font.mediumByLanguage()
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = PARENT_ID
                            topMargin = dip(38)
                            startToStart = R.id.screen_inner_widget_02_id_06
                            endToEnd = R.id.screen_inner_widget_02_id_06
                        }

                        val beforeIndicator = Algorithm.indicatorOf<Indicator.Content.Result>(
                            when (isReverse) {
                                true  -> 10 - comparisionData.beforeAverage
                                false -> comparisionData.beforeAverage
                            }.conv5PIValue()
                        )

                        textView(
                            Spanner().apply {
                                val labelString = beforeIndicator.label

                                // subLabelString 은 별도로 구한다.
                                val subLabelString = Algorithm.indicatorOf<Indicator.Content.Result>(
                                    comparisionData.beforeAverage.conv5PIValue()
                                ).run {
                                    context.getString(measureLabelList[value]).withOutEnter()
                                }

                                "$labelString| ($subLabelString)"
                                    .split('|').let { strings ->
                                        val sp1 = 53
                                        val sp2 = 28
                                        val font1 = Font.CustomTypefaceSpan(Font.boldByLanguage())
                                        val font2 = Font.CustomTypefaceSpan(Font.mediumByLanguage())
                                        append(strings[0], Spans.sizeSP(sp1), Spans.custom(font1))
                                        append(strings[1], Spans.sizeSP(sp2), Spans.custom(font2))
                                    }
                            }.trim()
                        ) {
                            id = R.id.screen_inner_widget_02_id_05
                            textColorResource = R.color.x_9c9c9c
                        }.lparams(wrapContent, wrapContent) {
                            topMargin = dip(14f)
                            topToBottom = R.id.screen_inner_widget_02_id_04
                            startToStart = R.id.screen_inner_widget_02_id_06
                            endToEnd = R.id.screen_inner_widget_02_id_06
                        }

                        nonSpacingTextView(beforeConcentrationType) {
                            textSize = 22.7f
                            typeface = Font.boldByLanguage()
                            textColorResource = R.color.x_afb3c0
                            letterSpacing = -0.01f
                            isVisible = when(ServiceCategory.of(result.category)){
                                ServiceCategory.CONCENTRATION_TRAINING -> true
                                else -> false
                            }
                        }.lparams(wrapContent,wrapContent) {
                            topMargin = dip(18.7f)
                            startToStart = R.id.screen_inner_widget_02_id_06
                            endToEnd = R.id.screen_inner_widget_02_id_06
                            topToBottom = R.id.screen_inner_widget_02_id_05
                        }

                        val pinRes = when(ServiceCategory.of(result.category)) {
                            ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.drawable.ic_pin_02
                            else ->  R.drawable.ic_pin_01
                        }

                        BrainResultGraphComponent<T>(
                            result = comparisionData.beforeAverage.conv5PIValue(),
                            indicatorValue = comparisionData.beforeAverage,
                            indicatorResourceData = beforeIndicator.convertResourceData(),
                            pinResId = pinRes,
                            labelResIdList = measureLabelList
                        ).createView(
                            AnkoContext.create(owner.requireContext(), owner)
                        ).lparams(wrapContent, wrapContent) {
                            setMargins(dip(70-22.3f),dip(71.3f),dip(71.3f),0)
                            endToEnd = PARENT_ID
                            startToStart = PARENT_ID
                            topToBottom = R.id.screen_inner_widget_02_id_05
                        }.also { v ->
                            v.id = R.id.screen_inner_widget_02_id_06
                            addView(v)
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(544.7f),dip(316f)) {
                    topToTop = PARENT_ID
                    startToStart = PARENT_ID
                }

                cardView {
                    id = R.id.screen_inner_widget_02_id_08
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_r26_7_ffffff

                    constraintLayout {
                        // after 뷰 구성
                        nonSpacingTextView(R.string.content_result_screen_084) {
                            id = R.id.screen_inner_widget_02_id_09
                            textSize = 24f
                            typeface = Font.mediumByLanguage()
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = PARENT_ID
                            topMargin = dip(38)
                            startToStart = R.id.screen_inner_widget_02_id_11
                            endToEnd = R.id.screen_inner_widget_02_id_11
                        }

                        val afterIndicator = Algorithm.indicatorOf<Indicator.Content.Result>(
                            when (isReverse) {
                                true  -> 10 - comparisionData.afterAverage
                                false -> comparisionData.afterAverage
                            }.conv5PIValue()
                        )

                        textView(
                            Spanner().apply {
                                val labelString = Algorithm.indicatorOf<Indicator.Content.Result>(
                                    when (isReverse) {
                                        true  -> 10 - comparisionData.afterAverage
                                        false -> comparisionData.afterAverage
                                    }.conv5PIValue()
                                ).label
                                val subLabelString = Algorithm.indicatorOf<Indicator.Content.Result>(
                                    comparisionData.afterAverage.conv5PIValue()
                                ).run {
                                    context.getString(measureLabelList[value]).withOutEnter()
                                }

                                "$labelString| ($subLabelString)"
                                    .split('|').let { strings ->
                                        val sp1 = 53
                                        val sp2 = 28
                                        val font1 = Font.CustomTypefaceSpan(Font.boldByLanguage())
                                        val font2 = Font.CustomTypefaceSpan(Font.mediumByLanguage())
                                        append(strings[0], Spans.sizeSP(sp1), Spans.custom(font1))
                                        append(strings[1], Spans.sizeSP(sp2), Spans.custom(font2))
                                    }
                            }.trim()
                        ) {
                            id = R.id.screen_inner_widget_02_id_10
                            textColorResource = R.color.x_2e2e2e
                        }.lparams(wrapContent, wrapContent) {
                            topMargin = dip(14f)
                            topToBottom = R.id.screen_inner_widget_02_id_09
                            startToStart = R.id.screen_inner_widget_02_id_11
                            endToEnd = R.id.screen_inner_widget_02_id_11
                        }

                        nonSpacingTextView(afterConcentrationType) {
                            textSize = 22.7f
                            typeface = Font.boldByLanguage()
                            textColorResource = R.color.x_afb3c0
                            letterSpacing = -0.01f
                            isVisible = when(ServiceCategory.of(result.category)){
                                ServiceCategory.CONCENTRATION_TRAINING -> true
                                else -> false
                            }
                        }.lparams(wrapContent,wrapContent) {
                            topMargin = dip(18.7f)
                            startToStart = R.id.screen_inner_widget_02_id_11
                            endToEnd = R.id.screen_inner_widget_02_id_11
                            topToBottom = R.id.screen_inner_widget_02_id_10
                        }

                        val pinRes = when(ServiceCategory.of(result.category)) {
                            ServiceCategory.BRAIN_STABILITY_TRAINING            -> R.drawable.ic_pin_02
                            else ->  R.drawable.ic_pin_01
                        }

                        BrainResultGraphComponent<T>(
                            result = comparisionData.afterAverage.conv5PIValue(),
                            indicatorValue = comparisionData.afterAverage,
                            indicatorResourceData = afterIndicator.convertResourceData(),
                            pinResId = pinRes,
                            labelResIdList = measureLabelList
                        ).createView(
                            AnkoContext.create(owner.requireContext(), owner)
                        ).lparams(wrapContent, wrapContent) {
                            setMargins(dip(70-22.3f),dip(71.3f),dip(71.3f),0)
                            endToEnd = PARENT_ID
                            startToStart = PARENT_ID
                            topToBottom = R.id.screen_inner_widget_02_id_10
                        }.also { v ->
                            v.id = R.id.screen_inner_widget_02_id_11
                            addView(v)
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(544.7f),0) {
                    topToTop = R.id.screen_inner_widget_02_id_03
                    bottomToBottom = R.id.screen_inner_widget_02_id_03
                    endToEnd = PARENT_ID
                }
            }.lparams(0,0) {
                topMargin = dip(29.3f)
                topToBottom = R.id.screen_inner_widget_02_id_02
                startToStart = R.id.screen_inner_widget_02_id_02
                endToEnd = R.id.screen_inner_widget_02_id_02
                bottomToBottom = PARENT_ID
            }

            // 화살표
            imageView(R.drawable.ic_result_arrow) {
                id = R.id.screen_inner_widget_02_id_07
            }.lparams(wrapContent, wrapContent) {
                topMargin = dip(128.7f)
                marginStart = dip(596.7f)
                topToTop = R.id.screen_inner_widget_02_id_17
                startToStart = PARENT_ID
            }

            // 주의 사항
            imageView(R.drawable.ic_information) {
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.screen_inner_widget_02_id_12
                endToStart = R.id.screen_inner_widget_02_id_12
                marginEnd = dip(10.7f)
                bottomToBottom = R.id.screen_inner_widget_02_id_12
            }

            nonSpacingTextView(R.string.content_result_screen_130) {
                id = R.id.screen_inner_widget_02_id_12
                typeface = Font.kopubDotumMedium
                textSize = 18.7f
                textColorResource = R.color.x_9196a5
                letterSpacing = -0.1f
                includeFontPadding = false
                lines = 1
            }.lparams(wrapContent, wrapContent) {
                topToBottom = R.id.screen_inner_widget_02_id_17
                topMargin = dip(38.7f)
                endToEnd = PARENT_ID
                marginEnd = dip(60.0f)
            }
        }
    }

    private fun measurementItemLabel(): Array<Int> = arrayOf(
        R.string.brain_check_graph_level_01,
        R.string.brain_check_graph_level_02,
        R.string.brain_check_graph_level_03,
        R.string.brain_check_graph_level_04,
        R.string.brain_check_graph_level_05
    )

    private fun String.withOutEnter() = this.replace("\n","")

    private fun getConcentrationTypeLabel(category:String,dataList:DoubleArray):String {
        return ServiceCategory.of(category).takeIf {
            it == ServiceCategory.CONCENTRATION_TRAINING
        }?.run {
            Algorithm.indicatorOf<Indicator.EEG.ConcentrationType>(dataList).code
        }?.let { code ->
            Code.of(code)
        }?.label ?: ""
    }
    private fun checkKeepRelax() = ServiceAlgorithm.EFFECTIVE_TIME_THRESHOLD.let {
        when (isReverse) {
            true  -> it > comparisionData.beforeAverage && it > comparisionData.afterAverage
            false -> it <= comparisionData.beforeAverage && it <= comparisionData.afterAverage
        }
    }

    private fun Indicator.Content.Result.convertResourceData() = when(value) {
        0    -> BrainResultGraphComponent.IndicatorResourceData.VERY_LOW
        1    -> BrainResultGraphComponent.IndicatorResourceData.LOW
        2    -> BrainResultGraphComponent.IndicatorResourceData.NORMAL
        3    -> BrainResultGraphComponent.IndicatorResourceData.HIGH
        else -> BrainResultGraphComponent.IndicatorResourceData.VERY_HIGH
    }
}