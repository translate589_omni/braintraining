package omnifit.brain.training.component

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.*
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.helper.SignUpData
import omnifit.brain.training.helper.SignUpData.Companion.Gender
import omnifit.brain.training.util.decryptAes256
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.guideline
import org.jetbrains.anko.constraint.layout.matchConstraint

class SignUpDataPopComponent<in T : MaterialDialog> (
    val data: SignUpData,
    val onConfirm: (SignUpData) -> Unit = {},
    val onCancel: () -> Unit = {}
) : AnkoComponent<T> {

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 왼쪽 끝 선
            guideline {
                id = R.id.screen_inner_guide_line_01
            }.lparams(0, 0) {
                orientation = VERTICAL
                guideBegin = dip(62f)
            }

            // 중앙 뷰 왼쪽 선
            guideline {
                id = R.id.screen_inner_guide_line_02
            }.lparams(0, 0) {
                orientation = VERTICAL
                guideBegin = dip(224.7f)
            }

            // 중앙 뷰 왼쪽 선
            guideline {
                id = R.id.screen_inner_guide_line_03
            }.lparams(0, 0) {
                orientation = VERTICAL
                guideBegin = dip(492.7f)
            }

            // 중앙 뷰 왼쪽 선
            guideline {
                id = R.id.screen_inner_guide_line_04
            }.lparams(0, 0) {
                orientation = VERTICAL
                guideBegin = dip(620.7f)
            }

            // 오른쪽 끝 선
            guideline {
                id = R.id.screen_inner_guide_line_05
            }.lparams(0, 0) {
                orientation = VERTICAL
                guideEnd = dip(60.7f)
            }

            // 타이틀
            nonSpacingTextView("회원가입") {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumBold
                textSize = 41f
                textColorResource = R.color.x_000000
                letterSpacing = -0.05f
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_guide_line_01
                topToTop = PARENT_ID
                topMargin = dip(52.7f)
            }

            // 닫기
            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(34.7f),dip(49.3f),dip(59.3f),dip(44.7f))
                onDebounceClick { owner.dismiss() }
            }.lparams(dip(136), dip(136)) {
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            view {
                id = R.id.dialog_widget_01_id_03
                backgroundColorResource = R.color.x_eaeaea
            }.lparams(matchConstraint,dip(53.3f)) {
                topToBottom = R.id.dialog_widget_01_id_02
            }

            nonSpacingTextView("필수 입력") {
                id = R.id.dialog_widget_01_id_04
                textSize = 22f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                startToStart = R.id.screen_inner_guide_line_01
                topToTop = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_03
            }

            view {
                id = R.id.dialog_widget_01_id_05
                backgroundColorResource = R.color.x_979b9d
            }.lparams(dip(1.3f),dip(20)) {
                marginStart = dip(12.7f)
                topToTop = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_03
                startToEnd = R.id.dialog_widget_01_id_04
                endToStart = R.id.dialog_widget_01_id_06
            }

            nonSpacingTextView("회원 가입 시 입력한 생년월일과 핸드폰 번호로 로그인 할 수 있습니다.") {
                id = R.id.dialog_widget_01_id_06
                textSize = 20f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                marginStart = dip(12.7f)
                startToEnd = R.id.dialog_widget_01_id_05
                topToTop = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_03
            }

            // 1 번째 줄
            nonSpacingTextView("이름") {
                id = R.id.dialog_widget_01_id_07
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                marginStart = dip(35.3f)
                startToStart = R.id.screen_inner_guide_line_01
                topToBottom = R.id.dialog_widget_01_id_03
                bottomToBottom = R.id.dialog_widget_01_id_11
            }

            nonSpacingTextView(data.name.decryptAes256()) {
                id = R.id.dialog_widget_01_id_08
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                marginEnd = dip(10)
                startToStart = R.id.screen_inner_guide_line_02
                endToEnd = R.id.screen_guide_line_03
                topToTop = R.id.dialog_widget_01_id_07
                bottomToBottom = R.id.dialog_widget_01_id_07
            }

            nonSpacingTextView("성별") {
                id = R.id.dialog_widget_01_id_09
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                marginStart = dip(35.3f)
                startToStart = R.id.screen_inner_guide_line_03
                topToTop = R.id.dialog_widget_01_id_07
                bottomToBottom = R.id.dialog_widget_01_id_07
            }

            nonSpacingTextView(
                when(data.gender) {
                    Gender.MALE.code -> "남자"
                    else -> "여자"
                }
            ) {
                id = R.id.dialog_widget_01_id_10
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                startToStart = R.id.screen_inner_guide_line_04
                endToEnd = R.id.screen_inner_guide_line_05
                topToTop = R.id.dialog_widget_01_id_07
                bottomToBottom = R.id.dialog_widget_01_id_07
            }

            view {
                id = R.id.dialog_widget_01_id_11
                backgroundColorResource = R.color.x_ebebeb
            }.lparams(0,dip(2)) {
//                topMargin = dip(78)
                topToBottom = R.id.dialog_widget_01_id_03
                bottomToTop = R.id.dialog_widget_01_id_16
                startToStart = R.id.screen_inner_guide_line_01
                endToEnd = R.id.screen_inner_guide_line_05
            }

            // 2번째 줄
            nonSpacingTextView("생년월일") {
                id = R.id.dialog_widget_01_id_12
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                marginStart = dip(35.3f)
                startToStart = R.id.screen_inner_guide_line_01
                topToBottom = R.id.dialog_widget_01_id_11
                bottomToTop = R.id.dialog_widget_01_id_16
            }

            nonSpacingTextView(data.birthDate.decryptAes256()) {
                id = R.id.dialog_widget_01_id_13
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                marginEnd = dip(10)
                startToStart = R.id.screen_inner_guide_line_02
                endToEnd = R.id.screen_guide_line_03
                topToTop = R.id.dialog_widget_01_id_12
                bottomToBottom = R.id.dialog_widget_01_id_12
            }

            nonSpacingTextView("핸드폰") {
                id = R.id.dialog_widget_01_id_14
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                marginStart = dip(35.3f)
                startToStart = R.id.screen_inner_guide_line_03
                topToTop = R.id.dialog_widget_01_id_12
                bottomToBottom = R.id.dialog_widget_01_id_12
            }

            nonSpacingTextView(data.phoneNumber.decryptAes256()) {
                id = R.id.dialog_widget_01_id_15
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                startToStart = R.id.screen_inner_guide_line_04
                endToEnd = R.id.screen_inner_guide_line_05
                topToTop = R.id.dialog_widget_01_id_12
                bottomToBottom = R.id.dialog_widget_01_id_12
            }

            view {
                id = R.id.dialog_widget_01_id_16
                backgroundColorResource = R.color.x_eaeaea
            }.lparams(matchConstraint,dip(53.3f)) {
                topMargin = dip(240)
                topToBottom = R.id.dialog_widget_01_id_02
            }

            nonSpacingTextView("선택 입력") {
                id = R.id.dialog_widget_01_id_17
                textSize = 22f
                textColorResource = R.color.x_979b9d
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                startToStart = R.id.screen_inner_guide_line_01
                topToTop = R.id.dialog_widget_01_id_16
                bottomToBottom = R.id.dialog_widget_01_id_16
            }

            // 1 번째 줄
            nonSpacingTextView("이메일") {
                id = R.id.dialog_widget_01_id_18
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumBold
            }.lparams(wrapContent,wrapContent) {
                topMargin = dip(29.3f)
                marginStart = dip(35.3f)
                startToStart = R.id.screen_inner_guide_line_01
                topToBottom = R.id.dialog_widget_01_id_16
            }

            nonSpacingTextView(data.email?.decryptAes256() ?: "") {
                id = R.id.dialog_widget_01_id_19
                textSize = 22.7f
                textColorResource = R.color.x_353739
                typeface = Font.kopubDotumMedium
            }.lparams(wrapContent,wrapContent) {
                marginEnd = dip(10)
                startToStart = R.id.screen_inner_guide_line_02
                endToEnd = R.id.screen_guide_line_03
                topToTop = R.id.dialog_widget_01_id_18
                bottomToBottom = R.id.dialog_widget_01_id_18
            }

            button("수정하기") {
                id = R.id.dialog_widget_01_id_20
                backgroundResource = R.drawable.selector_bg_r40_btn_02
                textSize = 32.0f
                typeface = Font.kopubDotumMedium
                textColorResource = R.color.x_ffffff
                stateListAnimator = null
                onDebounceClick {
                    onCancel()
                }
            }.lparams(dip(263.3f), dip(74.3f)) {
                startToStart = PARENT_ID
                endToStart = R.id.dialog_widget_01_id_21
                topToTop = R.id.dialog_widget_01_id_21
                bottomToBottom = R.id.dialog_widget_01_id_21
            }

            button("확인") {
                id = R.id.dialog_widget_01_id_21
                backgroundResource = R.drawable.selector_bg_r40_btn_01
                textSize = 32.0f
                typeface = Font.kopubDotumMedium
                textColorResource = R.color.x_ffffff
                stateListAnimator = null
                onDebounceClick {
                    onConfirm(data)
                }
            }.lparams(dip(263.3f), dip(74.3f)) {
                topMargin = dip(90.7f)
                bottomMargin = dip(233.3f)
                marginStart = dip(17.3f)
                verticalBias = 0.0f
                horizontalChainStyle = CHAIN_PACKED
                startToEnd = R.id.dialog_widget_01_id_20
                endToEnd = PARENT_ID
                topToBottom = R.id.dialog_widget_01_id_16
                bottomToBottom = PARENT_ID
            }
        }
    }
}