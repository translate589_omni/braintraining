package omnifit.brain.training.component

import android.animation.LayoutTransition
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.provider.Settings
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_PACKED
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple
import com.rodolfonavalon.shaperipplelibrary.model.Circle
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.training.*
import omnifit.brain.training.view.*
import omnifit.commons.common.BaseObservableProperty
import omnifit.commons.common.util.LifecycleTrigger
import omnifit.commons.omnifitbrain.Error
import omnifit.commons.omnifitbrain.Headset
import omnifit.commons.omnifitbrain.Link
import omnifit.commons.omnifitbrain.useOmnifitBrain
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch
import timber.log.Timber

class HeadsetSettingPopComponent<in T : MaterialDialog> : AnkoComponent<T> {

    @ExperimentalUnsignedTypes
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop
            layoutTransition = LayoutTransition()
            onTouch(returnValue = true) { _, _ ->
            }

            // 타이틀
            nonSpacingTextView(R.string.headset_setting_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 29.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(50.7f)
                topToTop = PARENT_ID
                topMargin = dip(42.0f)
            }

            // 닫기
            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(30.0f))
                onDebounceClick {
                    close(owner)
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.dialog_widget_01_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.dialog_widget_01_id_01
                verticalBias = 0.5f
            }

            // 진행 메세지
            textSwitcher {
                id = R.id.dialog_widget_01_id_03
                setFactory {
                    TextView(context).apply {
                        typeface = Font.kopubDotumMedium
                        textSize = 24.0f
                        textColorResource = R.color.x_1c1c1c
                        letterSpacing = -0.03f
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(125.5f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // Finding
            shapeRipple {
                id = R.id.dialog_widget_01_id_04
                rippleShape = Circle()
                rippleColor = ContextCompat.getColor(context, R.color.x_1b7ae2)
                rippleCount = 2
                rippleDuration = 1200
                rippleStrokeWidth = dip(1.75f)
                isEnableStrokeStyle = true
                isEnableColorTransition = true
                isVisible = false
            }.lparams(dip(543.3f), dip(543.3f)) {
                startToStart = R.id.dialog_widget_01_id_05
                topToTop = R.id.dialog_widget_01_id_05
                endToEnd = R.id.dialog_widget_01_id_05
                bottomToBottom = R.id.dialog_widget_01_id_05
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // Linking
            rotateLoading {
                id = R.id.dialog_widget_01_id_09
                setStrokeWidth(dip(1.5f))
                setLoadingColor(ContextCompat.getColor(context, R.color.x_32a0f6))
            }.lparams(dip(425.0f), dip(425.0f)) {
                startToStart = R.id.dialog_widget_01_id_05
                topToTop = R.id.dialog_widget_01_id_05
                endToEnd = R.id.dialog_widget_01_id_05
                bottomToBottom = R.id.dialog_widget_01_id_06
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // 헤드셋 연결 상태
            imageSwitcher {
                id = R.id.dialog_widget_01_id_05
                setFactory {
                    ImageView(context).apply {
                        imageResource =
                            connectionStateDrawables[LinkDrawableState.UNREGISTERED.ordinal]
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToTop = R.id.dialog_widget_01_id_06
                verticalChainStyle = CHAIN_PACKED
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // 헤드셋 장치명
            textSwitcher {
                id = R.id.dialog_widget_01_id_06
                setFactory {
                    TextView(context).apply {
                        typeface = Font.latoRegular
                        textSize = 23.0f
                        setTextColor(
                            AppCompatResources.getColorStateList(
                                context,
                                R.color.selector_headset_name_text
                            )
                        )
                        letterSpacing = -0.01f
                        lines = 1
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                        isEnabled = false
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
                bindHeadsetName(lifecycleTrigger, Bindable.handlingHeadset)
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_05
                topToBottom = R.id.dialog_widget_01_id_05
                topMargin = dip(13.2f)
                endToEnd = R.id.dialog_widget_01_id_05
                bottomToBottom = PARENT_ID
                bottomMargin = dip(15.0f)
                horizontalBias = 0.5f
            }

            button(R.string.headset_setting_090) {
                id = R.id.dialog_widget_01_id_07
                backgroundResource = R.drawable.selector_headset_btn_01
                gravity = Gravity.CENTER
                textSize = 24.0f
                typeface = Font.kopubDotumBold
                isEnabled = false
                setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r10_btn_text_03))
                stateListAnimator = null
                onDebounceClick {
                    if (BluetoothAdapter.getDefaultAdapter().isEnabled) {
                        linkHeadset()
                    }
                    else requestBluetoothAdapterEnable(context)
                }
            }.lparams(0, 0) {
                startToEnd = R.id.dialog_widget_01_id_08
                marginStart = dip(13.3f)
                topToTop = R.id.dialog_widget_01_id_08
                bottomToBottom = R.id.dialog_widget_01_id_08
                endToEnd = PARENT_ID
                marginEnd = dip(50.7f)
            }

            // 검색하기
            button(R.string.headset_setting_100) {
                id = R.id.dialog_widget_01_id_08
                backgroundResource = R.drawable.selector_headset_btn_01
                textSize = 24.0f
                typeface = Font.kopubDotumBold
                setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r10_btn_text_03))
                stateListAnimator = null
                onDebounceClick {
                    useOmnifitBrain {
                        if (isNotLinking()) {
                            findHeadset()
                        }
                    }
                }
            }.lparams(0, dip(74.3f)) {
                startToStart = PARENT_ID
                marginStart = dip(50.7f)
                endToStart = R.id.dialog_widget_01_id_07
                goneEndMargin = dip(50.7f)
                topToBottom = R.id.dialog_widget_01_id_09
                bottomToBottom = PARENT_ID
                bottomMargin = dip(50)
                verticalBias = 0f

//                endToEnd = PARENT_ID
//                marginEnd = dip(50.7f)
//                topToBottom = R.id.dialog_widget_01_id_09
//                topMargin = dip(25.0f)
//                horizontalBias = 0.5f
            }

            // 공간만들기
            view {}.lparams(matchParent, dip(30.0f)) {
                topToBottom = R.id.dialog_widget_01_id_08
            }
        }.apply {
            observeLinkStateChange()

            owner.setOnShowListener {
                if (BluetoothAdapter.getDefaultAdapter().isEnabled && Bindable.handlingHeadset.value.isEmpty) {
                    findHeadset()
                }
            }
        }
    }

    private fun View.close(owner: T) {
        lifecycleTrigger.destroyed()
        useOmnifitBrain {
            with(getMostRecentlyUsedHeadset()) {
                //UNREGISTER
                if (isEmpty) {
                    updateProgressMessage()
                    updateLinkStateDrawable()
                    updateHeadsetName()
                    updateHeadsetNameFocusable(false)
                    updateCircularRippleVisible(false)
                    updateRotateLoadingVisible(false)
                }
                else {
                    updateUiByLinkState(getLinkState())
                }
            }
        }
        owner.dismiss()
    }

    private fun requestBluetoothAdapterEnable(
        context: Context,
        lazyAction: () -> Unit = {}
    ) {
        context.startActivity(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
    }

    private val connectionStateDrawables: Array<Int> by lazy {
        arrayOf(
            R.drawable.img_headset,
            R.drawable.img_headset_earphone_all_off,
            R.drawable.img_headset_earphone_off,
            R.drawable.img_headset_eeg_off,
            R.drawable.img_headset_all_on
        )
    }

    private val lifecycleTrigger: LifecycleTrigger by lazy {
        LifecycleTrigger()
    }

//    private var handlingHeadset: Headset = Headset.empty()

    @Suppress("CheckResult")
    private fun View.observeLinkStateChange() {

        useOmnifitBrain {

            observeLinkStateChange()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .bindUntilEvent(lifecycleTrigger, Lifecycle.Event.ON_DESTROY)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { s ->
                        updateUiByLinkState(s)
                    },
                    { e ->
                        e.printStackTrace()
                        updateCircularRippleVisible(false)
                        updateRotateLoadingVisible(false)
                    }
                )
        }
    }

    @Suppress("CheckResult")
    private fun View.findHeadset() {
        useOmnifitBrain {

            isFinding {
                stopFind()
            }

            startFind {
                onStart {
                    Timber.i("[HEADSET_SETTING:FIND] --> ON_START")

                    // 링크 상태에서 기기 검색시 배터리 UI Invisible
                    Bindable.isHeadsetLinked.value = false

                    updateChangeButtonFocusable(false)
                    updateConnectButtonFocusable(false)
                    updateProgressMessage(R.string.headset_setting_030)
                    updateLinkStateDrawable(LinkDrawableState.UNREGISTERED.ordinal)
                    updateHeadsetNameFocusable(false)
                    updateCircularRippleVisible(true)
                }
                onFound {
                    Timber.i("[HEADSET_SETTING:FIND] --> ON_FOUND:$it")
                    Bindable.handlingHeadset.value = it
                    updateProgressMessage()
                    updateLinkStateDrawable(LinkDrawableState.UNLINKED.ordinal)
                    updateHeadsetNameFocusable(false)
                    updateConnectButtonFocusable(true)
                    updateCircularRippleVisible(false)
                }
                onFinish {
                    Timber.i("[HEADSET_SETTING:FIND] --> ON_FINISH")
                    if (isShown && Bindable.handlingHeadset.value.isEmpty) {
                        updateProgressMessage(R.string.headset_setting_020)
                        updateCircularRippleVisible(false)
                    } else {
                        updateProgressMessage(R.string.headset_setting_150)
                    }
                    updateChangeButtonFocusable(true)
                }
                onError { e ->
                    Timber.e("[HEADSET_SETTING:FIND] --> ON_ERROR:$e")
                    when (e) {
                        Error.BLUETOOTH_NOT_AVAILABLE,
                        Error.LOCATION_PERMISSION_NOT_GRANTED -> Unit
                        Error.BLUETOOTH_NOT_ENABLED           -> requestBluetoothAdapterEnable(context)
                        Error.LOCATION_SERVICE_NOT_ENABLED    -> {
                            MaterialDialog(context)
                                .show {
                                    hideNavigation()
                                    customView(
                                        view = BasicPopComponent<MaterialDialog>(
                                            title = context.getString(R.string.headset_setting_110),
                                            message = context.getString(R.string.headset_setting_120),
                                            buttonActionListener = object : BasicPopComponent.ButtonActionListener {
                                                override fun onPositive() {
                                                    context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                                                    dismiss()
                                                }

                                                override fun onNegative() {
                                                    dismiss()
                                                }
                                            }
                                        )
                                            .createView(AnkoContext.create(context, this))
                                            .also { v ->
                                                (v as? ConstraintLayout)?.layoutParams =
                                                    FrameLayout.LayoutParams(
                                                        dip(295.5f),
                                                        FrameLayout.LayoutParams.WRAP_CONTENT
                                                    )
                                            },
                                        noVerticalPadding = true
                                    )
                                    cornerRadius(15.0f)
                                    cancelable(false)
                                    cancelOnTouchOutside(false)
                                    setOnDismissListener {
                                    }
                                }
                        }
                        Error.OMNIFIT_BRAIN_HEADSET_NOT_FOUND -> {
                            if (isShown) {
                                updateProgressMessage(R.string.headset_setting_040)
                                updateHeadsetName()
                                updateCircularRippleVisible(false)
                                updateChangeButtonFocusable(true)
                            }
                        }
                        else                                  -> Unit
                    }
                }
            }
        }
    }

    private fun View.linkHeadset() {
        Timber.v("[HEADSET_SETTING] --> ${Bindable.handlingHeadset.value}")
        if (!Bindable.handlingHeadset.value.isEmpty) {
            updateProgressMessage()
            updateChangeButtonFocusable(false)
            updateConnectButtonFocusable(false)
            updateCircularRippleVisible(false)
            updateRotateLoadingVisible(true)

            useOmnifitBrain {
                Bindable.isHeadsetLinking.value = true
                updateProgressMessage(String.format(context.getString(R.string.headset_setting_050,Bindable.handlingHeadset.value.name),Bindable.handlingHeadset.value.name))

                startLink(Bindable.handlingHeadset.value) {
                    onSuccess {
                        Bindable.handlingHeadset.value = it
                        Bindable.isHeadsetLinking.value = false
                    }
                    onFailure { headset, error ->
                        updateConnectButtonFocusable(true)
                        updateChangeButtonFocusable(true)
                        updateCircularRippleVisible(false)
                        updateRotateLoadingVisible(false)
                        when (error) {
                            Error.EARPHONE_DEVICE_NOT_FOUND,
                            Error.EARPHONE_DEVICE_LINK_FAILURE -> (context as? UIFrame)?.alert(context, R.string.headset_setting_080)
                            else                               -> error.cause?.printStackTrace()
                        }
                        Bindable.isHeadsetLinking.value = false
                    }
                }
            }
        }
    }

    private fun View.updateUiByLinkState(state: Link.State) {
        useOmnifitBrain {
            isLinking()
                .let { isLinking ->
                    Timber.v("[HEADSET_SETTING] --> State [${state::class.java.simpleName}] isLinking [$isLinking]")
                    when (state) {
                        is Link.State.Unlinked -> {
                            if (!Bindable.handlingHeadset.value.isEmpty) {
                                updateProgressMessage(if (isLinking) String.format(context.getString(R.string.headset_setting_050),state.headset.name) else context.getString(R.string.headset_setting_020))
                                updateLinkStateDrawable(LinkDrawableState.UNLINKED.ordinal)
                                updateHeadsetNameFocusable(false)
                                updateConnectButtonFocusable(!isLinking)
                                updateChangeButtonFocusable(!isLinking)
                                updateCircularRippleVisible(false)
                                updateRotateLoadingVisible(isLinking)
                            }
                        }
                        is Link.State.EegMonitorLinked -> {
                            updateProgressMessage(if (isLinking) String.format(context.getString(R.string.headset_setting_050),state.headset.name) else context.getString(R.string.headset_setting_020))
                            updateLinkStateDrawable(LinkDrawableState.EARPHONE_UNLINKED.ordinal)
                            updateConnectButtonFocusable(!isLinking)
                            updateChangeButtonFocusable(!isLinking)
                            updateHeadsetNameFocusable(false)
                            updateConnectButtonFocusable(!isLinking)
                        }
                        is Link.State.EarphoneLinked -> {
                            updateProgressMessage(if (isLinking) String.format(context.getString(R.string.headset_setting_050),state.headset.name) else context.getString(R.string.headset_setting_020))
                            updateLinkStateDrawable(LinkDrawableState.EEG_MONITOR_UNLINKED.ordinal)
                            updateConnectButtonFocusable(!isLinking)
                            updateChangeButtonFocusable(!isLinking)
                            updateHeadsetNameFocusable(false)
                        }
                        is Link.State.Linked -> {
                            updateProgressMessage(String.format(context.getString(R.string.headset_setting_140),state.headset.name))
                            updateLinkStateDrawable(LinkDrawableState.LINKED.ordinal)
                            updateHeadsetNameFocusable(true)
                            updateConnectButtonFocusable(false)
                            updateChangeButtonFocusable(true)
                            updateCircularRippleVisible(false)
                            updateRotateLoadingVisible(false)
                        }
                    }
                }
        }
    }

    private fun View.updateProgressMessage(@StringRes msg: Int) {
        updateProgressMessage(context.getString(msg))
    }

    private fun View.updateProgressMessage(msg: String = "") {
        with(parent as View) {
            find<TextSwitcher>(R.id.dialog_widget_01_id_03).setText(msg)
        }
    }

    private fun View.updateFindButtonMessage(@StringRes msg: Int) {
        updateFindButtonMessage(context.getString(msg))
    }

    private fun View.updateFindButtonMessage(msg: String = "") {
        with(parent as View) {
            find<Button>(R.id.dialog_widget_01_id_08).setText(msg)
        }
    }

    private fun View.updateLinkStateDrawable(level: Int = LinkDrawableState.UNREGISTERED.ordinal) {
        with(parent as View) {
            find<ImageSwitcher>(R.id.dialog_widget_01_id_05).setImageResource(
                connectionStateDrawables[level]
            )
        }
    }

    private fun View.updateHeadsetName(headset: Headset = Headset.empty()) {
        with(parent as View) {
            find<TextSwitcher>(R.id.dialog_widget_01_id_06).setText(
                headset.name ?: context.getString(R.string.headset_setting_060)
            )
        }
    }

    private fun View.updateHeadsetNameFocusable(enabled: Boolean = false) {
        with(parent as View) {
            find<TextSwitcher>(R.id.dialog_widget_01_id_06).currentView.isSelected = enabled
        }
    }

    private fun View.updateCircularRippleVisible(visible: Boolean = false) {
        with(parent as View) {
            find<ShapeRipple>(R.id.dialog_widget_01_id_04).isVisible = visible
        }
    }

    private fun View.updateRotateLoadingVisible(visible: Boolean = false) {
        with(parent as View) {
            find<RotateLoading>(R.id.dialog_widget_01_id_09).run {
                if (visible) start()
                else stop()
            }
        }
    }

    private fun View.updateConnectButtonFocusable(enabled: Boolean = true) {
        with(parent as View) {
            find<Button>(R.id.dialog_widget_01_id_07).isEnabled = enabled
        }
    }

    private fun View.updateChangeButtonFocusable(enabled: Boolean = true) {
        with(parent as View) {
            find<Button>(R.id.dialog_widget_01_id_08).isEnabled = enabled
        }
    }

    private fun <T : TextSwitcher> T.bindHeadsetName(
        owner: LifecycleOwner,
        bindable: BaseObservableProperty<Headset>,
        @StringRes format: Int = -1
    ) {
        fun bind(v: Headset) {
            context.runOnUiThread {
                setCurrentText(v.name)
            }
        }

        val valueChangeListener: (Headset) -> Unit = { s ->
            bind(s)
        }

        bind(bindable.value)
        bindable.addValueChangeListener(valueChangeListener)
        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun unbind() {
                bindable.removeValueChangeListener(valueChangeListener)
                owner.lifecycle.removeObserver(this)
            }
        })
    }

    enum class LinkDrawableState {
        UNREGISTERED,
        UNLINKED,
        EARPHONE_UNLINKED,
        EEG_MONITOR_UNLINKED,
        LINKED
    }
}