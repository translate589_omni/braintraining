package omnifit.brain.training.component

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.text.Html
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isInvisible
import androidx.core.view.setPadding
import com.afollestad.materialdialogs.MaterialDialog
import com.liulishuo.okdownload.OkDownload
import com.liulishuo.okdownload.core.Util
import com.liulishuo.okdownload.core.cause.EndCause
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIFrame
import omnifit.brain.training.getAppVersion
import omnifit.brain.training.http.ServiceConfiguration
import omnifit.brain.training.util.ApkDownloadManager
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import java.io.File

class AppUpdatePopComponent<in T : MaterialDialog> : AnkoComponent<T> {

    private var updateAppVerionNoti: TextView? = null
    private var closeButton: ImageButton? = null
    private var downloadProgressBar: ProgressBar? = null
    private var updateButton: Button? = null
    private var cancelButton: Button? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.app_update_screen_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 29.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(50.0f)
                topToTop = PARENT_ID
                topMargin = dip(48.0f)
            }

            // 닫기
            closeButton = imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(30.0f))
                onDebounceClick { owner.dismiss() }
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.dialog_widget_01_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.dialog_widget_01_id_01
                verticalBias = 0.5f
            }

            // 버전
            updateAppVerionNoti = textView {
                id = R.id.dialog_widget_01_id_03
                includeFontPadding = false
                typeface = Font.kopubDotumMedium
                textSize = 26.7f
                textColorResource = R.color.x_1c1c1c
                setLineSpacing(sp(40).toFloat(), 0f)
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_01
                topToBottom = R.id.dialog_widget_01_id_01
                topMargin = dip(32.0f)
            }

            downloadProgressBar = horizontalProgressBar {
                id = R.id.dialog_widget_01_id_04
                progressTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.x_2da3f5))
                progressTintMode = PorterDuff.Mode.SRC_IN
                max = 10000
                isInvisible = true
            }.lparams(0, wrapContent) {
                topToBottom = R.id.dialog_widget_01_id_03
                startToStart = R.id.dialog_widget_01_id_05
                endToEnd = R.id.dialog_widget_01_id_06
                topMargin = dip(20.0f)
            }

            cancelButton = button(R.string.app_update_screen_030) {
                id = R.id.dialog_widget_01_id_05
                backgroundResource = R.drawable.selector_bg_r10_btn_02
                textSize = 32.0f
                textColorResource = R.color.x_2ba4f5
                typeface = Font.kopubDotumMedium
                stateListAnimator = null
                onDebounceClick { owner.dismiss() }
            }.lparams(dip(263.3f), dip(74.3f)) {
                startToStart = PARENT_ID
                endToStart = R.id.dialog_widget_01_id_06
                topToBottom = R.id.dialog_widget_01_id_04
                topMargin = dip(20.0f)

            }

            updateButton = button(R.string.app_update_screen_010) {
                id = R.id.dialog_widget_01_id_06
                backgroundResource = R.drawable.selector_bg_r10_btn_03
                textSize = 32.0f
                typeface = Font.kopubDotumMedium
                textColorResource = R.color.x_ffffff
                stateListAnimator = null
                onDebounceClick {
                    checkNetworkAvailable(
                        { update(context, owner) },
                        {
                            (context as? UIFrame)?.alert(
                                context,
                                R.string.app_update_screen_060,
                                gravity = Gravity.CENTER
                            )
                        }
                    )
                }
            }.lparams(dip(263.3f), dip(74.3f)) {
                startToEnd = R.id.dialog_widget_01_id_05
                endToEnd = PARENT_ID
                bottomToBottom = R.id.dialog_widget_01_id_05
            }

            view {}.lparams(dip(5.0f), dip(30.0f)) {
                topToBottom = R.id.dialog_widget_01_id_05
            }
        }.apply {
            updateAppVersionMessage(context)
        }
    }

    private fun checkNetworkAvailable(onAvailable: () -> Unit, notAvailable: () -> Unit) {
        OkDownload.with().context().getSystemService(Context.CONNECTIVITY_SERVICE)?.run {
            when (Util.isNetworkAvailable(this as ConnectivityManager)) {
                true -> onAvailable()
                false -> notAvailable()
            }
        } ?: notAvailable()
    }

    private fun update(context: Context, owner: T) {
        ServiceConfiguration.get(context)
            .getAppUpdateConfigurationListThatRequireUpdate()?.let { configuration ->
                configuration.appDownloadUrl.takeUnless {
                    it.isBlank()
                }?.let { url ->
                    ApkDownloadManager.Builder(context, url)
                        .onTaskStart {
                            context.runOnUiThread {
                                changeButtonEnableState(false)
                                downloadProgressBar?.isInvisible = false
                            }
                        }
                        .onProgressUpdate { percent ->
                            context.runOnUiThread {
                                downloadProgressBar?.run {
                                    progress = (percent * max).toInt()
                                }
                            }
                        }
                        .onTaskEnd { cause, filePath ->
                            context.runOnUiThread {
                                changeButtonEnableState(true)
                                downloadProgressBar?.isInvisible = true
                            }

                            cause.takeIf { it == EndCause.COMPLETED }?.run {
                                filePath?.let {
                                    installApk(context, filePath)
                                    owner.dismiss()
                                } ?: run {
                                    (context as? UIFrame)?.alert(
                                        context,
                                        R.string.app_update_screen_050,
                                        gravity = Gravity.CENTER
                                    )
                                }
                            } ?: run {
                                context.runOnUiThread {
                                    changeButtonEnableState(true)
                                    downloadProgressBar?.isInvisible = true
                                    (context as? UIFrame)?.alert(
                                        context,
                                        R.string.app_update_screen_050,
                                        gravity = Gravity.CENTER
                                    )
                                }
                            }
                        }
                        .build()
                        .downloadApk()
                } ?: (context as? UIFrame)?.alert(
                    context,
                    R.string.app_update_screen_040,
                    gravity = Gravity.CENTER
                )
            }
    }

    private fun installApk(context: Context, filePath: String) {
        val downLoadApkFileMimeType = "application/vnd.android.package-archive"
        val contentUriAuthority = "omnifit.brain.training.file.provider"

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> {
                Intent(Intent.ACTION_INSTALL_PACKAGE).apply {
                    data = FileProvider.getUriForFile(context, contentUriAuthority, File (filePath))
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
            }
            else -> {
                Intent(Intent.ACTION_VIEW).apply {
                    setDataAndType(Uri.fromFile(File(filePath)), downLoadApkFileMimeType)
                }
            }
        }.let {
            try {
                context.startActivity(it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
                (context as? UIFrame)?.alert(
                    context,
                    R.string.app_update_screen_070,
                    gravity = Gravity.CENTER
                )
            }
        }
    }

    private fun changeButtonEnableState(isEnable: Boolean) {
        updateButton?.isEnabled = isEnable
        cancelButton?.isEnabled = isEnable
        closeButton?.isEnabled = isEnable
    }

    private fun updateAppVersionMessage(context: Context) {
        var appVersion = getAppVersion(context)
        ServiceConfiguration.get(context).run {
            getAppUpdateConfigurationListThatRequireUpdate()?.let {
                appVersion = it.appVersion
            }
        }

        updateAppVerionNoti?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(
                context.resources.getString(R.string.app_update_screen_020, appVersion),
                Html.FROM_HTML_OPTION_USE_CSS_COLORS
            )
        } else {
            Html.fromHtml(context.resources.getString(R.string.app_update_screen_020, appVersion))
        }
    }
}