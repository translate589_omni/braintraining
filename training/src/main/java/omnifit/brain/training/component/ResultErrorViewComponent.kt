package omnifit.brain.training.component

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.setPadding
import omnifit.brain.training.*
import omnifit.brain.training.screen.MainScreen
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch

class ResultErrorViewComponent<in T : UIScreen>(val category: ServiceCategory) : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.screen_inner_widget_01_id_root_container
            backgroundColorResource = R.color.x_000000_op95
            onTouch(returnValue = true) { _, _ ->
            }

            // 닫기
            imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.screen_inner_widget_01_id_01
                scaleType = ImageView.ScaleType.FIT_CENTER
                backgroundColor = Color.TRANSPARENT
                setPadding(dip(10.0f))
                onDebounceClick {
                    owner.screenTo(MainScreen())
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = PARENT_ID
                topMargin = dip(41.8f)
                endToEnd = PARENT_ID
                marginEnd = dip(13.0f)
            }

            imageView(R.drawable.img_result_error) {
                id = R.id.screen_inner_widget_01_id_03
                scaleType = ImageView.ScaleType.FIT_CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToTop = R.id.screen_inner_widget_01_id_02
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // 다시하기
            button(R.string.common_030) {
                id = R.id.screen_inner_widget_01_id_02
                backgroundResource = R.drawable.selector_bg_r10_btn_01
                textSize = 30.0f
                typeface = Font.latoRegular
                setTextColor(
                    AppCompatResources.getColorStateList(
                        owner.requireContext(),
                        R.color.selector_r10_btn_text_01
                    )
                )
                stateListAnimator = null
                isAllCaps = false
                onDebounceClick {
                    owner.screenTo(MainScreen())
                }
            }.lparams(dip(553.0f), dip(60f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(80f)
                horizontalBias = 0.5f
            }
        }.applyRecursively { v ->
            (v as? ConstraintLayout)?.layoutParams = ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }
}