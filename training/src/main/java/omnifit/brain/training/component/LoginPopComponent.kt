package omnifit.brain.training.component

import android.accounts.NetworkErrorException
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.text.Html
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.afollestad.materialdialogs.MaterialDialog
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.UIFrame
import omnifit.brain.training.db.model.Administrator
import omnifit.brain.training.helper.ContentHelper
import omnifit.brain.training.http.WebApi
import omnifit.brain.training.http.XomnifitHeader
import omnifit.brain.training.view.NonSpacingTextView
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import omnifit.commons.http.retrofit.error.HttpExceptionWrapper
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onEditorAction
import org.jetbrains.anko.sdk27.coroutines.onFocusChange
import timber.log.Timber
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class LoginPopComponent<in T : MaterialDialog> (
    private val onLoginSuccess: () -> Unit,
    private val onLoginFailed: () -> Unit
) : AnkoComponent<T> {

    private var loginID: EditText? = null
    private var loginPW: EditText? = null
    private var closeBtn: ImageButton? = null
    private var progressBaseDataToUpdate: ProgressBar? = null
    private var progressMessageToUpdate: NonSpacingTextView? = null
    var startLogin: Button? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.login_010) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 41f
                textColorResource = R.color.x_000000
                letterSpacing = -0.08f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(62.0f)
                topToTop = R.id.dialog_widget_01_id_02
                bottomToBottom = R.id.dialog_widget_01_id_02
            }

            // 닫기
            closeBtn = imageButton(R.drawable.selector_btn_popup_close) {
                id = R.id.dialog_widget_01_id_02
                backgroundColor = Color.TRANSPARENT
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(34.7f),dip(49.3f),dip(59.3f),dip(44.7f))
                onDebounceClick { owner.dismiss() }
            }.lparams(dip(136), dip(136)) {
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
            }

            loginID = editText {
                id = R.id.dialog_widget_01_id_03
                backgroundResource = R.drawable.selector_bg_r11_3_view_01
                textColorResource = R.color.x_32363a
                textSize = 33.7f
                typeface = Font.kopubDotumMedium
                hint = context.getString(R.string.login_080)
                hintTextColor = ContextCompat.getColor(context,R.color.x_9e9e9e)
                imeOptions = EditorInfo.IME_ACTION_NEXT
                inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                lines = 1
                ems = 10

                setText(Administrator.get(context).loginId)
                setPadding(dip(28),0,dip(28),0)
                onFocusChange { _, hasFocus ->
                    if (hasFocus) {
                        selectAll()
                    }
                }
            }.lparams(0,dip(80)) {
                setMargins(dip(62),dip(14),dip(58),0)
                topToBottom = R.id.dialog_widget_01_id_02
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
            }

            loginPW = editText {
                id = R.id.dialog_widget_01_id_04
                backgroundResource = R.drawable.selector_bg_r11_3_view_01
                textColorResource = R.color.x_32363a
                textSize = 29.7f
                typeface = Font.kopubDotumMedium
                hint = context.getString(R.string.login_090)
                hintTextColor = ContextCompat.getColor(context,R.color.x_9e9e9e)
                imeOptions = EditorInfo.IME_ACTION_DONE
                inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                lines = 1
                ems = 10

                setPadding(dip(28),0,dip(28),0)

                onFocusChange { _, hasFocus ->
                    if (hasFocus) {
                        selectAll()
                    }
                }
                onEditorAction { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        excuteLoginAction(context)
                    }
                }
            }.lparams(0,dip(80)) {
                topMargin = dip(20.7f)
                topToBottom = R.id.dialog_widget_01_id_03
                startToStart = R.id.dialog_widget_01_id_03
                endToEnd = R.id.dialog_widget_01_id_03
            }

            progressBaseDataToUpdate = horizontalProgressBar {
                id = R.id.dialog_widget_01_id_05
                isIndeterminate = true
                indeterminateTintList =
                    AppCompatResources.getColorStateList(context, R.color.x_2da3f5)
                indeterminateTintMode = PorterDuff.Mode.SRC_ATOP
                isInvisible = true
            }.lparams(dip(0.0f), wrapContent) {
                startToStart = R.id.dialog_widget_01_id_04
                endToEnd = R.id.dialog_widget_01_id_04
                topToBottom = R.id.dialog_widget_01_id_04
                topMargin = dip(5.0f)
            }

            // 진행 메세지
            progressMessageToUpdate = nonSpacingTextView {
                id = R.id.dialog_widget_01_id_06
                lines = 2
                gravity = Gravity.CENTER
                typeface = Font.kopubDotumMedium
                textSize = 24.0f
                textColorResource = R.color.x_6f6f6f
                includeFontPadding = false
            }.lparams(wrapContent, dip(48.0f)) {
                startToStart = R.id.dialog_widget_01_id_05
                endToEnd = R.id.dialog_widget_01_id_05
                topToBottom = R.id.dialog_widget_01_id_04
                topMargin = dip(25.0f)
            }

            startLogin = button(R.string.login_100) {
                id = R.id.dialog_widget_01_id_07
                backgroundResource = R.drawable.selector_bg_r40_btn_02
                textSize = 32.0f
                typeface = Font.kopubDotumMedium
                textColorResource = R.color.x_ffffff
                stateListAnimator = null
                onDebounceClick {
                    //onPressedLogin(loginID?.text?.toString() ?: "", loginPW?.text?.toString() ?: "")
                    excuteLoginAction(context)
                }
            }.lparams(dip(421.3f), dip(80f)) {
                topMargin = dip(53.3f - 48f)
                bottomMargin = dip(54.7f)
                verticalBias = 0f
                startToStart = R.id.dialog_widget_01_id_04
                endToEnd = R.id.dialog_widget_01_id_04
                topToBottom = R.id.dialog_widget_01_id_06
                bottomToBottom = PARENT_ID
            }
        }
    }

    private fun excuteLoginAction(context:Context) {
        (context as? UIFrame)?.run {
            hideSoftKeyboard()
            hideNavigation()
            loginID?.clearFocus()
            loginPW?.clearFocus()

            if (loginID?.text.isNullOrEmpty() || loginPW?.text.isNullOrEmpty()) {
                alert(context, if (loginID?.text.isNullOrEmpty()) R.string.login_020 else R.string.login_030)
            } else {
                checkConfirmable(
                    context,
                    loginID?.text?.toString() ?: "",
                    loginPW?.text?.toString() ?: ""
                )
            }
        }
    }


    private fun updateProgressMessage(context:Context, @StringRes res: Int) {
        context.runOnUiThread {
            progressMessageToUpdate?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(getString(res), Html.FROM_HTML_OPTION_USE_CSS_COLORS)
            } else {
                Html.fromHtml(getString(res))
            }
        }
    }

    @ExperimentalUnsignedTypes
    private fun checkConfirmable(
        context: Context,
        inputLoginId: String,
        inputLoginPw: String
    ) {
        val owner = context as LifecycleOwner

        fun checkBaseContent() {
            updateProgressMessage(context,R.string.login_070)
            ContentHelper.initLoad(context) { _progress ->
                if (_progress == 102) {
                    Single.timer(1L, TimeUnit.SECONDS)
                        .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { onLoginSuccess() },
                            { e -> Timber.d(e) }
                        )
                }
            }
        }

        fun executeError(th: Throwable) {
            when (th) {
                is SocketTimeoutException -> R.string.api_error_msg_050
                is HttpExceptionWrapper   -> {
                    Timber.e("--> Http status code[${th.httpError.status}], message[${th.httpError.message}]")
                    th.httpError.message.let { code ->
                        when (XomnifitHeader.ResultCode.of(code)) {
                            XomnifitHeader.ResultCode.ERROR_NONEXISTENT_ID        -> R.string.api_error_msg_020
                            XomnifitHeader.ResultCode.ERROR_MISMATCH_PW           -> R.string.api_error_msg_030
                            XomnifitHeader.ResultCode.M2015                       -> R.string.api_error_msg_040
                            //XomnifitHeader.ResultCode.ERROR_UNAVAILABLE_ACCESS_TOKEN -> R.string.
                            XomnifitHeader.ResultCode.ERROR_RESPONSE_EMPTY_ENTITY -> R.string.api_error_msg_070
                            XomnifitHeader.ResultCode.ERROR_REQUEST_FAILURE       -> R.string.api_error_msg_080
                            else                                                  -> {
                                Timber.e(th)
                                R.string.api_error_msg_090
                            }
                        }
                    }
                }
                else                      -> {
                    Timber.e(th)
                    R.string.api_error_msg_090
                }
            }.let { messageResId ->
                updateProgressMessage(context,messageResId)

                context.runOnUiThread {
                    closeBtn?.isEnabled = true
                    startLogin?.isEnabled = true
                    progressBaseDataToUpdate?.isVisible = false
                }
            }
        }

        fun executeApiError(e:Throwable) {
            when (e) {
                is NetworkErrorException -> {
                    if (Administrator.get(context).loginId.isNotEmpty()) checkBaseContent()
                    else executeError(e)
                }
                else                     -> executeError(e)
            }
        }

        closeBtn?.isEnabled = false
        startLogin?.isEnabled = false
        progressBaseDataToUpdate?.isVisible = true

        updateProgressMessage(context,R.string.login_040)
        WebApi.loginAdministrator(context,
            owner,
            inputLoginId,
            inputLoginPw,
            {
                updateProgressMessage(context,R.string.login_050)
                WebApi.requireBaseData(context, owner,
                    {
                        updateProgressMessage(context, R.string.login_060)
                        WebApi.getUsers(context, owner,
                            onComplete = {
                                checkBaseContent()
                            },
                            onError = { e -> executeApiError(e) }
                        )
                    },
                    { e -> executeApiError(e) }
                )
            },
            { e -> executeApiError(e) }
        )
    }
}