package omnifit.brain.training.component

import android.graphics.Color
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.training.Font
import omnifit.brain.training.R
import omnifit.brain.training.view.nonSpacingTextView
import omnifit.brain.training.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class BrainDisconnectPopComponent<in T : MaterialDialog>(
        private val onConfirm: () -> Unit = {}
) : AnkoComponent<T> {

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_bg_pop

            // 타이틀
            nonSpacingTextView(R.string.common_040) {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.kopubDotumMedium
                textSize = 29.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(50.0f)
                topToTop = PARENT_ID
                topMargin = dip(48.0f)
            }

            nonSpacingTextView(R.string.headset_setting_040) {
                id = R.id.dialog_widget_01_id_02
                typeface = Font.kopubDotumMedium
                textSize = 26.7f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.01f
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.dialog_widget_01_id_01
                topToBottom = R.id.dialog_widget_01_id_01
                topMargin = dip(28.7f)
            }

            imageButton(R.drawable.selector_confirm_02) {
                backgroundColor = Color.TRANSPARENT
                onDebounceClick {
                    owner.dismiss()
                    onConfirm()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.dialog_widget_01_id_02
                topMargin = dip(40.7f)
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(30.0f)
            }
        }
    }
}