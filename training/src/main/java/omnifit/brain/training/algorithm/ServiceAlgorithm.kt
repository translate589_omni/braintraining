package omnifit.brain.training.algorithm

import omnifit.brain.training.Bindable
import omnifit.commons.binauralbeatgenerator.BinauralBeatGenerator
import omnifit.commons.common.DTP_HH_MM_SS
import omnifit.commons.common.floorToDecimal
import omnifit.commons.common.json.fromJson
import omnifit.commons.common.secondsToFormatString
import timber.log.Timber
import kotlin.math.roundToInt

//import android.graphics.Color
//import androidx.annotation.ColorInt
//import omnifit.commons.algorithm.Indicatable
//import timber.log.Timber
//import kotlin.math.ln
//import kotlin.math.roundToInt
//


object ServiceAlgorithm {
    val EFFECTIVE_TIME_THRESHOLD: Double = 5.0
    val CONTENT_RESULT_SCORE_RANGES: Array<IntRange> = arrayOf(
        0..20,
        21..40,
        41..60,
        61..80,
        81..100
    )

    fun computeEffectiveTimeRatio(
        effectiveTime: Long,
        totalTime: Long
    ): Int = effectiveTime
        .toDouble()
        .div(totalTime.toDouble())
        .times(100.0)
        .roundToInt()

    fun convEffectiveTimeRatioToScore(ratio: Int): Int = when (ratio) {
        in 0..20  -> ratio * 2
        in 21..60 -> ratio + 20
        in 61..62 -> 81
        in 63..64 -> 82
        in 65..66 -> 83
        in 67..68 -> 84
        in 69..70 -> 85
        in 71..72 -> 86
        in 73..74 -> 87
        in 75..76 -> 88
        in 77..78 -> 89
        in 79..80 -> 90
        in 81..82 -> 91
        in 83..84 -> 92
        in 85..86 -> 93
        in 87..88 -> 94
        in 89..90 -> 95
        in 91..92 -> 96
        in 93..94 -> 97
        in 95..96 -> 98
        in 97..98 -> 99
        else      -> 100
    }

    fun exchangeBinauralBeatGeneration(
        elapsedTime: Int,
        meanConcentrationIndicatorValue: Double = 0.0
    ) {

        if (BinauralBeatGenerator.isGenerating
            && elapsedTime != 0
            && elapsedTime % 60 == 0
        ) {
            meanConcentrationIndicatorValue
                .toBinauralBeatFrequency()
                .let { frequency ->
                    Bindable.algorithmMonitorLog.value = "측정된 평균 집중도 [${meanConcentrationIndicatorValue.floorToDecimal(1)}] --> 바이노럴비트 [${frequency}Hz]"
                    if (frequency != BinauralBeatGenerator.beatFrequency) {
                        BinauralBeatGenerator.setBeatFrequency(frequency)
                        BinauralBeatGenerator.generate()

                        Timber.v(
                            "[인공지능 집중 도우미] --> 진행중 변경 : 기준 바이노럴비트 [13Hz], " +
                                    "측정된 평균 집중도 [$meanConcentrationIndicatorValue], " +
                                    "변경 바이노럴비트 [${frequency}Hz], " +
                                    "진행시간 : [${elapsedTime.secondsToFormatString(DTP_HH_MM_SS)}], "
                        )
                    }
                    else Timber.v("[인공지능 집중 도우미] --> 이전[${frequency}Hz]과 동일")
                }
        }
    }
}

/**
 * 측정된 집중도 값에 따른 바이노럴비트 크기(Hz)를 돌려준다.
 *
 * @return 바이노럴비트 Hz
 */
fun Double.toBinauralBeatFrequency(): Double {
    return when {
        this <= 2.68 -> 13.0
        this <= 4.14 -> 14.0
        this <= 4.89 -> 15.0
        this <= 5.43 -> 16.0
        this <= 5.83 -> 17.0
        this <= 6.16 -> 18.0
        this <= 6.83 -> 19.0
        else         -> 20.0
    }
}

fun String.toDoubleArray(): DoubleArray = doubleArrayOf().fromJson(this)

