package omnifit.brain.training

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.text.Spanned
import android.widget.ImageButton
import android.widget.ImageView
import androidx.annotation.AnimRes
import androidx.core.text.parseAsHtml
import androidx.fragment.app.Fragment
import androidx.fragment.app.commitNow
import com.afollestad.materialdialogs.MaterialDialog
import org.jetbrains.anko.AnkoContext

fun UIFrame.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = supportFragmentManager.commitNow(allowStateLoss = true) {
    setCustomAnimations(enter, exit)
    replace(R.id.screen_container, screen, screen::class.java.simpleName)
}.run { true }

fun UIScreen.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = (requireActivity() as? UIFrame)?.screenTo(screen, enter, exit) ?: false

fun AnkoContext<Fragment>.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = (owner as? UIScreen)?.screenTo(screen, enter, exit) ?: false

fun UIFrame.identifier(
    name: String,
    defType: String,
    defPackage: String
): Int = resources.getIdentifier(name, defType, defPackage)

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥ RESOURCE
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

fun String.htmlAsSpanned(): Spanned = parseAsHtml()
fun String.replaceNewLineAsSpanned(): Spanned = replace("\\n", "<br>").parseAsHtml()
fun String.replaceNewLineAsSpace(): String = replace("\\n", " ")
fun String.replaceNewLineAs(): String = replace("\n", "")
fun String.removeSquareBrackets(): String = replace("""[\[\]]""".toRegex(), "")

fun ImageView.into(drawable: Drawable?) {
    setImageDrawable(drawable)
}

fun ImageButton.into(drawable: Drawable?) {
    setImageDrawable(drawable)
}

fun MaterialDialog.hideNavigation() {
    window?.decorView?.systemUiVisibility = UIFrame.DEFAULT_SYSTEM_UI_FLAG
}

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

fun getAppVersion(context: Context): String {
    return try {
        context.packageManager.getPackageInfo(context.packageName, 0).versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        "0.0.0"
    }
}