/**
 * AndroidNativeInterface는 Android naitvie 기능 호출에 필요한 interface를 제공하는 클래스입니다.
 *
 * @classDescription
 * @type {Object}
 */
var AndroidNativeInterface = (function() {
	/**
	 *  NativeInterface initialize.
	 */
	function initialize(){
		console.log("AndroidNativeInterface >> initialize called");
	};


    function onGameStart(level)
    {
        console.log("AndroidNativeInterface >> onGameStart :: level = " + level);
        if (window['omnigame'] && window['omnigame'].onGameStart){
            window.omnigame.onGameStart(String(level));
        }
    }

    function onGameEnd(score)
    {
        console.log("AndroidNativeInterface >> onGameEnd :: score = " + score);
        if (window['omnigame'] && window['omnigame'].onGameEnd){   
            window.omnigame.onGameEnd(String(score));
        }
    }

    function onNextGame(result)
    {
        console.log("AndroidNativeInterface >> onNextGame :: result = " + result);
        if (window['omnigame'] && window['omnigame'].onNextGame){   
            window.omnigame.onNextGame(String(result));
        }
    }

    function onStart(data,time)
    {
        if (window['omnigame'] && window['omnigame'].onStart) {
            window['omnigame'].onStart(String(data),String(time));
        }else{
            window.onStart();
        }
    }
	//////////////////////////////////////////////////////////////////////////////////////////////////
    function onkeydown(val){
         console.log("AndroidNativeInterface >> keyDown val :::"+ val.keyCode);
        if (val.keyCode >= 48 && val.keyCode <= 57){
            //0 ~ 9
            window.callJS(val.keyCode - 48);
           
        }else if (val.keyCode == 189){
            // -
            window.callJS(10);
            
        }
        // else if(val.keyCode == 13){
        //     var data =  '{"last_yn": "N","logo_skip_yn": "Y","intro_skip_yn": "N","result_skip_yn": "N",	"sham_mode_yn": "N",	"define_game_level": "2",	"play_time": "30",	"logo_auto_skip_yn": "Y",	"logo_skip_wait_time": "8000",	"result_auto_skip_yn": "N",	"result_skip_wait_time": "180"}';
        //     OnNativeEvent("onStart",data,180);

        // }
    }

	// Native -> UI 함수 호출 ///////////////////////////////////////////////////////////////////////////
	function OnNativeEvent(command, data, time){
		console.log("AndroidNativeInterface   ++++++++++++++++++++++     command = " + command + ", data = " + data + ", time = " + time);

        var result;
        switch (command)
        {
            case "callJS":
                if (Number(data) < 0){
                    data = 0;
                    return;
                }
                PlayGameScene.prototype.setScore(Number(data));
                break;
            case "callLevelJS":
                // IntroScene.prototype.LEVEL = Number(data);
                // PlayGameScene.prototype.LEVEL = Number(data);
                break;
            case "onStart":
                /** onStart 이벤트를 받는다 **/
                // var data =  '{"logo_skip_yn": "Y","intro_skip_yn": "N","result_skip_yn": "N",	"sham_mode_yn": "Y",	"define_game_level": "2",	"play_time": "30",	"logo_auto_skip_yn": "Y",	"logo_skip_wait_time": "8000",	"result_auto_skip_yn": "N",	"result_skip_wait_time": "180", "result_button_code": "BTN001"}';
                
                if(typeof data == 'string'){
                    onSetGameConfig((data));
                }else{
                    onSetGameConfig(JSON.stringify(data));
                }
                if (currentScene == GAME_SCENE.INTRO_SCENE){
                    IntroScene.prototype.onStartAudio();
                }else if (currentScene == GAME_SCENE.PLAY_SCENE){
                    PlayGameScene.prototype.onStartAudio();
                }                
                break;
        }
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////

	/* Public API */
	return {
		Initialize      : initialize,
        onGameStart     : onGameStart,
        onGameEnd       : onGameEnd,
        onNextGame      : onNextGame,
        onStart         : onStart,
        onkeydown       : onkeydown,
		OnNativeEvent   : OnNativeEvent
	};

})();
