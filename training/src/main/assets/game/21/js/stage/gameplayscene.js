// main game scene

var PlayGameScene = new Phaser.Class(function(){
    // PlayGame 상태값
    var STATE = {
        STATE_READY : 0,
        STATE_PLAY_GAME : 1,
        STATE_DISPLAY_RESULT : 2,
        STATE_END_GAME : 3
    };
    var testMode = false;
    // 결과 화면에서 선택된 버튼의 상태를 위한 변수
    var GAME_BTN_STATE = {
        NONE : 0,
        RESTART : 1,
        EXIT : 2
    };

    // 게임 정보를 저장.
    var _GAME_PLAY_INFO = {
        SHOOTING_CNT : 0,// 몇회째 게임인지
        //BONUS_SCORE : 0,//보너스 점수 총합.
        CURRENT_DETAIL_SCORE : [],// 현재 올라오는 집중력 점수
        GAME_SCORE:[],//30번의 게임 평균 점수(6초 단위 평균)
        BONUS_SCORE:[],//29회 게임 보너스 점수.
        TOTAL_SCORE:[] // 게임중 올라온 모든 점수를 저장
    }
    // 화살의 각도
    var _sootingAngle    = 55;


    var __CHECK_TIME__  = 180;  // 180초 측정
    var __MAX_SHOOTING_CNT__ = 30; // 최대 발사 횟수
    var _SCORE       = null;   // 브레인 측정 점수
    var _currentScore = null   // 현재 측정 점수(3회 평균)
    var _prevScore   = null;   // 직전 측정 점수
    var _TOTAL_SCORE = null; // 점수 총합 + 보너스 총합
    var _prevprevScore = null; // 2번 전 점수
    var _gameResult = 0;        // native에 onNextGame 함수 호출시에 전달한 파라메타 (결과값) // 1: bad 2 : not good 3 : good 4 : very good 5 : excellent
    var _totalValue = 0;        /** 게임 종료시 onNextGame에 게임 결과(percent)를 전달 하기 위한 변수 추가 **/
    var _playgameScene       = null;  //this scope

    // 오디오 저장 변수
    var audio_play_bgm      = null; // Phaser -JS- Audio
    var audio_ready_start   = null; // Phaser -JS- Audio
    var audio_game_win      = null; // Phaser audio
    // var audio_game_lose     = null; // Phaser audio
    // var audio_arm_wrestling = null; // Phaser audio - 힘쓰는 소리
    var audio_cheerRing     = null; // Phaser audio
    var countSound          = null; // 화살 쏘는 타이머 소리.
    var arrowSound          = null; // 화살 쏘고 맞는 소리 휙~~ 탁!!.
    var arrowSound2          = null; // 화살 쏘는 소리 휙~~.
    var arrowSound3          = null; // 화살 맞는 소리 탁.



    // Scene 전체의 넓이, 높이 변수
    var _width           = 0;
    var _height          = 0;
    var _screenWidth     = 0;

    // 현재 게임 Scene의 상태값 변수
    var __gameState = STATE.STATE_READY;

    // GageBar 관련 변수
    var _gageBarWidth    = null;
    var __INTERVAL_GAGE__  = 2.8;
    var __MAX_SCORE_WIDTH__   = __INTERVAL_GAGE__ * 10 * 10;
    var __MIN_SCORE_WIDTH__   = 0;

    // 결과 화면에서 버튼 이벤트 처리를 위한 변수
    var _isGameAgain     = GAME_BTN_STATE.NONE;
    var _isSooting       = false; // 화살 쏘는 중인지....
    var sumScoreValue   = 0;
    var arrowType       = 1;
    var canFire         = 'not';// 'not', 'resume', 'fire'
    var updateCnt       = 0; // 화살 조준점이 떨리는 것을 모든 update되는 frame에 반응할수 없어 5frame에 한번씩 떨리게 만들기 위한 frame 체크 변수


    var reductionValue = 1;
    // var mode = 0;   // 0:default, 1: zoom,
    var arrowMode = 0; // 0:arrowEffect + hitEffect, 1: arrow + hitEffect, 2: arrow
    // 샴 데이터를 처리하기 위한 배열
    var _aSiamData = [];
    var _strSiamRatio = "";

    // Toast Message
    var _messageEn = "Focus on shooting the arrow in the very center of the target.";
    var _messageKr = "과녁의 정중앙에 화살을 쏘는 것에 집중하세요.";

    return{
        Extends: Phaser.Scene,
        initialize:

        function PlayGameScene ()
        {
            Phaser.Scene.call(this, { key: 'gameplayscene' });
        },
        preload: function ()
        {
            // 현재 게임 Scene 설정 (외부에서 판단하기 위해서)
            currentScene = GAME_SCENE.PLAY_SCENE;
            if (!isNaN(_gGamePlayTime)){
                __CHECK_TIME__ = _gGamePlayTime;
            }

            this.checkTime = __CHECK_TIME__;
            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                _aSiamData = this.setSham();
            }
            // if (isAndroid){
            //     audio_ready_start = new Audio('assets/audio/ready_start.wav');
            //     audio_ready_start.autoplay = true;
            //     audio_ready_start.loop = false;
            //     console.log('Android mobile >> audio_ready_start autoplay == true');
            // }
            console.log("PlayGameScene >> called preload");
        },
        create: function ()
        {
            _playgameScene = this;  // 해당 Scene 내의 전역변수 설정
            _width       = this.game.config.width; // 게임 화면 넓이
            _screenWidth = this.game.config.width; // 화면 전체 넓이
            _height = this.game.config.height;   // 화면 전체 높이
            var correctionValue = 1;
            if (_width > 600){
                _width = 600;
            }else{
                correctionValue = 0.8;
            }

            _playgameScene.consoleLog('create is ready in PlayGameScene');
            // 그룹 생성
            this.gameGroup = this.add.group();
            this.resultGroup = this.add.group();
            /***************************** 게임 플레이 ***************************/

            // 배경 이미지
            this.bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'game_bg');
            // this.bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'game_bg');
            // if (this.game.config.width > this.game.config.height){
                this.bg.displayWidth = _screenWidth; //scale.setTo(0.5, 0.5);
                // this.bg.displayHeight = _screenWidth * 1.78;//this.game.config.height;
                this.bg.displayHeight = this.bg.height;// * (this.bg.displayWidth/this.bg.width);
            // }else{
            //     this.bg.displayWidth = _width; //scale.setTo(0.5, 0.5);
            //     this.bg.displayHeight = this.game.config.height;
            // }

            this.bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'game_bg');
            this.bg.displayWidth = _screenWidth; //scale.setTo(0.5, 0.5);
            this.bg.displayHeight = this.game.config.height;

            if (this.game.config.width > this.game.config.height){
                this.bg.displayWidth = _screenWidth; //scale.setTo(0.5, 0.5);
                this.bg.displayHeight = _screenWidth * 1.78;//this.game.config.height;
            }else{
                this.bg.displayWidth = _screenWidth; //scale.setTo(0.5, 0.5);
                this.bg.displayHeight = this.game.config.height;
            }




            this.arrowBg = this.add.image(_screenWidth/2, _height/2, 'intro_bg_zoom');
            this.arrowBg.displayHeight = this.bg.displayHeight;
            this.arrowBg.displayWidth = this.bg.displayWidth;
            this.arrowBg.visible = false;

            this.focus_line = this.add.image(_screenWidth/2, _height/2, 'focus_line');
            this.focus_line.displayHeight = this.bg.displayHeight;
            this.focus_line.displayWidth = this.bg.displayWidth;

            this.focus_line.visible = false;
            // 과녁 생성
            this.startCountAni = this.anims.create({
                key: 'targetBoardZoomIn',
                frames: [
                    { key: 'targetBoard01'},
                    { key: 'targetBoard02'},
                    { key: 'targetBoard03'},
                    { key: 'targetBoard04'}
                ],
                sound : this.readySound,
                frameRate: 30,
                delay: 100,
                repeat: 0
            });
            this.startCountAni = this.anims.create({
                key: 'targetBoardZoomOut',
                frames: [
                    { key: 'targetBoard01' }
                ],
                sound : this.readySound,
                frameRate: 30,
                delay: 0,
                repeat: 0
            });
            this.targetBoardImg = this.add.sprite(_screenWidth/2, _height*0.5, 'targetBoard01').setOrigin(0.5,0.3);

            this.targetBoardImg.displayWidth = _width*0.8;
            this.targetBoardImg.displayHeight = this.targetBoardImg.height * (this.targetBoardImg.displayWidth/this.targetBoardImg.width);
            this.targetBoardImg.on('animationcomplete', this.targetZoom, this);


            this.targetBoardImg.play('targetBoardZoomIn');



            this.targetBoard = this.add.image(this.targetBoardImg.x, this.targetBoardImg.y, 'targetBoard').setOrigin(0.5,0.3);
            // this.targetBoard = this.add.image(_screenWidth/2, _height*0.7, 'targetBoard').setOrigin(0.5,1);
            this.targetBoard.displayWidth = _width*1.2;
            this.targetBoard.displayHeight = this.targetBoard.height * (this.targetBoard.displayWidth/this.targetBoard.width);
            this.targetBoard.visible = false;
            this.arrowMark = this.add.sprite(this.targetBoard.x, this.targetBoard.y, 'arrowMark').setOrigin(0.5,0.3);
            this.arrowMark.visible = false;
            // 궁수 생성
              this.bow = this.add.sprite(_screenWidth/2, _height, 'ic_bow').setScale(0.3* correctionValue).setOrigin(0.5,1);
            this.bow.x += _width*0.35;
            this.Archer = this.add.sprite(this.bow.x, _height, 'ic_arm').setScale(0.3 * correctionValue).setOrigin(0.5,1);
            this.Archer.x -= this.bow.displayWidth/4.5;
            this.createAnimation(this);

            this.arrow00 = this.add.sprite(this.bow.x, this.bow.y, 'arrow01').setOrigin(0.5,0.2).setScale(0.5);
            this.arrow00.x -= this.bow.displayWidth/6;
            this.arrow00.y -= this.bow.displayHeight * 0.37;
            arrowType = 1;

            this.bow.y += this.bow.displayHeight/6;
            this.Archer.y += this.bow.displayHeight/6;
            this.arrow00.y += this.bow.displayHeight/6;

            // 조준점
            var arrowFocusSize = _width * 0.1;
            this.arrowfocus = this.add.image(_screenWidth/2, _height*0.35, 'arrowfocus');//.setScale(reductionValue);
            this.arrowfocus.setDisplaySize(arrowFocusSize,arrowFocusSize);
            this.arrowfocus.alpha =0;

            // 화살 각도
            var rotateAngle = Math.atan2(this.arrow00.x - this.targetBoardImg.x ,this.arrow00.y - this.targetBoardImg.y)*180/Math.PI;
            this.arrow00.angle = -rotateAngle;
            _sootingAngle = 90 - rotateAngle;
            _playgameScene.consoleLog('_sootingAngle : '+ _sootingAngle);

            /* 화살 발사 효과 */
            this.particles = this.add.particles('flares');
            this.arrowEmitter = this.particles.createEmitter({
                frame: 'red',
                lifespan: 2000,
                speed: { min: 200, max: 600 },
                angle: _sootingAngle,
                gravityY: 0,
                scale: { start: 0.4, end: 0 },
                quantity: 2,
                active: true,
                visible: false,
                blendMode: 'ADD'
            });
            /* 화살 과녁 명중 효과 */
            this.explosion = this.add.particles('explosion');
            this.explosionEmitter = this.explosion.createEmitter({
                frame: 'muzzleflash2',
                lifespan: 200,
                scale: { start: 1, end: 0 },
                rotate: { start: 0, end: 180 },
                on: false
                // frame: 'red',
                // angle: { min: 160, max: 300, steps: 3 },
                // lifespan: 100,
                // speed: 300,
                // quantity: 3,
                // scale: { start: 0.2, end: 0 },
                // on: false

            });

            // 연승시 추가 점수 표시 텍스트 (text)
            var textStyleOfBonusScore = null;
            if (_width < 600){
                textStyleOfBonusScore = { font: "bold 35px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfBonusScore = { font: "bold 45px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.bonusScoreText = this.add.text(_playgameScene.targetBoardImg.x - _playgameScene.targetBoardImg.displayWidth/5 , _playgameScene.targetBoardImg.y - _playgameScene.targetBoardImg.displayHeight/2 - 50 , "+ 5", textStyleOfBonusScore).setOrigin(0.5, 0.5);
            this.tweenBonusScore = this.tweens.add({
                targets : this.bonusScoreText,
                x : _playgameScene.targetBoardImg.x - _playgameScene.targetBoardImg.displayWidth/5,
                y : _playgameScene.targetBoardImg.y - _playgameScene.targetBoardImg.displayHeight - 50,
                duration : 1000,
                delay : 0,
                ease : 'Back.Out',
                repeat : 0,
                onComplete: function () {
                    _playgameScene.bonusScoreText.visible = false;
                }
            });
            this.bonusScoreText.visible = false;

            var scoreboard_height = (_width * this.game.textures.get("ground_score_board").getSourceImage().height) / this.game.textures.get("ground_score_board").getSourceImage().width;
            this.scoreBoard = this.add.image(_screenWidth/2, 0, 'ground_score_board').setDisplaySize(_width, scoreboard_height).setOrigin(0.5, 0);

            if (isDisplaySiamRatio && (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END)) {
                this.siamRatioText = this.add.text(_screenWidth/2  , this.scoreBoard.displayHeight , _strSiamRatio, { font: "bold 15px Arial", fill: "#cde6ed", boundsAlignH: "center", boundsAlignV: "middle" }).setOrigin(0.5, 0.5);
            }

            var rate = (this.scoreBoard.displayHeight/this.scoreBoard.height);
            this.scorebtnNow = this.add.image(_screenWidth/2, 0, 'ic_scoreboard_01').setOrigin(0.5);//.setScale(0.5);
            this.scorebtnPrev = this.add.image(_screenWidth/2, 0, 'ic_scoreboard_02').setOrigin(0.5);//.setScale(0.5);
            this.scorebtnPrevPrev = this.add.image(_screenWidth/2, 0, 'ic_scoreboard_03').setOrigin(0.5);//.setScale(0.5);

            this.scorebtnNow.displayHeight = this.scorebtnPrev.displayHeight = this.scorebtnPrevPrev.displayHeight =  this.scorebtnNow.height * rate;
            this.scorebtnNow.displayWidth =  this.scorebtnPrev.displayWidth = this.scorebtnPrevPrev.displayWidth =  this.scorebtnNow.width * rate;

            var scoreboardTerm = (this.scoreBoard.displayWidth/2 - (this.scorebtnNow.displayWidth *3))/4;
            this.scorebtnNow.x +=  scoreboardTerm*1.5 * correctionValue;
            this.scorebtnPrev.x =  this.scorebtnNow.x + this.scorebtnNow.displayWidth + scoreboardTerm;
            this.scorebtnPrevPrev.x =  this.scorebtnPrev.x + this.scorebtnPrev.displayWidth + scoreboardTerm;
            this.scorebtnNow.y = this.scorebtnPrev.y = this.scorebtnPrevPrev.y = this.scoreBoard.y+this.scoreBoard.displayHeight*0.82 - this.scorebtnNow.displayHeight/2;

            // Score / win / lose 텍스트 (text)
            var textStyleOfScore = null;
            if (_width < 600){
                textStyleOfScore = { font: "bold 15px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfScore = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.scoreText = this.add.text(this.scoreBoard.x - this.scoreBoard.displayWidth * 0.2  , this.scoreBoard.displayHeight * 0.68 , 0, textStyleOfScore).setOrigin(0.5, 0.5);

            this.currentScore = this.add.text(this.scorebtnNow.x, this.scorebtnNow.y, 0, textStyleOfScore).setOrigin(0.5, 0.7);
            this.prevScore = this.add.text(this.scorebtnPrev.x, this.scorebtnPrev.y, 0, textStyleOfScore).setOrigin(0.5, 0.7);
            this.prevprevScore = this.add.text(this.scorebtnPrevPrev.x, this.scorebtnPrevPrev.y, 0, textStyleOfScore).setOrigin(0.5, 0.7);

            this.playAlarm = this.add.image(_screenWidth/2 + _width*0.4, _height * 0.25, 'ground_play_alarm').setDisplaySize(_width * 0.2, _width * 0.2);//.setDisplaySize(100,100);

            this.gageBarBG = this.add.graphics();


            this.gageBar = this.add.graphics();
            var gage_width = 340;
            var gage_height = (gage_width * this.game.textures.get("ground_gage_empty").getSourceImage().height) / this.game.textures.get("ground_gage_empty").getSourceImage().width;
            this.mentalGage = this.add.image(_screenWidth/2, _height * 0.93, 'ground_gage_empty').setDisplaySize(gage_width, gage_height);

            var x = _screenWidth/2 - this.mentalGage.displayWidth/2 + 55;
            var y = _height * 0.93;

            this.gageBarBG.fillStyle(0x212f39, 1);
            this.gageBarBG.fillRoundedRect(x, y - 12, __MAX_SCORE_WIDTH__, 20, 8);

            // Touch the screen 텍스트 (text)
            var textStyleOfAlarm = null;
            if (_width < 600){
                textStyleOfAlarm = { font: "bold 20px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfAlarm = { font: "bold 30px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.alarmText = this.add.text(this.playAlarm.x , this.playAlarm.y , this.checkTime, textStyleOfAlarm).setOrigin(0.5);

            // Native에서 오는 텍스트 출력을 위한 변수
            // var textStyleOfScore = { font: "bold 20px Arial", fill: "#FFF", boundsAlignH: "center", boundsAlignV: "middle" };
            if(testMode == true){
                this.scoreDisplayText = this.add.text(_width/2, 20, "callJS : " + _SCORE, textStyleOfScore).setOrigin(0.5);
                this.scoreDisplayText.visible = false;
            }
            this.updateGage();

            this.ic_scoreresult = this.add.image(_screenWidth*2, _height/3 , 'ic_scoreresult');
                this.ic_scoreresult.displayWidth = _width;//this.bg.displayWidth;
                this.ic_scoreresult.displayHeight = this.ic_scoreresult.height * (this.ic_scoreresult.displayWidth/this.ic_scoreresult.width);

            this.ic_scoreresult_text = this.add.text(_screenWidth/2, this.ic_scoreresult.y*1.05, '+6',{ fontSize: 100, fontFamily:'bold Arial',color:'#F00'}).setOrigin(0.5);
                this.ic_scoreresult_text.setStroke('#fff', 15);
                this.ic_scoreresult_text.visible = false;

            this.gameGroup.add(this.arrowBg);
            this.gameGroup.add(this.focus_line);
            this.gameGroup.add(this.targetBoardImg);
            this.gameGroup.add(this.targetBoard);

            this.gameGroup.add(this.arrowfocus);
            this.gameGroup.add(this.bow);
            this.gameGroup.add(this.Archer);
            this.gameGroup.add(this.arrow00);
            this.gameGroup.add(this.particles);


            this.gameGroup.add(this.playAlarm);
            this.gameGroup.add(this.alarmText);
            if(testMode == true){
                this.gameGroup.add(this.scoreDisplayText);
            }
            // this.playGroup.add(this.matchDisplayText);
            this.gameGroup.add(this.mentalGage);
            this.gameGroup.add(this.gageBar);
            this.gameGroup.add(this.gageBarBG);
            this.gameGroup.add(this.ic_scoreresult);
            this.gameGroup.add(this.ic_scoreresult_text);



            this.result_popup_bg = this.add.image(_screenWidth/2, _height/2, 'result_ic_result_popup_bg').setDisplaySize(_width * 0.75, _width * 0.75 * 1.62);
            var yResultBtn = this.result_popup_bg.displayHeight/2 + _height/2;
            this.result_popup_view_result_sel = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_sel').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_view_result_sel.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_sel.setInteractive();
            this.result_popup_view_result_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_view_result_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_nor.setInteractive();
            this.result_popup_next_game_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_nextgame_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_next_game_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_next_game_nor.setInteractive();

            this.result_popup_exit_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_exit_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_exit_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_exit_nor.setInteractive();


            

            // _playgameScene.consoleLog('_width = ' + _width);
            // scale이 최대 넓이 600px일 경우에 0.3
            // 각 해상도에 따라서 최대 넓이 기준으로 비율 조정
            var scaleOfResultText = 0.3 * _width/600;
            this.result_score = this.add.sprite(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_bad').setScale(scaleOfResultText);
            // this.result_popup_text_bad = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_bad').setScale(scaleOfResultText);
            // this.result_popup_text_notgood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_notgood').setScale(scaleOfResultText);
            // this.result_popup_text_good = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_good').setScale(scaleOfResultText);
            // this.result_popup_text_verygood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_verygood').setScale(scaleOfResultText);
            // this.result_popup_text_excellent = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_excellent').setScale(scaleOfResultText);

            // 위치는 결과 텍스트를 기존으로 밑으로 붙임.
            var textStyleOfResultScore = null;
            if (_width < 600){
                textStyleOfResultScore = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
            }else {
                textStyleOfResultScore = { font: "bold 35px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.scoreTextOfResult = this.add.text(_screenWidth/2 , this.result_popup_bg.y + (this.result_score.displayHeight + 9) , 231, textStyleOfResultScore).setOrigin(0.5, 1);
            this.scoreTextOfResult.setStroke('#000', 5);

            // 높이의 기준은 _width 로 하며, 해당 넓이에 따라서 비율로 처리
            var scaleOfResultArrow = 0.38 * _width/600;
            this.result_popup_arrow = this.add.image(_screenWidth/2 - this.result_popup_bg.displayWidth/3, _height/2  + _width * 0.25 , 'result_popup_arrow').setScale(scaleOfResultArrow);

            // 결과 팝업에서 보여주는 점수 텍스트
            var textStyleOfResultStandardScore = null;
            if (_width < 600){
            textStyleOfResultStandardScore = { font: "bold 13px Arial", fill: "#000", align: "right", boundsAlignH: "right", boundsAlignV: "middle"};
            }else {
            textStyleOfResultStandardScore = { font: "bold 20px Arial", fill: "#000", align: "right"/*, boundsAlignH: "right", boundsAlignV: "middle"*/ };
            }
            this.scoreTextOfResult1 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_score.displayHeight + 7) , "0 ~ 20 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult2 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_score.displayHeight + 7) , "21 ~ 40 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult3 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_score.displayHeight + 7) , "41 ~ 60 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult4 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_score.displayHeight + 7) , "61 ~ 80 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult5 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_score.displayHeight + 7) , "81 ~ 100 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);



            this.resultGroup.add(this.result_popup_bg);
            this.resultGroup.add(this.result_popup_view_result_nor);
            this.resultGroup.add(this.result_popup_view_result_sel);
            this.resultGroup.add(this.result_popup_next_game_nor);
            this.resultGroup.add(this.result_popup_exit_nor);
            this.resultGroup.add(this.result_score );
            this.resultGroup.add(this.scoreTextOfResult);
            this.resultGroup.add(this.result_popup_arrow);
            this.resultGroup.add(this.scoreTextOfResult1);
            this.resultGroup.add(this.scoreTextOfResult2);
            this.resultGroup.add(this.scoreTextOfResult3);
            this.resultGroup.add(this.scoreTextOfResult4);
            this.resultGroup.add(this.scoreTextOfResult5);
            this.consoleLog('this.result_popup_bg.height : '+ this.result_popup_bg.displayHeight +"  y : "+ this.result_popup_bg.y);
            this.consoleLog('this.result_popup_view_result_nor.height : '+ this.result_popup_view_result_nor.displayHeight +"  y : "+ this.result_popup_view_result_nor.y);
            this.consoleLog('_height : '+ _height +"  yResultBtn : "+ yResultBtn);

            var resultBgTop = ( this.result_popup_bg.y-this.result_popup_bg.displayHeight/2);
            var resultBtnBottom = ( this.result_popup_view_result_nor.y+this.result_popup_view_result_nor.displayHeight/2);
            var distanceY = resultBgTop-(_height-(resultBtnBottom-resultBgTop))/2;
            this.consoleLog('resultBgTop : '+ resultBgTop +"  resultBtnBottom : "+ resultBtnBottom +"  distanceY : "+ distanceY);
            _playgameScene.tweens.add({
                targets: _playgameScene.resultGroup.getChildren(),
                x:'+='+ _screenWidth,
                y:'-='+distanceY,
                duration: 10,
                speed:1000,
                // ease: 'Bounce'
                ease: 'Liner'
            });

            this.resultGroup.toggleVisible(); // 기본 숨기기
            this.result_popup_view_result_sel.visible = false;

            // Event
            this.result_popup_view_result_nor.on('pointerdown', function(porinter, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = true;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = false;
            });
            this.result_popup_view_result_sel.on('pointerup', function(porinter, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = true;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = false;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });
            this.result_popup_next_game_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = true;
                _playgameScene.result_popup_exit_nor.visible = false;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });
            this.result_popup_exit_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = true;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });

            if (_gGameMode == GAME_MODE.NORMAR_END || _gGameMode == GAME_MODE.SIAM_END){
                this.result_popup_view_result_nor.visible = false;
            }else{
                this.result_popup_view_result_sel.visible = false;
            }

            // Audio
            if (_gIsPlayingAudioByPhaser){
                audio_play_bgm  = this.sound.add('audio_play_bgm');
                audio_game_win  = this.sound.add('audio_game_win');
                //audio_game_lose = this.sound.add('audio_game_lose');
                //audio_arm_wrestling = this.sound.add('audio_arm_wrestling');
                audio_cheerRing = this.sound.add('audio_cheerRing');
                countSound = this.sound.add('countBgm');
                arrowSound = this.sound.add('arrowBgm');
                arrowSound2 = this.sound.add('arrowSound');
                arrowSound3 = this.sound.add('arrowSoundShot');
            }else{
                audio_play_bgm  = new Audio('assets/audio/Game_play_BGM.mp3');
                audio_game_win  =  new Audio('assets/audio/game_clear_good(Result).wav');
                //audio_game_lose =  new Audio('audio_game_lose');
                //audio_arm_wrestling =  new Audio('audio_arm_wrestling');
                audio_cheerRing =  new Audio('assets/audio/cheerRing.mp3');
                countSound =  new Audio('assets/audio/countSound.wav');
                arrowSound =  new Audio('assets/audio/arrowSound01.mp3');
                arrowSound2 =  new Audio('assets/audio/arrowSound02.mp3');
                arrowSound3 =  new Audio('assets/audio/arrowSound03.mp3');
            }
            /********************************************************************/
            /********************************************************************/
            // 스타트 오디오 시작
            if (_gIsPlayingAudioByPhaser == true){
                audio_ready_start = this.sound.add('audio_ready_start');
                audio_ready_start.play();
                // 시작 오디오 play시에 처리
                // 1. native에 onGameStart 함수 호출
                /****************** 반드시 호출 필요 ******************/
                nativeInterface.onGameStart(_gGameDifficultyStep);
                /****************** 반드시 호출 필요 ******************/
                // 2. siam 개임인 경우, siam 데이터 생성
                audio_ready_start.once('play', function(){
                    _playgameScene.consoleLog('audio_ready_start start to play!!!');
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.
                    /****************** 반드시 호출 필요 ******************/
                    // nativeInterface.onGameStart(_gGameDifficultyStep);
                    /****************** 반드시 호출 필요 ******************/
                });
                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.on('complete', function(){
                    _playgameScene.showToast();
                    audio_ready_start.currentTime = 0;
                    // _playgameScene.consoleLog("start munsic ended");
                    _playgameScene.checkTime = __CHECK_TIME__;  // Timer 초기화

                    _playgameScene.startCntTween = _playgameScene.tweens.add({
                        targets: _playgameScene.startCount,
                        duration: 500,
                        alpha: 0
                    });

                    _playgameScene.arrowTween = _playgameScene.tweens.add({
                        targets: _playgameScene.arrow,
                        duration: 100,
                        alpha: 0.8
                    });


                    _playgameScene.targetBoard.visible = true;
                    //_playgameScene.targetBoard.setScale(1);

                    _playgameScene.timedEvent = _playgameScene.time.addEvent({
                        delay: 1000,                // ms
                        callback: _playgameScene.updateTimer,   // 측정 타이머 함수
                        callbackScope: _playgameScene,
                        repeat: _playgameScene.checkTime
                    });

                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    __gameState = STATE.STATE_PLAY_GAME;
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.

                    audio_play_bgm.play({loop:true, volume:0.5});

                    // this.timedEvent = this.time.addEvent({ delay: 1000, callback: this.onEvent, callbackScope: this, loop: true });
                });
            }else{
                // 시작 오디오 play시에 처리
                // 1. native에 onGameStart 함수 호출
                // 2. siam 개임인 경우, siam 데이터 생성
                audio_ready_start = new Audio('assets/audio/ready_start.wav');
                audio_ready_start.autoplay = true;
                audio_ready_start.loop = false;
                audio_ready_start.addEventListener('play', function(){
                    _playgameScene.consoleLog('Android :: audio_ready_start start to play!!!');
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.
                    /****************** 반드시 호출 필요 ******************/
                    nativeInterface.onGameStart(_gGameDifficultyStep);
                    /****************** 반드시 호출 필요 ******************/
                });

                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.addEventListener("ended", function(){
                    _playgameScene.showToast();
                    audio_ready_start.currentTime = 0;
                    // _playgameScene.consoleLog("start munsic ended");
                    _playgameScene.checkTime = __CHECK_TIME__;  // Timer 초기화

                    _playgameScene.startCntTween = _playgameScene.tweens.add({
                        targets: _playgameScene.startCount,
                        duration: 500,
                        alpha: 0
                    });

                    _playgameScene.arrowTween = _playgameScene.tweens.add({
                        targets: _playgameScene.arrow,
                        duration: 100,
                        alpha: 0.8
                    });


                    _playgameScene.targetBoard.visible = true;
                    //_playgameScene.targetBoard.setScale(1);

                    _playgameScene.timedEvent = _playgameScene.time.addEvent({
                        delay: 1000,                // ms
                        callback: _playgameScene.updateTimer,   // 측정 타이머 함수
                        callbackScope: _playgameScene,
                        repeat: _playgameScene.checkTime
                    });

                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    __gameState = STATE.STATE_PLAY_GAME;
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.

                    audio_play_bgm.play();
                    audio_play_bgm.loop = true;
                    audio_play_bgm.volume = 0.5;
                });
            }
            /***************************  이벤트 처리  ***************************/
            // 터치 이벤트 처리
            // this.pointer = this.input.on('pointerup', this.doNextState);
            //nativeInterface.onGameStart(_gGameDifficultyStep); //난이도
            //nativeInterface.onStart(null);
            // nativeInterface.onGameStart(_gGameDifficultyStep);
        },
        update: function ()
        {
            var rotateAngle;
            if(testMode == true){
                this.scoreDisplayText.setText("callJS : " + _SCORE);
            }
            if (__gameState == STATE.STATE_READY){
                rotateAngle = Math.atan2(this.arrow00.x - this.arrowfocus.x,this.arrow00.y - this.arrowfocus.y)*180/Math.PI;
                this.arrow00.angle = -rotateAngle;
                this.bow.angle = -rotateAngle*0.2;
                this.Archer.angle = -rotateAngle*0.2;

            }else if (__gameState == STATE.STATE_PLAY_GAME){
                /* 게임 진행 화면 상태 */
                if(_isSooting != true){
                    this.updateGage();
                    rotateAngle = Math.atan2(this.arrow00.x - this.arrowfocus.x,this.arrow00.y - this.arrowfocus.y)*180/Math.PI;
                    this.arrow00.angle = -rotateAngle;
                    this.bow.angle = -rotateAngle*0.2;
                    this.Archer.angle = -rotateAngle*0.2;
                }

            }else if (__gameState == STATE.STATE_DISPLAY_RESULT){
                /* 게임 결과 화면 상태 */
                _playgameScene.pauseAudio(audio_play_bgm);
            }else if (__gameState == STATE.STATE_END_GAME){
                /* 게임 종료하는 상태 */

            }

        },
/**             native->web                      */
        onStartAudio: function()
        {
            _playgameScene.consoleLog("PlayGame >> onStart mode = "+ mode);
            // if (mode !== undefined){
            //     if (mode >= GAME_MODE.NORMAR && mode <= GAME_MODE.SIAM_END){
            //         // 정의된 게임 모드인 경우
            //         _gGameMode = mode;
            //     }else{
            //         // default 게임 모드 설정
            //         _gGameMode = GAME_MODE.NORMAR;
            //     }
            // }else {
            //     // intro scene에서 설정된 게임 모드가 없을 경우
            //     if (_gGameMode == null){
            //         // default 게임 모드 설정
            //         _gGameMode = GAME_MODE.NORMAR;
            //     }
            // }
        },
        setScore: function (score)
        {

            // if(__gameState == STATE.STATE_READY ){
            //     if(audio_ready_start.isPlaying == true){
            //         return;
            //     }
            //     audio_ready_start.play();
            //     return;
            // }

            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                var siam_score = _aSiamData.pop();
                _playgameScene.consoleLog("setScore >> score = " + score + ", siam score = " + siam_score);
                score = siam_score;
            }

            if(score < 0)score = 0;
            _SCORE = score;

            // _playgameScene.callAudio(countSound);
            // countSound.play();

            _GAME_PLAY_INFO.CURRENT_DETAIL_SCORE.push(_SCORE);
            _GAME_PLAY_INFO.TOTAL_SCORE.push(_SCORE);
            // _playgameScene.consoleLog('_GAME_PLAY_INFO.CURRENT_DETAIL_SCORE : '+ _GAME_PLAY_INFO.CURRENT_DETAIL_SCORE);
            // _playgameScene.consoleLog('_GAME_PLAY_INFO.CURRENT_DETAIL_SCORE.length'+ _GAME_PLAY_INFO.CURRENT_DETAIL_SCORE.length);
            sumScoreValue = this.sumScore(_GAME_PLAY_INFO.CURRENT_DETAIL_SCORE);

            if(sumScoreValue > 14){
                if(arrowType != 3){
                    _playgameScene.arrow00.play('arrowChange03');
                    arrowType = 3;
                }
            }else if(sumScoreValue > 10 ){
                if(arrowType != 2){
                    _playgameScene.arrow00.play('arrowChange02');
                    arrowType =2;
                }
            }else{
                if(arrowType != 1){
                    _playgameScene.arrow00.play('arrowChange01');
                    arrowType =1;
                }
            }
        },
/**            timer                             */
        updateTimer: function()
        {
            _playgameScene.checkTime -= 1; // One second
            // _playgameScene.consoleLog('남은 시간 : '+ _playgameScene.checkTime);
            if(_playgameScene.checkTime < 0 ){
                _playgameScene.alarmText.setText(0);
            }else{
                _playgameScene.alarmText.setText(_playgameScene.checkTime);
            }
            // 테스트용 자동 처리.
            if(testMode == true && _gGameMode >= 3){
                if(  _playgameScene.checkTime<180 && _playgameScene.checkTime%2==0){
                    _playgameScene.setScore(Phaser.Math.Between(8,10));
                }
            }
            if(_playgameScene.checkTime < 0){
                _playgameScene.timedEvent.remove();
                _playgameScene.updateGage();
                //_playgameScene.playingNextGame();
            }
        },
/**             func                             */
        /** 게이지 출력 함수 */
        updateGage: function()
        {

            if (__gameState != STATE.STATE_PLAY_GAME)return;
            updateCnt++;

            if(this.arrowfocus.alpha==0){
                this.arrowfocus.alpha = 1;
            }

            var len = _GAME_PLAY_INFO.CURRENT_DETAIL_SCORE.length;
            var scoreAverage = this.average(_GAME_PLAY_INFO.CURRENT_DETAIL_SCORE);
            if(len==0 && scoreAverage == 0){
                scoreAverage = _currentScore;
            }else if(len > 0 && scoreAverage<=0){
                scoreAverage = 0;
            }

            var x = _screenWidth/2 - this.mentalGage.displayWidth/2 + 55;
            var y = _height * 0.93;

            this.gageBarBG.clear();
            this.gageBarBG.fillStyle(0x212f39, 1);
            this.gageBarBG.fillRoundedRect(x, y - 12, __MAX_SCORE_WIDTH__, 20, 8);

            this.gageBar.clear();
            this.gageBar.fillStyle(0xffd400, 1);
            if ( _gageBarWidth < __INTERVAL_GAGE__ * 10 * scoreAverage){
                _gageBarWidth += __INTERVAL_GAGE__;
                if (_gageBarWidth >= __MAX_SCORE_WIDTH__){
                    _gageBarWidth = __MAX_SCORE_WIDTH__;
                    this.arrowCheck(len,scoreAverage);
                }
            }else if( _gageBarWidth >= __INTERVAL_GAGE__ * 10 * scoreAverage){

                if (_gageBarWidth <= __MIN_SCORE_WIDTH__){
                    _gageBarWidth = 0;
                    this.gageBar.fillRoundedRect(x, y - 12, _gageBarWidth, 20, 8);
                    this.arrowCheck(len,scoreAverage);


                }else{
                    _gageBarWidth -= __INTERVAL_GAGE__;

                    if(_gageBarWidth <= __INTERVAL_GAGE__ * 10 * scoreAverage){
                        _gageBarWidth = __INTERVAL_GAGE__ * 10 * scoreAverage;
                        this.arrowCheck(len,scoreAverage);

                    }
                }
            }else{
                this.arrowCheck(len,scoreAverage);
            }
            this.gageBar.fillRoundedRect(x, y - 12, _gageBarWidth, 20, 8);

            var angle;
            // 5frame 당 한번씩 조준점 떨림.
            if(updateCnt%5 == 0){
                updateCnt = 0;
                if(len == 0){
                    angle = 0;
                }else{444
                    angle = (Math.random() * 10 + 5); // 떨림 표현.
                }
                var scoreWidth = 0;
                switch(scoreAverage){
                    case 0:
                        scoreWidth = this.targetBoard.displayWidth * 0.2;
                        break
                    case 1:
                        scoreWidth = this.targetBoard.displayWidth * 0.175
                        break
                    case 2:
                        scoreWidth = this.targetBoard.displayWidth * 0.157
                        break;
                    case 3:
                        scoreWidth = this.targetBoard.displayWidth * 0.139
                        break;
                    case 4:
                        scoreWidth = this.targetBoard.displayWidth * 0.123
                        break;
                    case 5:
                        scoreWidth = this.targetBoard.displayWidth * 0.108
                        break
                    case 6:
                        scoreWidth = this.targetBoard.displayWidth * 0.091
                        break;
                    case 7:
                        scoreWidth = this.targetBoard.displayWidth * 0.074
                        break
                    case 8:
                        scoreWidth = this.targetBoard.displayWidth * 0.057
                        break;
                    case 9:
                        scoreWidth = this.targetBoard.displayWidth * 0.028
                        break;
                    case 10:
                        scoreWidth = this.targetBoard.displayWidth * 0.008
                        break;
                }
                this.arrowfocus.y = parseInt((this.targetBoard.y)-Math.sin(270)*(20 * (10-scoreAverage)-8)+angle);
                this.arrowfocus.x = parseInt((this.targetBoard.x)-Math.cos(270)*(scoreWidth)+angle);
            }
            // 집중력 값이 1번 왔을때 까지 화살이 보이지 않는 다면 화살을 노출
            if(len == 1 && this.arrow00.visible == false){
                this.arrow00.visible = true;
            }


            // 활을 쏘아도 될때 조준점의 위치를 마킹해 두고 이전에 쏜 흔적을 위치.
            if(canFire == 'fire'){
                canFire = 'not';
                this.arrowMark.x = this.arrowfocus.x;
                this.arrowMark.y = this.arrowfocus.y;
                this.sooting(scoreAverage);
            }

        },
        // 집중력 값이 3번 올라오고 현재 canFire가 not인 경우에
        arrowCheck: function(len,scoreAverage)
        {
            if(_playgameScene.checkTime < 0){
                if(len >= 3 && canFire == 'not'){
                    _playgameScene.pauseAudio(audio_cheerRing); 
                    _GAME_PLAY_INFO.GAME_SCORE.push(scoreAverage);
                    _currentScore = scoreAverage;
                    this.particles.setScale(1.2);
                    _playgameScene.arrowEmitter.setScale(1.2);
                    setTimeout(function(){canFire = 'fire';},300);
                    canFire = 'resume';

                }else{
                    _playgameScene.playingNextGame();
                    return;
                }

            }else{
                if(len >= 3 && canFire == 'not'){
                    _playgameScene.pauseAudio(audio_cheerRing); 
                    _GAME_PLAY_INFO.GAME_SCORE.push(scoreAverage);
                    _currentScore = scoreAverage;
                    this.particles.setScale(1.2);
                    _playgameScene.arrowEmitter.setScale(1.2);
                    setTimeout(function(){canFire = 'fire';},300);
                    canFire = 'resume';

                }
            }
        },
        sooting: function(scoreAverage)
        {
            _isSooting = true;
            if(scoreAverage > 6){//7,8,9
                arrowMode = 0;
            }else if(scoreAverage < 4){//0,1,2,3
                arrowMode = 2;

            }else{
                arrowMode = 1;
            }

            this.arrowEmitter.active = true;
            this.arrowEmitter.angle.propertyValue = _sootingAngle;
            this.arrowBg.visible = true;
            this.focus_line.visible = true;
            this.arrowMark.visible = false;
            this.targetBoard.visible = false;
            this.targetBoardImg.alpha = 1;
            // this.targetBoardImg.anims.playReverse('targetBoardZoomIn');
            this.targetBoardImg.play('targetBoardZoomOut');
            this.arrowfocus.x = this.targetBoardImg.x+this.targetBoardImg.displayWidth*0.025;
            this.arrowfocus.y = this.targetBoardImg.y;//-this.targetBoardImg.height*0.1;
            _playgameScene.callAudio(arrowSound2);
            if(arrowMode == 0){
                this.arrowEmitter.visible = true;
                this.particles.setPosition(this.arrow00.x, this.arrow00.y);
                this.tweens.add({
                    targets: this.particles,
                    x:this.arrowfocus.x,
                    y:this.arrowfocus.y,
                    duration: 500,
                    speed:1000,
                    ease: 'Quad.easeIn',
                    // ease: 'Back.easeIn',
                    //ease: 'Sine',
                    scale:0.1,
                    //delay:300,
                    onComplete: this.hitArrow
                });
            }else if(arrowMode == 1 || arrowMode == 2){
                this.tweens.add({
                    targets: this.arrow00,
                    x:this.arrowfocus.x,
                    y:this.arrowfocus.y,
                    duration: 500,
                    speed:2000,
                    ease: 'Quad.easeIn',
                    // ease: 'Back.easeIn',
                    //ease: 'Sine',
                    //ease: 'Bounce',
                    scale:0.02,
                    //delay:300,
                    onComplete: this.hitArrow

                });
            }


            if(arrowMode == 0){
                this.arrow00.visible = false;
            }
        },
        hitArrow: function()
        {
            //_playgameScene.consoleLog("hitArrow          arrowMode : "+ scoreAverage);
            _playgameScene.arrowEmitter.visible = false;
            // _playgameScene.input.on('pointerdown', function (pointer) {
            //     _playgameScene.consoleLog('clicked!!!!!!!!!!!!!!!!!!');

            //     _playgameScene.explosion.emitParticleAt(pointer.x, pointer.y);
            // });
            _playgameScene.callAudio(arrowSound3);
            //if(_currentScore > 4){
            //if(arrowMode != 2){
                //_playgameScene.explosion.emitParticleAt(_playgameScene.targetBoardImg.x, _playgameScene.targetBoardImg.y);
                _playgameScene.explosion.emitParticleAt(_playgameScene.arrowfocus.x, _playgameScene.arrowfocus.y);
                // _playgameScene.explosion.emitParticleAt(_playgameScene.targetBoardImg.x, _playgameScene.targetBoardImg.y-_playgameScene.targetBoardImg.displayHeight/2);
            //}
            _playgameScene.tweens.add({
                targets: _playgameScene.ic_scoreresult,
                x:_screenWidth/2,
                duration: 100,
                speed:2000,
                // ease: 'Liner',
                ease: 'Back.Out',
                //ease: 'Bounce',
                // scale:0.02,
                //delay:300,
                // onComplete: this.doNextState
                onComplete: function(){
                    _playgameScene.ic_scoreresult_text.visible = true;
                    var scoretxt = '+'+_currentScore;
                    _playgameScene.ic_scoreresult_text.setText(scoretxt);
                }
            });


            setTimeout(_playgameScene.playingNextGame,400);
        },
        playingNextGame: function()
        {
            if(__gameState == STATE.STATE_DISPLAY_RESULT) {
                return;
            }
            _isSooting = false;
            _playgameScene.arrowEmitter.visible = false;
            _playgameScene.arrowBg.visible = false;
            _playgameScene.focus_line.visible = false;

            _playgameScene.ic_scoreresult.x = _screenWidth*2;
            _playgameScene.ic_scoreresult_text.visible = false;
            _playgameScene.targetBoardImg.play('targetBoardZoomIn');
            if(arrowMode == 1 || arrowMode == 2){
                _playgameScene.arrow00.x = _playgameScene.bow.x - _playgameScene.bow.displayWidth/6;
                _playgameScene.arrow00.y = _playgameScene.bow.y - _playgameScene.bow.displayHeight * 0.37;
                _playgameScene.arrow00.setScale(0.5);
            }
            if(_playgameScene.checkTime < 0){

            }else{

                _GAME_PLAY_INFO.SHOOTING_CNT += 1;
                _GAME_PLAY_INFO.CURRENT_DETAIL_SCORE = [];

                _prevprevScore = _prevScore;
                // 이전전 점수
                _playgameScene.prevprevScore .setText(_prevprevScore);
                // 현재 점수.
                _currentScore = _GAME_PLAY_INFO.GAME_SCORE[_GAME_PLAY_INFO.SHOOTING_CNT-1];
                if(_GAME_PLAY_INFO.SHOOTING_CNT>1){
                    _prevScore = _GAME_PLAY_INFO.GAME_SCORE[_GAME_PLAY_INFO.SHOOTING_CNT-2];
                    // 이전 점수
                    _playgameScene.prevScore.setText(_prevScore);
                    if(_prevScore >= 8 && _currentScore >= 8 ){
                        _GAME_PLAY_INFO.BONUS_SCORE.push(5);
                        _playgameScene.callAudio(audio_cheerRing);
                        _playgameScene.bonusScoreText.visible = true;
                        _playgameScene.tweenBonusScore.restart();
                    }
                }
                _playgameScene.currentScore.setText(_currentScore);
            }

            _TOTAL_SCORE = _playgameScene.sumScore(_GAME_PLAY_INFO.GAME_SCORE)+  _playgameScene.sumScore(_GAME_PLAY_INFO.BONUS_SCORE);
            _playgameScene.scoreText.setText(_TOTAL_SCORE);

            // _playgameScene.consoleLog("현재 평균 점수 총합 : " + _playgameScene.sumScore(_GAME_PLAY_INFO.GAME_SCORE));
            // _playgameScene.consoleLog("현재 보너스 점수 총합 : " + _playgameScene.sumScore(_GAME_PLAY_INFO.BONUS_SCORE));
            // _playgameScene.consoleLog("_GAME_PLAY_INFO.GAME_SCORE.length : " + _GAME_PLAY_INFO.GAME_SCORE.length+"   ,_GAME_PLAY_INFO.SHOOTING_CNT :  "+ _GAME_PLAY_INFO.SHOOTING_CNT);
            if(_playgameScene.checkTime <= 0 || _GAME_PLAY_INFO.GAME_SCORE.length >= __MAX_SHOOTING_CNT__){
                __gameState = STATE.STATE_DISPLAY_RESULT;
                _playgameScene.gameGroup.toggleVisible();
                _playgameScene.arrow00.visible = false;
                _playgameScene.ic_scoreresult_text.visible = false;
                _playgameScene.focus_line.visible = false;
                _playgameScene.resultGroup.toggleVisible();

                setTimeout(function(){
                    _playgameScene.onResultState();
                }, 500);
            }else{
                _playgameScene.arrow00.play('arrowChange01');
                arrowType =1;
            }

        },
        onResultState: function()
        {
            //var totalScore = _playgameScene.sumScore(_GAME_PLAY_INFO.GAME_SCORE) + _playgameScene.sumScore(_GAME_PLAY_INFO.BONUS_SCORE);
            // _playgameScene.consoleLog('  doNextState   totalScore  : '+totalScore);
            // 0~70
            // 71~140
            // 141~210
            // 211~280
            // 281~445
            // result_bad      : 228
            // result_notgood : 205
            // result_good  : 182
            // result_verygood : 160
            // result_excellent : 138

            // 화살표 y 좌표 값
            // _height/2 + _width * 0.465
            // _height/2 + _width * 0.41
            // _height/2 + _width * 0.36
            // _height/2 + _width * 0.305
            // _height/2 + _width * 0.25

            // 게임 결과 (_gameResult)
            // 1: bad 2 : not good 3 : good 4 : very good 5 : excellent

            // 게임 시간에 따른 스코어 설정 및 스코어 표시
            var badScroe = 70;
            var notgoodScore = 140;
            var goodScore = 210;
            var verygoodScore = 280;
            var MAX_PLAY_TIME = 180;
            var timeRate = parseInt(MAX_PLAY_TIME/__CHECK_TIME__);
            // var gameFullCount = parseInt(__CHECK_TIME__/6);// 6초마다 1회 게임.
            var MAX_POINT = parseInt(350 / timeRate);
            // var MAX_POINT = (gameFullCount * 10)+((gameFullCount-1) * 5);


            // var scoreRate = MAX_PLAY_TIME/__CHECK_TIME__;

            badScroe        = 20;//parseInt( badScroe / scoreRate );
            notgoodScore    = 40;//parseInt( notgoodScore / scoreRate );
            goodScore       = 60;//parseInt( goodScore / scoreRate );
            verygoodScore   = 80;//parseInt( verygoodScore / scoreRate );

            var totalValue  = parseInt((_TOTAL_SCORE / ( MAX_POINT  )) * 100); // 배점 계산 필요

            if (totalValue > 100){
                totalValue = 100;
            }

            // native에 게임이 끝났음을 알림. 게임이 끝나면 callJS로 데이터를 올려주지 않음.
            /****************** 반드시 호출 필요 ******************/
            nativeInterface.onGameEnd( _TOTAL_SCORE );
            /****************** 반드시 호출 필요 ******************/

            _playgameScene.scoreTextOfResult1.setY(_height/2 + _width * 0.39);
            _playgameScene.scoreTextOfResult2.setY(_height/2 + _width * 0.34);
            _playgameScene.scoreTextOfResult3.setY(_height/2 + _width * 0.285);
            _playgameScene.scoreTextOfResult4.setY(_height/2 + _width * 0.23);
            _playgameScene.scoreTextOfResult5.setY(_height/2 + _width * 0.175);



            if(totalValue<=badScroe){
                _playgameScene.result_score.play('result_bad');
                _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.39);
                _gameResult = 1;
            }else if(totalValue > badScroe && totalValue <= notgoodScore){
                _playgameScene.result_score.play('result_notgood');
                _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.34);
                _gameResult = 2
            }else if(totalValue > notgoodScore && totalValue <= goodScore){
                _playgameScene.result_score.play('result_good');
                _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.29);
                _gameResult = 3;
            }else if(totalValue > goodScore && totalValue <= verygoodScore){
                _playgameScene.result_score.play('result_verygood');
                _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.23);
                _gameResult = 4;
            }else{
                _playgameScene.result_score.play('result_excellent');
                _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.18);
                _gameResult = 5;
            }

             /** onStart 이벤트를 받아 _result_skip 처리 한다 **/
            if(_result_skip == 'Y'){
                __gameState == STATE.STATE_DISPLAY_RESULT;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
                return;
            }


            _totalValue = totalValue;
            _playgameScene.scoreTextOfResult.setText(_TOTAL_SCORE +' ('+ totalValue +'%)');

            // 게임 모드에 따라서 버튼을 보여주도록 처리
            if (_gGameMode == GAME_MODE.NORMAR || _gGameMode == GAME_MODE.SIAM){
                // 다음 게임
                    _playgameScene.result_popup_next_game_nor.visible = true;
                    _playgameScene.result_popup_view_result_nor.visible = false;
                    _playgameScene.result_popup_view_result_sel.visible = false;
                    _playgameScene.result_popup_exit_nor.visible = false;
            }else{
                if(_result_button_code == 'BTN001'){
                    _playgameScene.result_popup_next_game_nor.visible = false;
                    _playgameScene.result_popup_view_result_nor.visible = false;
                    _playgameScene.result_popup_view_result_sel.visible = false;
                    _playgameScene.result_popup_exit_nor.visible = true;
                } else {
                // if(_result_button_code == 'BTN003'){
                    // 결과 보기  NORMAR_END , SIAM_END : 마지막 게임
                    _playgameScene.result_popup_next_game_nor.visible = false;
                    _playgameScene.result_popup_view_result_sel.visible = false;
                    _playgameScene.result_popup_view_result_nor.visible = true;
                    _playgameScene.result_popup_exit_nor.visible = false;
                }
            }

            _playgameScene.tweens.add({
                targets: _playgameScene.resultGroup.getChildren(),
                x:'-='+_screenWidth,
                duration: 1000,
                speed:1000,
                // ease: 'Bounce'
                ease: 'Liner'
            });
            __gameState = STATE.STATE_DISPLAY_RESULT;

             // 결과 화면 설정
            //audio_game_win.play();  // audio play
            _playgameScene.callAudio(audio_game_win);
            //nativeInterface.onGameEnd( _TOTAL_SCORE );
             /**
             * onStart 이벤트를 받는다
             * '_result_auto_skip' 이 'Y'인 경우
             * '_result_skip_wait_time' 후에 강제 종료 타이머 처리
             */
            if(_result_auto_skip == 'Y'){
                _playgameScene.time.addEvent({
                    delay: _result_skip_wait_time,                // ms
                    callback: _playgameScene.nextGameTime,   // 측정 타이머 함수
                    callbackScope: _playgameScene,
                    repeat: 0
                });
            }
        },
        nextGameTime: function()
        {
            _isGameAgain = GAME_BTN_STATE.EXIT;
            _playgameScene.doNextState();
        },
        /** 게임 상태에 따른 이벤트 처리 */
        doNextState: function ()
        {
            _playgameScene.consoleLog("__gameState = " + __gameState);
            if (__gameState == STATE.STATE_DISPLAY_RESULT){
                _playgameScene.pauseAudio(audio_game_win);
                _playgameScene.pauseAudio(audio_cheerRing);
                _playgameScene.pauseAudio(countSound);
                _playgameScene.pauseAudio(arrowSound);
                if (_isGameAgain == GAME_BTN_STATE.RESTART){
                    // __gameState = STATE.STATE_READY; // 게임 State 변경
                    // _playgameScene.resultGroup.toggleVisible();
                    // _playgameScene.playGroup.toggleVisible();
                    // _playgameScene.resetData(); // 데이터 초기화
                    // _playgameScene.scene.start('gameplayscene');
                }else if (_isGameAgain == GAME_BTN_STATE.EXIT){
                    // 게임 종료
                    /****************** 반드시 호출 필요 ******************/
                    __gameState = STATE.STATE_END_GAME;
                    // 모든 사운드 종료
                    // audio_play_bgm.pause();
                    // audio_game_win.pause();
                    // audio_cheerRing.pause();
                    // countSound.pause();
                    // arrowSound.pause();

                    audio_play_bgm      = null;
                    audio_ready_start   = null;
                    audio_game_win      = null;
                    // audio_arm_wrestling = null;
                    audio_cheerRing     = null;
                    countSound          = null;
                    arrowSound          = null;

                    var params= '{"result":'+_gameResult+',"percent":'+_totalValue+'}';
                    nativeInterface.onNextGame(params);
                    /****************** 반드시 호출 필요 ******************/
                }
            }
        },
        resetData: function()
        {

        },
/**             effect                          */
        createAnimation : function(scope)
        {
            scope.arroAni = scope.anims.create({
                key: 'arrowChange01',
                frames: [
                    { key: 'arrow01' }
                ],
                frameRate: 6,
                delay: 0,
                repeat: 0
            });
            scope.arroAni = scope.anims.create({
                key: 'arrowChange02',
                frames: [
                    // { key: 'arrow02' },
                    { key: 'arrow02', duration: 500 }
                ],
                frameRate: 6,
                delay: 0,
                repeat: 0
            });
            scope.arroAni = scope.anims.create({
                key: 'arrowChange03',
                frames: [
                    { key: 'arrow02' },
                    { key: 'arrow03', duration: 500 }
                ],
                frameRate: 6,
                delay: 0,
                repeat: 0
            });
            // result animation
            scope.resultAni = scope.anims.create({
                key: 'result_bad',
                frames: [
                    { key: 'result_ic_scoretext_bad' }
                ],
                frameRate: 1,
                delay: 0,
                repeat: 0
            });
            scope.resultAni = scope.anims.create({
                key: 'result_notgood',
                frames: [
                    { key: 'result_ic_scoretext_notgood'}
                ],
                frameRate: 1,
                delay: 0,
                repeat: 0
            });
            scope.resultAni = scope.anims.create({
                key: 'result_good',
                frames: [
                    { key: 'result_ic_scoretext_good'}
                ],
                frameRate: 1,
                delay: 0,
                repeat: 0
            });
            scope.resultAni = scope.anims.create({
                key: 'result_verygood',
                frames: [
                    { key: 'result_ic_scoretext_verygood'}
                ],
                frameRate: 1,
                delay: 0,
                repeat: 0
            });
            scope.resultAni = scope.anims.create({
                key: 'result_excellent',
                frames: [
                    { key: 'result_ic_scoretext_excellent'}
                ],
                frameRate: 1,
                delay: 0,
                repeat: 0
            });
        },
        targetZoom: function()
        {
            if(!_isSooting)
            {
                 this.targetBoard.visible = true;
                 this.targetBoardImg.alpha = 0;

                 if(_currentScore != null) this.arrowMark.visible = true;
            } else {
                this.targetBoard.visible = false;

            }
        },
/**             sham                            */
        setSham: function()
        {
            var CON_ARR = [];
            let val = 0;
            let successCnt = 0;
            let successMax = 10;
            let successMin = 5;
            let failMax = 4;
            let failMin = 0;
            // let ratio_arr = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8];
            let ratio_arr = [0.2, 0.4, 0.6, 0.8, 0.9];
            var ratio = ratio_arr.sort(function () {return 0.5 - Math.random()}).pop();
            // _playgameScene.consoleLog(`ratio = > ${ratio * 100}% 확율의 성공`);
            for (let i = 0; i < 120; i++)
            {
                if (Math.random() < ratio)
                {
                    val = Math.floor(Math.random() * (successMax - successMin + 1)) + successMin;
                } else {
                    val = Math.floor(Math.random() * (failMax - failMin + 1)) + failMin;
                }
                CON_ARR.push(val);
                if (val > 4)
                {
                    successCnt += 1;
                }
            }
            if (isDisplaySiamRatio)
            {   // 샴 통계용
                var average = 0, total = 0;
                for (var j = 0; j < CON_ARR.length; j++){
                    total += CON_ARR[j];
                }
                average = total / CON_ARR.length;
                _strSiamRatio = "ratio : " + (ratio * 100) + " (" + average + ")%";
                console.log(_strSiamRatio);
            }
            // _playgameScene.consoleLog(`shamSuccessCnt => ${successCnt} six distributor => ${Math.floor(successCnt / 6)}`);
            // _playgameScene.consoleLog(`CON_ARR => ${CON_ARR}`);
            return CON_ARR;
        },
/**             util                            */
        // 점수 총합
        sumScore: function(array)
        {
            var sum = 0.0;
            if(array.length == 0) return 0;
            for (var i = 0; i < array.length; i++)
            {
              sum += array[i];
            }
            return sum;
        },
        // 최대 3개 값 평균.
        average: function(array)
        {
            var sum = 0.0;
            var len = array.length;
            if(len == 0) return 0;
            else if(len > 3) len = 3;
            for (var i = 0; i < len; i++)
            {
              sum += array[i];
            }
            return Math.round(sum / len);
        },
        callAudio: function (calledAuiod)
        {
            //_playgameScene.consoleLog('callAudio : '+ calledAuiod);
            var audio = calledAuiod;
           
            // if (audio.isPlaying)
            // {
            //     audio.pause();
            //     audio.currentTime = 0;
            // }
            if (_gIsPlayingAudioByPhaser){
                if (audio.isPlaying == true){  // Phaser audio
                    audio.pause();
                    audio.currentTime = 0;
                }
            }else {
                if (audio.paused == false){ // JS audio
                    audio.pause();
                    audio.currentTime = 0;
                }
            }
            audio.play();
            
        },
        pauseAudio:function (calledAuiod){
            if (_gIsPlayingAudioByPhaser){
                if (calledAuiod.isPlaying == true){  // Phaser audio
                    calledAuiod.pause();
                    calledAuiod.currentTime = 0;
                }
            }else {
                if (calledAuiod.paused == false){ // JS audio
                    calledAuiod.pause();
                    calledAuiod.currentTime = 0;
                }
            }
            return true;
        },
        consoleLog(str)
        {
            if(testMode == true)
            {
                console.log(str);
            }
        },
        showToast: function() {

            var toast = document.getElementById("toastMessageBar");

            if (_gLang == "ko"){
                toast.innerHTML = _messageKr;
            }else {
                toast.innerHTML = _messageEn;
            }

            $("#toastMessageBar").show(500);
            setTimeout(function(){
                $("#toastMessageBar").hide(500);
            }, 5000);
        }
    };


}());
