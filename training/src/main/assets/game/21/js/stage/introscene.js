﻿// Phaser3 example game
// tutorial scene

var IntroScene = new Phaser.Class(function(){

    var STATE = {
        INIT_STATE : 0,
        READY_STATE : 1
    };
    var gameState = 0;
    var introMusic = null;// intro 사운드
    var arrowEffectSound = null;
    var arrowEffectSound2 = null;
    var iconX;
    var dx = 0;
    var tweenPlay = false;
    // Scene 전역 스코프 변수
    var _playgameScene       = null;
    var charcterTargetX = null;
    var charaterMove = false;
    var _timer     = null; //5초후 시작
    var _width;
    var bullets = null;
    // var _shotCount = 3;

    return {
        Extends: Phaser.Scene,

        initialize:

        function IntroScene ()
        {
            Phaser.Scene.call(this, { key: 'intro_scene' });
            // console.log("Construct of IntroScene");
        },
        preload: function ()
        {
            currentScene = GAME_SCENE.INTRO_SCENE;
            gameState = STATE.INIT_STATE;
            /****** preloading ******/
            var progressBar = this.add.graphics();
            var progressBox = this.add.graphics();
            var percentText = this.make.text({
                x: window.innerWidth/2,
                y:window.innerHeight/2+25,
                style: {
                    font : '18px monospace',
                    fill : '#ffffff'
                }
            });
            percentText.setOrigin(0.5);
            progressBox.fillStyle('0x222222', 0.8);
            progressBox.fillRoundedRect(window.innerWidth/2 - 150, window.innerHeight/2, 300, 50, 5);
            /****** 인트로 화면 ******/
            // image

            this.load.image('game_bg', 'assets/images/game/archery_game_bg.png');

            this.load.image('intro_text_02', 'assets/images/intro/intro_text_02.png');
            if (_gLang == "ko"){
                this.load.image('intro_icon', 'assets/images/intro/arrow_intro_icon_kr.png');
            }else{
                this.load.image('intro_icon', 'assets/images/intro/arrow_intro_icon.png');
            }
            // 발사체


            // 과녁

            this.load.image('target', 'assets/images/game/ic_target_01.png');
            this.load.atlas('flares', 'assets/particles/flares.png', 'assets/particles/flares.json');
            this.load.atlas('explosion', 'assets/particles/explosion.png', 'assets/particles/explosion.json');

            /****** 게임 중인 화면 *****/
            // 배경 이미지

            /* 캐릭터 */
            /* 활 */
            /* 화살 */
            this.load.image('intro_bg_zoom', 'assets/images/game/archeryGameBg_blur.png');
            this.load.image('ic_bow', 'assets/images/game/ic_bow1.png'); // 활
            this.load.image('ic_arm', 'assets/images/game/ic_arm.png'); // 팔

            this.load.image('arrow01', 'assets/images/game/ic_arrow_01.png');
            this.load.image('arrow02', 'assets/images/game/ic_arrow_03.png');
            this.load.image('arrow03', 'assets/images/game/ic_arrow_04.png');
            this.load.image('arrowMark', 'assets/images/game/ic_target_mark.png');
            this.load.image('ic_smallarrow_01', 'assets/images/game/ic_smallarrow_01.png');

            // 과녁
            this.load.image('targetBoard01', 'assets/images/game/ic_target_01.png');
            this.load.image('targetBoard02', 'assets/images/game/ic_target_02.png');
            this.load.image('targetBoard03', 'assets/images/game/ic_target_03.png');
            this.load.image('targetBoard04', 'assets/images/game/ic_target_04.png');
            this.load.image('targetBoard', 'assets/images/game/ic_target_04.png');

            // 조준점
            this.load.image('arrowfocus', 'assets/images/game/ic_targetpoint.png');

            // 게임 중인 화면 - 화살 날아가는 모션, 이후 화살 박히는 모션
            this.load.atlas('flares', 'assets/images/arrowGame/flares.png', 'assets/images/arrowGame/flares.json');

            // 게임 요소
            this.load.image('ground_play_alarm', 'assets/images/game/clock_empty.png');
            this.load.image('ground_gage_empty', 'assets/images/game/gage_empty.png');
            this.load.image('ground_score_board', 'assets/images/game/electric_sign.png');
            this.load.image('ic_scoreboard_01', 'assets/images/game/ic_scoreboard_01.png');
            this.load.image('ic_scoreboard_02', 'assets/images/game/ic_scoreboard_02.png');
            this.load.image('ic_scoreboard_03', 'assets/images/game/ic_scoreboard_03.png');
            this.load.image('ic_scoreresult', 'assets/images/game/ic_scoreresult.png');
            this.load.image('focus_line', 'assets/images/game/blur_bg_radialline.png');


            // 게임 결과 화면
            // this.load.image('result_ic_result_popup_bg', 'assets/images/game_popup_bg2.jpg');
            this.load.image('result_ic_result_popup_bg', 'assets/images/game_popup_bg.png');
            this.load.image('result_ic_scoretext_bad', 'assets/images/popup_result_text_01.png');
            this.load.image('result_ic_scoretext_excellent', 'assets/images/popup_result_text_05.png');
            this.load.image('result_ic_scoretext_good', 'assets/images/popup_result_text_03.png');
            this.load.image('result_ic_scoretext_notgood', 'assets/images/popup_result_text_02.png');
            this.load.image('result_ic_scoretext_verygood', 'assets/images/popup_result_text_04.png');
            // this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor2.png');
            if (_gLang == "ko"){
                this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor_kr.png');
                this.load.image('result_btn_resultview_sel', 'assets/images/btn_result_01_sel_kr.png');
                this.load.image('result_btn_nextgame_nor'  , 'assets/images/btn_result_popup_next_nor_kr.png');
                this.load.image('result_btn_exit_nor'      , 'assets/images/btn_result_popup_exit_nor_kr.png');
            }else{
                this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor.png');
                this.load.image('result_btn_resultview_sel', 'assets/images/btn_result_01_sel.png');
                this.load.image('result_btn_nextgame_nor'  , 'assets/images/btn_result_popup_next_nor.png');
                this.load.image('result_btn_exit_nor'      , 'assets/images/btn_result_popup_exit_nor.png');
            }
            this.load.image('result_popup_arrow', 'assets/images/popup_arrow.png');

            // audio
            this.load.audio('audio_ready_start', ['assets/audio/ready_start.wav']);
            this.load.audio('audio_intro_bgm', ['assets/audio/Game_Intro_BGM.mp3']);
            this.load.audio('audio_play_bgm', ['assets/audio/Game_play_BGM.mp3']);
            this.load.audio('audio_game_win', ['assets/audio/game_clear_good(Result).wav']);
            this.load.audio('audio_cheerRing', ['assets/audio/cheerRing.mp3']);

            this.load.audio('arrowBgm','assets/audio/arrowSound01.mp3');
            this.load.audio('arrowSound','assets/audio/arrowSound02.mp3');
            this.load.audio('arrowSoundShot','assets/audio/arrowSound03.mp3');
            this.load.audio('countBgm', ['assets/audio/countSound.wav']);
            this.load.on('progress', function(value){
                progressBar.clear();
                progressBar.fillStyle(0xffffff,1);
                progressBar.fillRoundedRect(window.innerWidth/2+10-150, window.innerHeight/2+10, 280*value, 30, 5);
                percentText.setText("Loading "+Math.round(value*100) + "%");
            });

            this.load.on('complete', function(){
                // progressBox.destory();
                // progressBar.destory();
            });
        },

        create: function ()
        {

            _playgameScene = this;
            var width = _width = this.game.config.width;
            var height = _height = this.game.config.height;
            iconX = width/2;

            /** onStart 이벤트를 받는다 **/
            // logoSkip 이 Y 인경우 logoSkip 함수를 실행하고
            // 그 결과가 true(skip 실행)이면 이하의 코드를 실행하지 않는다
            var logoSkip = _playgameScene.logoSkip('intro');
            if(logoSkip == true){
                return;
            }

            this.introGroup = this.add.group();
            // set background image
            this.introBg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'game_bg');
            this.introBg.displayWidth = width; //scale.setTo(0.5, 0.5);
            this.introBg.displayHeight = this.game.config.height;

            if (this.game.config.width > this.game.config.height){
                this.introBg.displayWidth = width; //scale.setTo(0.5, 0.5);
                this.introBg.displayHeight = width * 1.78;//this.game.config.height;
            }else{
                this.introBg.displayWidth = _width; //scale.setTo(0.5, 0.5);
                this.introBg.displayHeight = this.game.config.height;
            }



            this.target = this.add.image(this.game.config.width/2,  height*0.5, 'target').setOrigin(0.5,0.3);
            this.target.displayWidth = width*0.8;
            this.target.displayHeight = this.target.height * (this.target.displayWidth/this.target.width);
            // this.target.play('targetBoard1');


            this.introIcon = this.physics.add.image(-this.game.config.width, (height/2), 'intro_icon').setOrigin(0.5);

            if (this.game.config.width > this.game.config.height){
                this.introIcon.displayWidth = this.introIcon.width * (this.introBg.displayWidth/this.introBg.width);//width; //scale.setTo(0.5, 0.5);
                this.introIcon.displayHeight = this.introIcon.height * (this.introIcon.displayWidth/this.introIcon.width);
            }else{
                this.introIcon.displayWidth = _width; //scale.setTo(0.5, 0.5);
                this.introIcon.displayHeight = this.introIcon.height * (this.introIcon.displayWidth/this.introIcon.width);
            }

            // text
            this.pressTextImg = this.add.image(this.game.config.width/2, height*0.9, 'intro_text_02');//.setDisplaySize(width/2, width/2 * 0.11);
            this.pressTextImg.displayWidth = width/2;
            this.pressTextImg.displayHeight = this.pressTextImg.height * (this.pressTextImg.displayWidth/this.pressTextImg.width);

            this.physics.add.collider(this.introIcon, this.target);
            arrowEffectSound = this.sound.add('arrowSoundShot');
            arrowEffectSound2 = this.sound.add('arrowSound');

            this.explosion = this.add.particles('explosion');
            this.explosionEmitter = this.explosion.createEmitter({
                frame: 'muzzleflash2',
                lifespan: 200,
                scale: { start: 1, end: 0 },
                rotate: { start: 0, end: 180 },
                on: false
                // frame: 'red',
                // angle: { min: 160, max: 300, steps: 3 },
                // lifespan: 100,
                // speed: 300,
                // quantity: 3,
                // scale: { start: 0.2, end: 0 },
                // on: false

            });
            // this.explosion.createEmitter({
            //     frame: 'stone',
            //     angle: { min: 240, max: 300 },
            //     speed: { min: 400, max: 600 },
            //     quantity: { min: 2, max: 10 },
            //     lifespan: 1000,
            //     alpha: { start: 1, end: 0 },
            //     scale: { min: 0.00001, max: 0.001 },
            //     rotate: { start: 0, end: 360, ease: 'Back.easeOut' },
            //     gravityY: 800,
            //     on: false
            // });
            var Bullet = new Phaser.Class({

                Extends: Phaser.GameObjects.Image,
                initialize:

                function Bullet (scene)
                {

                    this.gameObj = scene;
                    var ranVal = Phaser.Math.Between(0,5);
                    console.log('ranVal  :   '+ ranVal);

                    Phaser.GameObjects.Image.call(this, scene, 0, 0, 'ic_smallarrow_01');
                    //Phaser.GameObjects.Image.call(this, scene, 0, 0, 'arrow01');

                    this.speed = Phaser.Math.GetSpeed(1000, 1);

                },
                fire: function (x, y)
                {
                    console.log('Bullet  fire');
                    //var randomVal = Phaser.Math.Between(3,6);
                    this.setOrigin(0.5,0);
                    this.setPosition(x, height);
                    //this.setScale(randomVal/10);
                    this.setScale(1);
                    this.setActive(true);
                    this.setVisible(true);
                },

                update: function (time, delta)
                {
                    // console.log('delta : '+delta);
                    this.y -= this.speed * delta;
                    if(this.scaleY > 0.04){
                         this.scaleY -= 0.03;
                    }
                    if(this.scaleX > 0.04){
                         this.scaleX -= 0.03;
                    }

                    if(this.x > this.gameObj.target.x-_width/4 && this.x < this.gameObj.target.x+_width/4)
                    {

                        if (this.y < _playgameScene.target.y)
                        {
                            this.setActive(false);
                            tweenPlay = true;
                            this.gameObj.explosion.emitParticleAt(this.x, this.y);
                            arrowEffectSound.play();
                            // _shotCount++;
                            // switch(_shotCount){
                            //     case 1 :
                            //         this.gameObj.target.play('targetBoard3');
                            //     break;
                            //     case 2:
                            //         this.gameObj.target.play('targetBoard2');
                            //     break;
                            //     case 3 :
                            //         this.gameObj.target.play('targetBoard1');
                            //     break;
                            //     case 4 :
                            //             tweenPlay = true;
                            //     break;
                            // }
                            this.setVisible(false);
                        }
                    } else{

                        if(this.y < this.gameObj.target.y){
                            this.gameObj.explosion.emitParticleAt(this.x, this.y);
                            arrowEffectSound.play();
                            //this.gameObj.setParticle(this.x,this.y);
                            this.setActive(false);
                            this.setVisible(false);
                        }
                    }
                // }
                    if(this.x > this.gameObj.introIcon.x-this.gameObj.introIcon.width/2 && this.x < this.gameObj.introIcon.x+this.gameObj.introIcon.width/2)
                    {
                        if (this.y < (this.gameObj.introIcon.y))
                        {
                            this.setActive(false);
                            //this.gameObj.introIconTween.restart();
                            //this.setVisible(false);
                            //this.gameObj.tweenPlay = true;

                            //tweenPlay = true;
                            this.gameObj.setParticle(this.x,this.y);
                            // this.gameObj.introIcon.y -= Phaser.Math.Between(5, 60);
                            this.setVisible(false);
                        } else if(this.y < -(this.gameObj.introIcon.height/2)){
                            this.setActive(false);
                            this.setVisible(false);
                        }
                    }else{
                        // if(this.gameObj.targetBoardImg.visible == false){
                            // if(this.y < this.gameObj.targetBoardImg.y){
                            //     this.gameObj.explosion.emitParticleAt(this.x, this.y);
                            //     this.setActive(false);
                            //     this.setVisible(false);
                            // }
                        // }
                    }
                }
            });
            bullets = this.add.group({
                classType: Bullet,
                maxSize: 100,
                runChildUpdate: true
            });

            this.scene.scene.input.on('pointerdown', function(pointer){
                var touchX = pointer.x;
                var touchY = pointer.y;
                var bullet = bullets.get();
                if(_timer != null){
                    _timer = null;
                }
                if (bullet)
                {
                   bullet.fire(touchX, touchY);
                    //lastFired = time + 50;
                }
                arrowEffectSound2.play();
            })

            // this.introGroup.add(this.introBg);
            this.introGroup.add(this.target);
            this.introGroup.add(this.introIcon);
            //this.introGroup.add(this.targetBoardImg);
            this.introGroup.add(this.pressTextImg);

            //this.makeNewTargetX();
            this.particles = this.add.particles('flares');
            // 키보드 입력 인식
            // this.cursors = this.input.keyboard.createCursorKeys();
            if (isIos != true && isAndroid != true){
                // testMode
                nativeInterface.onStart(3,120);
                // nativeInterface.onStart(1);

            }
            if (_gIsPlayingAudioByPhaser){
                introMusic = this.sound.add('audio_intro_bgm');
                introMusic.play({loop:true});
            }
            /**
             * onStart 이벤트를 받는다
             * '_logo_auto_skip' 이 'Y'인 경우
             * '_logo_skip_wait_time' 후에 logo 를 skip한다.
             */
            if(_logo_auto_skip == 'Y'){
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }
                _timer = _playgameScene.time.delayedCall(_logo_skip_wait_time, function(){
                    var touchX = this.game.config.width/2;
                    var touchY = this.game.config.height/2;
                    var bullet = bullets.get();

                    if (bullet)
                    {
                        bullet.fire(touchX, touchY);
                        //lastFired = time + 50;
                    }
                    arrowEffectSound2.play();
                    _timer = null;
                }, null, _playgameScene);  // delay in ms
            }

        },
        setParticle(x,y,status)
        {

            var particle = this.particles.createEmitter({
                // frame: 'white',
                // x: x,
                // y: y,
                // speed: 100,
                // tint: [ 0xffff00, 0xff0000, 0x00ff00, 0x0000ff ],
                // lifespan: 5,
                // frequency: 5,
                // maxParticles: 10,
                // stepRate: 0,

                // blendMode: 'NORMAL'
                frame: 'white',
                // alpha: { start: 1, end: 0 },
                scale: { start: 0.5, end: 2.5 },
                tint: [ 0xffff00, 0xff0000, 0x00ff00, 0x0000ff ],
                speed: 50,
                accelerationY: -200,
                lifespan: { min: 400, max: 1100 },
                blendMode: 'ADD',
                frequency: 10,
                maxParticles: 10,
                x: x,
                y: y

            });
            // _playgameScene.explosion.emitParticleAt(x, y);
            arrowEffectSound.play();
        },
        makeNewTargetX: function(){
            // console.log("this.game.config.width/2-_width/2  = "+ (this.game.config.width/2-_width/2));
            // console.log("this.game.config.width/2+_width/2  = "+ (this.game.config.width/2+_width/2));
            charcterTargetX = Phaser.Math.Between((this.game.config.width/2-_width/4),(this.game.config.width/2+_width/4));
            var time = Phaser.Math.Between(500,1000);

            this.tweenChacterX = this.tweens.add({
                targets : this.targetBoardImg,
                x : charcterTargetX,
                duration : time,
                delay : 0,

                ease : 'Back.Out',
                repeat : 0,
                onComplete: function () {
                    charcterTargetX = null;
                }
            });

        },
        update: function()
        {
            // console.log("called update : time = " + ((this.time.now / 1000) % 2));
            if (((this.time.now / 1000) % 1) >= 0.5){
                this.pressTextImg.setAlpha(0);
            }else{
                this.pressTextImg.setAlpha(1);
            }
            if(charcterTargetX == null) {

                this.makeNewTargetX();
                //this.targetBoardImg.x = charcterTargetX = this.game.config.width/2;
            }
            // if(charaterMove == true){
            //     this.tweenChacterX.stop();
            //     if((this.targetBoardImg.x+150) > (this.game.config.width/2+_width/2)-100){
            //         this.targetBoardImg.x -= 150;

            //     }else if((this.targetBoardImg.x-150) < (this.game.config.width/2-_width/2)+100){
            //         this.targetBoardImg.x += 150;
            //     }else{
            //         this.targetBoardImg.x -= 150;
            //     }
            //     charaterMove = false;
            //     this.makeNewTargetX();
            // }
            if(tweenPlay == true){
                this.tweenChacterX.stop();
                // this.targetBoardImg.visible = false;
                dx =  parseInt((iconX-this.introIcon.x)*1.2) + dx * 0.8;

                this.introIcon.setVelocityX(dx);
                if(dx < 0.05 && dx > -0.05){
                    this.introIcon.setVelocityX(0);
                    tweenPlay = false;

                    if(_gGameMode != null){
                        gameState = STATE.READY_STATE;
                    }
                    this.doNextState();
                }
            }

        },
        // onStartAudio: function(mode, playTime)
        // {
        //     // Scene 진입시에 play할 audio 재생
        //     console.log("Intro >> onStart :: mode = "  + mode);

        //     // 게임 모드 설정
        //     if (mode !== undefined){
        //         if (mode >= GAME_MODE.NORMAR && mode <= GAME_MODE.SIAM_END){
        //             // 정의된 게임 모드인 경우
        //             _gGameMode = mode;
        //         }else{
        //             // default 게임 모드 설정
        //             _gGameMode = GAME_MODE.NORMAR;
        //         }
        //     }else {
        //         // 게임 모드 설정 전인 경우
        //         if (_gGameMode == null){
        //             // default 게임 모드 설정
        //             _gGameMode = GAME_MODE.NORMAR;
        //         }
        //     }
        //     _gGamePlayTime = playTime;

        //     // 시작 음악 재생
        //     if (isAndroid){
        //         introMusic = new Audio('assets/audio/Game_Intro_BGM.mp3');
        //         introMusic.autoplay = true;
        //         introMusic.loop = true;
        //     }
        //     this.doNextState();
        // },
        /** onStart 이벤트를 받아 설정하는 부분 제거한다 **/
        onStartAudio: function()
        {
            // Scene 진입시에 play할 audio 재생
            console.log("Intro >> onStartAudio ");
            // 시작 음악 재생
            // if (_gIsPlayingAudioByPhaser == false && isIos == false && _logo_skip == 'N'){
            if (_gIsPlayingAudioByPhaser == false && _logo_skip == 'N'){
                introMusic = new Audio('assets/audio/Game_Intro_BGM.mp3');
                introMusic.autoplay = true;
                introMusic.loop = true;
            }
            this.doNextState();
        },
        pauseAudio:function (calledAuiod){
            if (_gIsPlayingAudioByPhaser){
                if (calledAuiod.isPlaying == true){  // Phaser audio
                    calledAuiod.pause();
                    calledAuiod.currentTime = 0;
                }
            }else {
                if (calledAuiod.paused == false){ // JS audio
                    calledAuiod.pause();
                    calledAuiod.currentTime = 0;
                }
            }
            return true;
        },
        doNextState: function ()
        {
            if (_gGameMode == null){
                console.log('Need to call window.onStart function');
                return;
            }

            if (gameState == STATE.READY_STATE)
            {
                // if (isAndroid == false){
                //     //if (introMusic.currentTime > 0){
                //         if(introMusic.isPlaying == true){
                //             introMusic.pause();
                //             introMusic.currentTime = 0;
                //         }

                // }else{
                //     if (introMusic.currentTime > 0){
                //         introMusic.pause();
                //         introMusic.currentTime = 0;
                //     }
                // }
                _playgameScene.pauseAudio(introMusic);
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }
                _playgameScene.introGroup.toggleVisible();
                _playgameScene.scene.scene.input.removeAllListeners();
                _playgameScene.scene.start('gameplayscene');
            }
        },


        /** onStart 이벤트를 받아 logo skip 처리 한다 **/
        logoSkip : function(str){
            //console.log('logoSkip : '+str+" _logo_skip : "+ _logo_skip);
            if(_playgameScene && _logo_skip == 'Y'){
                
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }
                _playgameScene.scene.scene.input.removeAllListeners();
                _playgameScene.scene.start('gameplayscene');
                return true;
            }
            return false;
        },
    };
}());