// Phaser3 example game
// tutorial scene

var IntroScene = new Phaser.Class(function(){

    var STATE = {
        INIT_STATE : 0,
        READY_STATE : 1
    };
    var gameState = 0;
    var introMusic = null;

    // Scene 전역 스코프 변수
    var _introgameScene       = null;
    var _timer     = null;

    return {
        Extends: Phaser.Scene,

        initialize:

        function IntroScene ()
        {
            Phaser.Scene.call(this, { key: 'intro_scene' });
        },
        preload: function ()
        {
            currentScene = GAME_SCENE.INTRO_SCENE;
            gameState = STATE.INIT_STATE;

            var progressBar = this.add.graphics();
            var progressBox = this.add.graphics();
            var percentText = this.make.text({
                x: window.innerWidth/2,
                y:window.innerHeight/2+25,
                style: {
                    font : '18px monospace',
                    fill : '#ffffff'
                }
            });
            percentText.setOrigin(0.5);
            progressBox.fillStyle(0x222222, 0.8);
            progressBox.fillRoundedRect(window.innerWidth/2 - 150, window.innerHeight/2, 300, 50, 5);

            // INTRO_SCENE
            if (this.game.config.width > this.game.config.height){
                this.load.image('intro_bg', 'assets/image/intro_bg_landscape.png');
            }else{
                this.load.image('intro_bg', 'assets/image/intro_bg.png');
            }
            this.load.image('intro_text_02', 'assets/image/intro_text_02.png');

            // PLAY_SCENE
            this.load.image('play_bg', 'assets/image/play_bg.png');
            this.load.image('play_bg_effect', 'assets/image/play_bg_effect.png');
            this.load.image('ic_star_normal', 'assets/image/ic_star_normal.png');
            this.load.image('ic_star_spin', 'assets/image/ic_star_spin.png');
            this.load.spritesheet('ic_hand_ani', 'assets/image/ic_hand_ani.png', { frameWidth: 890, frameHeight: 890 });
            this.load.image('ground_score_board', 'assets/image/electric-sign.png');
            this.load.spritesheet('hand_push', 'assets/image/hand_push.png', { frameWidth: 800, frameHeight: 1017 });
            this.load.image('ground_gage_empty', 'assets/image/gage_empty.png');
            this.load.image('pencil_lead', 'assets/image/pencil_lead.png');
            this.load.image('ground_play_alarm', 'assets/image/clock_empty.png');

            // RESULT_SCENE
            this.load.image('result_ic_result_popup_bg', 'assets/image/ic_result_popup_bg.png');
            this.load.image('result_ic_scoretext_bad', 'assets/image/popup_result_text_01.png');
            this.load.image('result_ic_scoretext_notgood', 'assets/image/popup_result_text_02.png');
            this.load.image('result_ic_scoretext_good', 'assets/image/popup_result_text_03.png');
            this.load.image('result_ic_scoretext_verygood', 'assets/image/popup_result_text_04.png');
            this.load.image('result_ic_scoretext_excellent', 'assets/image/popup_result_text_05.png');
            this.load.image('result_ic_popup_arrow', 'assets/image/popup_arrow.png');
            if (_gLang == "ko"){
                this.load.image('result_btn_resultview_nor', 'assets/image/btn_result_01_nor_kr.png');
                this.load.image('result_btn_nextgame_nor', 'assets/image/btn_result_popup_next_nor_kr.png');
                this.load.image('result_btn_exit_nor'      , 'assets/image/btn_result_popup_exit_nor_kr.png');
            }else{
                this.load.image('result_btn_resultview_nor', 'assets/image/btn_result_01_nor_en.png');
                this.load.image('result_btn_nextgame_nor', 'assets/image/btn_result_popup_next_nor_en.png');
                this.load.image('result_btn_exit_nor'      , 'assets/images/btn_result_popup_exit_nor.png');
            }



            // audio
            this.load.audio('audio_intro_bgm', ['assets/audio/Game_Intro_BGM.mp3']);
            this.load.audio('audio_ready_start', ['assets/audio/ready_start.mp3']);
            this.load.audio('audio_play_bgm', ['assets/audio/Game_play_BGM.mp3']);
            this.load.audio('audio_game_win', ['assets/audio/game_clear_good(Result).mp3']);
            this.load.audio('audio_game_lose', ['assets/audio/game_fail_bad(Result).mp3']);
            this.load.audio('audio_click_1', ['assets/audio/click_effect_1.mp3']);
            this.load.audio('audio_click_2', ['assets/audio/click_effect_2.mp3']);
            this.load.audio('audio_click_3', ['assets/audio/click_effect_3.mp3']);
            this.load.audio('audio_click_4', ['assets/audio/click_effect_4.mp3']);
            this.load.audio('audio_click_5', ['assets/audio/click_effect_5.mp3']);
            this.load.audio('audio_falling', ['assets/audio/falling_effect.mp3']);

            this.load.on('progress', function(value){
                progressBar.clear();
                progressBar.fillStyle(0xffffff,1);
                progressBar.fillRoundedRect(window.innerWidth/2+10-150, window.innerHeight/2+10, 280*value, 30, 5);
                percentText.setText("Loading "+Math.round(value*100) + "%");
            });

            this.load.on('complete', function(){
                // progressBox.destory();
                // progressBar.destory();
            });
        },

        create: function ()
        {
            var width = this.game.config.width;
            var height = this.game.config.height;

            _introgameScene = this;

            var logoSkip = _introgameScene.logoSkip('intro');
            if(logoSkip == true){
                return;
            }

            // set background image
            var bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'intro_bg');
            bg.displayWidth = width; //scale.setTo(0.5, 0.5);
            bg.displayHeight = this.game.config.height;

            // text
            this.pressTextImg = this.add.image(this.game.config.width/2, height - height/10, 'intro_text_02').setDisplaySize(width/2, width/2 * 0.11);

            this.pointer = this.input.on('pointerup', this.doNextState);

            if (isIos == false && isAndroid == false){
                nativeInterface.onStart(3, 60);
            }

            if (_logo_auto_skip == "Y"){
                if (debugFlag){
                    console.log("Intro >> called logo_auto_skip ");
                }
                _timer = _introgameScene.time.delayedCall(_logo_skip_wait_time, function(){
                    _introgameScene.doNextState();
                }, null, _introgameScene);  // delay in ms
            }
        },
        update: function()
        {
            // console.log("called update : time = " + ((this.time.now / 1000) % 2));
            if (((this.time.now / 1000) % 1) >= 0.5){
                this.pressTextImg.setAlpha(0);
            }else{
                this.pressTextImg.setAlpha(1);
            }
        },
        onStartAudio: function(mode, playTime)
        {
            // Scene 진입시에 play할 audio 재생
            if (debugFlag){
                console.log("Intro >> onStart :: mode = "  + mode);
            }

            // 시작 음악 재생
            if (isIos == false && _logo_skip=="N"){
                introMusic = new Audio('assets/audio/Game_Intro_BGM.mp3');
                introMusic.autoplay = true;
                introMusic.loop = true;
            }
        },
        doNextState: function ()
        {
            if (debugFlag){
                console.log('doNextState was called! ==>' + STATE.INIT_STATE);
                console.log("gameState = " + gameState + ", _gGameMode = " + _gGameMode);
            }
            if (_gGameMode == null){
                if (debugFlag){
                    console.log('Need to call window.onStart function');
                }
                return;
            }
            if (gameState == STATE.INIT_STATE){
                gameState = STATE.READY_STATE;
                if (isIos == false && introMusic){
                    if (introMusic.paused == false){
                        introMusic.pause();
                        introMusic.currentTime = 0;
                    }
                }
                if(_timer != null){
                    _timer.remove();    // 5초 timer 제거
                    _timer = null;
                }
                _introgameScene.scene.start('gameplayscene');
            }
        },
        /** onStart 이벤트를 받아 logo skip 처리 한다 **/
        logoSkip : function(str){
            //console.log('logoSkip : '+str+" _logo_skip : "+ _logo_skip);
            if(_introgameScene && _logo_skip == 'Y'){
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }

                _introgameScene.scene.scene.input.removeAllListeners();
                _introgameScene.scene.start('gameplayscene');
                return true;
            }
            return false;
        }
    };
}());