﻿var PlayGameScene = new Phaser.Class(function(){

    var STATE = {
        INTRO_SCENE : 0,
        PLAY_SCENE : 1,
        RESULT_SCENE : 2
    };

    var timeOutFlag = false;
    var __CHECK_TIME__  = 0;  // 180초 측정
    var SCORE       = 0;  // 브레인 측정 점수
    var CRITERIA_SCORE = 5; // 샤프심 이동처리 기준점수
    var CRITERIA_BONUS_SCORE = 7; // 보너스 효과 제공 기준점수
    // var LEVEL       = 2;  // 게임 레벨
    var introMusic = null;
    var thisScence = null;
    var _timer       = null;
    var VIEW_MAX_WIDTH = 0;
    var nextGameTimer;
    var nextGameTimeSec = 5000; // 5초

    var debugScoreDisplayText;
    var debugRatio=0;
    var debugShamScore=0;
    var devugRealScore=0;

    // GageBar 관련 변수
    var _gageBarWidth    = null;
    var __INTERVAL_GAGE__  = 2.8;
    var __MAX_SCORE_WIDTH__   = __INTERVAL_GAGE__ * 10 * 10;
    var __MIN_SCORE_WIDTH__   = 0;
    var upGage          = false;

    // audio 저장 변수
    var audio_ready_start = null;
    var audio_intro_bgm = null;
    var audio_play_bgm = null;
    var audio_game_win = null;
    var audio_game_lose = null;
    var audio_click_1 = null;
    var audio_click_2 = null;
    var audio_click_3 = null;
    var audio_click_4 = null;
    var audio_click_5 = null;
    var audio_falling = null;

    var width           = 0;
    var height          = 0;
    var gageBarWidth    = null;
    var intervalOfGage  = 3;
    var maxScoreWidth   = 30 * 10;
    var minScoreWidth   = 0;
    var prevScore       = 0;

    var play_bg;
    var play_bg_effect;
    var ic_star_normal;
    var ic_star_spin;
    var ic_hand_animation;
    var handPushConfig;

    var imgHand;
    var pencilLeadMove;
    var pencilLeadInitX = 0;
    var pencilLeadInitY = 0;
    var pencilLeadFinalInitX = 0;
    var pencilLeadFinalInitY = 0;
    var pencilLeadFinal;
    var pencilLeadMoveCnt = 0;
    var result_popup_bg;
    var bonusScoreText;
    var tweenBonusScore;

    var increaseMaxMove = 10;
    var increaseMove = 15;
    var increaseX = 0.57;
    var increaseY = 0.81;

    var totalPoint = 0;
    var viewTotalPoint = 0;
    var totalDropPencilLead = 0;
    var incresePoint = 1;
    var extraPoint = 1;
    var _gameResult = 0;

    // 샴 데이터를 처리하기 위한 배열
    var _aSiamData = [];
    var _strSiamRatio = "";
    var siamRatioText;

    // Toast Message
    var _messageEn = "Focus on taking out as much pencil lead as possible.";
    var _messageKr = "샤프심을 최대한 많이 빼내는 것에 집중하세요.";

    return {
        Extends: Phaser.Scene,

        initialize:

        function PlayGameScene ()
        {
            Phaser.Scene.call(this, { key: 'gameplayscene' });
        },
        preload: function ()
        {
            // 현재 게임 Scene 설정 (외부에서 판단하기 위해서)
            currentScene = GAME_SCENE.PLAY_SCENE;
            // 게임 플레이 시간 설정
            if (!isNaN(_gGamePlayTime)){
                __CHECK_TIME__ = _gGamePlayTime;
            }
            // this.checkTime = __CHECK_TIME__;

            // 샴 게임에 대한 데이터 생성
            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                _aSiamData = this.setSham();
            }

            // if (isAndroid){
            //     audio_ready_start = new Audio('assets/audio/ready_start.wav');
            //     audio_ready_start.autoplay = true;
            //     audio_ready_start.loop = false;
            // }
            if (debugFlag) {console.log("PlayGameScene >> called preload");}
        },
        create: function ()
        {
            thisScence = this;
            thisScence.checkTime = __CHECK_TIME__;

            width = this.game.config.width;
            height = this.game.config.height;

            if(width > 600){
                VIEW_MAX_WIDTH = 600;
            }else{
                VIEW_MAX_WIDTH = width;
            }

            if(debugFlag){
                console.log("this.game.config.width = " + this.game.config.width + " / VIEW_MAX_WIDTH = "+ VIEW_MAX_WIDTH +" / this.game.config.height = " + this.game.config.height);
                console.log("this.game.canvas.width = " + this.game.canvas.width + "this.game.canvas.height = " + this.game.canvas.height);
            }

            // this.introSceneGroupUi = this.add.group();
            //     // set background image
            //     var bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'intro_bg');
            //     bg.displayWidth = this.game.config.width; //scale.setTo(0.5, 0.5);
            //     bg.displayHeight = this.game.config.height;

            //     this.pressTextImg = this.add.image(width/2, height - height/10, 'intro_text_02').setDisplaySize(width/2, width/2 * 0.11)
            //     if(_logo_skip == "N"){
            //         this.introSceneGroupUi.add(bg);
            //         this.introSceneGroupUi.add(this.pressTextImg);
            //     }
            // if(currentScene != STATE.INTRO_SCENE){
            //     this.introSceneGroupUi.toggleVisible(); // 숨기기
            // }

            this.gameSceneGroupUi = this.add.group();
                play_bg_effect = this.add.image(this.game.config.width/2, this.game.config.height/2, 'play_bg_effect');
                play_bg_effect.displayWidth = this.game.config.width;
                play_bg_effect,displayHeight = this.game.config.height;

                play_bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'play_bg');
                play_bg.displayWidth = this.game.config.width;
                play_bg.displayHeight = this.game.config.height;

                ic_star_spin = this.add.image(0 - (width+200) , 0 - (height+200), 'ic_star_spin').setOrigin(0.5).setDisplaySize(width*1.2, width * 1.2);
                ic_star_normal = this.add.image(width/2 , height/2, 'ic_star_normal').setOrigin(0.5).setDisplaySize(width*1.2, width * 1.2);
                ic_hand_animation = this.add.sprite(0 - (width+200),0 - (height+200), 'ic_hand_ani').setOrigin(0.5).setDisplaySize(width*1.2, width * 1.2);

                var handAniConfig = {
                    key: 'handAniEffect',
                    frames: this.anims.generateFrameNumbers('ic_hand_ani'),
                    frameRate: 4,
                    yoyo: false,
                    repeat: 0
                };
                handAnim = this.anims.create(handAniConfig);
                ic_hand_animation.anims.load('handAniEffect');
                ic_hand_animation.on('animationcomplete', this.icHandAnimComplete, this);

                var scoreboard_height = (VIEW_MAX_WIDTH * this.game.textures.get("ground_score_board").getSourceImage().height) / this.game.textures.get("ground_score_board").getSourceImage().width;
                this.scoreBoard = this.add.image(width/2, 0, 'ground_score_board').setDisplaySize(VIEW_MAX_WIDTH, scoreboard_height).setOrigin(0.5, 0);
                var textStyleOfTotalFall = null;
                if (VIEW_MAX_WIDTH < 600){
                    textStyleOfTotalFall = { font: "bold 24px Arial", fill: "#48DFFF", boundsAlignH: "center", boundsAlignV: "middle" };
                }else {
                    textStyleOfTotalFall = { font: "bold 36px Arial", fill: "#48DFFF", boundsAlignH: "center", boundsAlignV: "middle" };
                }
                this.fallText = this.add.text(this.scoreBoard.x - this.scoreBoard.displayWidth * 0.2  , this.scoreBoard.displayHeight * 0.66 , totalDropPencilLead, textStyleOfTotalFall).setOrigin(0.5, 0.5);

                var textStyleOfScore = null;
                if (VIEW_MAX_WIDTH < 600){
                    textStyleOfScore = { font: "bold 24px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                }else {
                    textStyleOfScore = { font: "bold 36px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                }
                this.totalPoint = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.23 , this.scoreBoard.displayHeight * 0.66 , totalPoint, textStyleOfScore).setOrigin(0.5, 0.5);

                var playAlarm = this.add.image(width * 0.2, scoreboard_height + (scoreboard_height * 0.5), 'ground_play_alarm').setOrigin(0.5).setDisplaySize(VIEW_MAX_WIDTH * 0.2, VIEW_MAX_WIDTH * 0.2);
                // if(debugFlag){
                //     debugScoreDisplayText = this.add.text(width/2, scoreboard_height + 5, " debug info", textStyleOfScore).setOrigin(0.5);
                // }
                if(debugFlag){
                    siamRatioText = this.add.text(width/2  , scoreboard_height + 5 , _strSiamRatio, { font: "bold 15px Arial", fill: "#cde6ed", boundsAlignH: "center", boundsAlignV: "middle" }).setOrigin(0.5, 0.5);
                }

                imgHand = this.add.sprite(width/2 + (width*0.18), height - height/2, 'hand_push').setOrigin(0.5).setDisplaySize(VIEW_MAX_WIDTH*0.8, VIEW_MAX_WIDTH * 0.8 * 1.27).setDepth(2);

                pencilLeadInitX = imgHand.x - (imgHand.displayWidth * 0.272);
                pencilLeadInitY = imgHand.y + (imgHand.displayWidth * 0.04);
                pencilLeadMove = this.add.image(pencilLeadInitX, pencilLeadInitY, 'pencil_lead').setOrigin(0.5).setDisplaySize(VIEW_MAX_WIDTH*0.8*0.0071428, VIEW_MAX_WIDTH*0.8*0.0071428*75).setDepth(1);
                pencilLeadMove.setAngle(35);

                pencilLeadFinalInitX = imgHand.x - (imgHand.displayWidth * 0.59);
                pencilLeadFinalInitY = imgHand.y + (imgHand.displayWidth * 0.5);
                pencilLeadFinal = this.physics.add.image(-10, 10, 'pencil_lead').setOrigin(0.5).setDisplaySize(VIEW_MAX_WIDTH*0.8*0.0071428, VIEW_MAX_WIDTH*0.8*0.0071428*75).setDepth(1);

                // 보너스 추가 점수 표시 텍스트 (text)
                var textStyleOfBonusScore = null;
                if (VIEW_MAX_WIDTH < 600){
                    textStyleOfBonusScore = { font: "bold 55px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                }else {
                    textStyleOfBonusScore = { font: "bold 75px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                }
                bonusScoreText = this.add.text(imgHand.x - imgHand.displayWidth/2 , imgHand.y, "+ 5", textStyleOfBonusScore).setOrigin(0.5, 0.5);
                tweenBonusScore = this.tweens.add({
                    targets : bonusScoreText,
                    alpha: { from: 0, to: 1 },
                    x : imgHand.x - imgHand.displayWidth/2,
                    y : imgHand.y - imgHand.displayWidth/2,
                    duration : 1000,
                    delay : 0,
                    ease : 'Back.Out',
                    repeat : 0,
                    onComplete: function () {
                        bonusScoreText.visible = false;
                    }
                });
                bonusScoreText.visible = false;

                this.gageBarBG = this.add.graphics();
                this.gageBar = this.add.graphics();
                var gage_width = 340;
                var gage_height = (gage_width * this.game.textures.get("ground_gage_empty").getSourceImage().height) / this.game.textures.get("ground_gage_empty").getSourceImage().width;
                this.mentalGage = this.add.image(width/2, height * 0.93, 'ground_gage_empty').setDisplaySize(gage_width, gage_height).setDepth(2); ;


                var textStyleOfAlarm = null;
                if (VIEW_MAX_WIDTH < 600){
                    textStyleOfAlarm = { font: "bold 20px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
                }else {
                    textStyleOfAlarm = { font: "bold 30px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
                }
                this.alarmText = this.add.text(playAlarm.x , playAlarm.y , thisScence.checkTime, textStyleOfAlarm).setOrigin(0.5);

                handPushConfig1 = {
                    key: 'pushEffect1',
                    frames: this.anims.generateFrameNumbers('hand_push'),
                    frameRate: 4,
                    yoyo: false,
                    repeat: 0
                };
                handPushConfig2 = {
                    key: 'pushEffect2',
                    frames: this.anims.generateFrameNumbers('hand_push'),
                    frameRate: 6,
                    yoyo: false,
                    repeat: 1
                };
                handPushConfig3 = {
                    key: 'pushEffect3',
                    frames: this.anims.generateFrameNumbers('hand_push'),
                    frameRate: 8,
                    yoyo: false,
                    repeat: 2
                };
                handPushConfig4 = {
                    key: 'pushEffect4',
                    frames: this.anims.generateFrameNumbers('hand_push'),
                    frameRate: 10,
                    yoyo: false,
                    repeat: 3
                };
                handPushConfig5 = {
                    key: 'pushEffect5',
                    frames: this.anims.generateFrameNumbers('hand_push'),
                    frameRate: 12,
                    yoyo: false,
                    repeat: 4
                };
                anim1 = this.anims.create(handPushConfig1);
                anim2 = this.anims.create(handPushConfig2);
                anim3 = this.anims.create(handPushConfig3);
                anim4 = this.anims.create(handPushConfig4);
                anim5 = this.anims.create(handPushConfig5);
                imgHand.anims.load('pushEffect1');
                imgHand.anims.load('pushEffect2');
                imgHand.anims.load('pushEffect3');
                imgHand.anims.load('pushEffect4');
                imgHand.anims.load('pushEffect5');
                imgHand.on('animationcomplete', this.imgHandAnimComplete, this);



                this.gameSceneGroupUi.add(play_bg_effect);
                this.gameSceneGroupUi.add(play_bg);
                this.gameSceneGroupUi.add(ic_star_spin);
                this.gameSceneGroupUi.add(ic_star_normal);
                this.gameSceneGroupUi.add(ic_hand_animation);
                this.gameSceneGroupUi.add(this.scoreBoard);
                // if(debugFlag){
                //     this.gameSceneGroupUi.add(debugScoreDisplayText);
                // }
                if(debugFlag){
                    this.gameSceneGroupUi.add(siamRatioText);
                }
                this.gameSceneGroupUi.add(this.fallText);
                this.gameSceneGroupUi.add(this.totalPoint);
                this.gameSceneGroupUi.add(playAlarm);
                this.gameSceneGroupUi.add(this.alarmText);
                this.gameSceneGroupUi.add(pencilLeadMove);
                this.gameSceneGroupUi.add(pencilLeadFinal);
                this.gameSceneGroupUi.add(imgHand);
                this.gameSceneGroupUi.add(this.mentalGage);
                this.gameSceneGroupUi.add(this.gageBar);
                this.gameSceneGroupUi.add(this.gageBarBG);


            // if(currentScene != STATE.PLAY_SCENE){
            //     this.gameSceneGroupUi.toggleVisible(); // 숨기기
            // }

            this.resultSceneGroupUi = this.add.group();
                this.result_bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'play_bg');
                this.result_bg.displayWidth = this.game.config.width;
                this.result_bg.displayHeight = this.game.config.height;

                result_popup_bg = this.add.image(width/2, height/2 - (height*0.1), 'result_ic_result_popup_bg').setDisplaySize(VIEW_MAX_WIDTH * 0.75, VIEW_MAX_WIDTH * 0.75 * 1.62);
                this.result_popup_view_result_nor = this.add.image(width/2, height/2, 'result_btn_resultview_nor').setDisplaySize(VIEW_MAX_WIDTH/2, VIEW_MAX_WIDTH/2 * 0.32);
                this.result_popup_view_result_nor.y = height - this.result_popup_view_result_nor.displayHeight + 2;
                this.result_popup_view_result_nor.setInteractive();
                this.result_popup_next_game_nor = this.add.image(width/2, height/2, 'result_btn_nextgame_nor').setDisplaySize(VIEW_MAX_WIDTH/2, VIEW_MAX_WIDTH/2 * 0.32);
                this.result_popup_next_game_nor.y = height - this.result_popup_next_game_nor.displayHeight + 2;
                this.result_popup_next_game_nor.setInteractive();

                this.result_popup_exit_nor = this.add.image(width/2, height/2, 'result_btn_exit_nor').setDisplaySize(VIEW_MAX_WIDTH/2, VIEW_MAX_WIDTH/2 * 0.32);
                this.result_popup_exit_nor.y = height - this.result_popup_exit_nor.displayHeight + 2;
                this.result_popup_exit_nor.setInteractive();

                // scale이 최대 넓이 600px일 경우에 0.3
                // 각 해상도에 따라서 최대 넓이 기준으로 비율 조정
                var scaleOfResultText = 0.3 * VIEW_MAX_WIDTH/600;
                this.result_popup_text_bad = this.add.image(width/2 , result_popup_bg.y, 'result_ic_scoretext_bad').setScale(scaleOfResultText);
                this.result_popup_text_notgood = this.add.image(width/2 , result_popup_bg.y, 'result_ic_scoretext_notgood').setScale(scaleOfResultText);
                this.result_popup_text_good = this.add.image(width/2 , result_popup_bg.y, 'result_ic_scoretext_good').setScale(scaleOfResultText);
                this.result_popup_text_verygood = this.add.image(width/2 , result_popup_bg.y, 'result_ic_scoretext_verygood').setScale(scaleOfResultText);
                this.result_popup_text_excellent = this.add.image(width/2 , result_popup_bg.y, 'result_ic_scoretext_excellent').setScale(scaleOfResultText);

                // 위치는 결과 텍스트를 기존으로 밑으로 붙임.
                var textStyleOfResultScore = null;
                if (VIEW_MAX_WIDTH < 600){
                    textStyleOfResultScore = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
                }else {
                    textStyleOfResultScore = { font: "bold 35px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                }
                this.scoreTextOfResult = this.add.text(width/2, result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , totalPoint, textStyleOfResultScore).setOrigin(0.5, 1);
                this.scoreTextOfResult.setStroke('#000', 5);

                // 높이의 기준은 width 로 하며, 해당 넓이에 따라서 비율로 처리
                var scaleOfResultArrow = 0.38 * VIEW_MAX_WIDTH/600;
                this.result_popup_arrow = this.add.image(width/2 - result_popup_bg.displayWidth/3, height/2  + VIEW_MAX_WIDTH * 0.25 , 'result_ic_popup_arrow').setScale(scaleOfResultArrow);

                // 결과 팝업에서 보여주는 점수 텍스트
                var textStyleOfResultStandardScore = null;
                if (VIEW_MAX_WIDTH < 600){
                textStyleOfResultStandardScore = { font: "bold 13px Arial", fill: "#000", align: "right", boundsAlignH: "right", boundsAlignV: "middle"};
                }else {
                textStyleOfResultStandardScore = { font: "bold 20px Arial", fill: "#000", align: "right"/*, boundsAlignH: "right", boundsAlignV: "middle"*/ };
                }
                this.scoreTextOfResult1 = this.add.text(width/2 + result_popup_bg.displayWidth/3 , result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "0 ~ 20 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
                this.scoreTextOfResult2 = this.add.text(width/2 + result_popup_bg.displayWidth/3 , result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "21 ~ 40 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
                this.scoreTextOfResult3 = this.add.text(width/2 + result_popup_bg.displayWidth/3 , result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "41 ~ 60 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
                this.scoreTextOfResult4 = this.add.text(width/2 + result_popup_bg.displayWidth/3 , result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "61 ~ 80 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);
                this.scoreTextOfResult5 = this.add.text(width/2 + result_popup_bg.displayWidth/3 , result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "81 ~ 100 %", textStyleOfResultStandardScore).setOrigin(1, 0.5);


                this.resultSceneGroupUi.add(this.result_bg);
                this.resultSceneGroupUi.add(result_popup_bg);
                this.resultSceneGroupUi.add(this.result_popup_view_result_nor);
                this.resultSceneGroupUi.add(this.result_popup_next_game_nor);
                this.resultSceneGroupUi.add(this.result_popup_exit_nor);
                this.resultSceneGroupUi.add(this.result_popup_text_bad);
                this.resultSceneGroupUi.add(this.result_popup_text_notgood);
                this.resultSceneGroupUi.add(this.result_popup_text_good);
                this.resultSceneGroupUi.add(this.result_popup_text_verygood);
                this.resultSceneGroupUi.add(this.result_popup_text_excellent);
                this.resultSceneGroupUi.add(this.scoreTextOfResult);
                this.resultSceneGroupUi.add(this.result_popup_arrow);
                this.resultSceneGroupUi.add(this.scoreTextOfResult1);
                this.resultSceneGroupUi.add(this.scoreTextOfResult2);
                this.resultSceneGroupUi.add(this.scoreTextOfResult3);
                this.resultSceneGroupUi.add(this.scoreTextOfResult4);
                this.resultSceneGroupUi.add(this.scoreTextOfResult5);

            if(currentScene != STATE.RESULT_SCENE){
                this.resultSceneGroupUi.toggleVisible(); // 숨기기
            }

            // Event
            this.result_popup_view_result_nor.on('pointerup', function(porinter, x, y, event){
                event.stopPropagation();
                thisScence.doNextState();
            });
            this.result_popup_next_game_nor.on('pointerup', function(pointer, x, y, event){
                event.stopPropagation();
                thisScence.doNextState();
            });
            this.result_popup_exit_nor.on('pointerup', function(pointer, x, y, event){
                event.stopPropagation();
                thisScence.doNextState();
            });

            if (_gIsPlayingAudioByPhaser){
                audio_ready_start = this.sound.add('audio_ready_start');
                audio_intro_bgm = this.sound.add('audio_intro_bgm');
                audio_play_bgm = this.sound.add('audio_play_bgm');
                audio_game_win = this.sound.add('audio_game_win');
                audio_game_lose = this.sound.add('audio_game_lose');
                audio_click_1 = this.sound.add('audio_click_1');
                audio_click_2 = this.sound.add('audio_click_2');
                audio_click_3 = this.sound.add('audio_click_3');
                audio_click_4 = this.sound.add('audio_click_4');
                audio_click_5 = this.sound.add('audio_click_5');
                audio_falling = this.sound.add('audio_falling');
            }else{
                audio_ready_start = new Audio('assets/audio/ready_start.mp3');
                audio_intro_bgm = new Audio('assets/audio/Game_Intro_BGM.mp3');
                audio_play_bgm = new Audio('assets/audio/Game_play_BGM.mp3');
                audio_game_win = new Audio('assets/audio/game_clear_good(Result).mp3');
                audio_game_lose = new Audio('assets/audio/game_fail_bad(Result).mp3');
                audio_click_1 = new Audio('assets/audio/click_effect_1.mp3');
                audio_click_2 = new Audio('assets/audio/click_effect_2.mp3');
                audio_click_3 = new Audio('assets/audio/click_effect_3.mp3');
                audio_click_4 = new Audio('assets/audio/click_effect_4.mp3');
                audio_click_5 = new Audio('assets/audio/click_effect_5.mp3');
                audio_falling = new Audio('assets/audio/falling_effect.mp3');
            }

            this.pointer = this.input.on('pointerup', this.doNextState);

            // if (isIos == false && isAndroid == false){
            //     if(debugFlag){
            //         console.log("run function : nativeInterface.onStart(3, 180)");
            //     }
            //     nativeInterface.onStart(3, 180);
            // }

            // onStartAudio() 초기화 건을 create 함수로 가져옴
            if(debugFlag){
                // sham 데이터 비율 및 평균점수 노출
                siamRatioText.setText(_strSiamRatio);
            }

            if (_gIsPlayingAudioByPhaser){
                audio_ready_start.play();

                audio_ready_start.on('complete', function(){
                    thisScence.showToast();

                    if(debugFlag){
                        console.log("completed audio_ready_start. init TIME val.");
                    }

                    audio_ready_start.currentTime = 0;
                    thisScence.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = thisScence.time.addEvent({
                        delay: 1000,
                        callback: thisScence.updateTimerupdateTimer,
                        callbackScope: thisScence.scene,
                        repeat: thisScence.checkTime
                    });

                    if(SOUND_BACKGROUND){
                        audio_play_bgm.play({loop:true, volume:SOUND_VOLUME});
                    }

                    nativeInterface.onGameStart(_gGameDifficultyStep);
                });
            }else{
                audio_ready_start.play();

                audio_ready_start.addEventListener("ended", function(){
                    thisScence.showToast();

                    if(debugFlag){
                        console.log("completed audio_ready_start. init TIME val.");
                    }

                    audio_ready_start.currentTime = 0;
                    thisScence.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = thisScence.time.addEvent({
                        delay: 1000,
                        callback: thisScence.updateTimerupdateTimer,
                        callbackScope: thisScence.scene,
                        repeat: thisScence.checkTime
                    });

                    if(SOUND_BACKGROUND){
                        audio_play_bgm.play();
                        audio_play_bgm.loop = true;
                        audio_play_bgm.volume = SOUND_VOLUME;
                    }

                    nativeInterface.onGameStart(_gGameDifficultyStep);
                });
            }

        },
        update: function()
        {
            if(currentScene == STATE.PLAY_SCENE){
                // if(debugFlag){
                //     debugScoreDisplayText.setText("Ratio="+debugRatio+"/R_S="+devugRealScore+"/S_S="+debugShamScore);
                // }
                thisScence.alarmText.setText(thisScence.checkTime);
                thisScence.fallText.setText(totalDropPencilLead);
                thisScence.totalPoint.setText(totalPoint);
                this.updateGage();
            }
        },
        setScore: function (score)
        {
            if(debugFlag){
                console.log("called setScore >> score = " + score + ", currentScene = " + currentScene + ", _gGameMode = " + _gGameMode);
            }
            if (currentScene == STATE.PLAY_SCENE){

                devugRealScore = score;

                if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                    var siam_score = _aSiamData.pop();
                    if(debugFlag){
                        console.log("setScore >> score = " + score + ", siam score = " + siam_score);
                    }
                    debugShamScore = siam_score;
                    score = siam_score;
                }

                if(score <= 0){
                    // 반응 없음
                }else{
                    incresePoint = 0;
                    if(score >= 9){
                        incresePoint = 5;
                    }else if(score >= 7){
                        incresePoint = 4;
                    }else if(score >= 5){
                        incresePoint = 3;
                    }else if(score >= 3){
                        incresePoint = 2;
                    }else if(score >= 1){
                        incresePoint = 1;
                    }
                    totalPoint = totalPoint + incresePoint;
                    this.movePencilLead(score);
                }

                prevScore = SCORE;
                SCORE = score;
                if (prevScore < SCORE){
                    upGage  = true;
                }else{
                    upGage  = false;
                }

            }

        },
        updateGage : function()
        {
           var x = width/2 - this.mentalGage.displayWidth/2 + 55;
           var y = height * 0.93;

           this.gageBarBG.clear();
           this.gageBarBG.fillStyle(0x212f39, 1);
           this.gageBarBG.fillRoundedRect(x, y - 12, __MAX_SCORE_WIDTH__, 20, 8);

           this.gageBar.clear();
           this.gageBar.fillStyle(0xffd400, 1);
           if (upGage && _gageBarWidth < __INTERVAL_GAGE__ * 10 * SCORE){
               _gageBarWidth += __INTERVAL_GAGE__;
               if (_gageBarWidth >= __MAX_SCORE_WIDTH__){
                   _gageBarWidth = __MAX_SCORE_WIDTH__;
               }
           }else if(upGage == false && _gageBarWidth >= __INTERVAL_GAGE__ * 10 * SCORE){
               _gageBarWidth -= __INTERVAL_GAGE__;
               if (_gageBarWidth <= __MIN_SCORE_WIDTH__){
                   _gageBarWidth = 0;
               }
           }
           this.gageBar.fillRoundedRect(x, y - 12, _gageBarWidth, 20, 8);
        },
        updateTimerupdateTimer : function ()
        {
            if (currentScene == STATE.PLAY_SCENE){
                thisScence.checkTime--;
                if (thisScence.checkTime < 0){
                    timeOutFlag = true;
                    thisScence.doNextState();
                }
            }else{
                _timer.paused = true;
            }
        },
        movePencilLead: function(score){
            // 번쩍이는 effect를 클릭 시점과 유사하게 처리 (lazy load)
            setTimeout(function() {
                ic_star_spin.x = width/2;
                ic_star_spin.y = height/2;
            }, 300);

            // 집중도 값에 따라 클릭 애니메이션 효과처리
            if(score >= 9){
                imgHand.anims.play('pushEffect5');
                audio_click_5.play();
                audio_click_5.currentTime = 0;
            }else if(score >= 7){
                imgHand.anims.play('pushEffect4');
                audio_click_4.play();
                audio_click_5.currentTime = 0;
            }else if(score >= 5){
                imgHand.anims.play('pushEffect3');
                audio_click_3.play();
                audio_click_3.currentTime = 0;
            }else if(score >= 3){
                imgHand.anims.play('pushEffect2');
                audio_click_2.play();
                audio_click_2.currentTime = 0;
            }else{
                imgHand.anims.play('pushEffect1');
                audio_click_1.play();
                audio_click_1.currentTime = 0;
            }

            var currentClickCnt = Math.ceil(score/2);
            pencilLeadMoveCnt = pencilLeadMoveCnt + currentClickCnt;

            if(pencilLeadMoveCnt < increaseMaxMove){
                //샤프심 빠짐 시점을 클릭 시점과 유사하게 처리 (lazy load)
                setTimeout(function() {
                    pencilLeadMove.x = pencilLeadMove.x - ((increaseMove*increaseX)*currentClickCnt);
                    pencilLeadMove.y = pencilLeadMove.y + ((increaseMove*increaseY)*currentClickCnt);
                }, 300);
            }else if(pencilLeadMoveCnt>=increaseMaxMove){
                play_bg.x = 0 - width;
                play_bg.y = 0 - height;

                ic_hand_animation.x = width/2;
                ic_hand_animation.y = height/2;
                ic_hand_animation.anims.play('handAniEffect');

                audio_falling.play();
                audio_falling.currentTime = 0;

                totalDropPencilLead = totalDropPencilLead +1;
                totalPoint = totalPoint + extraPoint;

                pencilLeadMove.x = pencilLeadInitX;
                pencilLeadMove.y = pencilLeadInitY;

                pencilLeadFinal.x = pencilLeadFinalInitX;
                pencilLeadFinal.y = pencilLeadFinalInitY;
                pencilLeadFinal.body.velocity.set(-200,400);
                pencilLeadFinal.setCollideWorldBounds(true);
                pencilLeadFinal.setCollideWorldBounds(true, 0, 0.3);
                pencilLeadFinal.setAngle(35);

                pencilLeadMoveCnt = 0;

                setTimeout(function() {
                    pencilLeadFinal.setCollideWorldBounds(false);
                }, 1500);

            }


            // 지정 집중도 이상일 때 누적포인트 애니메이션 효과
            if(score >= CRITERIA_BONUS_SCORE){
                bonusScoreText.setText("" + totalPoint);
                bonusScoreText.visible = true;
                tweenBonusScore.restart();
            }

        },
        imgHandAnimComplete : function(){
            ic_star_spin.x = 0 - width+200;
            ic_star_spin.y = 0 - height+200;
        },
        icHandAnimComplete : function(){
            ic_hand_animation.x = 0 - width+200;
            ic_hand_animation.y = 0 - height+200;

            play_bg.x = width/2;
            play_bg.y = height/2;
        },
        onStartAudio: function()
        {

        },
        doNextState: function ()
        {
            if(debugFlag){
                console.log('doNextState was called! ==>' + currentScene + " / _gGameMode = " + _gGameMode);
            }

            if (currentScene == STATE.INTRO_SCENE){
                // //  webBrowser 예외처리 (debug 일때만 적용)
                // if(debugFlag && debugWebCallJsFlag){
                //     console.log('Debug Exception Call : _gGameMode is null => Setting Sham Data');
                //     window.onStart(3);
                // }

                // if(_gGameMode == null){
                //     console.log('Need to call window.onStart(1, 180) ~ window.onStart(4, 180) function');
                //     return;
                // }

                // if (isIos == false ){
                //     if(audio_intro_bgm.isPlaying){
                //         audio_intro_bgm.pause();
                //         audio_intro_bgm.currentTime = 0;
                //     }
                // }
                // // thisScence.introSceneGroupUi.toggleVisible();
                // thisScence.gameSceneGroupUi.toggleVisible();
                // currentScene = STATE.PLAY_SCENE;

                // audio_ready_start.play();

                // audio_ready_start.on('complete', function(){
                //     thisScence.showToast();

                //     if(debugFlag){
                //         console.log("completed audio_ready_start. init TIME val.");
                //     }

                //     audio_ready_start.currentTime = 0;
                //     thisScence.checkTime = __CHECK_TIME__;  // Timer 초기화
                //     _timer = thisScence.time.addEvent({
                //         delay: 1000,
                //         callback: thisScence.updateTimerupdateTimer,
                //         callbackScope: thisScence.scene,
                //         repeat: thisScence.checkTime
                //     });

                //     if(SOUND_BACKGROUND){
                //         audio_play_bgm.play({loop:true, volume:SOUND_VOLUME});
                //     }

                //     nativeInterface.onGameStart(_gGameDifficultyStep);
                // });
            }else if (currentScene == STATE.PLAY_SCENE && timeOutFlag){
                if(_result_skip == 'N'){
                    thisScence.gameSceneGroupUi.toggleVisible();
                    thisScence.resultSceneGroupUi.toggleVisible();
                }

                if (_gIsPlayingAudioByPhaser){
                    if (audio_play_bgm.isPlaying){
                        audio_play_bgm.pause();
                        audio_play_bgm.currentTime = 0;
                    }
                }else {
                    if (audio_play_bgm.paused == false){
                        audio_play_bgm.pause();
                        audio_play_bgm.currentTime = 0;
                    }
                }

                viewTotalPoint = totalPoint;

                audio_game_win.play();  // audio play
                thisScence.setDisplayResultPopup(); // 결과 팝업 내용 처리

                currentScene = STATE.RESULT_SCENE;

                // 게임종료 : send to APP
                nativeInterface.onGameEnd(viewTotalPoint);

                // 자동으로 APP 통보 (다음게임 또는 종료)
                if(_result_skip == 'N'){
                    if(_result_auto_skip == "Y"){
                        nextGameTimer = setTimeout(function() {
                            thisScence.doNextState();
                        }, _result_skip_wait_time);
                    }
                }else{
                    // 결과창 skip 일 때
                    thisScence.doNextState();
                }
            }else if (currentScene == STATE.RESULT_SCENE){
                if (_gIsPlayingAudioByPhaser){
                    if (audio_game_win.isPlaying){
                        audio_game_win.pause();
                        audio_game_win.currentTime = 0;
                    }
                    if (audio_game_lose.isPlaying){
                        audio_game_lose.pause();
                        audio_game_lose.currentTime = 0;
                    }
                }else{
                    if (audio_game_win.paused == false){
                        audio_game_win.pause();
                        audio_game_win.currentTime = 0;
                    }
                    if (audio_game_lose.paused == false){
                        audio_game_lose.pause();
                        audio_game_lose.currentTime = 0;
                    }
                }

                if (_timer != null){
                    if(debugFlag){
                        console.log("exit >> _timer = " + _timer);
                    }
                    _timer.remove();
                }

                var params= '{"result":'+_gameResult+',"percent":'+totalPoint+'}';
                nativeInterface.onNextGame(params);
            }else if (currentScene == STATE.PLAY_SCENE && debugFlag && debugWebCallJsFlag){
                console.log("Debug Exception Call callJS(0)");
                window.callJS(0);
            }
        },
        createEnded: function()
        {
            if(debugFlag){
                console.log("called createEnded function");
            }
            //nativeInterface.onStart(null);
        },
        setSham : function() {
            CON_ARR = [];
            var val = 0;
            var beforeVal = 0;
            var conSuccessCnt = 0;
            var successCnt = 0;
            var successMax = 10;
            var successMin = CRITERIA_SCORE;
            var failMax = (CRITERIA_SCORE-1);
            var failMin = 0;
            var ratio_arr = [0.2, 0.5, 0.8, 0.9];
            debugRatio = ratio_arr.sort(function () {return 0.5 - Math.random()}).pop();
            if(debugFlag){
                console.log('ratio = > ${'+ debugRatio + ' * 100}% 확율의 성공');
            }
            for (var i = 0; i < 120; i++) {
              if (Math.random() < debugRatio) {
                val = Math.floor(Math.random() * (successMax - successMin + 1)) + successMin;
              } else {
                val = Math.floor(Math.random() * (failMax - failMin + 1)) + failMin;
              }
              CON_ARR.push(val);
            }
            if(debugFlag){
                console.log('CON_ARR =>'+CON_ARR);

                var average = 0, total = 0;
                for (var j = 0; j < CON_ARR.length; j++){
                    total += CON_ARR[j];
                }
                average = total / CON_ARR.length;
                _strSiamRatio = "ratio : " + (debugRatio * 100) + " (" + average.toFixed(2) + ")%";
                console.log(_strSiamRatio);
            }
            return CON_ARR;
        },
        setDisplayResultPopup : function()
        {
            thisScence.result_popup_text_bad.visible = false;
            thisScence.result_popup_text_notgood.visible = false;
            thisScence.result_popup_text_good.visible = false;
            thisScence.result_popup_text_verygood.visible = false;
            thisScence.result_popup_text_excellent.visible = false;

            // 게임 시간에 따른 스코어 설정 및 스코어 표시
            var badScroe = 100;
            var notgoodScore = 200;
            var goodScore = 300;
            var verygoodScore = 400;
            var MAX_POINT = 500;
            var MAX_PLAY_TIME = 180;
            var scoreRate = MAX_PLAY_TIME/__CHECK_TIME__;

            badScroe        = 20;//parseInt( badScroe / scoreRate );
            notgoodScore    = 40;//parseInt( notgoodScore / scoreRate );
            goodScore       = 60;//parseInt( goodScore / scoreRate );
            verygoodScore   = 80;//parseInt( verygoodScore / scoreRate );

            totalPoint    = parseInt((totalPoint / ( MAX_POINT / scoreRate )) * 100); // 배점 계산 필요
            if (totalPoint > 100){
                totalPoint = 100;
            }

            thisScence.scoreTextOfResult1.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.465);
            thisScence.scoreTextOfResult2.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.415);
            thisScence.scoreTextOfResult3.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.36);
            thisScence.scoreTextOfResult4.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.305);
            thisScence.scoreTextOfResult5.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.253);

            if (totalPoint >= 0 && totalPoint <= badScroe){
                if(_result_skip == "N"){
                    thisScence.result_popup_text_bad.visible = true;
                    thisScence.result_popup_arrow.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.47);
                }
                _gameResult = 1;
            }else if (totalPoint >= (badScroe+1) && totalPoint <= notgoodScore){
                if(_result_skip == "N"){
                    thisScence.result_popup_text_notgood.visible = true;
                    thisScence.result_popup_arrow.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.42);
                }
                _gameResult = 2;
            }else if (totalPoint >= (notgoodScore+1) && totalPoint <= goodScore){
                if(_result_skip == "N"){
                    thisScence.result_popup_text_good.visible = true;
                    thisScence.result_popup_arrow.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.37);
                }
                _gameResult = 3;
            }else if (totalPoint >= (goodScore+1) && totalPoint <= verygoodScore){
                if(_result_skip == "N"){
                    thisScence.result_popup_text_verygood.visible = true;
                    thisScence.result_popup_arrow.setY(result_popup_bg.y+ VIEW_MAX_WIDTH * 0.315);
                }
                _gameResult = 4;
            }else if (totalPoint >= (verygoodScore+1)){
                if(_result_skip == "N"){
                    thisScence.result_popup_text_excellent.visible = true;
                    thisScence.result_popup_arrow.setY(result_popup_bg.y + VIEW_MAX_WIDTH * 0.267);
                }
                _gameResult = 5;
            }

            if(_result_skip == "Y"){
                // _result_skip이 "Y"인 경우에, 화면 처리하지 않고 return
                return;
            }


            // 최종 점수 표시
            thisScence.scoreTextOfResult.setText(viewTotalPoint + " ("+totalPoint+"%)");


            // 게임 모드에 따라서 버튼을 보여주도록 처리
            if (_gGameMode == GAME_MODE.NORMAR || _gGameMode == GAME_MODE.SIAM){
                thisScence.result_popup_next_game_nor.visible = true;
                thisScence.result_popup_view_result_nor.visible = false;
                thisScence.result_popup_exit_nor.visible = false;
            }else{
                if(_result_button_code == 'BTN001'){
                    thisScence.result_popup_next_game_nor.visible = false;
                    thisScence.result_popup_view_result_nor.visible = false;
                    thisScence.result_popup_exit_nor.visible = true;
                }else{
                    thisScence.result_popup_next_game_nor.visible = false;
                    thisScence.result_popup_view_result_nor.visible = true;
                    thisScence.result_popup_exit_nor.visible = false;
                }
            }
        },
        setSoundBackground : function(data)
        {
            if(debugFlag){
                console.log("called setSoundBackground : " + data + " / currentScene : " + currentScene + " / SOUND_BACKGROUND : "+SOUND_BACKGROUND);
            }
            if(data == "off"){
                SOUND_BACKGROUND = false;
                if (currentScene == STATE.INTRO_SCENE){
                    if (_gIsPlayingAudioByPhaser){
                        if (audio_intro_bgm.isPlaying){
                            audio_intro_bgm.pause();
                            audio_intro_bgm.currentTime = 0;
                            if(debugFlag){
                                console.log("audio_play_bgm : 1");
                            }
                        }
                    }else{
                        if (audio_intro_bgm.paused == false){
                            audio_intro_bgm.pause();
                            audio_intro_bgm.currentTime = 0;
                            if(debugFlag){
                                console.log("audio_play_bgm : 1");
                            }
                        }
                    }
                }
                if (currentScene == STATE.PLAY_SCENE){
                    if (_gIsPlayingAudioByPhaser){
                        if (audio_play_bgm.isPlaying){
                            audio_play_bgm.pause();
                            audio_play_bgm.currentTime = 0;
                            if(debugFlag){
                                console.log("audio_play_bgm : 2");
                            }
                        }
                    }else{
                        if (audio_play_bgm.paused == false){
                            audio_play_bgm.pause();
                            audio_play_bgm.currentTime = 0;
                            if(debugFlag){
                                console.log("audio_play_bgm : 2");
                            }
                        }
                    }
                }
            }else{
                SOUND_BACKGROUND = true;
                if (currentScene == STATE.INTRO_SCENE){
                    if (_gIsPlayingAudioByPhaser){
                        if (!audio_intro_bgm.isPlaying){
                            audio_intro_bgm.play({loop:true});
                        }
                    }else{
                        if (audio_intro_bgm.paused){
                            audio_intro_bgm.play();
                            audio_intro_bgm.loop = true;
                        }
                    }
                    if(debugFlag){
                        console.log("audio_play_bgm : 3");
                    }
                }
                if (currentScene == STATE.PLAY_SCENE){
                    if (_gIsPlayingAudioByPhaser){
                        if (!audio_play_bgm.isPlaying){
                            audio_play_bgm.play({loop:true});
                        }
                    }else{
                        if (audio_play_bgm.paused){
                            audio_play_bgm.play();
                            audio_play_bgm.loop = true;
                        }
                    }
                    if(debugFlag){
                        console.log("audio_play_bgm : 4");
                    }
                }
            }
        },
        showToast: function() {
            var toast = document.getElementById("toastMessageBar");

            if (_gLang == "ko"){
                toast.innerHTML = _messageKr;
            }else {
                toast.innerHTML = _messageEn;
            }
            $("#toastMessageBar").show(500);
            setTimeout(function(){
                $("#toastMessageBar").hide(500);
            }, 5000);
        },
         /** onStart 이벤트를 받아 logo skip 처리 한다 **/
         logoSkip : function(str){
            //console.log('logoSkip : '+str+" _logo_skip : "+ _logo_skip);
            if(_introgameScene && _logo_skip == 'Y'){
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }

                _introgameScene.scene.scene.input.removeAllListeners();
                _introgameScene.scene.start('gameplayscene');
                return true;
            }
            return false;
        }
    };
}());