/**
 * BrowserInterface는 mobile의 browser 기능 호출에 필요한 interface를 제공하는 클래스입니다.
 *
 * @classDescription
 * @type {Object}
 */
var BrowserInterface = (function() {
    var InterfaceName = "browser";
	/**
	 *  NativeInterface initialize.
	 */
	function initialize(){
		console.log("BrowserInterface >> initialize called");
    };

    function getInterfaceName(){
        return InterfaceName;
    }

	// Call funtion of Native Interface ///////////////////////////////////////////////////////////////////////
  function onGameStart(level)
    {
        console.log("BrowserInterface >> onGameStart :: level = " + level);
    }

    function onGameEnd(score)
    {
        console.log("BrowserInterface >> onGameEnd :: score = " + score);
    }

    function onNextGame(result)
    {
        console.log("BrowserInterface >> onNextGame :: result = " + result);
    }

    function onStart(data)
    {
        console.log("BrowserInterface >> onStart" + data);
        window.onStart(data);
    }

	//////////////////////////////////////////////////////////////////////////////////////////////////
    function onkeydown(val){
        //console.log("BrowserInterface >> keyDown val :::"+ val.keyCode);
        if (val.keyCode >= 48 && val.keyCode <= 57){
          //0 ~ 9
          window.callJS(val.keyCode - 48);
      }else if (val.keyCode == 189){
          // -
          window.callJS(10);
      }
    }

	// Native -> UI 함수 호출 ///////////////////////////////////////////////////////////////////////////
	function OnNativeEvent(command, data, data1){
		//console.log("NativeInterfaceForBrowser   ++++++++++++++++++++++     command = " + command + ", data = " + data);

        var result;
        switch (command)
        {
            case "callJS":
                if (Number(data) < 0){
                    data = 0;
                    // 2019.11.13 Siam 모드일 경우에는 정상적으로 처리되기 때문에 callJS를 무시하도록 변경함.
                    return;
                }
                PlayGameScene.prototype.setScore(Number(data));;
                break;
            case "callLevelJS":
                // PlayGameScene.prototype.LEVEL = Number(data);
                break;
            case "onStart":
                var data =  '{"logo_skip_yn": "Y","intro_skip_yn": "N","result_skip_yn": "N", "sham_mode_yn": "Y",	"define_game_level": "2",	"play_time": "5",	"logo_auto_skip_yn": "Y",	"logo_skip_wait_time": "5",	"result_auto_skip_yn": "Y",	"result_skip_wait_time": "5", "result_button_code": "BTN001"}';

                console.log("typeof data : "+ (typeof data));
                if(typeof data == 'string'){
                    onSetGameConfig((data));
                }else{
                    onSetGameConfig(JSON.stringify(data));
                }

                if (currentScene == GAME_SCENE.INTRO_SCENE){
                    // PlayGameScene.prototype.onStartAudio(Number(data), Number(data1));
                    IntroScene.prototype.onStartAudio();
                }else if (currentScene == GAME_SCENE.PLAY_SCENE){
                }
                break;
            case "soundBackground":
                    PlayGameScene.prototype.setSoundBackground(data);
                    break;
        }
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////

	/* Public API */
	return {
		Initialize      : initialize,
        onGameStart     : onGameStart,
        onGameEnd       : onGameEnd,
        onNextGame      : onNextGame,
        onStart         : onStart,
        onkeydown       : onkeydown,
        OnNativeEvent   : OnNativeEvent,
        getInterfaceName : getInterfaceName
	};

})();
