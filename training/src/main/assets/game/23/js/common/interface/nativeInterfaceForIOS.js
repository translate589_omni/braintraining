/**
 * iOSNativeInterface는 iOS naitvie 기능 호출에 필요한 interface를 제공하는 클래스입니다.
 *
 * @classDescription
 * @type {Object}
 */
var IosNativeInterface = (function() {
    var InterfaceName = "ios";
	/**
	 *  NativeInterface initialize.
	 */
	function initialize(){
		console.log("IosNativeInterface >> initialize called");
    };
    
    function getInterfaceName(){
        return InterfaceName;
    }

    function onGameStart(level)
    {
        console.log("IosNativeInterface >> onGameStart :: level = " + level);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onGameStart) {
            window['webkit'].messageHandlers['onGameStart'].postMessage(String(level));
        }
    }

    function onGameEnd(score)
    {
        console.log("IosNativeInterface >> onGameEnd :: score = " + score);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onGameEnd) {
            window['webkit'].messageHandlers['onGameEnd'].postMessage(String(score));
        }
    }

    function onNextGame(result)
    {
        console.log("IosNativeInterface >> onNextGame :: result = " + result);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onNextGame) {
            window['webkit'].messageHandlers['onNextGame'].postMessage(String(result));
        }
    }

    function onStart(data)
    {
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onStart) {
            window['webkit'].messageHandlers.onStart.postMessage(String(data));
        }else{
            window.onStart();
        }
    }
	//////////////////////////////////////////////////////////////////////////////////////////////////
    function onkeydown(val){
        //console.log("IosNativeInterface >> keyDown val :::"+ val.keyCode);
        if (val.keyCode >= 48 && val.keyCode <= 57){
            //0 ~ 9
            window.callJS(val.keyCode - 48);
        }else if (val.keyCode == 189){
            // -
            window.callJS(10);
        }
    }

	// Native -> UI 함수 호출 ///////////////////////////////////////////////////////////////////////////
	function OnNativeEvent(command, data, data1){
		//console.log("iOSNativeInterface   ++++++++++++++++++++++     command = " + command + ", data = " + data);

        var result;
        switch (command)
        {
            case "callJS":
                if (Number(data) < 0){
                    data = 0;
                    // 2019.11.13 Siam 모드일 경우에는 정상적으로 처리되기 때문에 callJS를 무시하도록 변경함.
                    return;
                }
                PlayGameScene.prototype.setScore(Number(data));
                break;
            case "callLevelJS":
                // IntroScene.prototype.LEVEL = data;
                // PlayGameScene.prototype.LEVEL =  Number(data);
                break;
            case "onStart":
                if(typeof data == 'string'){
                    onSetGameConfig((data));
                }else{
                    onSetGameConfig(JSON.stringify(data));
                }

                if (currentScene == GAME_SCENE.INTRO_SCENE){
                    // PlayGameScene.prototype.onStartAudio(Number(data), Number(data1));
                    IntroScene.prototype.onStartAudio();
                }else if (currentScene == GAME_SCENE.PLAY_SCENE){
                    //PlayGameScene.prototype.onStartAudio(data);
                }
                break;
            case "soundBackground":
                PlayGameScene.prototype.setSoundBackground(data);
                break;                
        }
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////

	/* Public API */
	return {
		Initialize      : initialize,
        onGameStart     : onGameStart,
        onGameEnd       : onGameEnd,
        onNextGame      : onNextGame,
        onStart         : onStart,
        onkeydown       : onkeydown,
        OnNativeEvent   : OnNativeEvent,
        getInterfaceName : getInterfaceName
	};
})();
