/**
 * NativeAppInterface는 naitvie 기능 호출에 필요한 interface를 제공하는 클래스입니다.
 *
 * @classDescription
 * @type {Object}
 */
var NativeAppInterface = (function() {

	/* Private API */
	var nativeInterface = null;	

	/* Private propertry */
	var isIos = null;
	var isAndroid = null;

	/**
	 *  NativeInterface 초기화 작업
	 */
	function initialize(){
		var ua = navigator.userAgent.toLowerCase();
		isIos = /iphone|ipad/.test(ua);
		isAndroid = /android/.test(ua);
		
		if(nativeInterface != null){

			return;
		}

		if (isIos){
			nativeInterface = IosNativeInterface;
		}else if (isAndroid){
			nativeInterface = AndroidNativeInterface;
		}else{	// android, ios를 제외한 Browser
			nativeInterface = BrowserInterface;
		}

		if(nativeInterface != null)
		{
			nativeInterface.Initialize();
		}
	};

	// Native Interface 함수 호출 ///////////////////////////////////////////////////////////////////////
	function getInstance(){
		if(nativeInterface == null)
		{
			initialize();
		}

		return nativeInterface;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////

	/* Public API */
	return {
		Initialize : initialize,
		getInstance	: getInstance,
	};

})();

window.callJS = function(data) {
	// console.log("callJS ==> "+data);
	nativeInterface.OnNativeEvent("callJS", data);
};

window.callLevelJS = function(data) {
	// console.log("callLevelJS ==> "+data);
	nativeInterface.OnNativeEvent("callLevelJS", data);
};

window.onStart = function(config, playTime) {
	nativeInterface.OnNativeEvent("onStart", config, playTime);
};

window.soundBackground = function(data){
	// data = "on" or "off"
	nativeInterface.OnNativeEvent("soundBackground", data);
};
