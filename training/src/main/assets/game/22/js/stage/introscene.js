﻿// Phaser3 example game
// tutorial scene

var IntroScene = new Phaser.Class(function(){

    var STATE = {
        INIT_STATE : 0,
        READY_STATE : 1
    };
    var gameState = 0;
    var introMusic = null;

    // Scene 전역 스코프 변수
    var _introgameScene       = null;
    var _timer     = null;

    return {
        Extends: Phaser.Scene,

        initialize:

        function IntroScene ()
        {
            Phaser.Scene.call(this, { key: 'intro_scene' });
        },
        preload: function ()
        {
            currentScene = GAME_SCENE.INTRO_SCENE;
            gameState = STATE.INIT_STATE;

            // 게임 로딩 바
            var progressBar = this.add.graphics();
            var progressBox = this.add.graphics();
            var percentText = this.make.text({
                x: window.innerWidth/2,
                y:window.innerHeight/2+25,
                style: {
                    font : '18px monospace',
                    fill : '#ffffff'
                }
            });
            percentText.setOrigin(0.5);
            progressBox.fillStyle(0x222222, 0.8);
            progressBox.fillRoundedRect(window.innerWidth/2 - 150, window.innerHeight/2, 300, 50, 5);


            // image
            if (_gLang == "ko"){
                if (this.game.config.width > this.game.config.height){
                    this.load.image('intro_bg', 'assets/images/intro_ls_bg_kr.png');
                }else{
                    this.load.image('intro_bg', 'assets/images/intro_bg_kr.png');
                }
            }else{
                if (this.game.config.width > this.game.config.height){
                    // 가로 이미지
                    this.load.image('intro_bg', 'assets/images/intro_ls_bg_en.png');
                }else{
                    // 세로 이미지
                    this.load.image('intro_bg', 'assets/images/intro_bg_en.png');
                }
            }

            this.load.image('intro_text_02', 'assets/images/ic_touch_text.png');
            if (debugMode){
                console.log("IntroScene >> called preload");
            }

            /****** 게임 중인 화면 *****/
            // 배경 이미지
            this.load.image('playing_bg', 'assets/images/arm_wrestling_bg.png');
            this.load.image('playing_bg_gallery_01', 'assets/images/gallery_01.png');
            this.load.image('playing_bg_gallery_02', 'assets/images/gallery_02.png');
            this.load.image('playing_bg_ring', 'assets/images/arm_wrestling_bg2.png');

            // 캐릭터 얼굴
            this.load.image('character_01', 'assets/images/character_01.png');
            this.load.image('character_01_ing', 'assets/images/character_01_ing.png');
            this.load.image('character_01_lose', 'assets/images/character_01_lose.png');
            this.load.image('character_01_win', 'assets/images/character_01_win.png');
            this.load.image('character_02', 'assets/images/character_02.png');
            this.load.image('character_02_ing', 'assets/images/character_02_ing.png');
            this.load.image('character_02_lose', 'assets/images/character_02_lose.png');
            this.load.image('character_02_win', 'assets/images/character_02_win.png');
            this.load.image('character_03', 'assets/images/character_03.png');
            this.load.image('character_03_ing', 'assets/images/character_03_ing.png');
            this.load.image('character_03_lose', 'assets/images/character_03_lose.png');
            this.load.image('character_03_win', 'assets/images/character_03_win.png');
            this.load.image('character_04', 'assets/images/character_04.png');
            this.load.image('character_04_ing', 'assets/images/character_04_ing.png');
            this.load.image('character_04_lose', 'assets/images/character_04_lose.png');
            this.load.image('character_04_win', 'assets/images/character_04_win.png');
            this.load.image('character_05', 'assets/images/character_05.png');
            this.load.image('character_05_ing', 'assets/images/character_05_ing.png');
            this.load.image('character_05_lose', 'assets/images/character_05_lose.png');
            this.load.image('character_05_win', 'assets/images/character_05_win.png');
            // 캐릭터 팔
            this.load.image('arm_center', 'assets/images/arm_center.png');
            this.load.image('arm_left_01', 'assets/images/arm_left_01.png');
            this.load.image('arm_left_02', 'assets/images/arm_left_02.png');
            this.load.image('arm_left_03', 'assets/images/arm_left_03.png');
            this.load.image('arm_left_04', 'assets/images/arm_left_04.png');
            this.load.image('arm_left_05', 'assets/images/arm_left_05.png');
            this.load.image('arm_left_06', 'assets/images/arm_left_06.png');
            this.load.image('arm_left_07', 'assets/images/arm_left_07.png');
            this.load.image('arm_left_08', 'assets/images/arm_left_08.png');
            this.load.image('arm_left_09', 'assets/images/arm_left_09.png');
            this.load.image('arm_left_10', 'assets/images/arm_left_10.png');
            this.load.image('arm_left_11', 'assets/images/arm_left_11.png');
            this.load.image('arm_right_01', 'assets/images/arm_right_01.png');
            this.load.image('arm_right_02', 'assets/images/arm_right_02.png');
            this.load.image('arm_right_03', 'assets/images/arm_right_03.png');
            this.load.image('arm_right_04', 'assets/images/arm_right_04.png');
            this.load.image('arm_right_05', 'assets/images/arm_right_05.png');
            this.load.image('arm_right_06', 'assets/images/arm_right_06.png');
            this.load.image('arm_right_07', 'assets/images/arm_right_07.png');
            this.load.image('arm_right_08', 'assets/images/arm_right_08.png');
            this.load.image('arm_right_09', 'assets/images/arm_right_09.png');
            this.load.image('arm_right_10', 'assets/images/arm_right_10.png');
            this.load.image('arm_right_11', 'assets/images/arm_right_11.png');

            // 게임 요소
            this.load.image('ground_play_alarm', 'assets/images/clock_empty.png');
            this.load.image('ground_gage_empty', 'assets/images/gage_empty.png');
            this.load.image('ground_score_board', 'assets/images/electric-sign.png');

            // 게임 결과 화면
            this.load.image('result_ic_result_popup_bg', 'assets/images/game_popup_bg.png');
            this.load.image('result_ic_scoretext_bad', 'assets/images/popup_result_text_01.png');
            this.load.image('result_ic_scoretext_notgood', 'assets/images/popup_result_text_02.png');
            this.load.image('result_ic_scoretext_good', 'assets/images/popup_result_text_03.png');
            this.load.image('result_ic_scoretext_verygood', 'assets/images/popup_result_text_04.png');
            this.load.image('result_ic_scoretext_excellent', 'assets/images/popup_result_text_05.png');
            this.load.image('result_ic_popup_arrow', 'assets/images/popup_arrow.png');
            if (_gLang == "ko"){
                this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor_kr.png');
                this.load.image('result_btn_resultview_sel', 'assets/images/btn_result_01_sel_kr.png');
                this.load.image('result_btn_nextgame_nor', 'assets/images/btn_result_popup_next_nor_kr.png');
                this.load.image('result_btn_exit_nor', 'assets/images/btn_result_popup_exit_nor_kr.png');
            }else {
                this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor_en.png');
                this.load.image('result_btn_resultview_sel', 'assets/images/btn_result_01_sel_en.png');
                this.load.image('result_btn_nextgame_nor', 'assets/images/btn_result_popup_next_nor_en.png');
                this.load.image('result_btn_exit_nor', 'assets/images/btn_result_popup_exit_nor_en.png');
            }

            // audio
            this.load.audio('audio_ready_start', ['assets/audio/ready_start.mp3']);
            this.load.audio('audio_play_bgm', ['assets/audio/Game_play_BGM.mp3']);
            this.load.audio('audio_game_win', ['assets/audio/game_clear_good(Result).mp3']);
            this.load.audio('audio_game_lose', ['assets/audio/game_fail_bad(Result).mp3']);
            // this.load.audio('audio_arm_wrestling', ['assets/audio/arm_wrestling.wav']);
            this.load.audio('audio_cheerRing', ['assets/audio/cheerRing.mp3']);
            this.load.audio('audio_winning', ['assets/audio/winning.mp3']);
            this.load.audio('audio_losing', ['assets/audio/losing.mp3']);

            this.load.on('progress', function(value){
                progressBar.clear();
                progressBar.fillStyle(0xffffff,1);
                progressBar.fillRoundedRect(window.innerWidth/2+10-150, window.innerHeight/2+10, 280*value, 30, 5);
                percentText.setText("Loading "+Math.round(value*100) + "%");
            });

            this.load.on('complete', function(){
                // progressBox.destory();
                // progressBar.destory();
            });
        },

        create: function ()
        {
            var width = this.game.config.width;
            var height = this.game.config.height;

            _introgameScene = this;

            var logoSkip = _introgameScene.logoSkip('intro');
            if(logoSkip == true){
                return;
            }

            // set background image
            var bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'intro_bg');
            bg.displayWidth = width; //scale.setTo(0.5, 0.5);
            bg.displayHeight = this.game.config.height;

            // text
            this.pressTextImg = this.add.image(this.game.config.width/2, height - height/10, 'intro_text_02').setDisplaySize(width/2, width/2 * 0.11);

            this.pointer = this.input.on('pointerup', this.doNextState);

            if (isIos == false && isAndroid == false){
                nativeInterface.onStart(3, 60);
            }

            if (_logo_auto_skip == "Y"){
                _timer = _introgameScene.time.delayedCall(_logo_skip_wait_time, function(){
                    _introgameScene.doNextState();
                }, null, _introgameScene);  // delay in ms
            }

            if (_gIsPlayingAudioByPhaser){
                introMusic  = _introgameScene.sound.add('audio_play_bgm');
                introMusic.play({loop:true, volume:0.5});
            }

        },
        update: function()
        {
            // console.log("called update : time = " + ((this.time.now / 1000) % 2));
            if (((this.time.now / 1000) % 1) >= 0.5){
                this.pressTextImg.setAlpha(0);
            }else{
                this.pressTextImg.setAlpha(1);
            }
        },
        onStartAudio: function(mode, playTime)
        {
            // Scene 진입시에 play할 audio 재생
            if (debugMode){
                console.log("Intro >> onStart :: mode = "  + mode);
            }

            // 시작 음악 재생
            // if (_gIsPlayingAudioByPhaser == false && isIos == false){
            if (_gIsPlayingAudioByPhaser == false && _logo_skip=="N" ){
                introMusic = new Audio('assets/audio/Game_Intro_BGM.mp3');
                introMusic.autoplay = true;
                introMusic.loop = true;
            }
        },
        doNextState: function ()
        {
            if (debugMode){
                console.log('doNextState was called! ==>' + STATE.INIT_STATE);
                console.log("gameState = " + gameState + ", _gGameMode = " + _gGameMode);
            }
            if (_gGameMode == null){
                if (debugMode){
                    console.log('Need to call window.onStart function');
                }
                return;
            }
            if (gameState == STATE.INIT_STATE){
                gameState = STATE.READY_STATE;

                if (_gIsPlayingAudioByPhaser == false){
                    if (introMusic.currentTime > 0){
                        introMusic.pause();
                        introMusic.currentTime = 0;
                    }
                }else{
                    if (introMusic.isPlaying == true){  // Phaser audio
                        introMusic.pause();
                        introMusic.currentTime = 0;
                    }
                }
                if(_timer != null){
                    _timer.remove();    // 5초 timer 제거
                    _timer = null;
                }
                _introgameScene.scene.start('gameplayscene');
            }
        },
        /** onStart 이벤트를 받아 logo skip 처리 한다 **/
        logoSkip : function(str){
            //console.log('logoSkip : '+str+" _logo_skip : "+ _logo_skip);
            if(_introgameScene && _logo_skip == 'Y'){
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }

                _introgameScene.scene.scene.input.removeAllListeners();
                _introgameScene.scene.start('gameplayscene');
                return true;
            }
            return false;
        }
    };
}());