// main game scene

var PlayGameScene = new Phaser.Class(function(){
    // PlayGame 상태값
    var STATE = {
        STATE_READY : 0,
        STATE_PLAY_GAME : 1,
        STATE_DISPLAY_RESULT : 2,
        STATE_END_GAME : 3
    };

    // 결과 화면에서 선택된 버튼의 상태를 위한 변수
    var GAME_BTN_STATE = {
        NONE : 0,
        RESTART : 1,
        EXIT : 2
    };

    // 팔씨름 각도를 처리하기 위한 상태 변수
    var ARM_STATE = {
        WIN : 6,
        MIDDLE_LEFT_LEFT : 5,
        MIDDLE_LEFT : 4,
        MIDDLE : 3,
        MIDDLE_RIGHT : 2,
        MIDDLE_RIGHT_RIGHT : 1,
        LOSE : 0
    };

    // 팔씨름 힘주는 효과음 상태값
    var SOUND_WRESLING_STATE = {
        PLAY : 1,
        STOP : 2
    };
    var __CHECK_TIME__  = 180;  // 180초 측정
    var _SCORE       = 0;//ARM_STATE.MIDDLE;    // 브레인 측정 점수
    var _prevScore   = 0;//ARM_STATE.MIDDLE;    // 직전 측정 점수
    var _isPlayingAnim = true;
    var _WIN_COUNT   = 0;    // 게임 승 횟수
    var _LOSE_COUNT  = 0;    // 게임 패 횟수
    var _TOTAL_SCORE = 0;   // 게임 점수
    var _gameResult = 0;    // native에 onNextGame 함수 호출시에 전달한 파라메타 (결과값) // 1: bad 2 : not good 3 : good 4 : very good 5 : excellent
    var _percentage_score = 0; // 배점 계산 (비율 계산)
    var _prevMatch   = "first"; // 이전 게임의 승패 first / win / lose
    // 캐릭터 별로 3회 이상 출현 제한이 있기 때문에 아래와 같이 각 캐릭터별로 3회만 입력하고 순서를 랜덤으로 썩음.
    var _orderOfCharater = ['0','0','0','1','1','1','2','2','2','3','3','3','4','4','4','0','0','0','1','1','1','2','2','2','3','3','3','4','4','4'];
    // _orderOfCharater 의 값에서 순차적으로 가져오기 위한 index 값.
    var _indexOfCharactorOrder = 0;
    var _isFinished = false;
    var _timer       = null;

    // Scene 전역 스코프 변수
    var _playgameScene       = null;

    // 오디오 저장 변수
    var audio_play_bgm      = null; // Phaser -JS- Audio
    var audio_ready_start   = null; // Phaser -JS- Audio
    var audio_game_win      = null; // Phaser audio
    var audio_game_lose     = null; // Phaser audio
    // var audio_arm_wrestling = null; // Phaser audio
    var audio_cheerRing     = null; // Phaser audio
    var audio_arm_winning   = null;
    var audio_arm_losing    = null;

    // Scene 전체의 넓이, 높이 변수
    var _width           = 0;
    var _height          = 0;
    var _screenWidth     = 0;

    // 현재 게임 Scene의 상태값 변수
    var _gameState = STATE.STATE_READY;
    // GageBar 관련 변수
    var _gageBarWidth    = null;
    var __INTERVAL_GAGE__  = 2.8;
    var __MAX_SCORE_WIDTH__   = __INTERVAL_GAGE__ * 10 * 10;
    var __MIN_SCORE_WIDTH__   = 0;
    var _upGage          = false;

    // 캐릭터 저장 변수
    var _character = new Array(5);
    var _characterState = ARM_STATE.MIDDLE;
    var _curentCharacterState = ARM_STATE.MIDDLE;

    // 승/패 점수 저장 변수
    var _contLoseScore = 0;
    var _contWinScore = 0;

    // 결과 화면에서 버튼 이벤트 처리를 위한 변수
    var _isGameAgain     = GAME_BTN_STATE.NONE;
    // 새로운 도전자와 게임 시작하는지 아닌지 체크하기 위한 변수
    var _isNewChallenge = true;

    // 샴 데이터를 처리하기 위한 배열
    var _aSiamData = [];
    var _strSiamRatio = "";

    // Toast Message
    var _messageEn = "Focus on knocking down your opponent's arm.";
    var _messageKr = "상대방의 팔을 넘기는데 집중하세요.";

    return{
        Extends: Phaser.Scene,
        initialize:

        function PlayGameScene ()
        {
            Phaser.Scene.call(this, { key: 'gameplayscene' });
        },
        preload: function ()
        {
            // 현재 게임 Scene 설정 (외부에서 판단하기 위해서)
            currentScene = GAME_SCENE.PLAY_SCENE;
            // 게임 플레이 시간 설정
            if (!isNaN(_gGamePlayTime)){
                __CHECK_TIME__ = _gGamePlayTime;
            }
            this.checkTime = __CHECK_TIME__;

            // 샴 게임에 대한 데이터 생성
            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                _aSiamData = this.setSham();
            }

            // if (isAndroid){
            //     audio_ready_start = new Audio('assets/audio/ready_start.wav');
            //     audio_ready_start.autoplay = true;
            //     audio_ready_start.loop = false;
            // }
            if (debugMode) {console.log("PlayGameScene >> called preload");}
        },

        create: function ()
        {
            _playgameScene = this;  // 해당 Scene 내의 전역변수 설정
            _width = this.game.config.width; // 게임 화면 넓이
            _screenWidth = this.game.config.width; // 화면 전체 넓이
            _height = this.game.config.height;   // 화면 전체 높이

            if (_width > 600){
                _width = 600;
            }

            if (debugMode) {
                console.log('create is ready in PlayGameScene');
                console.log('_width = ' + _width + ", _height = " + _height);
                console.log('_screenWidth = ' + _screenWidth);
            }

            /***************************** 게임 플레이 ***************************/
            // 배경 이미지
            this.add.image(_screenWidth/2, _height/2, 'playing_bg').setDisplaySize(_screenWidth, _height);
            this.anims.create({
                key: 'gallery',
                frames: [
                    { key: 'playing_bg_gallery_01' },
                    { key: 'playing_bg_gallery_02'}
                ],
                frameRate: 1,
                repeat: -1
            });
            this.anim_gallery = this.add.sprite(_screenWidth/2, _height - _width * 0.5, 'playing_bg_gallery_01').setDisplaySize(_screenWidth, _width * 0.9).play('gallery');
            this.bg_ring_image = this.add.image(_screenWidth/2, _height, 'playing_bg_ring').setDisplaySize(_screenWidth, _width * 0.75).setOrigin(0.5, 1);

            // 캐릭터 생성
            this.createCharactor(_playgameScene);
            // 캐릭터 팔 생성
            this.createCharaterArmAnim(_playgameScene);

            this.playGroup = this.add.group();

            var scoreboard_height = (_width * this.game.textures.get("ground_score_board").getSourceImage().height) / this.game.textures.get("ground_score_board").getSourceImage().width;
            this.scoreBoard = this.add.image(_screenWidth/2, 0, 'ground_score_board').setDisplaySize(_width, scoreboard_height).setOrigin(0.5, 0);

            if (isDisplaySiamRatio && (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END)) {
                this.siamRatioText = this.add.text(_screenWidth/2  , this.scoreBoard.displayHeight , _strSiamRatio, { font: "bold 15px Arial", fill: "#cde6ed", boundsAlignH: "center", boundsAlignV: "middle" }).setOrigin(0.5, 0.5);
            }

            // Score / win / lose 텍스트 (text)
            var textStyleOfTotalScore = null;
            if (_width < 600){
                textStyleOfTotalScore = { font: "bold 30px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfTotalScore = { font: "bold 40px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.scoreText = this.add.text(this.scoreBoard.x - this.scoreBoard.displayWidth * 0.2  , this.scoreBoard.displayHeight * 0.68 , _TOTAL_SCORE, textStyleOfTotalScore).setOrigin(0.5, 0.5);

            var textStyleOfScore = null;
            if (_width < 600){
                textStyleOfScore = { font: "bold 22px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfScore = { font: "bold 33px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.winText = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.23 , this.scoreBoard.displayHeight * 0.68 , _WIN_COUNT, textStyleOfScore).setOrigin(0.5, 0.5);
            this.loseText = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.36 , this.scoreBoard.displayHeight * 0.68 , _LOSE_COUNT, textStyleOfScore).setOrigin(0.5, 0.5);

            // 연승시 추가 점수 표시 텍스트 (text)
            var textStyleOfBonusScore = null;
            if (_width < 600){
                textStyleOfBonusScore = { font: "bold 55px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfBonusScore = { font: "bold 75px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.bonusScoreText = this.add.text(_playgameScene.character_01.x - _playgameScene.character_01.displayWidth/3 , _playgameScene.character_01.y - _playgameScene.character_01.displayHeight/2 - 50 , "+ 5", textStyleOfBonusScore).setOrigin(0.5, 0.5);
            this.tweenBonusScore = this.tweens.add({
                targets : this.bonusScoreText,
                alpha: { from: 0, to: 1 },
                x : _playgameScene.character_01.x - _playgameScene.character_01.displayWidth/3,
                y : _playgameScene.character_01.y - _playgameScene.character_01.displayHeight - 50,
                duration : 1000,
                delay : 0,
                ease : 'Back.Out',
                repeat : 0,
                onComplete: function () {
                    _playgameScene.bonusScoreText.visible = false;
                }
            });
            this.bonusScoreText.visible = false;

            var playAlarm = this.add.image(_screenWidth * 0.8, _height * 0.2, 'ground_play_alarm').setDisplaySize(_width * 0.2, _width * 0.2);//.setDisplaySize(100,100);

            this.gageBarBG = this.add.graphics();
            this.gageBar = this.add.graphics();
            var gage_width = 340;
            var gage_height = (gage_width * this.game.textures.get("ground_gage_empty").getSourceImage().height) / this.game.textures.get("ground_gage_empty").getSourceImage().width;
            this.mentalGage = this.add.image(_screenWidth/2, _height * 0.93, 'ground_gage_empty').setDisplaySize(gage_width, gage_height);

            // Touch the screen 텍스트 (text)
            var textStyleOfAlarm = null;
            if (_width < 600){
                textStyleOfAlarm = { font: "bold 20px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfAlarm = { font: "bold 30px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.alarmText = this.add.text(playAlarm.x , playAlarm.y , this.checkTime, textStyleOfAlarm).setOrigin(0.5);
            // this.alarmText.x = this.alarmText.x - (this.alarmText.width);

            // Native에서 오는 텍스트 출력을 위한 변수
            // var textStyleOfScore = { font: "bold 20px Arial", fill: "#FFF", boundsAlignH: "center", boundsAlignV: "middle" };
            if (debugMode){
                this.scoreDisplayText = this.add.text(_width/2, 20, "callJS : " + _SCORE, textStyleOfScore).setOrigin(0.5);
                this.playGroup.add(this.scoreDisplayText);
            }
            this.updateGage();

            this.playGroup.add(playAlarm);
            this.playGroup.add(this.alarmText);
            this.playGroup.add(this.mentalGage);
            this.playGroup.add(this.gageBar);
            this.playGroup.add(this.gageBarBG);

            // this.playGroup.toggleVisible(); // 기본 숨기기
            /********************************************************************/
            /*****************************  게임 결과  ***************************/
            this.resultGroup = this.add.group();

            this.result_popup_bg = this.add.image(_screenWidth/2, _height/2, 'result_ic_result_popup_bg').setDisplaySize(_width * 0.75, _width * 0.75 * 1.62);

            var yResultBtn = this.result_popup_bg.displayHeight/2 + _height/2;

            this.result_popup_view_result_sel = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_sel').setDisplaySize(_width/2, _width/2 * 0.32);
            // 결과 버튼 위치 조정
            if (yResultBtn + this.result_popup_view_result_sel.displayHeight > _height){
                yResultBtn = _height - this.result_popup_view_result_sel.displayHeight;
            }
            this.result_popup_view_result_sel.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_sel.setInteractive();


            this.result_popup_view_result_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_view_result_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_nor.setInteractive();
            this.result_popup_next_game_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_nextgame_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_next_game_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_next_game_nor.setInteractive();
            this.result_popup_exit_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_exit_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_exit_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_exit_nor.setInteractive();


            // scale이 최대 넓이 600px일 경우에 0.3
            // 각 해상도에 따라서 최대 넓이 기준으로 비율 조정
            var scaleOfResultText = 0.3 * _width/600;
            this.result_popup_text_bad = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_bad').setScale(scaleOfResultText);
            this.result_popup_text_notgood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_notgood').setScale(scaleOfResultText);
            this.result_popup_text_good = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_good').setScale(scaleOfResultText);
            this.result_popup_text_verygood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_verygood').setScale(scaleOfResultText);
            this.result_popup_text_excellent = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_excellent').setScale(scaleOfResultText);

            // 위치는 결과 텍스트를 기존으로 밑으로 붙임.
            var textStyleOfResultScore = null;
            if (_width < 600){
                textStyleOfResultScore = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
            }else {
                textStyleOfResultScore = { font: "bold 35px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.scoreTextOfResult = this.add.text(_screenWidth/2 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , _TOTAL_SCORE, textStyleOfResultScore).setOrigin(0.5, 1);
            this.scoreTextOfResult.setStroke('#000', 5);

            // 높이의 기준은 _width 로 하며, 해당 넓이에 따라서 비율로 처리
            var scaleOfResultArrow = 0.38 * _width/600;
            this.result_popup_arrow = this.add.image(_screenWidth/2 - this.result_popup_bg.displayWidth/3, _height/2  + _width * 0.25 , 'result_ic_popup_arrow').setScale(scaleOfResultArrow);

            // 결과 팝업에서 보여주는 점수 텍스트
            var textStyleOfResultStandardScore = null;
            if (_width < 600){
                textStyleOfResultStandardScore = { font: "bold 13px Arial", fill: "#000", align: "right", boundsAlignH: "right", boundsAlignV: "middle"};
            }else {
                textStyleOfResultStandardScore = { font: "bold 20px Arial", fill: "#000", align: "right"/*, boundsAlignH: "right", boundsAlignV: "middle"*/ };
            }
            this.scoreTextOfResult1 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "0 ~ 20", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult2 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "21 ~ 40", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult3 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "41 ~ 60", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult4 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "61 ~ 80", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult5 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , "81 ~ 100", textStyleOfResultStandardScore).setOrigin(1, 0.5);


            this.resultGroup.add(this.result_popup_bg);
            this.resultGroup.add(this.result_popup_view_result_nor);
            this.resultGroup.add(this.result_popup_view_result_sel);
            this.resultGroup.add(this.result_popup_next_game_nor);
            this.resultGroup.add(this.result_popup_exit_nor);
            this.resultGroup.add(this.result_popup_text_bad);
            this.resultGroup.add(this.result_popup_text_notgood);
            this.resultGroup.add(this.result_popup_text_good);
            this.resultGroup.add(this.result_popup_text_verygood);
            this.resultGroup.add(this.result_popup_text_excellent);
            this.resultGroup.add(this.scoreTextOfResult);
            this.resultGroup.add(this.result_popup_arrow);
            this.resultGroup.add(this.scoreTextOfResult1);
            this.resultGroup.add(this.scoreTextOfResult2);
            this.resultGroup.add(this.scoreTextOfResult3);
            this.resultGroup.add(this.scoreTextOfResult4);
            this.resultGroup.add(this.scoreTextOfResult5);

            this.resultGroup.toggleVisible(); // 기본 숨기기
            if (_gGameMode == GAME_MODE.NORMAR_END || _gGameMode == GAME_MODE.SIAM_END){
                this.result_popup_view_result_nor.visible = false;
            }else{
                this.result_popup_view_result_sel.visible = false;
            }

            // Event
            this.result_popup_view_result_nor.on('pointerdown', function(porinter, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = true;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = false;
            });
            this.result_popup_view_result_sel.on('pointerup', function(porinter, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = true;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = false;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });
            this.result_popup_next_game_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = true;
                _playgameScene.result_popup_exit_nor.visible = false;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });
            this.result_popup_exit_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_next_game_nor.visible = false;
                _playgameScene.result_popup_exit_nor.visible = true;
                _isGameAgain = GAME_BTN_STATE.EXIT;
                _playgameScene.doNextState();
            });
            // Audio
            if (_gIsPlayingAudioByPhaser){
                audio_play_bgm  = this.sound.add('audio_play_bgm');
                audio_game_win  = this.sound.add('audio_game_win');
                audio_game_lose = this.sound.add('audio_game_lose');
                audio_cheerRing = this.sound.add('audio_cheerRing');
                audio_arm_winning   = this.sound.add('audio_winning');
                audio_arm_losing    = this.sound.add('audio_losing');
            }else{
                audio_play_bgm = new Audio('assets/audio/Game_play_BGM.mp3');
                audio_game_win  = new Audio('assets/audio/game_clear_good(Result).mp3');
                audio_game_lose = new Audio('assets/audio/game_fail_bad(Result).mp3');
                audio_cheerRing = new Audio('assets/audio/cheerRing.mp3');
                audio_arm_winning   = new Audio('assets/audio/winning.mp3');
                audio_arm_losing    = new Audio('assets/audio/losing.mp3');
            }
            /********************************************************************/
            /********************************************************************/
            // 스타트 오디오 시작
            if (_gIsPlayingAudioByPhaser == true)
            {
                audio_ready_start = this.sound.add('audio_ready_start');
                audio_ready_start.play();

                /****************** 반드시 호출 필요 ******************/
                nativeInterface.onGameStart(_gGameDifficultyStep);
                /****************** 반드시 호출 필요 ******************/

                // 시작 오디오 play시에 처리
                // 1. native에 onGameStart 함수 호출
                // 2. siam 개임인 경우, siam 데이터 생성
                audio_ready_start.once('play', function(){
                    if (debugMode){console.log('Phaser :: audio_ready_start start to play!!!');}
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.
                    // /****************** 반드시 호출 필요 ******************/
                    // nativeInterface.onGameStart(_gGameDifficultyStep);
                    // /****************** 반드시 호출 필요 ******************/
                });

                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.on('complete', function(){
                // audio_ready_start.addEventListener("ended", function(){
                    _playgameScene.showToast();
                    audio_ready_start.currentTime = 0;
                    _playgameScene.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = _playgameScene.time.addEvent({
                        delay: 1000,                // ms
                        callback: _playgameScene.updateTimer,   // 측정 타이머 함수
                        //args: [],
                        callbackScope: _playgameScene.scene,
                        repeat: _playgameScene.checkTime
                    });
                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    _gameState = STATE.STATE_PLAY_GAME;

                    audio_play_bgm.play({loop:true, volume:0.5});
                    _playgameScene.doNextState();
                });
            }
            else{
                audio_ready_start = new Audio('assets/audio/ready_start.mp3');
                audio_ready_start.autoplay = true;
                audio_ready_start.loop = false;
                // 시작 오디오 play시에 처리
                // 1. native에 onGameStart 함수 호출
                // 2. siam 개임인 경우, siam 데이터 생성
                audio_ready_start.addEventListener('play', function(){
                    if (debugMode) {console.log('Android :: audio_ready_start start to play!!!');}
                    // native에 측정 시작을 알림. callJS로 측정 점수를 올리기 시작함.
                    /****************** 반드시 호출 필요 ******************/
                    nativeInterface.onGameStart(_gGameDifficultyStep);
                    /****************** 반드시 호출 필요 ******************/
                });

                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.addEventListener("ended", function(){
                    _playgameScene.showToast();
                    audio_ready_start.currentTime = 0;
                    _playgameScene.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = _playgameScene.time.addEvent({
                        delay: 1000,                // ms
                        callback: _playgameScene.updateTimer,   // 측정 타이머 함수
                        callbackScope: _playgameScene.scene,
                        repeat: _playgameScene.checkTime
                    });
                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    _gameState = STATE.STATE_PLAY_GAME;

                    audio_play_bgm.play();
                    audio_play_bgm.loop = true;
                    _playgameScene.doNextState();
                });
            }
        },
        onStartAudio: function()
        {
            // nothing...
        },
        update: function ()
        {
            if (_gameState == STATE.STATE_PLAY_GAME){
                /* 게임 진행 화면 상태 */
                if (this.checkTime < 0 || _isFinished){
                    this.doNextState();
                }else{
                    // 캐릭터 상태 표시
                    this.updateCharater();
                    // GageBar 업데이트
                    this.updateGage();
                }

                if (this.checkTime < 0){ // 게임의 마지막 0초까지 맞아야할거 같아서 수정함.
                    this.checkTime = 0;
                }
                this.alarmText.setText(this.checkTime);
                if (debugMode){
                    this.scoreDisplayText.setText("callJS : " + _SCORE);
                }
                this.scoreText.setText(_TOTAL_SCORE);
                this.winText.setText(_WIN_COUNT);
                this.loseText.setText(_LOSE_COUNT);


            }else if (_gameState == STATE.STATE_DISPLAY_RESULT){
                /* 게임 결과 화면 상태 */
                if (_gIsPlayingAudioByPhaser){
                    if (audio_play_bgm.isPlaying == true){  // Phaser audio
                        audio_play_bgm.pause();
                        audio_play_bgm.currentTime = 0;
                    }
                }else {
                    if (audio_play_bgm.paused == false){ // JS audio
                        audio_play_bgm.pause();
                        audio_play_bgm.currentTime = 0;
                    }
                }
            }else if (_gameState == STATE.STATE_END_GAME){
                /* 게임 종료하는 상태 */

            }

        },
        setScore: function (score)
        {
            if (_gameState != STATE.STATE_PLAY_GAME || _isFinished){
                if (debugMode) {console.log("setScore >> return by _gameState == " + _gameState);}
                return;
            }

            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                var siam_score = _aSiamData.pop();
                if (debugMode) {console.log("setScore >> score = " + score + ", siam score = " + siam_score);}
                score = siam_score;
            }
            _prevScore = _SCORE;
            _SCORE = score;

            if (_prevScore < _SCORE){
                _upGage  = true;
            }else if(_prevScore > _SCORE){
                _upGage  = false;
            }

            if (debugMode) {console.log("setScore >> _prevScore = " + _prevScore + ", _SCORE = " + _SCORE + ", _isPlayingAnim = " + _isPlayingAnim);}

            /**
             * 대전 디자인
                - 이기는 경우와 지는 경우 각각 단계당 30도씩 팔 각도 변화
                - 집중력 0 : (6초 이상) 180도 / (6초미만) 150도
                집중력 1, 2 : 120도
                집중력 3 : 90도
                집중력 4, 5 : 60도
                집중력 6 이상 : (6초 이상) 0도 / (6초 미만) 30도
             */
            if (_SCORE > 0 && _SCORE < 6){
                _contLoseScore = 0;
                _contWinScore = 0;
            }
            switch (_SCORE){
                case 0:
                    _contWinScore = 0;
                    _contLoseScore++;
                    if (_contLoseScore >= 3){
                        _characterState = ARM_STATE.LOSE;
                    }else{
                        _characterState = ARM_STATE.MIDDLE_RIGHT_RIGHT;
                    }
                break;
                case 1: case 2:
                    _characterState = ARM_STATE.MIDDLE_RIGHT;
                break;
                case 3:
                    _characterState = ARM_STATE.MIDDLE;
                break;
                case 4: case 5:
                    _characterState = ARM_STATE.MIDDLE_LEFT;
                break;
                case 6: case 7: case 8: case 9: case 10:
                    _contLoseScore = 0;
                    _contWinScore++;
                    if (_contWinScore >= 3){
                        _characterState = ARM_STATE.WIN;
                    }else{
                        _characterState = ARM_STATE.MIDDLE_LEFT_LEFT;
                    }
                break;
            }

            if (_curentCharacterState != _characterState){
                _isPlayingAnim = false;
            }

            if (debugMode) {console.log("_curentCharacterState = " + _curentCharacterState + ", _characterState = " + _characterState);}
        },
        /** 게임 상태에 따른 이벤트 처리 */
        doNextState: function ()
        {
            if (debugMode) {console.log("_gameState = " + _gameState);}
            if (_gameState == STATE.STATE_READY){

            }else if (_gameState == STATE.STATE_PLAY_GAME){ // 게임 실행 상태
                if (_playgameScene.checkTime <= 0 || _isFinished){
                    _gameState = STATE.STATE_DISPLAY_RESULT; // 게임 State 변경
                    // native에 게임이 끝났음을 알림. 게임이 끝나면 callJS로 데이터를 올려주지 않음.
                    /****************** 반드시 호출 필요 ******************/
                    nativeInterface.onGameEnd( _TOTAL_SCORE );
                    /****************** 반드시 호출 필요 ******************/

                    // 현재 _SCORE 초기화(강제 키이벤트 발생)
                    window.dispatchEvent(new KeyboardEvent('keydown', {keyCode: '48'}));

                    /** onStart 이벤트를 받아 _result_skip 처리 한다 **/
                    if(_result_skip == 'Y'){
                        _playgameScene.setDisplayResultPopup(); // 결과 팝업 내용 처리
                        _isGameAgain = GAME_BTN_STATE.EXIT;
                        _playgameScene.doNextState();
                        return;
                    }

                    // 화면에 보여지는 개체 그룹 조정
                    _playgameScene.playGroup.toggleVisible();
                    _playgameScene.resultGroup.toggleVisible();
                    // 대전 상태 감추기
                    _character[_orderOfCharater[(_indexOfCharactorOrder - 1) < 0?0:(_indexOfCharactorOrder == _orderOfCharater.length)? _indexOfCharactorOrder - 1 : _indexOfCharactorOrder]].win.visible = false;
                    _character[_orderOfCharater[(_indexOfCharactorOrder - 1) < 0?0:(_indexOfCharactorOrder == _orderOfCharater.length)? _indexOfCharactorOrder - 1 : _indexOfCharactorOrder]].ing.visible = false;
                    _character[_orderOfCharater[(_indexOfCharactorOrder - 1) < 0?0:(_indexOfCharactorOrder == _orderOfCharater.length)? _indexOfCharactorOrder - 1 : _indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[(_indexOfCharactorOrder - 1) < 0?0:(_indexOfCharactorOrder == _orderOfCharater.length)? _indexOfCharactorOrder - 1 : _indexOfCharactorOrder]].lose.visible = false;
                    // 팔 감추기
                    _playgameScene.armImgCenter.visible = false;
                    _playgameScene.left2ToWin.visible = false;
                    _playgameScene.left1ToLeft2.visible = false;
                    _playgameScene.centerToLeft.visible = false;
                    _playgameScene.centerToRight.visible = false;
                    _playgameScene.right1ToRight2.visible = false;
                    _playgameScene.right2ToLose.visible = false;

                    // 관객 애니메이션 빠르게
                    _playgameScene.anim_gallery.anims.msPerFrame = 200;

                    // 현재 재생중인 audio를 확인하고, 재생중인 audio를 멈추자.
                    // javascript audio 개체이기 때문에 상태체크는 paused로 체크
                    if (_gIsPlayingAudioByPhaser){
                        if (audio_play_bgm.isPlaying == true){  // Phaser audio
                            audio_play_bgm.pause();
                            audio_play_bgm.currentTime = 0;
                        }
                    }else {
                        if (audio_play_bgm.paused == false){ // JS audio
                        audio_play_bgm.pause();
                        audio_play_bgm.currentTime = 0;
                    }
                    }
                    _playgameScene.checkTime = __CHECK_TIME__;  // Timer 초기화

                    // 결과 화면 설정
                    if (_TOTAL_SCORE <= 110){
                        audio_game_lose.play();  // lose audio play
                    }else{
                        audio_game_win.play();  // win audio play
                    }
                    _playgameScene.setDisplayResultPopup(); // 결과 팝업 내용 처리

                    /**
                     * onStart 이벤트를 받는다
                     * '_result_auto_skip' 이 'Y'인 경우
                     * '_result_skip_wait_time' 후에 강제 종료 타이머 처리
                     */
                    // 사용자 입력 5초 동안 없을시에, 강제 종료 타이머 처리
                    if(_result_auto_skip == 'Y'){
                        _timer = _playgameScene.time.delayedCall(_result_skip_wait_time, function(){
                            _isGameAgain = GAME_BTN_STATE.EXIT;
                            _playgameScene.doNextState();
                        }, null, _playgameScene);  // delay in ms
                    }
                }else{
                    // 무시
                }
            }else if (_gameState == STATE.STATE_DISPLAY_RESULT){
                if (_gIsPlayingAudioByPhaser){
                if (audio_game_win.isPlaying){
                    audio_game_win.pause();
                    audio_game_win.currentTime = 0;
                }
                if (audio_game_lose.isPlaying){
                    audio_game_lose.pause();
                    audio_game_lose.currentTime = 0;
                }
                }else{
                    if (audio_game_win.paused == false){
                        audio_game_win.pause();
                        audio_game_win.currentTime = 0;
                    }
                    if (audio_game_lose.paused == false){
                        audio_game_lose.pause();
                        audio_game_lose.currentTime = 0;
                    }
                }

                if (_isGameAgain == GAME_BTN_STATE.RESTART){
                    _gameState = STATE.STATE_READY; // 게임 State 변경
                    _playgameScene.resultGroup.toggleVisible();
                    _playgameScene.playGroup.toggleVisible();
                    _playgameScene.resetData(); // 데이터 초기화
                    _playgameScene.scene.start('gameplayscene');
                }else if (_isGameAgain == GAME_BTN_STATE.EXIT){
                    if (_timer != null){
                        _timer.remove();
                    }
                    var params= '{"result":'+_gameResult+',"percent":'+_percentage_score+'}';
                    // 게임 종료
                    /****************** 반드시 호출 필요 ******************/
                    nativeInterface.onNextGame(params);
                    /****************** 반드시 호출 필요 ******************/
                }
            }
        },
        /* 게이지 출력 함수 */
        updateGage : function()
        {
            var x = _screenWidth/2 - this.mentalGage.displayWidth/2 + 55;
            var y = _height * 0.93;

            this.gageBarBG.clear();
            this.gageBarBG.fillStyle(0x212f39, 1);
            this.gageBarBG.fillRoundedRect(x, y - 12, __MAX_SCORE_WIDTH__, 20, 8);

            this.gageBar.clear();
            this.gageBar.fillStyle(0xffd400, 1);
            if (_upGage && _gageBarWidth < __INTERVAL_GAGE__ * 10 * _SCORE){
                _gageBarWidth += __INTERVAL_GAGE__;
                if (_gageBarWidth >= __MAX_SCORE_WIDTH__){
                    _gageBarWidth = __MAX_SCORE_WIDTH__;
                }
            }else if(_upGage == false && _gageBarWidth >= __INTERVAL_GAGE__ * 10 * _SCORE){
                _gageBarWidth -= __INTERVAL_GAGE__;
                if (_gageBarWidth <= __MIN_SCORE_WIDTH__){
                    _gageBarWidth = 0;
                }
            }
            // #212F39
            // console.log("x=" + x + ", y = " + y + ", _SCORE = " + _SCORE + ", _gageBarWidth = " + _gageBarWidth);
            this.gageBar.fillRoundedRect(x, y - 12, _gageBarWidth, 20, 8);
        },
        updateTimer : function ()
        {
            // console.log("updateTimer : " + this.checkTime + ", _timer.delay = " + _timer.delay);
            if (_gameState == STATE.STATE_PLAY_GAME){
                if (_playgameScene.checkTime < 0){
                    _playgameScene.doNextState();
                }
                _playgameScene.checkTime--;
            }else{
                // 자동 종료를 위해서 아래 코드 삭제함.(2019.10.18)
                // _timer.paused = true;
            }
        },
        updateCharater : function ()
        {
            if (_isFinished){
                if (debugMode) {console.log("Game fineshed!!!!!!!!!!!!!!!!");}
                return;
            }
            // console.log("_orderOfCharater[_indexOfCharactorOrder] = " + _orderOfCharater[_indexOfCharactorOrder] + ", _indexOfCharactorOrder = " + _indexOfCharactorOrder);
            // 캐릭터 얼굴 표현
            switch (_curentCharacterState){
                case ARM_STATE.LOSE:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = true;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _playgameScene.anim_gallery.anims.msPerFrame = 250; // 관객 애니메이션 frame 속도 조절 : 250 == 1초에 4 프레임
                break;
                case ARM_STATE.MIDDLE_LEFT_LEFT:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = true;
                    _playgameScene.anim_gallery.anims.msPerFrame = 300;
                break;
                case ARM_STATE.MIDDLE_LEFT:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = true;
                    _playgameScene.anim_gallery.anims.msPerFrame = 500;
                break;
                case ARM_STATE.MIDDLE:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = true;
                    _playgameScene.anim_gallery.anims.msPerFrame = 1000;
                break;
                case ARM_STATE.MIDDLE_RIGHT:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = true;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                    _playgameScene.anim_gallery.anims.msPerFrame = 400;
                break;
                case ARM_STATE.MIDDLE_RIGHT_RIGHT:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = true;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                    _playgameScene.anim_gallery.anims.msPerFrame = 250;
                break;
                case ARM_STATE.WIN:
                    _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = true;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                    _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;
                    _playgameScene.anim_gallery.anims.msPerFrame = 200;
                break;
            }

            // 캐릭터 팔 표현
            if (_curentCharacterState != _characterState && _isPlayingAnim == false || _isNewChallenge){
                if (debugMode) { console.log("_prevScore = " + _prevScore + ", _SCORE = " + _SCORE + ", _curentCharacterState = " + _curentCharacterState + ", _characterState = "+ _characterState + ", _upGage = " + _upGage); }
                _isPlayingAnim = true;

                if (_isNewChallenge){   // 초기화
                    _characterState = ARM_STATE.MIDDLE;
                }
                switch (_curentCharacterState){
                    case ARM_STATE.WIN:
                        // _playgameScene.left1ToLeft2.visible = false;
                        // _playgameScene.left2ToWin.anims.play('left2_to_win');
                        // _playgameScene.left2ToWin.visible = true;
                    break;
                    case ARM_STATE.MIDDLE_LEFT_LEFT:

                        if (_curentCharacterState < _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, true);
                            _playgameScene.left1ToLeft2.visible = false;
                            _playgameScene.left2ToWin.anims.play('left2_to_win');
                            _playgameScene.left2ToWin.visible = true;
                            _curentCharacterState = ARM_STATE.WIN;
                        }else{
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, false);
                            _playgameScene.centerToLeft.visible = false;
                            _playgameScene.left1ToLeft2.anims.playReverse('left1_to_left2');
                            _playgameScene.left1ToLeft2.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_LEFT;
                        }
                    break;
                    case ARM_STATE.MIDDLE_LEFT:
                        if (_curentCharacterState < _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, true);

                            _playgameScene.centerToLeft.visible = false;
                            _playgameScene.left1ToLeft2.anims.play('left1_to_left2');
                            _playgameScene.left1ToLeft2.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_LEFT_LEFT;
                        }else{
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.STOP, false);

                            _playgameScene.left1ToLeft2.visible = false;
                            _playgameScene.centerToLeft.anims.playReverse('center_to_left1');
                            _playgameScene.centerToLeft.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE;
                        }
                    break;
                    case ARM_STATE.MIDDLE:
                        if (_isNewChallenge){
                            _playgameScene.left2ToWin.visible = false;
                            _playgameScene.right2ToLose.visible = false;
                            _playgameScene.armImgCenter.visible = true;
                        }else{
                        }
                        if (_curentCharacterState < _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, true);

                            _playgameScene.armImgCenter.visible = false;
                            _playgameScene.centerToRight.visible = false;
                            _playgameScene.centerToLeft.anims.play('center_to_left1');
                            _playgameScene.centerToLeft.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_LEFT;
                        }else if (_curentCharacterState > _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, false);

                            _playgameScene.armImgCenter.visible = false;
                            _playgameScene.centerToLeft.visible = false;
                            _playgameScene.centerToRight.anims.play('center_to_right1');
                            _playgameScene.centerToRight.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_RIGHT;
                        }
                    break;
                    case ARM_STATE.MIDDLE_RIGHT:
                        if (_curentCharacterState < _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.STOP, true);
                            _playgameScene.right1ToRight2.visible = false;
                            _playgameScene.centerToRight.anims.playReverse('center_to_right1');
                            _playgameScene.centerToRight.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE;
                        }else{
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, false);

                            _playgameScene.centerToRight.visible = false;
                            _playgameScene.right1ToRight2.anims.play('right1_to_right2');
                            _playgameScene.right1ToRight2.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_RIGHT_RIGHT;
                        }
                    break;
                    case ARM_STATE.MIDDLE_RIGHT_RIGHT:

                        if (_curentCharacterState < _characterState){
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, true);
                            _playgameScene.right1ToRight2.anims.playReverse('right1_to_right2');
                            _playgameScene.right1ToRight2.visible = true;
                            _curentCharacterState = ARM_STATE.MIDDLE_RIGHT;
                        }else{
                            _playgameScene.playSoundArmWrestling(SOUND_WRESLING_STATE.PLAY, false);
                            _playgameScene.right1ToRight2.visible = false;
                            _playgameScene.right2ToLose.anims.play('right2_to_win');
                            _playgameScene.right2ToLose.visible = true;
                            _curentCharacterState = ARM_STATE.LOSE;
                        }
                    break;
                    case ARM_STATE.LOSE:
                        // _playgameScene.right1ToRight2.visible = false;
                        // _playgameScene.right2ToLose.anims.play('right2_to_lose');
                        // _playgameScene.right2ToLose.visible = true;
                    break;
                }

                // 대전자와 시합 시작함.
                _isNewChallenge = false;
            }
        },
        // 팔씨름 효과음 재생 / 멈춤
        playSoundArmWrestling: function(state, isWinning)
        {
            // 넘길때 효과음
            if (isWinning !== undefined && isWinning){
                if (_gIsPlayingAudioByPhaser){
                if (audio_arm_winning.isPlaying){
                    audio_arm_winning.pause();
                    audio_arm_winning.currentTime = 0;
                }
                audio_arm_winning.play();
                }else {
                    if (audio_arm_winning.paused == false){
                        audio_arm_winning.pause();
                        audio_arm_winning.currentTime = 0;
                    }
                    audio_arm_winning.volume = 0.5;
                    audio_arm_winning.play();
                }
            }else { // 넘어갈때 효과음
                if (_gIsPlayingAudioByPhaser){
                if (audio_arm_losing.isPlaying){
                    audio_arm_losing.pause();
                    audio_arm_losing.currentTime = 0;
                }
                audio_arm_losing.play();
                }else {
                    if (audio_arm_losing.paused == false){
                        audio_arm_losing.pause();
                        audio_arm_losing.currentTime = 0;
                    }
                    audio_arm_losing.volume = 0.5;
                    audio_arm_losing.play();
                }
            }
            //     if (audio_arm_wrestling.isPlaying){
            //         audio_arm_wrestling.pause();
            //         audio_arm_wrestling.currentTime = 0;
            //     }
            //     audio_arm_wrestling.play();
            // if (state == SOUND_WRESLING_STATE.PLAY){
            // }else if (state == SOUND_WRESLING_STATE.STOP){
            //     if (audio_arm_wrestling.isPlaying){
            //         audio_arm_wrestling.pause();
            //         audio_arm_wrestling.currentTime = 0;
            //     }
            // }
        },
        createEnded: function()
        {
            // nativeInterface.onStart(null);
        },
        createCharactor : function (scope)
        {
            // 캐릭터 선택
            var x = _screenWidth/2;
            var y = _playgameScene.bg_ring_image.getTopCenter().y + _playgameScene.bg_ring_image.displayHeight/2;
            var width = _width * 0.8;
            var height = width;
            scope.character_01 = scope.add.image(x, y, 'character_01').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.character_01_ing = scope.add.image(x, y, 'character_01_ing').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_01_lose = scope.add.image(x, y, 'character_01_lose').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_01_win = scope.add.image(x, y, 'character_01_win').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_02 = scope.add.image(x, y, 'character_02').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_02_ing = scope.add.image(x, y, 'character_02_ing').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_02_lose = scope.add.image(x, y, 'character_02_lose').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_02_win = scope.add.image(x, y, 'character_02_win').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_03 = scope.add.image(x, y, 'character_03').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_03_ing = scope.add.image(x, y, 'character_03_ing').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_03_lose = scope.add.image(x, y, 'character_03_lose').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_03_win = scope.add.image(x, y, 'character_03_win').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_04 = scope.add.image(x, y, 'character_04').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_04_ing = scope.add.image(x, y, 'character_04_ing').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_04_lose = scope.add.image(x, y, 'character_04_lose').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_04_win = scope.add.image(x, y, 'character_04_win').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_05 = scope.add.image(x, y, 'character_05').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_05_ing = scope.add.image(x, y, 'character_05_ing').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_05_lose = scope.add.image(x, y, 'character_05_lose').setDisplaySize(width,height).setOrigin(0.5, 1);
            scope.character_05_win = scope.add.image(x, y, 'character_05_win').setDisplaySize(width,height).setOrigin(0.5, 1);

            scope.character_01.visible = false;
            scope.character_01_ing.visible = false;
            scope.character_01_lose.visible = false;
            scope.character_01_win.visible = false;
            scope.character_02.visible = false;
            scope.character_02_ing.visible = false;
            scope.character_02_lose.visible = false;
            scope.character_02_win.visible = false;
            scope.character_03.visible = false;
            scope.character_03_ing.visible = false;
            scope.character_03_lose.visible = false;
            scope.character_03_win.visible = false;
            scope.character_04.visible = false;
            scope.character_04_ing.visible = false;
            scope.character_04_lose.visible = false;
            scope.character_04_win.visible = false;
            scope.character_05.visible = false;
            scope.character_05_ing.visible = false;
            scope.character_05_lose.visible = false;
            scope.character_05_win.visible = false;

            _character[0] = new Array(4);
            _character[0].basic =  scope.character_01;
            _character[0].ing = scope.character_01_ing;
            _character[0].lose = scope.character_01_lose;
            _character[0].win = scope.character_01_win;
            _character[1] = new Array(4);
            _character[1].basic = scope.character_02;
            _character[1].ing = scope.character_02_ing;
            _character[1].lose = scope.character_02_lose;
            _character[1].win = scope.character_02_win;
            _character[2] = new Array(4);
            _character[2].basic = scope.character_03;
            _character[2].ing = scope.character_03_ing;
            _character[2].lose = scope.character_03_lose;
            _character[2].win = scope.character_03_win;
            _character[3] = new Array(4);
            _character[3].basic = scope.character_04;
            _character[3].ing = scope.character_04_ing;
            _character[3].lose = scope.character_04_lose;
            _character[3].win = scope.character_04_win;
            _character[4] = new Array(4);
            _character[4].basic = scope.character_05;
            _character[4].ing = scope.character_05_ing;
            _character[4].lose = scope.character_05_lose;
            _character[4].win = scope.character_05_win;

            scope.shuffle(_orderOfCharater);

            _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = true;
        },
        createCharaterArmAnim : function(scope)
        {
            var x = _screenWidth/2;
            var y = _height;
            var width = _width * 1.0;
            var height = width * 0.72;

            scope.armImgCenter = scope.add.image(x, y, 'arm_center').setDisplaySize(width, height).setOrigin(0.5, 1);

            scope.anims.create({
                key: 'center_to_left1',
                frames: [
                    { key: 'arm_center' },
                    { key: 'arm_left_01'},
                    { key: 'arm_left_02'},
                    { key: 'arm_left_03', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.centerToLeft = scope.add.sprite(x, y, 'arm_center').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.centerToLeft.anims.load('center_to_left1');
            scope.centerToLeft.visible = false;
            scope.centerToLeft.on('animationcomplete', function(){
                if (_curentCharacterState != _characterState){
                    _isPlayingAnim = false;
                }

            }, scope);

            scope.anims.create({
                key: 'left1_to_left2',
                frames: [
                    { key: 'arm_left_03' },
                    { key: 'arm_left_04'},
                    { key: 'arm_left_05'},
                    { key: 'arm_left_06', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.left1ToLeft2 = scope.add.sprite(x, y, 'arm_left_03').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.left1ToLeft2.anims.load('left1_to_left2');
            scope.left1ToLeft2.visible = false;
            scope.left1ToLeft2.on('animationcomplete', function(){
                if (_curentCharacterState != _characterState){
                    _isPlayingAnim = false;
                }
            }, scope);

            scope.anims.create({
                key: 'left2_to_win',
                frames: [
                    { key: 'arm_left_06'},
                    { key: 'arm_left_07'},
                    { key: 'arm_left_08'},
                    { key: 'arm_left_09'},
                    { key: 'arm_left_10'},
                    { key: 'arm_left_11', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.left2ToWin = scope.add.sprite(x, y, 'arm_left_06').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.left2ToWin.anims.load('left2_to_win');
            scope.left2ToWin.visible = false;
            //complet event 등록
            scope.left2ToWin.on('animationcomplete', function(){
                var winScore = 10;
                if (_gIsPlayingAudioByPhaser){
                if (audio_cheerRing.isPlaying){
                    audio_cheerRing.pause();
                    audio_cheerRing.currentTime = 0;
                }
                audio_cheerRing.play({volume:0.5});
                }else{
                    if (audio_cheerRing.paused == false){
                        audio_cheerRing.pause();
                        audio_cheerRing.currentTime = 0;
                    }
                    audio_cheerRing.volume = 0.5;
                    audio_cheerRing.play();
                }

                _isNewChallenge = true;
                _TOTAL_SCORE += 10;
                if (_prevMatch == "win"){
                    _TOTAL_SCORE += 5;
                    winScore += 5;
                }

                _playgameScene.bonusScoreText.setText("+ " + winScore);
                _playgameScene.bonusScoreText.visible = true;
                _playgameScene.tweenBonusScore.restart();

                _prevMatch = "win";
                _curentCharacterState = ARM_STATE.MIDDLE;
                _characterState = ARM_STATE.MIDDLE;
                _playgameScene.nextCharacter();
            }, scope);

            scope.anims.create({
                key: 'center_to_right1',
                frames: [
                    { key: 'arm_center' },
                    { key: 'arm_right_01'},
                    { key: 'arm_right_02'},
                    { key: 'arm_right_03'},
                    { key: 'arm_right_04', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.centerToRight = scope.add.sprite(x, y, 'arm_center').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.centerToRight.anims.load('center_to_right1');
            scope.centerToRight.visible = false;
            scope.centerToRight.on('animationcomplete', function(){
                if (_curentCharacterState != _characterState){
                    _isPlayingAnim = false;
                }
            }, scope);

            scope.anims.create({
                key: 'right1_to_right2',
                frames: [
                    { key: 'arm_right_04' },
                    { key: 'arm_right_05'},
                    { key: 'arm_right_06'},
                    { key: 'arm_right_07', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.right1ToRight2 = scope.add.sprite(x, y, 'arm_right_04').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.right1ToRight2.anims.load('right1_to_right2');
            scope.right1ToRight2.visible = false;
            scope.right1ToRight2.on('animationcomplete', function(){
                if (_curentCharacterState != _characterState){
                    _isPlayingAnim = false;
                }
            }, scope);

            scope.anims.create({
                key: 'right2_to_lose',
                frames: [
                    { key: 'arm_right_07' },
                    { key: 'arm_right_08'},
                    { key: 'arm_right_09'},
                    { key: 'arm_right_10'},
                    { key: 'arm_right_11', duration: 400 }
                ],
                frameRate: 10,
                repeat: 0
            });
            scope.right2ToLose = scope.add.sprite(x, y, 'arm_right_07').setDisplaySize(width, height).setOrigin(0.5, 1);
            scope.right2ToLose.anims.load('right2_to_lose');
            scope.right2ToLose.visible = false;
            //complet event 등록
            scope.right2ToLose.on('animationcomplete', function(){
                if (_gIsPlayingAudioByPhaser){
                if (audio_cheerRing.isPlaying){
                    audio_cheerRing.pause();
                    audio_cheerRing.currentTime = 0;
                }
                audio_cheerRing.play({volume:0.5});
                }else{
                    if (audio_cheerRing.paused == false){
                        audio_cheerRing.pause();
                        audio_cheerRing.currentTime = 0;
                    }
                    audio_cheerRing.volume = 0.5;
                    audio_cheerRing.play();
                }

                _isNewChallenge = true;
                _prevMatch   = "lose";
                _curentCharacterState = ARM_STATE.MIDDLE;
                _characterState = ARM_STATE.MIDDLE;
                _playgameScene.nextCharacter();
            }, scope);
        },
        // 전환될 캐릭터 상태값 처리
        nextCharacter : function()
        {
            if (_contLoseScore >= 3){
                _LOSE_COUNT++;
            }
            if (_contWinScore >= 3){
                _WIN_COUNT++;
            }
            if (_contWinScore >= 3 || _contLoseScore >= 3){
                _contWinScore = 0;
                _contLoseScore = 0;
                // 기존 상태 숨기기
                _character[_orderOfCharater[_indexOfCharactorOrder]].basic.visible = false;
                _character[_orderOfCharater[_indexOfCharactorOrder]].lose.visible = false;
                _character[_orderOfCharater[_indexOfCharactorOrder]].ing.visible = false;
                _character[_orderOfCharater[_indexOfCharactorOrder]].win.visible = false;

                // 다음 상태로 전환
                _indexOfCharactorOrder++;
                if (_indexOfCharactorOrder >= _orderOfCharater.length){
                    _isFinished = true;
                }
                if (debugMode) { console.log("_indexOfCharactorOrder = " + _indexOfCharactorOrder); }
            }
        },
        shuffle : function(array)
        {
            var j, x, i;
            for (i = array.length; i; i -= 1) {
                j = Math.floor(Math.random() * i);
                x = array[i - 1];
                array[i - 1] = array[j];
                array[j] = x;
            }
        },
        // 결과 팝업 내용 설정
        setDisplayResultPopup : function()
        {
            // bad / not good / good / very good / excellent 표시 및 화살표 위치 설정
            _playgameScene.result_popup_text_bad.visible = false;
            _playgameScene.result_popup_text_notgood.visible = false;
            _playgameScene.result_popup_text_good.visible = false;
            _playgameScene.result_popup_text_verygood.visible = false;
            _playgameScene.result_popup_text_excellent.visible = false;

            // 화살표 y 좌표 값
            // _height/2 + _width * 0.465
            // _height/2 + _width * 0.41
            // _height/2 + _width * 0.36
            // _height/2 + _width * 0.305
            // _height/2 + _width * 0.25

            // 게임 결과 (_gameResult)
            // 1: bad 2 : not good 3 : good 4 : very good 5 : excellent

            // 게임 시간에 따른 스코어 설정 및 스코어 표시
            var badScroe = 70;
            var notgoodScore = 140;
            var goodScore = 210;
            var verygoodScore = 280;
            var MAX_PLAY_TIME = 180;
            var scoreRate = MAX_PLAY_TIME/__CHECK_TIME__;
            var MAX_POINT = parseInt(350 / scoreRate);//(30/*MAX_PLAY_TIME초/2초/3회*/ / scoreRate * 10) + (30 / scoreRate * 5) - 5;

            badScroe        = 20;//15;//parseInt((badScroe / ( MAX_POINT / scoreRate )) * 100);
            notgoodScore    = 40;//31;//parseInt((notgoodScore / ( MAX_POINT / scoreRate )) * 100);
            goodScore       = 60;//46;//parseInt((goodScore / ( MAX_POINT / scoreRate )) * 100);
            verygoodScore   = 80;//62;//parseInt((verygoodScore / ( MAX_POINT / scoreRate )) * 100);

            if (debugMode) {
                console.log("_TOTAL_SCORE = " + _TOTAL_SCORE);
                console.log("MAX_POINT = "+ MAX_POINT + ", scoreRate = " + scoreRate);
                console.log("parseInt((_TOTAL_SCORE:"+_TOTAL_SCORE+" / MAX_POINT:"+MAX_POINT+") * 100) =" + parseInt((_TOTAL_SCORE / MAX_POINT) * 100));
                console.log("badScroe=" + badScroe + ", notgoodScore=" + notgoodScore + ", goodScore=" + goodScore + ", verygoodScore=" + verygoodScore);
            }

            var percentage_score    = parseInt((_TOTAL_SCORE / MAX_POINT) * 100); // 배점 계산 필요
            if (percentage_score > 100){
                percentage_score = 100;
            }
            _percentage_score = percentage_score; // 전역 변수에 배율 점수 저장

            if (debugMode) {
                console.log("setDisplayResultPopup >> scoreRate = " + scoreRate + ", _TOTAL_SCORE = " + _TOTAL_SCORE + ", percentage_score=" + percentage_score);
            }

            _playgameScene.scoreTextOfResult1.setY(_height/2 + _width * 0.465); // badScroe
            _playgameScene.scoreTextOfResult1.setText("0 ~ " + badScroe + " %");
            _playgameScene.scoreTextOfResult2.setY(_height/2 + _width * 0.415); // notgoodScore
            _playgameScene.scoreTextOfResult2.setText((badScroe + 1) + " ~ " + notgoodScore + " %");
            _playgameScene.scoreTextOfResult3.setY(_height/2 + _width * 0.36);  // goodScore
            _playgameScene.scoreTextOfResult3.setText((notgoodScore + 1) + " ~ " + goodScore + " %");
            _playgameScene.scoreTextOfResult4.setY(_height/2 + _width * 0.305); // verygoodScore
            _playgameScene.scoreTextOfResult4.setText((goodScore + 1) + " ~ " + verygoodScore + " %");
            _playgameScene.scoreTextOfResult5.setY(_height/2 + _width * 0.253); // excellent
            _playgameScene.scoreTextOfResult5.setText((verygoodScore + 1) + " ~ 100 %");

            if (percentage_score >= 0 && percentage_score <= badScroe){
                if(_result_skip == 'N'){
                    _playgameScene.result_popup_text_bad.visible = true;
                    _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.47);
                }
                _gameResult = 1;
            }else if (percentage_score > badScroe && percentage_score <= notgoodScore){
                if(_result_skip == 'N'){
                    _playgameScene.result_popup_text_notgood.visible = true;
                    _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.42);
                }
                _gameResult = 2;
            }else if (percentage_score > notgoodScore && percentage_score <= goodScore){
                if(_result_skip == 'N'){
                    _playgameScene.result_popup_text_good.visible = true;
                    _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.37);
                }
                _gameResult = 3;
            }else if (percentage_score > goodScore && percentage_score <= verygoodScore){
                if(_result_skip == 'N'){
                    _playgameScene.result_popup_text_verygood.visible = true;
                    _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.315);
                }
                _gameResult = 4;
            }else if (percentage_score > verygoodScore){
                if(_result_skip == 'N'){
                    _playgameScene.result_popup_text_excellent.visible = true;
                    _playgameScene.result_popup_arrow.setY(_height/2 + _width * 0.267);
                }
                _gameResult = 5;
            }

            if(_result_skip == 'Y'){
                // _result_skip이 "Y"인 경우에, 화면 처리하지 않고 return
                return;
            }


            // 최종 점수 표시
            _playgameScene.scoreTextOfResult.setText(_TOTAL_SCORE + " (" + percentage_score + "%)");

            // 게임 모드에 따라서 버튼을 보여주도록 처리
            if (_gGameMode == GAME_MODE.NORMAR || _gGameMode == GAME_MODE.SIAM){
                _playgameScene.result_popup_next_game_nor.visible = true;
                _playgameScene.result_popup_view_result_nor.visible = false;
                _playgameScene.result_popup_view_result_sel.visible = false;
                _playgameScene.result_popup_exit_nor.visible = false;
            }else{
                if(_result_button_code == 'BTN001'){
                    _playgameScene.result_popup_next_game_nor.visible = false;
                    _playgameScene.result_popup_view_result_sel.visible = false;
                    _playgameScene.result_popup_view_result_nor.visible = false;
                    _playgameScene.result_popup_exit_nor.visible = true;
                }else {
                    // 결과 보기  NORMAR_END , SIAM_END : 마지막 게임
                    _playgameScene.result_popup_next_game_nor.visible = false;
                    _playgameScene.result_popup_view_result_sel.visible = false;
                    _playgameScene.result_popup_view_result_nor.visible = true;
                    _playgameScene.result_popup_exit_nor.visible = false;
                }
            }
        },
        resetData : function()
        {
            _SCORE       = ARM_STATE.MIDDLE;    // 브레인 측정 점수
            _prevScore   = ARM_STATE.MIDDLE;    // 직전 측정 점수
            _isPlayingAnim = true;
            _WIN_COUNT   = 0;    // 게임 승 횟수
            _LOSE_COUNT  = 0;    // 게임 패 횟수
            _TOTAL_SCORE = 0;   // 게임 점수
            _prevMatch   = "first"; // 이전 게임의 승패 first / win / lose
            _indexOfCharactorOrder = 0;
            _isFinished = false;
            _contLoseScore = 0;
            _contWinScore = 0;
            _characterState = ARM_STATE.MIDDLE;
            _curentCharacterState = ARM_STATE.MIDDLE;
            _isNewChallenge = true;
            _gageBarWidth = 0;
        },
        setSham : function() {
            CON_ARR = [];
            var val = 0;
            var beforeVal = 0;
            var conSuccessCnt = 0;
            var successCnt = 0;
            var successMax = 10;
            var successMin = 5;
            var failMax = 4;
            var failMin = 0;
            // var ratio_arr = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8];
            var ratio_arr = [0.5, 0.6, 0.7, 0.8, 0.9];
            ratio = ratio_arr.sort(function () {return 0.5 - Math.random()}).pop();

            if (debugMode) { console.log('ratio = > ${ratio * 100}% 확율의 성공'); }
            for (var i = 0; i < 120; i++) {
              if (Math.random() < ratio) {
                val = Math.floor(Math.random() * (successMax - successMin + 1)) + successMin;
              } else {
                val = Math.floor(Math.random() * (failMax - failMin + 1)) + failMin;
              }
              CON_ARR.push(val);
              if (conSuccessCnt == 0 && val > 4) {
                conSuccessCnt = 1;
              } else if (conSuccessCnt == 0 && val < 4) {
                conSuccessCnt = 0;
              } else if (conSuccessCnt > 0 && val < 5) {
                conSuccessCnt -= 1;
              } else if (conSuccessCnt > 0 && val > 4) {
                if (beforeVal > 4 && val > 4)
                  conSuccessCnt += 1;
              }
              if (conSuccessCnt == 3) {
                successCnt += 1;
                conSuccessCnt = 0;
              }
              beforeVal = val;
            }

            if (debugMode){
                console.log('shamSuccessCnt => ${successCnt}');
                console.log('CON_ARR => ${CON_ARR}');
            }

            if (isDisplaySiamRatio)
            {   // 샴 통계용
                var average = 0, total = 0;
                for (var j = 0; j < CON_ARR.length; j++){
                    total += CON_ARR[j];
                }
                average = total / CON_ARR.length;
                _strSiamRatio = "ratio : " + (ratio * 100) + " (" + average + ")%";
                if (debugMode){
                    console.log(_strSiamRatio);
                }
            }
            return CON_ARR;
        },
        showToast: function() {
            var toast = document.getElementById("toastMessageBar");

            if (_gLang == "ko"){
                toast.innerHTML = _messageKr;
            }else {
                toast.innerHTML = _messageEn;
            }
            $("#toastMessageBar").show(500);
            setTimeout(function(){
                $("#toastMessageBar").hide(500);
            }, 5000);
        }
    };

}());
