﻿// 야구게임
var IntroScene = new Phaser.Class(function(){

    var STATE = {
        INIT_STATE : 0,
        READY_STATE : 1
    };
    var gameState = 0;
    var introMusic = null;

    // Scene 전역 스코프 변수
    var _introgameScene       = null;
    var _timer     = null;

    return {
        Extends: Phaser.Scene,

        initialize:

        function IntroScene ()
        {
            Phaser.Scene.call(this, { key: 'intro_scene' });
            // console.log("Construct of IntroScene");
        },
        preload: function ()
        {
            currentScene = GAME_SCENE.INTRO_SCENE;
            gameState = STATE.INIT_STATE;

            var progressBar = this.add.graphics();
            var progressBox = this.add.graphics();
            var percentText = this.make.text({
                x: window.innerWidth/2,
                y:window.innerHeight/2+25,
                style: {
                    font : '18px monospace',
                    fill : '#ffffff'
                }
            });
            percentText.setOrigin(0.5);
            progressBox.fillStyle(0x222222, 0.8);
            progressBox.fillRoundedRect(window.innerWidth/2 - 150, window.innerHeight/2, 300, 50, 5);

            // // image
            if (this.game.config.width > this.game.config.height){
                // 가로 이미지
                this.load.image('intro_bg', 'assets/images/baseball_game_intro_bg_large_'+_gLang+'.png');
                this.load.image('intro_text_02', 'assets/images/intro_text_02_large.png');
            }else{
                // 세로 이미지
                this.load.image('intro_bg', 'assets/images/baseball_game_intro_bg_'+_gLang+'.png');
                this.load.image('intro_text_02', 'assets/images/intro_text_02.png');
            }

            // 야구 배경이미지
            this.load.image('background', 'assets/images/baseball_game_bg.png');
            this.load.image('background_large', 'assets/images/baseball_game_bg_large.png');

            // 야구공
            this.load.image('ball', 'assets/images/ic_baseball.png');

            // 투수 모션
            this.load.image('pitcher0', 'assets/images/ic_pitcher_00.png');
            this.load.image('pitcher1', 'assets/images/ic_pitcher_01.png');
            this.load.image('pitcher2', 'assets/images/ic_pitcher_02.png');
            this.load.image('pitcher3', 'assets/images/ic_pitcher_03.png');

            // 타격 모션
            this.load.image('batter1', 'assets/images/ic_hitter_01.png');
            this.load.image('batter2', 'assets/images/ic_hitter_02.png');
            this.load.image('batter3', 'assets/images/ic_hitter_03.png');

            // 타격 가이드
            this.load.image('finger', 'assets/images/ic_finger_guide.png');

            // 배경 이미지

            /****** 게임 중인 화면 *****/
            // 배경 이미지
            // 게임 요소
            this.load.image('ground_play_alarm', 'assets/images/clock_empty.png');
            this.load.image('ground_gage_empty', 'assets/images/gage_empty.png');
            this.load.image('ground_score_board', 'assets/images/electric-sign_'+_gLang+'.png');

            // 게임 결과 화면
            this.load.image('result_ic_result_popup_bg', 'assets/images/game_popup.png');
            this.load.image('result_ic_scoretext_bad', 'assets/images/popup_result_text_01.png');
            this.load.image('result_ic_scoretext_notgood', 'assets/images/popup_result_text_02.png');
            this.load.image('result_ic_scoretext_good', 'assets/images/popup_result_text_03.png');
            this.load.image('result_ic_scoretext_verygood', 'assets/images/popup_result_text_04.png');
            this.load.image('result_ic_scoretext_excellent', 'assets/images/popup_result_text_05.png');
            this.load.image('result_ic_popup_arrow', 'assets/images/popup_arrow.png');
            this.load.image('result_btn_resultview_nor', 'assets/images/btn_result_01_nor_'+_gLang+'.png');
            this.load.image('result_btn_resultview_sel', 'assets/images/btn_result_01_sel_'+_gLang+'.png');
            this.load.image('result_btn_nextgame_nor', 'assets/images/btn_result_02_nor_'+_gLang+'.png');
            this.load.image('result_btn_exit_nor', 'assets/images/btn_result_popup_exit_nor_'+_gLang+'.png');
            this.load.image('result_btn_end', 'assets/images/btn_result_03_nor.png');

            // audio
            this.load.audio('audio_ready_start', ['assets/audio/ready_start.mp3']);
            this.load.audio('audio_play_bgm', ['assets/audio/Game_play_BGM.mp3']);
            this.load.audio('audio_game_win', ['assets/audio/game_clear_good(Result).mp3']);
            this.load.audio('audio_game_lose', ['assets/audio/game_fail_bad(Result).mp3']);
            this.load.audio('audio_cheerRing', ['assets/audio/cheerRing.mp3']);

            this.load.audio('audio_swing', ['assets/audio/Baseball_swing.wav']);
            this.load.audio('audio_hit', ['assets/audio/Baseball_hit.mp3']);

            this.load.on('progress', function(value){
                progressBar.clear();
                progressBar.fillStyle(0xffffff,1);
                progressBar.fillRoundedRect(window.innerWidth/2+10-150, window.innerHeight/2+10, 280*value, 30, 5);
                percentText.setText("Loading "+Math.round(value*100) + "%");
            });

            this.load.on('complete', function(){
                // progressBox.destory();
                // progressBar.destory();
            });

        },

        create: function ()
        {
            var width = this.game.config.width;
            var height = this.game.config.height;

            if (width > 600){
                // width = 600;
            }

            // set background image
            var bg = this.add.image(this.game.config.width/2, this.game.config.height/2, 'intro_bg');
            bg.displayWidth = width;
            bg.displayHeight = height;

            _introgameScene = this;

            var logoSkip = _introgameScene.logoSkip('intro');
            if(logoSkip == true){
                return;
            }

            // text
            this.pressTextImg = this.add.image(this.game.config.width/2, height - height/10, 'intro_text_02').setDisplaySize(width/2, width/2 * 0.11);

            this.pointer = this.input.on('pointerup', this.doNextState);

            if (isIos == false && isAndroid == false){
                nativeInterface.onStart(3, 180);
            }

            if (_logo_auto_skip == "Y"){
                _timer = _introgameScene.time.delayedCall(_logo_skip_wait_time, function(){
                    _introgameScene.doNextState();
                }, null, _introgameScene);  // delay in ms
            } 
            if(_logo_skip != 'Y'){
                // 시작 음악 재생
                if(_gIsPlayingAudioByPhaser) {
                    introMusic = this.sound.add('audio_play_bgm');
                    introMusic.autoplay = true;
                    // introMusic.loop = true;
                    introMusic.play({loop:true, volume:0.2}); 
                    
                }
                else {
                    introMusic = new Audio('assets/audio/Game_play_BGM.mp3');
                    introMusic.autoplay = true;
                    introMusic.loop = true;
                    introMusic.play(); 
                }
            
                
            }

        },
        update: function()
        {
            // console.log("called update : time = " + ((this.time.now / 1000) % 2));
            if (((this.time.now / 1000) % 1) >= 0.5){
                this.pressTextImg.setAlpha(0);
            }else{
                this.pressTextImg.setAlpha(1);
            }
        },
        onStartAudio: function()
        {
        },
        doNextState: function ()
        {            
            console.log('doNextState was called! ==>' + STATE.INIT_STATE);
            console.log("gameState = " + gameState + ", _gGameMode = " + _gGameMode);
            if (_gGameMode == null){
                console.log('Need to call window.onStart function');
                return;
            }
            if (gameState == STATE.INIT_STATE){
                gameState = STATE.READY_STATE;

                if (_gIsPlayingAudioByPhaser){
                    if (introMusic.isPlaying == true){  // Phaser audio
                        introMusic.pause();
                    }
                }
                else {
                    if (introMusic.paused == false){  // Phaser audio 
                         introMusic.pause();
                    }
                }

                if(_timer != null){
                    _timer.remove();    // 5초 timer 제거
                    _timer = null;
                }
                _introgameScene.scene.start('gameplayscene');
            }
        },
        /** onStart 이벤트를 받아 logo skip 처리 한다 **/
        logoSkip : function(str){
            //console.log('logoSkip : '+str+" _logo_skip : "+ _logo_skip);
            if(_introgameScene && _logo_skip == 'Y'){
                if(_timer != null){
                    _timer.remove();
                    _timer = null;
                }

                _introgameScene.scene.scene.input.removeAllListeners();
                _introgameScene.scene.start('gameplayscene');
                return true;
            }
            return false;
        }
    };
}());