// 야구 게임

var VIEW_MAX_WIDTH = 0;
var LEVEL       = 2;  // 게임 레벨
var debugFlag  = false;
var nextGameTimer;
var nextGameTimeSec = 5000; // 5초

var PlayGameScene = new Phaser.Class(function(){
    // PlayGame 상태값
    var STATE = {
        STATE_READY : 0,
        STATE_PLAY_GAME : 1,
        STATE_DISPLAY_RESULT : 2,
        STATE_END_GAME : 3
    };

    // PlayGame 상태값
    var STATE_HIT = {
        STATE_READY : 0,
        STATE_HITING : 1,
        STATE_END : 2
    };

    var hitresult_txt1;
    var hitresult_txt2;

    var __CHECK_TIME__  = 60;  // 180초 측정
    var _SCORE       = 0;//ARM_STATE.MIDDLE;    // 브레인 측정 점수
    var _prevScore   = 0;//ARM_STATE.MIDDLE;    // 직전 측정 점수
    var _TOTAL_SCORE = 0;   // 게임 점수
    // 캐릭터 별로 3회 이상 출현 제한이 있기 때문에 아래와 같이 각 캐릭터별로 3회만 입력하고 순서를 랜덤으로 썩음.
    var _isFinished = false;
    var _gameResult = 0;    // native에 onNextGame 함수 호출시에 전달한 파라메타 (결과값) // 1: bad 2 : not good 3 : good 4 : very good 5 : excellent
    var _percentage_score = 0; // 배점 계산 (비율 계산)

    // Scene 전역 스코프 변수
    var _app       = null;

    // 오디오 저장 변수
    var audio_play_bgm      = null; // Phaser -JS- Audio
    var audio_ready_start   = null; // Phaser -JS- Audio
    var audio_game_win      = null; // Phaser audio
    var audio_game_lose     = null; // Phaser audio
    var audio_hit           = null // 타격
    var audio_swing           = null // 헛스윙

    // Scene 전체의 넓이, 높이 변수
    var _width           = 0;
    var _height          = 0;
    var _screenWidth     = 0;

    // 현재 게임 Scene의 상태값 변수
    var _gameState = STATE.STATE_READY;
    // GageBar 관련 변수
    var _gageBarWidth    = null;
    var __INTERVAL_GAGE__  = 2.8;
    var __MAX_SCORE_WIDTH__   = __INTERVAL_GAGE__ * 10 * 10;
    var __MIN_SCORE_WIDTH__   = 0;
    var _upGage          = false;

    var tweenBonusScore1;
    var tweenBonusScore2;

    // 샴 데이터를 처리하기 위한 배열
    var _aSiamData = [];
    var _strSiamRatio = "";

    var scoreText, scoreHomerun,scoreHit1,scoreHit2,scoreHit3;
    var _CHK_COMBO=false;
    var _BASE_SCORE_4=5;   // 홈런 점수
    var _BASE_SCORE_3=4;   // 3루타 점수
    var _BASE_SCORE_2=3;   // 2루타 점수
    var _BASE_SCORE_1=2;   // 1루타 점수
    var _SCORE_1=0;
    var _SCORE_2=0;
    var _SCORE_3=0;
    var _SCORE_4=0;
    var thisScore=0;

    var camera;
    var speed = 6;
    var ball;
    var _hitState = 0;
    var offset = 1;

    var hit_zindex = 540;

    var pitcher;

    var finger;

    var _hitTarget = 'center'; // left, center, right
    var _hitResult = 0;         // [0:아웃, 1:1루타, 2:2루타, 3:3루타, 4:홈런]

    // Toast Message
    var _messageEn = "Focus on hitting the ball by touching the flying ball.";
    var _messageKr = "날아오는 공을 터치하여 공을 치는 것에 집중하세요.";

    var _messageHomerun_en = "HOMERUN";
    var _messageHomerun_kr = "홈런";

    var _messageHit_en = " Hit";
    var _messageHit_kr = "루타";

    var _messageOut_en = "OUT";
    var _messageOut_kr = "아웃";

    var scoreTextOfResult1;
    var scoreTextOfResult2;
    var scoreTextOfResult3;
    var scoreTextOfResult4;
    var scoreTextOfResult5;

    return{
        Extends: Phaser.Scene,
        initialize:

        function PlayGameScene ()
        {
            Phaser.Scene.call(this, { key: 'gameplayscene' });
        },
        preload: function ()
        {

            this.load.scenePlugin('Camera3DPlugin', 'plugins/camera3d.min.js', 'Camera3DPlugin', 'cameras3d');

            // 현재 게임 Scene 설정 (외부에서 판단하기 위해서)
            currentScene = GAME_SCENE.PLAY_SCENE;

            // 게임 플레이 시간 설정
            if (!isNaN(_gGamePlayTime)){
                __CHECK_TIME__ = _gGamePlayTime;
            }

            console.log(__CHECK_TIME__);

            this.checkTime = __CHECK_TIME__;
            if(_result_auto_skip == 'Y'){
                nextGameTimeSec = _result_skip_wait_time;
            }
            console.log("PlayGameScene >> called preload");
        },

        create: function ()
        {
            _app = this;  // 해당 Scene 내의 전역변수 설정
            _width = this.game.config.width; // 게임 화면 넓이
            _screenWidth = this.game.config.width; // 화면 전체 넓이
            _height = this.game.config.height;   // 화면 전체 높이

            if (_width > 600){
                _width = 600;
                VIEW_MAX_WIDTH = 600;
            }else{
                VIEW_MAX_WIDTH = _width;
            }


            // scale이 최대 넓이 600px일 경우에 0.3
            // 각 해상도에 따라서 최대 넓이 기준으로 비율 조정
            var scaleOfResultText = 0.3 * _width/600;

            console.log('create is ready in PlayGameScene');
            // audio_play_bgm.autoplay = false;
            /***************************** 게임 플레이 ***************************/
            if(_screenWidth > _height) {
                var bg = this.add.image(_screenWidth/2, _height/2, 'background_large').setDepth(-5000).setInteractive();;  // 야구장 배경
            }
            else {
                var bg = this.add.image(_screenWidth/2, _height/2, 'background').setDisplaySize(_width, _height).setDepth(-5000).setInteractive();;  // 야구장 배경
            }

            camera = this.cameras3d.add(80).setX(0).setY(-80).setZ(800).setPixelScale(250);    // 카메라 위치
                                        // 야구공 (x, y, z)

            // 투수 애니메이션 정의
            this.anims.create({
                key: 'pitcherAni',
                frames: [
                    { key: 'pitcher0', duration: 1000 },
                    { key: 'pitcher1', duration: 500 },
                    { key: 'pitcher2' },
                    { key: 'pitcher3' },
                ],
            });

            // 타자 애니메이션 정의
            this.anims.create({
                key: 'hitAni',
                frames: [
                    { key: 'batter1' }, { key: 'batter2' }, { key: 'batter3' }, { key: 'batter2' }, { key: 'batter1' }
                ],
                frameRate: 20,
            });

            // 투수
            pitcher = this.add.sprite(_screenWidth*0.52, _height*0.63, 'pitcher0').setDisplaySize(_width*0.15, _width*0.26);

            // 타자
            hitter = this.add.sprite(pitcher.x, _height*0.7, 'batter1').setDisplaySize(_width*0.5, _width*0.9).setOrigin(1, 0.5).setInteractive();

            // 야구공
            ball = camera.create(10, 20, 128, 'ball');

            // 클릭시 타격 애니메이션 실행
            hitter.on('pointerdown', function (pointer) {
                if(_gameState != STATE.STATE_PLAY_GAME) return;
                if(_hitState == STATE_HIT.STATE_READY) {
                    this.play('hitAni');
                    _app.collisionHit();
                }
            });

            bg.on('pointerdown', function (pointer) {
                if(_gameState != STATE.STATE_PLAY_GAME) return;
                if(_hitState == STATE_HIT.STATE_READY) {
                    hitter.play('hitAni');
                    _app.collisionHit();
                }
            });

            // 터치 가이드
            this.finger = this.add.image(hitter.x, hitter.y, 'finger').setDisplaySize(_width*0.2, _width*0.2);

            var scoreboard_height = (_width * this.game.textures.get("ground_score_board").getSourceImage().height) / this.game.textures.get("ground_score_board").getSourceImage().width;
            this.scoreBoard = this.add.image(_screenWidth/2, 0, 'ground_score_board').setDisplaySize(_width, scoreboard_height).setOrigin(0.5, 0);

            var textStyleOfScore1 = null;
            var textStyleOfScore2 = null;
            if (_width < 600){
                textStyleOfScore1 = { font: "bold 15px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                textStyleOfScore2 = { font: "bold 15px Arial", fill: "yellow", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfScore1 = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                textStyleOfScore2 = { font: "bold 25px Arial", fill: "yellow", boundsAlignH: "center", boundsAlignV: "middle" };
            }

            /**********************************************/
            // 상단 스코어
            this.scoreText = this.add.text(this.scoreBoard.x - this.scoreBoard.displayWidth * 0.2, this.scoreBoard.displayHeight * 0.62 , '0', textStyleOfScore1).setOrigin(0.5, 0.5);
            this.scoreHomerun = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.01, this.scoreBoard.displayHeight * 0.62 , '0', textStyleOfScore2).setOrigin(0.5, 0.5);
            this.scoreHit1 = this.add.text(this.scoreBoard.x  + this.scoreBoard.displayWidth * 0.12, this.scoreBoard.displayHeight * 0.62  , '0', textStyleOfScore2).setOrigin(0.5, 0.5);
            this.scoreHit2 = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.23, this.scoreBoard.displayHeight * 0.62 , '0', textStyleOfScore2).setOrigin(0.5, 0.5);
            this.scoreHit3 = this.add.text(this.scoreBoard.x + this.scoreBoard.displayWidth * 0.35, this.scoreBoard.displayHeight * 0.62 , '0', textStyleOfScore2).setOrigin(0.5, 0.5);

            /**********************************************/
            // 하단 게이지 판
            this.gageBarBG = this.add.graphics();
            this.gageBar = this.add.graphics();
            var gage_width = 340;
            var gage_height = (gage_width * this.game.textures.get("ground_gage_empty").getSourceImage().height) / this.game.textures.get("ground_gage_empty").getSourceImage().width;
            this.mentalGage = this.add.image(_screenWidth *0.5, _height * 0.93, 'ground_gage_empty').setDisplaySize(gage_width, gage_height);


            /**********************************************/
            // 타이머
            var txtAlram = null;
            if (_width < 600) txtAlram = { font: "bold 20px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };
            else  txtAlram = { font: "bold 30px Arial", fill: "#000", boundsAlignH: "center", boundsAlignV: "middle" };

            var playAlarm = this.add.image(this.scoreBoard.x+this.scoreBoard.x*0.7, scoreboard_height + (scoreboard_height * 0.5), 'ground_play_alarm').setDisplaySize(VIEW_MAX_WIDTH * 0.2, VIEW_MAX_WIDTH * 0.2);
            this.alarmText = this.add.text(playAlarm.x , playAlarm.y , this.checkTime, txtAlram).setOrigin(0.5);

            this.updateGage();

            this.playGroup = this.add.group();
            this.playGroup.add(playAlarm);
            this.playGroup.add(this.alarmText);
            this.playGroup.add(this.mentalGage);
            this.playGroup.add(this.gageBar);
            this.playGroup.add(this.gageBarBG);

            /*****************************  게임 결과  ***************************/
            this.resultGroup = this.add.group();

            this.result_popup_bg = this.add.image(_screenWidth/2, _height/2, 'result_ic_result_popup_bg').setDisplaySize(_width * 0.75, _width * 0.75 * 1.62);
            var yResultBtn = this.result_popup_bg.displayHeight/2 + _height/2;
            this.result_popup_view_result_sel = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_sel').setDisplaySize(_width/2, _width/2 * 0.32);
            // 결과 버튼 위치 조정
            if (/*this.result_popup_view_result_sel.y*/yResultBtn + this.result_popup_view_result_sel.displayHeight > _height){
                yResultBtn = _height - this.result_popup_view_result_sel.displayHeight;
            }
            this.result_popup_view_result_sel.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_sel.setInteractive();
            this.result_popup_view_result_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_resultview_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_view_result_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_view_result_nor.setInteractive();
            this.result_popup_next_game_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_nextgame_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_next_game_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_next_game_nor.setInteractive();
            this.result_popup_exit_nor = this.add.image(_screenWidth/2, _height/2 + yResultBtn, 'result_btn_exit_nor').setDisplaySize(_width/2, _width/2 * 0.32);
            this.result_popup_exit_nor.y = yResultBtn + this.result_popup_view_result_sel.displayHeight/2;
            this.result_popup_exit_nor.setInteractive();

            // scale이 최대 넓이 600px일 경우에 0.3
            // 각 해상도에 따라서 최대 넓이 기준으로 비율 조정
            var scaleOfResultText = 0.3 * _width/600;
            this.result_popup_text_bad = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_bad').setScale(scaleOfResultText);
            this.result_popup_text_notgood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_notgood').setScale(scaleOfResultText);
            this.result_popup_text_good = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_good').setScale(scaleOfResultText);
            this.result_popup_text_verygood = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_verygood').setScale(scaleOfResultText);
            this.result_popup_text_excellent = this.add.image(_screenWidth/2 , this.result_popup_bg.y, 'result_ic_scoretext_excellent').setScale(scaleOfResultText);

            // 위치는 결과 텍스트를 기존으로 밑으로 붙임.
            var textStyleOfResultScore = null;
            if (_width < 600){
                textStyleOfResultScore = { font: "bold 25px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
            }else {
                textStyleOfResultScore = { font: "bold 35px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            this.scoreTextOfResult = this.add.text(_screenWidth/2 , this.result_popup_bg.y + (this.result_popup_text_verygood.displayHeight + 7) , _TOTAL_SCORE, textStyleOfResultScore).setOrigin(0.5, 1);
            this.scoreTextOfResult.setStroke('#000', 5);

            // 높이의 기준은 _width 로 하며, 해당 넓이에 따라서 비율로 처리
            var scaleOfResultArrow = 0.38 * _width/600;
            this.result_popup_arrow = this.add.image(_screenWidth/2 - this.result_popup_bg.displayWidth/3, _height/2  + _width * 0.25 , 'result_ic_popup_arrow').setScale(scaleOfResultArrow);

            // 결과 팝업에서 보여주는 점수 텍스트
            var textStyleOfResultStandardScore = null;
            if (_width < 600){
            textStyleOfResultStandardScore = { font: "bold 13px Arial", fill: "#000", align: "right", boundsAlignH: "right", boundsAlignV: "middle"};
            }else {
            textStyleOfResultStandardScore = { font: "bold 20px Arial", fill: "#000", align: "right"/*, boundsAlignH: "right", boundsAlignV: "middle"*/ };
            }
            this.scoreTextOfResult1 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , _height/2 + _width * 0.465 , "0 ~ 20", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult2 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , _height/2 + _width * 0.415 , "21 ~ 40", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult3 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , _height/2 + _width * 0.36 , "41 ~ 60", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult4 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , _height/2 + _width * 0.305 , "61 ~ 80", textStyleOfResultStandardScore).setOrigin(1, 0.5);
            this.scoreTextOfResult5 = this.add.text(_screenWidth/2 + this.result_popup_bg.displayWidth/3 , _height/2 + _width * 0.253 , "81 ~ 100", textStyleOfResultStandardScore).setOrigin(1, 0.5);


            this.resultGroup.add(this.result_popup_bg);
            this.resultGroup.add(this.result_popup_view_result_nor);
            this.resultGroup.add(this.result_popup_view_result_sel);
            this.resultGroup.add(this.result_popup_next_game_nor);
            this.resultGroup.add(this.result_popup_exit_nor);
            this.resultGroup.add(this.result_popup_text_bad);
            this.resultGroup.add(this.result_popup_text_notgood);
            this.resultGroup.add(this.result_popup_text_good);
            this.resultGroup.add(this.result_popup_text_verygood);
            this.resultGroup.add(this.result_popup_text_excellent);
            this.resultGroup.add(this.scoreTextOfResult);
            this.resultGroup.add(this.result_popup_arrow);
            this.resultGroup.add(this.scoreTextOfResult1);
            this.resultGroup.add(this.scoreTextOfResult2);
            this.resultGroup.add(this.scoreTextOfResult3);
            this.resultGroup.add(this.scoreTextOfResult4);
            this.resultGroup.add(this.scoreTextOfResult5);

            this.resultGroup.toggleVisible(); // 기본 숨기기
            if (_gGameMode == GAME_MODE.NORMAR_END || _gGameMode == GAME_MODE.SIAM_END){
                this.result_popup_view_result_nor.visible = false;
            }else{
                this.result_popup_view_result_sel.visible = false;
            }

            // Event
            this.result_popup_view_result_nor.on('pointerdown', function(porinter, x, y, event){
                event.stopPropagation();
                _app.doNextState();
            });
            this.result_popup_next_game_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _app.doNextState();
            });
            this.result_popup_exit_nor.on('pointerdown', function(pointer, x, y, event){
                event.stopPropagation();
                _app.doNextState();
            });

            /***************************\*/
            // 타격 결과창
            if (_width < 600){
                textStyleOfResultScore1 = { font: "bold 40px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
                textStyleOfResultScore2 = { font: "bold 40px Arial", fill: "yellow", boundsAlignH: "center", boundsAlignV: "middle"};
            }else {
                textStyleOfResultScore1 = { font: "bold 40px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                textStyleOfResultScore2 = { font: "bold 40px Arial", fill: "yellow", boundsAlignH: "center", boundsAlignV: "middle"};
            }

            this.hitresult_txt1  = this.add.text(_screenWidth/2 , _height*0.2, '' , textStyleOfResultScore1).setOrigin(0.5, 1);
            this.hitresult_txt1.setStroke('#000', 5);
            this.hitresult_txt1.y = this.hitresult_txt1.displayHeight/2 + 200;
            // this.hitresult_txt1.visible = false;

            this.hitresult_txt2  = this.add.text(_screenWidth/2 , _height*0.2, '' , textStyleOfResultScore2).setOrigin(0.5, 1);
            this.hitresult_txt2.setStroke('#000', 5);
            this.hitresult_txt2.x = _screenWidth/2;
            this.hitresult_txt2.y = this.hitresult_txt1.y + 100;

            // var textStyleOfBonusScore = null;
            if (VIEW_MAX_WIDTH < 600){
                textStyleOfBonusScore = { font: "bold 55px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }else {
                textStyleOfBonusScore = { font: "bold 75px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            }
            tweenBonusScore1 = this.tweens.add({
                targets : _app.hitresult_txt1,
                alpha: { from: 0, to: 1 },
                x : _screenWidth/2,
                duration : 2000,
                delay : 0,
                ease : 'Back.Out',
                repeat : 0,
                onComplete: function () {
                    _app.hitresult_txt1.visible = false;
                }
            });

            tweenBonusScore2 = this.tweens.add({
                targets : _app.hitresult_txt2,
                alpha: { from: 0, to: 1 },
                x : _screenWidth/2,
                y : _app.hitresult_txt1.y + 50,
                duration : 2000,
                delay : 0,
                ease : 'Back.Out',
                repeat : 0,
                onComplete: function () {
                    _app.hitresult_txt2.visible = false;
                }
            });
            this.hitresult_txt1.visible = false;
            this.hitresult_txt2.visible = false;

            _app.showToast();

            // 네이티브에 게임시작 호출
            nativeInterface.onGameStart(LEVEL);

            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                _aSiamData = _app.setSham();
            }

            /********************************************************************/
            /********************************************************************/
            // 스타트 오디오 시작
             
            if(_gIsPlayingAudioByPhaser) {
                audio_play_bgm  = this.sound.add('audio_play_bgm');
                audio_game_win  = this.sound.add('audio_game_win');
                audio_game_lose = this.sound.add('audio_game_lose');
                audio_cheerRing = this.sound.add('audio_cheerRing');
                audio_hit  = this.sound.add('audio_hit');
                audio_swing  = this.sound.add('audio_swing');
                
                audio_ready_start = this.sound.add('audio_ready_start');
                audio_ready_start.play();

                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.on('complete', function(){ 
                    audio_ready_start.currentTime = 0;
                    _app.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = _app.time.addEvent({
                        delay: 1000,                // ms
                        callback: _app.updateTimer,   // 측정 타이머 함수
                        //args: [],
                        callbackScope: _app.scene,
                        repeat: _app.checkTime
                    });
                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    _gameState = STATE.STATE_PLAY_GAME;
                    _hitState = STATE_HIT.STATE_END;
    
                    audio_play_bgm.play({loop:true, volume:0.2}); 

                    _app.doNextState();
    
                });
            }
            else {
                audio_play_bgm  = new Audio('assets/audio/Game_play_BGM.mp3');
                audio_game_win  = new Audio('assets/audio/game_clear_good(Result).mp3');
                audio_game_lose = new Audio('assets/audio/game_fail_bad(Result).mp3'); 
                audio_cheerRing = new Audio('assets/audio/cheerRing.mp3'); 
                audio_hit  = new Audio('assets/audio/Baseball_hit.mp3');
                audio_swing  = new Audio('assets/audio/Baseball_swing.wav'); 

                audio_ready_start = new Audio('assets/audio/ready_start.mp3'); 
                audio_ready_start.play();

                // 시작 오디오 재생 완료시 처리
                // 1. 타이머 시작
                // 2. 배경 음악 재생
                // 3. 게임 play 시작
                audio_ready_start.addEventListener('ended', (event) => { 
                    // audio_ready_start.addEventListener("ended", function(){
        
                    audio_ready_start.currentTime = 0;
                    _app.checkTime = __CHECK_TIME__;  // Timer 초기화
                    _timer = _app.time.addEvent({
                        delay: 1000,                // ms
                        callback: _app.updateTimer,   // 측정 타이머 함수
                        //args: [],
                        callbackScope: _app.scene,
                        repeat: _app.checkTime
                    });
                    // 게임 내의 상태값 변경 (준비 -> 게임 시작)
                    _gameState = STATE.STATE_PLAY_GAME;
                    _hitState = STATE_HIT.STATE_END;
    
                    audio_play_bgm.play({loop:true, volume:0.2});
    
                    _app.doNextState();
    
                });
            }
             

        },
        onStartAudio: function()
        {
            // console.log("PlayGame >> onStart");
            // nothing...
        },
        collisionHit: function() {    // 타격했을때 상태 체크
            // 830 좌측  left
            // 850 중앙  center
            // 870 우측  right

            // _SCORE 가 9이상이면 홈런
            // _SCORE 가 8이상이면 3루타
            // _SCORE 가 6이상이면 2루타
            // _SCORE 가 4이상이면 1루타
            // _SCORE 가 3이하 아웃

            // console.log(hitter.z);

            var diff = hit_zindex-ball.z;
            // console.log('diff:'+diff);

            if(ball.z >= hit_zindex-50 && ball.z <= hit_zindex+50) {

                // 마우스 터치 위치(공위치)에 따라서 방향 결정함
                if(-10 < diff && diff < 10) {
                    _hitTarget = 'center';
                }
                else if(diff >= 10) {
                    _hitTarget = 'left';
                }
                else  {
                    _hitTarget = 'right';
                }

                // 뇌파 점수 기준으로 판정함
                if(_SCORE >= 9 ) {          // 홈런
                    audio_cheerRing.play(); // 환호 소리

                    if(_CHK_COMBO) {
                        thisScore = _BASE_SCORE_4 * 2;
                        this.hitresult_txt2.setText('Combo +'+thisScore);
                    }
                    else {
                        thisScore = _BASE_SCORE_4;
                        this.hitresult_txt2.setText('+'+thisScore);
                    }

                    _hitResult = 4;
                    _TOTAL_SCORE += thisScore;
                    this.scoreHomerun.setText(++_SCORE_4);
                    this.hitresult_txt1.setText(eval('_messageHomerun_'+_gLang));
                    // this.hitresult_txt2.setText('+'+thisScore);
                    // bonusScoreText.visible=true;
                    // tweenBonusScore.restart();
                    _CHK_COMBO = true;
                }
                else if(_SCORE >= 7 ) {     // 3루타
                    audio_cheerRing.play(); // 환호 소리

                    if(_CHK_COMBO) {
                        thisScore = _BASE_SCORE_3 * 2;
                        this.hitresult_txt2.setText('Combo +'+thisScore);
                    }
                    else {
                        thisScore = _BASE_SCORE_3;
                        this.hitresult_txt2.setText('+'+thisScore);
                    }

                    _hitResult = 3;
                    _TOTAL_SCORE += thisScore;
                    this.scoreHit3.setText(++_SCORE_3);
                    this.hitresult_txt1.setText('3'+eval('_messageHit_'+_gLang));
                    // this.hitresult_txt2.setText('+'+thisScore);
                    // bonusScoreText.visible=true;
                    // tweenBonusScore.restart();
                    _CHK_COMBO = true;
                }
                else if(_SCORE >= 5 ) {     // 2루타
                    _hitResult = 2;
                    _TOTAL_SCORE += _BASE_SCORE_2;
                    this.scoreHit2.setText(++_SCORE_2);
                    this.hitresult_txt1.setText('2'+ eval('_messageHit_'+_gLang));
                    this.hitresult_txt2.setText('+'+_BASE_SCORE_2);
                    // bonusScoreText.visible=true;
                    // tweenBonusScore.restart();
                    _CHK_COMBO = false;
                }
                else if(_SCORE >= 3 ) {     // 1루타
                    _hitResult = 1;
                    _TOTAL_SCORE += _BASE_SCORE_1;
                    this.scoreHit1.setText(++_SCORE_1);
                    this.hitresult_txt1.setText('1'+eval('_messageHit_'+_gLang));
                    this.hitresult_txt2.setText('+'+_BASE_SCORE_1);
                    // bonusScoreText.visible=true;
                    // tweenBonusScore.restart();
                    _CHK_COMBO = false;
                }
                else {
                    _hitResult = 0;         // 아웃
                    this.hitresult_txt1.setText(eval('_messageOut_'+_gLang));
                    this.hitresult_txt2.setText('');
                    _CHK_COMBO = false;
                }
                this.scoreText.setText(_TOTAL_SCORE);

                audio_hit.play();
            }
            else {
                 audio_swing.play();

                _hitTarget = 'swing';
                _hitResult = 0;    // 아웃
                this.hitresult_txt1.setText(eval('_messageOut_'+_gLang));
                this.hitresult_txt2.setText('');
                _CHK_COMBO = false;
            }

            _hitState= STATE_HIT.STATE_HITING;  // 타격 함

            this.hitresult_txt1.visible= true;
            this.hitresult_txt2.visible= true;
            tweenBonusScore1.restart();
            tweenBonusScore2.restart();

            setTimeout(function() {
                // 타격 결과창 사라지고 3초 뒤 다음 투구 진행
                // _app.hitresult.toggleVisible();
                if (_gameState == STATE.STATE_PLAY_GAME){
                    _app.doNextState();
                }
             }, 3000);
        },

        ballHitAnimation: function() {

             // hit : [0:아웃, 1:1루타, 2:2루타, 3:3루타, 4:홈런]

             if(_gameState == STATE.STATE_END_GAME ) return;

            if (_gameState == STATE.STATE_PLAY_GAME){
                if(_hitState == STATE_HIT.STATE_READY) {
                    if(ball.z < 800) ball.z += speed*2;
                    else {
                        hitter.play('hitAni');
                        _app.collisionHit();
                    }
                }
                else if (_hitState == STATE_HIT.STATE_HITING ) {

                    // 헛스윙일경우
                    if(_hitTarget == 'swing') {
                        if(ball.z < 900) ball.z += speed*2;
                        return true;
                    }

                    if(_hitTarget == 'left') offset = 4;
                    else if(_hitTarget == 'right') offset = 4;
                    else offset = 1;

                    if(_hitResult >= 3) {   // 3루타 이상
                        ball.y = ball.y - 9;
                        if(_hitTarget == 'left') ball.x = ball.x - offset;
                        else ball.x = ball.x + offset;
                        ball.z -= speed*6;
                    }
                    else if(_hitResult >= 2) {   // 2루타 이상

                        ball.y = ball.y - 4;
                        // ball.x = ball.x + offset;
                        if(_hitTarget == 'left') ball.x = ball.x - offset;
                        else ball.x = ball.x + offset;
                        ball.z -= speed*3;
                    }
                    else if(_hitResult >= 1) {   // 1루타 이상

                        if(ball.z < -600) {
                            return;
                        }

                        if(ball.z < 120) {
                            ball.y = ball.y+0.5;  // 공이 내려감
                        }
                        else if(ball.z < 400) {
                            ball.y = ball.y-1;  // 공이 천천히 떳다가
                        }
                        else if(ball.z < 630) {
                            ball.y = ball.y-1.5;  // 공이 천천히 떳다가
                        }
                        else {
                            ball.y = ball.y-2; // 공이 떳다가
                        }

                        if(_hitTarget == 'left') ball.x = ball.x - offset;
                        else ball.x = ball.x + offset;
                        // ball.x = ball.x + offset;
                        ball.z -= speed*2;
                    }
                    else {  // 그냥 내야 땅볼

                        if(ball.z < -400) {
                            return;
                        }

                        if(ball.z < 120) {
                            // ball.y = ball.y-0.01;
                        }
                        else {
                            ball.y = ball.y+3;
                        }
                        if(_hitTarget == 'left') ball.x = ball.x - offset;
                        else ball.x = ball.x + offset;
                        ball.z -= speed*2;
                    }
                }
            }

        },
        update: function ()
        {
            // if (isDisplaySiamRatio && (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END)){
            //     this.siamRatioText.setText(_strSiamRatio);
            // }

            if(_gameState == STATE.STATE_READY) {

                if (((this.time.now / 1000) % 1) >= 0.5){
                    this.finger.setAlpha(0);
                }else{
                    this.finger.setAlpha(1);
                }
            }
            else if (_gameState == STATE.STATE_PLAY_GAME){
                this.finger.setAlpha(0);

                /* 게임 진행 화면 상태 */
                this.alarmText.setText(this.checkTime);
                // this.scoreDisplayText.setText("callJS : " + _SCORE);

                if (this.checkTime <= 0 || _isFinished){
                    _app.doNextState();
                }else{
                    // GageBar 업데이트
                    this.updateGage();
                    // console.log('STATE_HIT.STATE_READY : ' + _hitState);
                    this.ballHitAnimation();
                    camera.update();
                }

                if (_gIsPlayingAudioByPhaser){
                    if (audio_play_bgm.isPlaying == false){  // Phaser audio
                        // audio_play_bgm.currentTime = 0;
                        audio_play_bgm.play();
                    }
                }
                else {
                    if (audio_play_bgm.paused == true){  // Phaser audio 
                        // audio_play_bgm.currentTime = 0;
                        audio_play_bgm.play();
                    }
                }

            }else if (_gameState == STATE.STATE_DISPLAY_RESULT){
                /* 게임 결과 화면 상태 */

            }else if (_gameState == STATE.STATE_END_GAME){
                /* 게임 종료하는 상태 */
            }

        },
        setScore: function (score)
        {
            if (_gameState != STATE.STATE_PLAY_GAME || _isFinished) return;
            if (_gGameMode == GAME_MODE.SIAM || _gGameMode == GAME_MODE.SIAM_END){
                var siam_score = _aSiamData.pop();
                console.log("setScore >> score = " + score + ", siam score = " + siam_score);
                score = siam_score;
            }
            _prevScore = _SCORE;
            _SCORE = score;

            if (_prevScore < _SCORE){
                _upGage  = true;
            }else if(_prevScore > _SCORE){
                _upGage  = false;
            }
        },
        /** 게임 상태에 따른 이벤트 처리 */
        doNextState: function ()
        {
            console.log("게임상태 : " + _gameState);

            if (_gameState == STATE.STATE_READY){   // 1. 게임 레디

            }else if (_gameState == STATE.STATE_PLAY_GAME){
                // 2. 게임 진행중 상태

                // 시간 되면 게임 종료
                if (_app.checkTime <= 0 || _isFinished){
                    _gameState = STATE.STATE_DISPLAY_RESULT;
                    _app.checkTime = __CHECK_TIME__;
                    _app.doNextState();
                }else{
                    // 투구 모션
                    pitcher.play('pitcherAni');
                    pitcher.on('animationcomplete', this.animComplete, this);
                }
            }else if (_gameState == STATE.STATE_DISPLAY_RESULT){
                // 3. 게임 결과 노출
                _gameState = STATE.STATE_END_GAME;

                _app.setDisplayResultPopup();
                nativeInterface.onGameEnd(_TOTAL_SCORE);

                 /** onStart 이벤트를 받아 _result_skip 처리 한다 **/
                 if(_result_skip == 'Y'){
                    _app.doNextState();
                    return;
                }
                // 자동으로 APP 통보 (다음게임 또는 종료)
                /**
                 * onStart 이벤트를 받는다
                 * '_result_auto_skip' 이 'Y'인 경우
                 * '_result_skip_wait_time' 후에 강제 종료 타이머 처리
                 */
                if(_result_auto_skip == 'Y'){
                    nextGameTimer = setTimeout(function() {
                        _app.doNextState();
                    }, nextGameTimeSec);
                }


            }
            else if (_gameState == STATE.STATE_END_GAME){
                // 4. 게임 종료
 
                if (audio_game_win.paused == false){  
                    audio_game_win.pause();
                    audio_game_win.currentTime = 0;
                } 
                if (audio_game_lose.paused == false){  
                    audio_game_lose.pause();
                    audio_game_lose.currentTime = 0;
                }

                if (_timer != null){
                    _timer.remove();
                }
                var params= '{"result":'+_gameResult+',"percent":'+_percentage_score+'}';
                nativeInterface.onNextGame(params);
            }
        },
        animComplete(animation, frame)
        {
            if(animation.key === 'pitcherAni')
            {
                _hitState = STATE_HIT.STATE_READY;
                ball.x = -10;
                ball.y = 20;
                ball.z = 128;
                hitDiff = false;
            }
        },
        /* 게이지 출력 함수 */
        updateGage : function()
        {
            var x = _screenWidth/2 - this.mentalGage.displayWidth/2 + 55;
            var y = _height * 0.93;

            this.gageBarBG.clear();
            this.gageBarBG.fillStyle(0x212f39, 1);
            this.gageBarBG.fillRoundedRect(x, y - 12, __MAX_SCORE_WIDTH__, 20, 8);

            this.gageBar.clear();
            this.gageBar.fillStyle(0xffd400, 1);
            if (_upGage && _gageBarWidth < __INTERVAL_GAGE__ * 10 * _SCORE){
                _gageBarWidth += __INTERVAL_GAGE__;
                if (_gageBarWidth >= __MAX_SCORE_WIDTH__){
                    _gageBarWidth = __MAX_SCORE_WIDTH__;
                }
            }else if(_upGage == false && _gageBarWidth >= __INTERVAL_GAGE__ * 10 * _SCORE){
                _gageBarWidth -= __INTERVAL_GAGE__;
                if (_gageBarWidth <= __MIN_SCORE_WIDTH__){
                    _gageBarWidth = 0;
                }
            }
            this.gageBar.fillRoundedRect(x, y - 12, _gageBarWidth, 20, 8);
        },
        updateTimer : function ()
        {
            if (_gameState == STATE.STATE_PLAY_GAME){
                _app.checkTime--;
                if (_app.checkTime < 0){
                    _app.doNextState();
                }
            }else{
                _timer.paused = true;
            }
        },
        setSham : function() {
            CON_ARR = [];
            var val = 0;
            var successMax = 10;    // 홈런
            var successMin = 5;     // 2루타부터 인정
            var failMax = 4;
            var failMin = 1;
            // var ratio_arr = [0.2, 0.5, 0.8];
            var ratio_arr = [0.5, 0.6, 0.7, 0.8, 0.9];
            debugRatio = ratio_arr.sort(function () {return 0.5 - Math.random()}).pop();
            if(debugFlag){
                console.log('ratio = > ${'+ debugRatio + ' * 100}% 확율의 성공');
            }
            for (var i = 0; i < 100; i++) {
              if (Math.random() < debugRatio) {
                val = Math.floor(Math.random() * (successMax - successMin + 1)) + successMin;
              } else {
                val = Math.floor(Math.random() * (failMax - failMin + 1)) + failMin;
              }
              CON_ARR.push(val);
            }
            if(debugFlag){
                console.log('CON_ARR =>'+CON_ARR);
            }

            return CON_ARR;
        },
        setDisplayResultPopup : function()
        {
            if (audio_play_bgm.paused == false){  
                audio_play_bgm.pause();
                // audio_play_bgm.currentTime = 0;
            }
            if (_TOTAL_SCORE <= 90){    // Bad, Not Good
                audio_game_lose.play();  // lose audio play
            }else{
                audio_game_win.play();  // win audio play
            }

            if(_result_skip == 'N'){
                _app.playGroup.toggleVisible();
                _app.resultGroup.toggleVisible();
            }

            _app.result_popup_text_bad.visible = false;
            _app.result_popup_text_notgood.visible = false;
            _app.result_popup_text_good.visible = false;
            _app.result_popup_text_verygood.visible = false;
            _app.result_popup_text_excellent.visible = false;

            // 게임 시간에 따른 스코어 설정 및 스코어 표시
            var scoreGap = 45;
            var badScroe = scoreGap*1;
            var notgoodScore = scoreGap*2;
            var goodScore = scoreGap*3;
            var verygoodScore = scoreGap*4;
            var MAX_PLAY_TIME = 180;
            var scoreRate = MAX_PLAY_TIME/__CHECK_TIME__;
            var MAX_POINT = scoreGap*5;

            badScroe        = 20;
            notgoodScore    = 40;
            goodScore       = 60;
            verygoodScore   = 80;

            var percentage_score    = parseInt((_TOTAL_SCORE / ( MAX_POINT / scoreRate )) * 100);  // 배점 계산 필요
            if (percentage_score > 100){
                percentage_score = 100;
            }
            _percentage_score = percentage_score; // 전역 변수에 배율 점수 저장

            if (percentage_score >= 0 && percentage_score <= badScroe){
                if(_result_skip == 'N'){
                    _app.result_popup_text_bad.visible = true;
                    _app.result_popup_arrow.setY(this.result_popup_bg.y + VIEW_MAX_WIDTH * 0.475);
                }
                _gameResult = 1;
            }else if (percentage_score >= (badScroe+1) && percentage_score <= notgoodScore){
                if(_result_skip == 'N'){
                    _app.result_popup_text_notgood.visible = true;
                    _app.result_popup_arrow.setY(this.result_popup_bg.y + VIEW_MAX_WIDTH * 0.42);
                }
                _gameResult = 2;
            }else if (percentage_score >= (notgoodScore+1) && percentage_score <= goodScore){
                if(_result_skip == 'N'){
                    _app.result_popup_text_good.visible = true;
                    _app.result_popup_arrow.setY(this.result_popup_bg.y + VIEW_MAX_WIDTH * 0.37);
                }
                _gameResult = 3;
            }else if (percentage_score >= (goodScore+1) && percentage_score <= verygoodScore){
                if(_result_skip == 'N'){
                    _app.result_popup_text_verygood.visible = true;
                    _app.result_popup_arrow.setY(this.result_popup_bg.y+ VIEW_MAX_WIDTH * 0.31);
                }
                _gameResult = 4;
            }else if (percentage_score >= (verygoodScore + 1)){
                if(_result_skip == 'N'){
                    _app.result_popup_text_excellent.visible = true;
                    _app.result_popup_arrow.setY(this.result_popup_bg.y + VIEW_MAX_WIDTH * 0.26);
                }
                _gameResult = 5;
            }

            if(_result_skip == 'Y'){
                // _result_skip이 "Y"인 경우에, 화면 처리하지 않고 return
                return;
            }

            _app.scoreTextOfResult1.setText('0 ~ ' + badScroe + '%');
            _app.scoreTextOfResult2.setText((badScroe+1) + ' ~ ' + notgoodScore + '%');
            _app.scoreTextOfResult3.setText((notgoodScore+1) + ' ~ ' + goodScore + '%');
            _app.scoreTextOfResult4.setText((goodScore+1) + ' ~ ' + verygoodScore + '%');
            _app.scoreTextOfResult5.setText((verygoodScore+1) + ' ~ 100%');

            // 최종 점수 표시
            _app.scoreTextOfResult.setText(_TOTAL_SCORE + " (" + percentage_score + "%)");

            // 게임 모드에 따라서 버튼을 보여주도록 처리
            if (_gGameMode == GAME_MODE.NORMAR || _gGameMode == GAME_MODE.SIAM){
                _app.result_popup_next_game_nor.visible = true;
                _app.result_popup_view_result_nor.visible = false;
                _app.result_popup_view_result_sel.visible = false;
                _app.result_popup_exit_nor.visible = false;
            }else{
                if(_result_button_code == 'BTN001'){
                    _app.result_popup_next_game_nor.visible = false;
                    _app.result_popup_view_result_sel.visible = false;
                    _app.result_popup_view_result_nor.visible = false;
                    _app.result_popup_exit_nor.visible = true;
                }else{
                    _app.result_popup_next_game_nor.visible = false;
                    _app.result_popup_view_result_sel.visible = false;
                    _app.result_popup_view_result_nor.visible = true;
                    _app.result_popup_exit_nor.visible = false;
                }
            }
        },
        showToast: function() {
            var toast = document.getElementById("toastMessageBar");

            if (_gLang == "kr"){
                toast.innerHTML = _messageKr;
            }else {
                toast.innerHTML = _messageEn;
            }
            $("#toastMessageBar").show(500);
            setTimeout(function(){
                $("#toastMessageBar").hide(500);
            }, 3000);
        }
    };

}());
