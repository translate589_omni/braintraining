/**
 * iOSNativeInterface는 iOS naitvie 기능 호출에 필요한 interface를 제공하는 클래스입니다.
 *
 * @classDescription
 * @type {Object}
 */
var IosNativeInterface = (function() {

	/**
	 *  NativeInterface initialize.
	 */
	function initialize(){
		console.log("IosNativeInterface >> initialize called");
	};

    function onGameStart(level)
    {
        console.log("IosNativeInterface >> onGameStart :: level = " + level);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onGameStart) {
            window['webkit'].messageHandlers['onGameStart'].postMessage( String(level) );
        }
    }

    function onGameEnd(score)
    {
        console.log("IosNativeInterface >> onGameEnd :: score = " + score);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onGameEnd) {
            window['webkit'].messageHandlers['onGameEnd'].postMessage( String(score) );
        }
    }

    function onNextGame(result)
    {
        console.log("IosNativeInterface >> onNextGame :: result = " + result);
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onNextGame) {
            window['webkit'].messageHandlers['onNextGame'].postMessage( String(result) );
        }
    }

    function onStart(data)
    {
        if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.onStart) {
            window['webkit'].messageHandlers.onStart.postMessage(String(data));
        }else{
            window.onStart();
        }
    }
	//////////////////////////////////////////////////////////////////////////////////////////////////
    function onkeydown(val){
        console.log("IosNativeInterface >> keyDown val :::"+ val.keyCode);
        if (val.keyCode >= 48 && val.keyCode <= 57){
            //0 ~ 9
            window.callJS(val.keyCode - 48);
        }else if (val.keyCode == 189){
            // -
            window.callJS(10);
        }
    }

	// Native -> UI 함수 호출 ///////////////////////////////////////////////////////////////////////////
	function OnNativeEvent(command, data, data1){
		console.log("AndroidNativeInterface   ++++++++++++++++++++++     command = " + command + ", data = " + data);

        var result;
        switch (command)
        {
            case "callJS":
                if (Number(data) < 0){
                    data = 0;
                }
                PlayGameScene.prototype.setScore(Number(data));
                // PlayGameScene.SCORE = data;
                break;
            case "callLevelJS":
                // IntroScene.prototype.LEVEL = Number(data);
                // PlayGameScene.prototype.LEVEL =  Number(data);
                break;
            case "onStart":
                if(typeof data == 'string'){
                    onSetGameConfig((data));
                }else{
                    onSetGameConfig(JSON.stringify(data));
                }
                if (currentScene == GAME_SCENE.INTRO_SCENE){
                    IntroScene.prototype.onStartAudio();
                }else if (currentScene == GAME_SCENE.PLAY_SCENE){
                    PlayGameScene.prototype.onStartAudio();
                }
                break;
        }
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////

	/* Public API */
	return {
		Initialize      : initialize,
        onGameStart     : onGameStart,
        onGameEnd       : onGameEnd,
        onNextGame      : onNextGame,
        onStart         : onStart,
        onkeydown       : onkeydown,
		OnNativeEvent   : OnNativeEvent
	};
})();